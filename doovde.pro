CONFIG                 -= release
CONFIG                 += debug

CONFIG                 += ordered mmx sse sse2

HEADERS                 = 
SOURCES                 = 
RESOURCES               = 
TARGET                  = doovde
INCLUDEPATH            += ./ ./src/
LIBS                   += -lm

WARNINGS               += -Wall -Wshadow -Werror -pedantic

CFLAGS_RELEASE         += -O3 -Wall -Wshadow -Werror
CFLAGS_DEBUG           += -g -O0 -Wall -Wshadow -Werror

LFLAGS_DEBUG           += -g

CXXFLAGS               += -Wno-write-strings -fno-exceptions -fno-rtti

QMAKE_CFLAGS           += $${CFLAGS}
QMAKE_CFLAGS_RELEASE   += $${CFLAGS_RELEASE}
QMAKE_CFLAGS_DEBUG     += $${CFLAGS_DEBUG}

QMAKE_CXXFLAGS         += $${CXXFLAGS} $${CFLAGS}
QMAKE_CXXFLAGS_RELEASE += $${CXXFLAGS_RELEASE} $${CFLAGS_RELEASE}
QMAKE_CXXFLAGS_DEBUG   += $${CXXFLAGS_DEBUG} $${CFLAGS_DEBUG}

QMAKE_LFLAGS_DEBUG     += $${LFLAGS_DEBUG}

TEMPLATE                = app

CODECFORTR              = UTF-8

mac {
	CONFIG         -= app_bundle
}

release {
	OBJECTS_DIR     = build/release/
}

debug {
	OBJECTS_DIR     = build/debug/
}

include(src/src.pri)
