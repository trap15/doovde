//---  System includes
#include <stdlib.h>
#include <stdint.h>

//---  Qt includes
#include <QtGui>
#include <QThread>

//---  Doovde includes
#include "log.h"
#include "core/core.h"
#include "main_thread.h"

Doovde::MainThread::MainThread(QObject *par) : QThread(par)
{
    running = false;
    stopped = false;
}

Doovde::MainThread::~MainThread()
{
    mutex.lock();
    stopped = true;
    running = false;
    mutex.unlock();
}

void Doovde::MainThread::run()
{
    bool can_leave = false;
    while(!can_leave) {
        mutex.lock();
        can_leave = stopped;
        if(!running || can_leave) {
            mutex.unlock();
            yieldCurrentThread();
            continue;
        }
        core->run();
        mutex.unlock();
        yieldCurrentThread();
    }
}

bool Doovde::MainThread::stopProcess()
{
    bool ostop;
    mutex.lock();
    stopped = true;
    ostop = stopped;
    mutex.unlock();
    Doovde::log(Doovde::LogLevel::MISC, "Main thread stopped.");
    return ostop;
}

bool Doovde::MainThread::pauseProcess()
{
    bool pause;
    mutex.lock();
    running = !running;
    pause = running;
    mutex.unlock();
    Doovde::log(Doovde::LogLevel::MISC, "Main thread %spaused.", pause ? "un" : "");
    return pause;
}
