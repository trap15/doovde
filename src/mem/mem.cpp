//---  System includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

//---  Doovde includes
#include "log.h"
#include "cpu/allegrex.h"
#include "mem/mem.h"

Doovde::Memory::Memory()
{
    for(int i = 0; i < 3; i++) {
        for(int l = 0; l < MEMTABLE_SIZE; l++) {
            entries[i][l] = NULL;
        }
    }
    alive = true;
}

Doovde::Memory::~Memory()
{
    alive = false;
}

void Doovde::Memory::reset()
{
    for(int i = 0; i < 3; i++) {
        for(int l = 0; l < MEMTABLE_SIZE; l++) {
            entries[i][l] = NULL;
        }
    }
}

#define VALIDATE_MEMORY_READ() do { \
    if(entries[mode][addr >> MEMADDR_BITS] == NULL) { \
        if(busErrCb != NULL) { \
            Doovde::log(Doovde::LogLevel::ERROR, "Unmapped read! %s %d 0x%08X!", SOURCENAME(cpu), mode, addr); \
            busErrCb(mode, addr, 0); \
        } \
        return 0; \
    } \
    if(!entries[mode][addr >> MEMADDR_BITS]->read) { \
        if(busErrCb != NULL) { \
            Doovde::log(Doovde::LogLevel::ERROR, "Illegal read! %s %d 0x%08X!", SOURCENAME(cpu), mode, addr); \
            busErrCb(mode, addr, 0); \
        } \
        return 0; \
    } \
} while(0)

#define VALIDATE_MEMORY_WRITE() do { \
    if(entries[mode][addr >> MEMADDR_BITS] == NULL) { \
        if(busErrCb != NULL) { \
            Doovde::log(Doovde::LogLevel::ERROR, "Unmapped write! %s %d 0x%08X 0x%08X!", SOURCENAME(cpu), mode, addr, val); \
            busErrCb(mode, addr, 0); \
        } \
        return; \
    } \
    if(!entries[mode][addr >> MEMADDR_BITS]->write) { \
        if(busErrCb != NULL) { \
            Doovde::log(Doovde::LogLevel::ERROR, "Illegal write! %s %d 0x%08X 0x%08X!", SOURCENAME(cpu), mode, addr, val); \
            busErrCb(mode, addr, 0); \
        } \
        return; \
    } \
} while(0)


/* We can optimize this if we convert the LWL/LWR and SWL/SWR stuff to use just [read|write]8
 * Until then we have to validate the memory every time...
 */

uint8_t Doovde::Memory::read8(Doovde::Allegrex* cpu, int mode, uint32_t addr)
{
    VALIDATE_MEMORY_READ();
    if(entries[mode][addr >> MEMADDR_BITS]->mem) {
        if(entries[mode][addr >> MEMADDR_BITS]->rdata == NULL)
            return 0;
        return ((uint8_t*)(entries[mode][addr >> MEMADDR_BITS]->rdata))[addr & MEMADDR_MASK];
    }else{
        return entries[mode][addr >> MEMADDR_BITS]->io_r8(this, cpu, addr);
    }
}

uint16_t Doovde::Memory::read16(Doovde::Allegrex* cpu, int mode, uint32_t addr)
{
    VALIDATE_MEMORY_READ();
    if(entries[mode][addr >> MEMADDR_BITS]->mem) {
        if(entries[mode][addr >> MEMADDR_BITS]->rdata == NULL)
            return 0;
        uint16_t val;
        val = ((uint8_t*)(entries[mode][addr >> MEMADDR_BITS]->rdata))[addr & MEMADDR_MASK];
        addr++;
        if((addr & MEMADDR_MASK) == 0)
            VALIDATE_MEMORY_READ();
        val |= ((uint8_t*)(entries[mode][addr >> MEMADDR_BITS]->rdata))[addr & MEMADDR_MASK] << 8;
        return val;
    }else{
        return entries[mode][addr >> MEMADDR_BITS]->io_r16(this, cpu, addr);
    }
}

uint32_t Doovde::Memory::read32(Doovde::Allegrex* cpu, int mode, uint32_t addr)
{
    VALIDATE_MEMORY_READ();
    if(entries[mode][addr >> MEMADDR_BITS]->mem) {
        if(entries[mode][addr >> MEMADDR_BITS]->rdata == NULL)
            return 0;
        uint32_t val = 0;
        val |= ((uint8_t*)(entries[mode][addr >> MEMADDR_BITS]->rdata))[addr & MEMADDR_MASK];
        addr++;
        if((addr & MEMADDR_MASK) == 0)
            VALIDATE_MEMORY_READ();
        val |= ((uint8_t*)(entries[mode][addr >> MEMADDR_BITS]->rdata))[addr & MEMADDR_MASK] << 8;
        addr++;
        if((addr & MEMADDR_MASK) == 0)
            VALIDATE_MEMORY_READ();
        val |= ((uint8_t*)(entries[mode][addr >> MEMADDR_BITS]->rdata))[addr & MEMADDR_MASK] << 16;
        addr++;
        if((addr & MEMADDR_MASK) == 0)
            VALIDATE_MEMORY_READ();
        val |= ((uint8_t*)(entries[mode][addr >> MEMADDR_BITS]->rdata))[addr & MEMADDR_MASK] << 24;
        return val;
    }else{
        return entries[mode][addr >> MEMADDR_BITS]->io_r32(this, cpu, addr);
    }
}

void Doovde::Memory::write8(Doovde::Allegrex* cpu, int mode, uint32_t addr, uint8_t val)
{
    VALIDATE_MEMORY_WRITE();
    if(entries[mode][addr >> MEMADDR_BITS]->mem) {
        if(entries[mode][addr >> MEMADDR_BITS]->wdata == NULL)
            return;
        ((uint8_t*)(entries[mode][addr >> MEMADDR_BITS]->wdata))[addr & MEMADDR_MASK] = val;
    }else{
        entries[mode][addr >> MEMADDR_BITS]->io_w8(this, cpu, addr, val);
    }
}

void Doovde::Memory::write16(Doovde::Allegrex* cpu, int mode, uint32_t addr, uint16_t val)
{
    VALIDATE_MEMORY_WRITE();
    if(entries[mode][addr >> MEMADDR_BITS]->mem) {
        if(entries[mode][addr >> MEMADDR_BITS]->wdata == NULL)
            return;
        ((uint8_t*)(entries[mode][addr >> MEMADDR_BITS]->wdata))[addr & MEMADDR_MASK] = val & 0xFF;
        val >>= 8;
        addr++;
        if((addr & MEMADDR_MASK) == 0)
            VALIDATE_MEMORY_WRITE();
        ((uint8_t*)(entries[mode][addr >> MEMADDR_BITS]->wdata))[addr & MEMADDR_MASK] = val & 0xFF;
    }else{
        entries[mode][addr >> MEMADDR_BITS]->io_w16(this, cpu, addr, val);
    }
}

void Doovde::Memory::write32(Doovde::Allegrex* cpu, int mode, uint32_t addr, uint32_t val)
{
    VALIDATE_MEMORY_WRITE();
    if(entries[mode][addr >> MEMADDR_BITS]->mem) {
        if(entries[mode][addr >> MEMADDR_BITS]->wdata == NULL)
            return;
        ((uint8_t*)(entries[mode][addr >> MEMADDR_BITS]->wdata))[addr & MEMADDR_MASK] = val & 0xFF;
        val >>= 8;
        addr++;
        if((addr & MEMADDR_MASK) == 0)
            VALIDATE_MEMORY_WRITE();
        ((uint8_t*)(entries[mode][addr >> MEMADDR_BITS]->wdata))[addr & MEMADDR_MASK] = val & 0xFF;
        val >>= 8;
        addr++;
        if((addr & MEMADDR_MASK) == 0)
            VALIDATE_MEMORY_WRITE();
        ((uint8_t*)(entries[mode][addr >> MEMADDR_BITS]->wdata))[addr & MEMADDR_MASK] = val & 0xFF;
        val >>= 8;
        addr++;
        if((addr & MEMADDR_MASK) == 0)
            VALIDATE_MEMORY_WRITE();
        ((uint8_t*)(entries[mode][addr >> MEMADDR_BITS]->wdata))[addr & MEMADDR_MASK] = val & 0xFF;
    }else{
        entries[mode][addr >> MEMADDR_BITS]->io_w32(this, cpu, addr, val);
    }
}

void Doovde::Memory::addEntry(int mode, uint32_t addr, uint32_t size, const char *tag,
                              bool r, bool w, void* mem)
{
    if(addr & MEMADDR_MASK) {
        Doovde::log(Doovde::LogLevel::WARNING, "Entry addr is not on table border!");
    }
    if(size & MEMADDR_MASK) {
        Doovde::log(Doovde::LogLevel::WARNING, "Entry size is not on table border!");
    }
    addr >>= MEMADDR_BITS;
    size >>= MEMADDR_BITS;
    for(uint32_t i = 0; i < size; i++) {
        uint8_t* memaddr;
        if(mem == NULL) memaddr = NULL;
        else memaddr = ((uint8_t*)mem) + (i << MEMADDR_BITS);

        MemEntry* entry = new MemEntry(tag, r, w, (void*)memaddr);
        if(mode & 1)
            entries[Doovde::Cop0::MODE_KERNEL][i + addr] = entry;
        if(mode & 2)
            entries[Doovde::Cop0::MODE_SUPERVISOR][i + addr] = entry;
        if(mode & 4)
            entries[Doovde::Cop0::MODE_USER][i + addr] = entry;
    }
}

void Doovde::Memory::addEntry(int mode, uint32_t addr, uint32_t size, const char *tag,
                              bool r, bool w, void* rmem, void *wmem)
{
    if(addr & MEMADDR_MASK) {
        Doovde::log(Doovde::LogLevel::WARNING, "Entry addr is not on table border!");
    }
    if(size & MEMADDR_MASK) {
        Doovde::log(Doovde::LogLevel::WARNING, "Entry size is not on table border!");
    }
    addr >>= MEMADDR_BITS;
    size >>= MEMADDR_BITS;
    for(uint32_t i = 0; i < size; i++) {
        uint8_t* rmemaddr;
        if(rmem == NULL) rmemaddr = NULL;
        else rmemaddr = ((uint8_t*)rmem) + (i << MEMADDR_BITS);
        uint8_t* wmemaddr;
        if(wmem == NULL) wmemaddr = NULL;
        else wmemaddr = ((uint8_t*)wmem) + (i << MEMADDR_BITS);

        MemEntry* entry = new MemEntry(tag, r, w, (void*)rmemaddr, (void*)wmemaddr);
        if(mode & 1)
            entries[Doovde::Cop0::MODE_KERNEL][i + addr] = entry;
        if(mode & 2)
            entries[Doovde::Cop0::MODE_SUPERVISOR][i + addr] = entry;
        if(mode & 4)
            entries[Doovde::Cop0::MODE_USER][i + addr] = entry;
    }
}

void Doovde::Memory::addEntry(int mode, uint32_t addr, uint32_t size, const char *tag,
                              bool r, bool w,
                              uint8_t  (*ior8) (Memory*, Doovde::Allegrex*, uint32_t),
                              uint16_t (*ior16)(Memory*, Doovde::Allegrex*, uint32_t),
                              uint32_t (*ior32)(Memory*, Doovde::Allegrex*, uint32_t),
                              void (*iow8) (Memory*, Doovde::Allegrex*, uint32_t, uint8_t),
                              void (*iow16)(Memory*, Doovde::Allegrex*, uint32_t, uint16_t),
                              void (*iow32)(Memory*, Doovde::Allegrex*, uint32_t, uint32_t))
{
    if(addr & MEMADDR_MASK) {
        Doovde::log(Doovde::LogLevel::WARNING, "Entry addr is not on table border!");
    }
    if(size & MEMADDR_MASK) {
        Doovde::log(Doovde::LogLevel::WARNING, "Entry size is not on table border!");
    }
    addr >>= MEMADDR_BITS;
    size >>= MEMADDR_BITS;
    for(uint32_t i = 0; i < size; i++) {
        MemEntry* entry = new MemEntry(tag, r, w, ior8, ior16, ior32, iow8, iow16, iow32);
        if(mode & 1)
            entries[Doovde::Cop0::MODE_KERNEL][i + addr] = entry;
        if(mode & 2)
            entries[Doovde::Cop0::MODE_SUPERVISOR][i + addr] = entry;
        if(mode & 4)
            entries[Doovde::Cop0::MODE_USER][i + addr] = entry;
    }
}
