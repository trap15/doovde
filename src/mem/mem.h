
#ifndef DOOVDE_MEM_H_
#define DOOVDE_MEM_H_

//---  Doovde includes
#include "cpu/cop0.h"

#define MEMTABLE_BITS   (20)
#define MEMADDR_BITS    (32 - MEMTABLE_BITS)
#define MEMTABLE_SIZE   (1 << MEMTABLE_BITS)
#define MEMADDR_SIZE    (1 << MEMADDR_BITS)
#define MEMTABLE_MASK   (MEMTABLE_SIZE - 1)
#define MEMADDR_MASK    (MEMADDR_SIZE - 1)

#define SOURCENAME(cpu) (cpu ? (cpu->media_engine ? "ME" : "SC") : "HW")

namespace Doovde {
    struct Allegrex;
    struct Memory {
        Memory();
        ~Memory();
        //---  Variables
        bool alive;
        struct MemEntry {
            MemEntry(const char *t, bool r, bool w, void* m) :
                    tag(t), read(r), write(w), mem(true), rdata(m), wdata(m) { }
            MemEntry(const char *t, bool r, bool w, void* rm, void* wm) :
                    tag(t), read(r), write(w), mem(true), rdata(rm), wdata(wm) { }
            MemEntry(const char *t, bool r, bool w,
                uint8_t (*ior8)(Memory*, Allegrex*, uint32_t),
                uint16_t (*ior16)(Memory*, Allegrex*, uint32_t),
                uint32_t (*ior32)(Memory*, Allegrex*, uint32_t),
                void (*iow8)(Memory*, Allegrex*, uint32_t, uint8_t),
                void (*iow16)(Memory*, Allegrex*, uint32_t, uint16_t),
                void (*iow32)(Memory*, Allegrex*, uint32_t, uint32_t)) :
                    tag(t), read(r), write(w), mem(false),
                    io_r8(ior8), io_r16(ior16), io_r32(ior32),
                    io_w8(iow8), io_w16(iow16), io_w32(iow32) { }
            ~MemEntry() {}
            const char *tag;
            bool read, write;
            bool mem; // false = I/O
            void* rdata;
            void* wdata;
            uint8_t  (*io_r8)(Memory*, Allegrex* mem, uint32_t addr);
            uint16_t (*io_r16)(Memory*, Allegrex* mem, uint32_t addr);
            uint32_t (*io_r32)(Memory*, Allegrex* mem, uint32_t addr);
            void (*io_w8)(Memory*, Allegrex* mem, uint32_t addr, uint8_t val);
            void (*io_w16)(Memory*, Allegrex* mem, uint32_t addr, uint16_t val);
            void (*io_w32)(Memory*, Allegrex* mem, uint32_t addr, uint32_t val);
        };

        // Memory entry tables for the 3 user modes (kernel, supervisor, user)
        MemEntry* entries[3][MEMTABLE_SIZE];

        void (*busErrCb)(int mode, uint32_t addr, int type); // 0 is read, 1 is write

        //---  Functions
        void reset();
        uint8_t read8(Doovde::Allegrex* cpu, int mode, uint32_t addr);
        uint16_t read16(Doovde::Allegrex* cpu, int mode, uint32_t addr);
        uint32_t read32(Doovde::Allegrex* cpu, int mode, uint32_t addr);
        void write8(Doovde::Allegrex* cpu, int mode, uint32_t addr, uint8_t val);
        void write16(Doovde::Allegrex* cpu, int mode, uint32_t addr, uint16_t val);
        void write32(Doovde::Allegrex* cpu, int mode, uint32_t addr, uint32_t val);

        void addEntry(int mode, uint32_t addr, uint32_t size, const char *tag, bool r, bool w, void* mem);
        void addEntry(int mode, uint32_t addr, uint32_t size, const char *tag, bool r, bool w, void* rmem, void* wmem);
        void addEntry(int mode, uint32_t addr, uint32_t size, const char *tag, bool r, bool w,
                uint8_t (*ior8)(Memory*, Allegrex*, uint32_t),
                uint16_t (*ior16)(Memory*, Allegrex*, uint32_t),
                uint32_t (*ior32)(Memory*, Allegrex*, uint32_t),
                void (*iow8)(Memory*, Allegrex*, uint32_t, uint8_t),
                void (*iow16)(Memory*, Allegrex*, uint32_t, uint16_t),
                void (*iow32)(Memory*, Allegrex*, uint32_t, uint32_t));
    };
}

#endif
