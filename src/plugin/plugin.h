
#ifndef DOOVDE_PLUGIN_H_
#define DOOVDE_PLUGIN_H_

struct PluginBase;

namespace Doovde {
    struct Plugin {
        Plugin();
        ~Plugin();
        //---  Variables
        bool alive;
        PluginBase* plug;
        void* lib;

        //---  Functions
        void load(char* filename);
        void unload();
        void reset();
        bool command(uint32_t cmd, uint32_t incnt, void* indata,
                    uint32_t outcnt, void* outcnt);
        inline bool commandSimple(uint32_t cmd) {
            return command(cmd, 0, NULL, 0, NULL);
        }
    };
}

#endif
