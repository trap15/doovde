//---  System includes
#include <stdlib.h>
#include <stdint.h>

//---  Doovde includes
#include "log.h"
#include "plugin/plugin.h"

//---  Plugin includes
#include "plugin_sdk/plugin_base.h"
#if POSIX_DYLIB
#  include <dlfcn.h>
#  define load_library(file)    dlopen(file, RTLD_LAZY)
#  define get_symbol(l, sym)    dlsym(l, sym)
#  define close_library(l)  dlclose(l)
#elif WIN32_DYLIB
#  include <windows.h>
#  define load_library(file)    LoadLibrary(file);
#  define get_symbol(l, sym)    GetProcAddress(l, sym)
#  define close_library(l)  FreeLibrary(l)
#else
#  error "Your OS is not supported by the plugin architecture."
#endif

typedef PluginBase* (create_plugin_t)();

Doovde::Plugin::Plugin()
{
    plug = NULL;
    alive = false;
}

Doovde::Plugin::~Plugin()
{
    if(plug != NULL)
        unload();
    alive = false;
}

void Doovde::Plugin::load(char* filename)
{
    lib = load_library(filename);
    if(lib == NULL) {
        Doovde::log(Doovde::LogLevel::ERROR, "Can't load plugin %s", filename);
        return;
    }
    void* create_obj;
    create_obj = get_symbol(lib, "create_plugin");
    if(create_obj == NULL) {
        Doovde::log(Doovde::LogLevel::ERROR, "Can't load plugin creator %s", filename);
        close_library(lib);
        return;
    }
    union { create_plugin_t* func; void* obj; } alias;
    alias.obj = create_obj;
    plug = alias.func();
    if(plug == NULL) {
        Doovde::log(Doovde::LogLevel::ERROR, "Can't create plugin plug %s", filename);
        close_library(lib);
        return;
    }

    plug->init(_plugin_log);
    if(plug->alive == false) {
        Doovde::log(Doovde::LogLevel::ERROR, "Can't init plugin %s", filename);
        unload();
    }
    alive = true;
}

void Doovde::Plugin::unload()
{
    alive = false;
    delete plug;
    plug = NULL;
    close_library(lib);
}

void Doovde::Plugin::reset()
{
    plug->reset();
}

bool Doovde::Plugin::command(uint32_t cmd, uint32_t incnt, void* indata, uint32_t outcnt, void* outdata)
{
    return plug->command(cmd, incnt, indata, outcnt, outdata);
}
