PLUGINPATH     = src/plugin
HEADERS       += $${PLUGINPATH}/plugin.h
SOURCES       += $${PLUGINPATH}/plugin.cpp
RESOURCES     += 

unix:LIBS     += -ldl
unix:DEFINES  += POSIX_DYLIB=1

win32:DEFINES += WIN32_DYLIB=1
