//---  System includes
#include <stdio.h>
#include <stdint.h>
#include <math.h>

//---  Doovde includes
#include "log.h"
#include "cpu/allegrex.h"
#include "cpu/decode.h"
#include "cpu/cop1.h"

Doovde::Cop1::Cop1()
{
    alive = true;
}

Doovde::Cop1::~Cop1()
{
    alive = false;
}

void Doovde::Cop1::copFunc(Doovde::Allegrex* cpu, uint32_t op)
{
    switch((op >> 21) & 0x1F) {
        case 0x00: /* MFC1 */
            if(RT(op) == 0)
                break;
            cpu->regs.gpr[RT(op)] = regs[FS(op)];
            break;
        case 0x01:
            /* Nothing */
            break;
        case 0x02: /* CFC1 */
            if(RT(op) == 0)
                break;
            cpu->regs.gpr[RT(op)] = ctrlregs[FS(op)];
            break;
        case 0x03:
            /* Nothing */
            break;
        case 0x04: /* MTC1 */
            regs[FS(op)] = cpu->regs.gpr[RT(op)];
            break;
        case 0x05:
            /* Nothing */
            break;
        case 0x06: /* CTC1 */
            switch(FS(op)) {
                case REG_REVISION:
                    break;
                default:
                    ctrlregs[FS(op)] = cpu->regs.gpr[RT(op)];
                    break;
            }
            break;
        case 0x07:
            /* Nothing */
            break;
        case 0x08: /* BC1F/BC1T/BC1FL/BC1TL */
            switch((op >> 16) & 0x3) {
                case 0: /* BC1F */
                    if(!(ctrlregs[REG_CONTROL] & (1 << 23))) {
                        cpu->pc += IMM_S(op) << 2;
                    }
                    break;
                case 1: /* BC1T */
                    if(ctrlregs[REG_CONTROL] & (1 << 23)) {
                        cpu->pc += IMM_S(op) << 2;
                    }
                    break;
                case 2: /* BC1FL */
                    if(!(ctrlregs[REG_CONTROL] & (1 << 23))) {
                        cpu->pc += IMM_S(op) << 2;
                    }else{
                        cpu->npc = cpu->pc;
                    }
                    break;
                case 3: /* BC1TL */
                    if(ctrlregs[REG_CONTROL] & (1 << 23)) {
                        cpu->pc += IMM_S(op) << 2;
                    }else{
                        cpu->npc = cpu->pc;
                    }
                    break;
            }
            break;
        case 0x09:
            /* Nothing */
            break;
        case 0x0A:
            /* Nothing */
            break;
        case 0x0B:
            /* Nothing */
            break;
        case 0x0C:
            /* Nothing */
            break;
        case 0x0D:
            /* Nothing */
            break;
        case 0x0E:
            /* Nothing */
            break;
        case 0x0F:
            /* Nothing */
            break;
        case 0x10: /* S */
            switch(op & 0x3F) {
                case 0x00: /* ADD.S */
                    fprs[FD(op)] = fprs[FS(op)] + fprs[FT(op)];
                    break;
                case 0x01: /* SUB.S */
                    fprs[FD(op)] = fprs[FS(op)] - fprs[FT(op)];
                    break;
                case 0x02: /* MUL.S */
                    fprs[FD(op)] = fprs[FS(op)] * fprs[FT(op)];
                    break;
                case 0x03: /* DIV.S */
                    fprs[FD(op)] = fprs[FS(op)] / fprs[FT(op)];
                    break;
                case 0x04: /* SQRT.S */
                    fprs[FD(op)] = sqrtf(fprs[FS(op)]);
                    break;
                case 0x05: /* ABS.S */
                    fprs[FD(op)] = fabs(fprs[FS(op)]);
                    break;
                case 0x06: /* MOV.S */
                    fprs[FD(op)] = fprs[FS(op)];
                    break;
                case 0x07: /* NEG.S */
                    fprs[FD(op)] = -fprs[FS(op)];
                    break;
                case 0x08:
                    /* Nothing */
                    break;
                case 0x09:
                    /* Nothing */
                    break;
                case 0x0A:
                    /* Nothing */
                    break;
                case 0x0B:
                    /* Nothing */
                    break;
                case 0x0C: /* ROUND.W.S */
                    fprs[FD(op)] = (int32_t)rintf(fprs[FS(op)]);
                    break;
                case 0x0D: /* TRUNC.W.S */
                    fprs[FD(op)] = (int32_t)fprs[FS(op)];
                    break;
                case 0x0E: /* CEIL.W.S */
                    fprs[FD(op)] = (int32_t)ceilf(fprs[FS(op)]);
                    break;
                case 0x0F: /* FLOOR.W.S */
                    fprs[FD(op)] = (int32_t)floorf(fprs[FS(op)]);
                    break;
                case 0x10:
                    /* Nothing */
                    break;
                case 0x11:
                    /* Nothing */
                    break;
                case 0x12:
                    /* Nothing */
                    break;
                case 0x13:
                    /* Nothing */
                    break;
                case 0x14:
                    /* Nothing */
                    break;
                case 0x15:
                    /* Nothing */
                    break;
                case 0x16:
                    /* Nothing */
                    break;
                case 0x17:
                    /* Nothing */
                    break;
                case 0x18:
                    /* Nothing */
                    break;
                case 0x19:
                    /* Nothing */
                    break;
                case 0x1A:
                    /* Nothing */
                    break;
                case 0x1B:
                    /* Nothing */
                    break;
                case 0x1C:
                    /* Nothing */
                    break;
                case 0x1D:
                    /* Nothing */
                    break;
                case 0x1E:
                    /* Nothing */
                    break;
                case 0x1F:
                    /* Nothing */
                    break;
                case 0x20:
                    /* Nothing */
                    break;
                case 0x21:
                    /* Nothing */
                    break;
                case 0x22:
                    /* Nothing */
                    break;
                case 0x23:
                    /* Nothing */
                    break;
                case 0x24: /* CVT.W.S */
                    fprs[FD(op)] = (int32_t)regs[FS(op)];
                    break;
                case 0x25:
                    /* Nothing */
                    break;
                case 0x26:
                    /* Nothing */
                    break;
                case 0x27:
                    /* Nothing */
                    break;
                case 0x28:
                    /* Nothing */
                    break;
                case 0x29:
                    /* Nothing */
                    break;
                case 0x2A:
                    /* Nothing */
                    break;
                case 0x2B:
                    /* Nothing */
                    break;
                case 0x2C:
                    /* Nothing */
                    break;
                case 0x2D:
                    /* Nothing */
                    break;
                case 0x2E:
                    /* Nothing */
                    break;
                case 0x2F:
                    /* Nothing */
                    break;
                case 0x30: /* C.F.S */
                case 0x31: /* C.UN.S */
                case 0x32: /* C.EQ.S */
                case 0x33: /* C.UEQ.S */
                case 0x34: /* C.OLT.S */
                case 0x35: /* C.ULT.S */
                case 0x36: /* C.OLE.S */
                case 0x37: /* C.ULE.S */
                case 0x38: /* C.SF.S */
                case 0x39: /* C.NGLE.S */
                case 0x3A: /* C.SEQ.S */
                case 0x3B: /* C.NGL.S */
                case 0x3C: /* C.LT.S */
                case 0x3D: /* C.NGE.S */
                case 0x3E: /* C.LE.S */
                case 0x3F: /* C.NGT.S */
                {
                    bool less, equal, unordered = false, set = false;
                    if(isnan(fprs[FT(op)]) || isnan(fprs[FS(op)])) {
                        less = false;
                        equal = false;
                        unordered = true;
                        if(op & 8) {
                            ; // Exception
                        }
                    }else{
                        less = (fprs[FT(op)] < fprs[FS(op)]);
                        equal = (fprs[FT(op)] < fprs[FS(op)]);
                    }
                    if((op & 1) && unordered) set = true;
                    if((op & 2) && equal) set = true;
                    if((op & 4) && less) set = true;
                    if(set)
                        ctrlregs[REG_CONTROL] |=  (1 << 23);
                    else
                        ctrlregs[REG_CONTROL] &= ~(1 << 23);
                    break; }
            }
            break;
        case 0x11:
            /* Nothing */
            break;
        case 0x12:
            /* Nothing */
            break;
        case 0x13:
            /* Nothing */
            break;
        case 0x14: /* W */
            switch(op & 0x3F) {
                case 0x00:
                    /* Nothing */
                    break;
                case 0x01:
                    /* Nothing */
                    break;
                case 0x02:
                    /* Nothing */
                    break;
                case 0x03:
                    /* Nothing */
                    break;
                case 0x04:
                    /* Nothing */
                    break;
                case 0x05:
                    /* Nothing */
                    break;
                case 0x06:
                    /* Nothing */
                    break;
                case 0x07:
                    /* Nothing */
                    break;
                case 0x08:
                    /* Nothing */
                    break;
                case 0x09:
                    /* Nothing */
                    break;
                case 0x0A:
                    /* Nothing */
                    break;
                case 0x0B:
                    /* Nothing */
                    break;
                case 0x0C:
                    /* Nothing */
                    break;
                case 0x0D:
                    /* Nothing */
                    break;
                case 0x0E:
                    /* Nothing */
                    break;
                case 0x0F:
                    /* Nothing */
                    break;
                case 0x10:
                    /* Nothing */
                    break;
                case 0x11:
                    /* Nothing */
                    break;
                case 0x12:
                    /* Nothing */
                    break;
                case 0x13:
                    /* Nothing */
                    break;
                case 0x14:
                    /* Nothing */
                    break;
                case 0x15:
                    /* Nothing */
                    break;
                case 0x16:
                    /* Nothing */
                    break;
                case 0x17:
                    /* Nothing */
                    break;
                case 0x18:
                    /* Nothing */
                    break;
                case 0x19:
                    /* Nothing */
                    break;
                case 0x1A:
                    /* Nothing */
                    break;
                case 0x1B:
                    /* Nothing */
                    break;
                case 0x1C:
                    /* Nothing */
                    break;
                case 0x1D:
                    /* Nothing */
                    break;
                case 0x1E:
                    /* Nothing */
                    break;
                case 0x1F:
                    /* Nothing */
                    break;
                case 0x20: /* CVT.S.W */
                    switch(ctrlregs[REG_CONTROL] & 3) {
                        case 0: /* Nearest */
                            regs[FD(op)] = (int32_t)rintf(fprs[FS(op)]);
                            break;
                        case 1: /* To Zero */
                            regs[FD(op)] = (int32_t)truncf(fprs[FS(op)]);
                            break;
                        case 2: /* Up */
                            regs[FD(op)] = (int32_t)ceilf(fprs[FS(op)]);
                            break;
                        case 3: /* Down */
                            regs[FD(op)] = (int32_t)floorf(fprs[FS(op)]);
                            break;
                    }
                    break;
                case 0x21:
                    /* Nothing */
                    break;
                case 0x22:
                    /* Nothing */
                    break;
                case 0x23:
                    /* Nothing */
                    break;
                case 0x24:
                    /* Nothing */
                    break;
                case 0x25:
                    /* Nothing */
                    break;
                case 0x26:
                    /* Nothing */
                    break;
                case 0x27:
                    /* Nothing */
                    break;
                case 0x28:
                    /* Nothing */
                    break;
                case 0x29:
                    /* Nothing */
                    break;
                case 0x2A:
                    /* Nothing */
                    break;
                case 0x2B:
                    /* Nothing */
                    break;
                case 0x2C:
                    /* Nothing */
                    break;
                case 0x2D:
                    /* Nothing */
                    break;
                case 0x2E:
                    /* Nothing */
                    break;
                case 0x2F:
                    /* Nothing */
                    break;
                case 0x30:
                    /* Nothing */
                    break;
                case 0x31:
                    /* Nothing */
                    break;
                case 0x32:
                    /* Nothing */
                    break;
                case 0x33:
                    /* Nothing */
                    break;
                case 0x34:
                    /* Nothing */
                    break;
                case 0x35:
                    /* Nothing */
                    break;
                case 0x36:
                    /* Nothing */
                    break;
                case 0x37:
                    /* Nothing */
                    break;
                case 0x38:
                    /* Nothing */
                    break;
                case 0x39:
                    /* Nothing */
                    break;
                case 0x3A:
                    /* Nothing */
                    break;
                case 0x3B:
                    /* Nothing */
                    break;
                case 0x3C:
                    /* Nothing */
                    break;
                case 0x3D:
                    /* Nothing */
                    break;
                case 0x3E:
                    /* Nothing */
                    break;
                case 0x3F:
                    /* Nothing */
                    break;
            }
            break;
        case 0x15:
            /* Nothing */
            break;
        case 0x16:
            /* Nothing */
            break;
        case 0x17:
            /* Nothing */
            break;
        case 0x18:
            /* Nothing */
            break;
        case 0x19:
            /* Nothing */
            break;
        case 0x1A:
            /* Nothing */
            break;
        case 0x1B:
            /* Nothing */
            break;
        case 0x1C:
            /* Nothing */
            break;
        case 0x1D:
            /* Nothing */
            break;
        case 0x1E:
            /* Nothing */
            break;
        case 0x1F:
            /* Nothing */
            break;
    }
}

void Doovde::Cop1::copFuncDisas(Doovde::Allegrex* cpu, uint32_t op, char opbuf[16], char argbuf[4][16])
{
    (void)cpu;
    sprintf(opbuf, ".dc.w");
    sprintf(argbuf[0], "0x%08X", op);
}

void Doovde::Cop1::lwc(Doovde::Allegrex* cpu, uint32_t op, uint32_t val)
{
    (void)cpu;
    regs[FT(op)] = val;
}

uint32_t Doovde::Cop1::swc(Doovde::Allegrex* cpu, uint32_t op)
{
    (void)cpu;
    return regs[FT(op)];
}

void Doovde::Cop1::reset(Doovde::Allegrex* cpu)
{
    (void)cpu;
    for(int i = 0; i < 32; i++) {
        regs[i] = 0;
        ctrlregs[i] = 0;
    }
    ctrlregs[REG_REVISION] = (rev_impl << 8) | (rev_maj << 4) | (rev_min << 0);
}
