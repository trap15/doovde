//---  System includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <float.h>

//---  Doovde includes
#include "log.h"
#include "cpu/allegrex.h"
#include "cpu/cop0.h"
#include "cpu/cop1.h"
#include "cpu/cop2.h"
#include "cpu/decode.h"

/* We don't do overflow exceptions because no one else does. Does the PSP actually make these!? */

Doovde::Allegrex::Allegrex(bool isMe) : media_engine(isMe)
{
    write8  = NULL;
    write16 = NULL;
    write32 = NULL;
    read8  = NULL;
    read16 = NULL;
    read32 = NULL;
    virt_to_phys = NULL;
    phys_to_virt = NULL;
    cop0 = new Cop0();
    cop1 = new Cop1();
    cop2 = new Cop2();
    fetching = false;
    alive = true;
}

Doovde::Allegrex::~Allegrex()
{
    delete cop0;
    delete cop1;
    delete cop2;
    alive = false;
}

int Doovde::Allegrex::run(int cycles)
{
    cycles_left += cycles;
    int startcycles = cycles_left;
    while(cycles_left > 0) {
        cycles_left -= runSingle();
    }
    return startcycles - cycles_left;
}

void Doovde::Allegrex::reset()
{
    for(int i = 0; i < 32; i++) {
        regs.gpr[i] = 0;
        regs.dbgr[i] = 0;
    }
    regs.lo = regs.hi = 0;
    op = 0; // Slight hack; first cycle is always a NOP. Allows easy pipeline simulation though.
    pc = npc = 0;
    int_state = 0;
    cycles_left = 0;
    ll = false;
    cop0->reset(this);
    cop1->reset(this);
    cop2->reset(this);
    exception(EXC_RESET);
}

static uint64_t _acc_read(Doovde::Allegrex* cpu)
{
    uint64_t ret = cpu->regs.hi;
    ret = (ret << 32) | cpu->regs.lo;
    return ret;
}

static void _acc_write(Doovde::Allegrex* cpu, uint64_t acc)
{
    cpu->regs.hi = acc >> 32;
    cpu->regs.lo = acc & 0xFFFFFFFF;
}

int Doovde::Allegrex::runSingle()
{
    static int countflip = 0;
    int cycles = 1;
    fetching = true;
    op = read32(this, pc);
    fetching = false;
    pc = npc;
    npc = pc + 4;
    switch((op >> 26) & 0x3F) {
        case 0x00: /* SPECIAL */
            switch(op & 0x3F) {
                case 0x00: /* SLL */
                    if(RD(op) == 0)
                        break;
                    regs.gpr[RD(op)] = regs.gpr[RT(op)] << SA(op);
                    break;
                case 0x01:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x02: /* SRL/ROR */
                    if(RD(op) == 0)
                        break;
                    if(RS(op) == 1) /* ROR */
                        regs.gpr[RD(op)] = (regs.gpr[RT(op)] >> SA(op)) | (regs.gpr[RT(op)] << (32-SA(op)));
                    else /* SRL */
                        regs.gpr[RD(op)] = regs.gpr[RT(op)] >> SA(op);
                    break;
                case 0x03: { /* SRA */
                    if(RD(op) == 0)
                        break;
                    bool sign = (regs.gpr[RT(op)] & 0x80000000) ? true : false;
                    regs.gpr[RD(op)] = (regs.gpr[RT(op)] >> SA(op)) | ((sign ? 0xFFFFFFFF : 0) << (32-SA(op)));
                    break;}
                case 0x04: /* SLLV */
                    if(RD(op) == 0)
                        break;
                    regs.gpr[RD(op)] = regs.gpr[RT(op)] << (regs.gpr[RS(op)] & 0x1F);
                    break;
                case 0x05:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x06: { /* SRLV/RORV */
                    if(RD(op) == 0)
                        break;
                    int rot = regs.gpr[RS(op)] & 0x1F;
                    if(SA(op) == 1) { /* RORV */
                        regs.gpr[RD(op)] = (regs.gpr[RT(op)] >> rot) | (regs.gpr[RT(op)] << (32-rot));
                    }else{ /* SRLV */
                        regs.gpr[RD(op)] = regs.gpr[RT(op)] >> rot;
                    }
                    break;}
                case 0x07: { /* SRAV */
                    if(RD(op) == 0)
                        break;
                    int rot = regs.gpr[RS(op)] & 0x1F;
                    bool sign = (regs.gpr[RT(op)] & 0x80000000) ? true : false;
                    regs.gpr[RD(op)] = (regs.gpr[RT(op)] >> rot) | ((sign ? 0xFFFFFFFF : 0) << (32-rot));
                    break;}
                case 0x08: /* JR */
                    npc = regs.gpr[RS(op)];
                    if(npc & 3)
                        exception(EXC_ADDRESS_LOAD, 1);
                    break;
                case 0x09: { /* JALR */
                    uint32_t tmp = regs.gpr[RS(op)];
                    if(RD(op) != 0)
                        regs.gpr[RD(op)] = npc;
                    npc = tmp;
                    if(npc & 3)
                        exception(EXC_ADDRESS_LOAD, 1);
                    break;}
                case 0x0A:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x0B:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x0C: /* SYSCALL */
                    exception(EXC_SYSCALL);
                    break;
                case 0x0D: /* BREAK */
                    exception(EXC_BREAKPOINT);
                    break;
                case 0x0F: /* SYNC */
                    Doovde::log(Doovde::LogLevel::WARNING, "SYNC instruction.");
                    break;
                case 0x10: /* MFHI */
                    if(RD(op) == 0)
                        break;
                    regs.gpr[RD(op)] = regs.hi;
                    break;
                case 0x11: /* MTHI */
                    regs.hi = regs.gpr[RS(op)];
                    break;
                case 0x12: /* MFLO */
                    if(RD(op) == 0)
                        break;
                    regs.gpr[RD(op)] = regs.lo;
                    break;
                case 0x13: /* MTLO */
                    regs.lo = regs.gpr[RS(op)];
                    break;
                case 0x14:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x15:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x16: { /* CLZ */
                    if(RD(op) == 0)
                        break;
                    if(RS(op) == 0) {
                        regs.gpr[RD(op)] = 32;
                        break;
                    }
                    int zerocount[16] = { 4, 3, 2, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0 };
                    uint32_t v = regs.gpr[RS(op)];
                    int cnt = 0;
                    for(int i = 0; i < 8; i++) {
                        int x = zerocount[(v >> (28 - (i << 2))) & 0xF];
                        cnt += x;
                        if(x != 4) {
                            break;
                        }
                    }
                    regs.gpr[RD(op)] = cnt;
                    break; }
                case 0x17: { /* CLO */
                    if(RD(op) == 0)
                        break;
                    if(RS(op) == 0) {
                        regs.gpr[RD(op)] = 0;
                        break;
                    }
                    int onecount[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 3, 4 };
                    uint32_t v = regs.gpr[RS(op)];
                    int cnt = 0;
                    for(int i = 0; i < 8; i++) {
                        int x = onecount[(v >> (28 - (i << 2))) & 0xF];
                        cnt += x;
                        if(x != 4) {
                            break;
                        }
                    }
                    regs.gpr[RD(op)] = cnt;
                    break; }
                case 0x18: { /* MULT */
                    int64_t val = (int32_t)regs.gpr[RS(op)] * (int32_t)regs.gpr[RT(op)];
                    _acc_write(this, val);
                    cycles = 12;
                    break; }
                case 0x19: { /* MULTU */
                    uint64_t val = regs.gpr[RS(op)] * regs.gpr[RT(op)];
                    _acc_write(this, val);
                    cycles = 12;
                    break; }
                case 0x1A: /* DIV */
                    if(regs.gpr[RT(op)] == 0) { /* Undefined */
                        regs.lo = regs.hi = -1;
                    }else{
                        regs.lo = ((int32_t)regs.gpr[RS(op)] / (int32_t)regs.gpr[RT(op)]) & 0xFFFFFFFF;
                        regs.hi = ((int32_t)regs.gpr[RS(op)] % (int32_t)regs.gpr[RT(op)]) & 0xFFFFFFFF;
                    }
                    cycles = 75;
                    break;
                case 0x1B: /* DIVU */
                    if(regs.gpr[RT(op)] == 0) { /* Undefined */
                        regs.lo = regs.hi = -1;
                    }else{
                        regs.lo = (regs.gpr[RS(op)] / regs.gpr[RT(op)]) & 0xFFFFFFFF;
                        regs.hi = (regs.gpr[RS(op)] % regs.gpr[RT(op)]) & 0xFFFFFFFF;
                    }
                    cycles = 75;
                    break;
                case 0x1C: { /* MADD */
                    int64_t acc = _acc_read(this);
                    int64_t val = (int32_t)regs.gpr[RS(op)] * (int32_t)regs.gpr[RT(op)];
                    _acc_write(this, acc + val);
                    break; }
                case 0x1D: { /* MADDU */
                    uint64_t acc = _acc_read(this);
                    uint64_t val = regs.gpr[RS(op)] * regs.gpr[RT(op)];
                    _acc_write(this, acc + val);
                    break; }
                case 0x1E:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x1F:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x20: /* ADD */
                    /* TODO: Integer Overflow Exception */
                    if(RD(op) == 0)
                        break;
                    regs.gpr[RD(op)] = regs.gpr[RS(op)] + regs.gpr[RT(op)];
                    break;
                case 0x21: /* ADDU */
                    if(RD(op) == 0)
                        break;
                    regs.gpr[RD(op)] = regs.gpr[RS(op)] + regs.gpr[RT(op)];
                    break;
                case 0x22: /* SUB */
                    /* TODO: Integer Overflow Exception */
                    if(RD(op) == 0)
                        break;
                    regs.gpr[RD(op)] = regs.gpr[RS(op)] - regs.gpr[RT(op)];
                    break;
                case 0x23: /* SUBU */
                    if(RD(op) == 0)
                        break;
                    regs.gpr[RD(op)] = regs.gpr[RS(op)] - regs.gpr[RT(op)];
                    break;
                case 0x24: /* AND */
                    if(RD(op) == 0)
                        break;
                    regs.gpr[RD(op)] = regs.gpr[RS(op)] & regs.gpr[RT(op)];
                    break;
                case 0x25: /* OR */
                    if(RD(op) == 0)
                        break;
                    regs.gpr[RD(op)] = regs.gpr[RS(op)] | regs.gpr[RT(op)];
                    break;
                case 0x26: /* XOR */
                    if(RD(op) == 0)
                        break;
                    regs.gpr[RD(op)] = regs.gpr[RS(op)] ^ regs.gpr[RT(op)];
                    break;
                case 0x27: /* NOR */
                    if(RD(op) == 0)
                        break;
                    regs.gpr[RD(op)] = ~(regs.gpr[RS(op)] | regs.gpr[RT(op)]);
                    break;
                case 0x28:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x29:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x2A: /* SLT */
                    if(RD(op) == 0)
                        break;
                    if((int32_t)regs.gpr[RS(op)] < (int32_t)regs.gpr[RT(op)])
                        regs.gpr[RD(op)] = 1;
                    else
                        regs.gpr[RD(op)] = 0;
                    break;
                case 0x2B: /* SLTU */
                    if(RD(op) == 0)
                        break;
                    if(regs.gpr[RS(op)] < regs.gpr[RT(op)])
                        regs.gpr[RD(op)] = 1;
                    else
                        regs.gpr[RD(op)] = 0;
                    break;
                case 0x2C: /* MAX */
                    if(RD(op) == 0)
                        break;
                    if((int32_t)regs.gpr[RS(op)] > (int32_t)regs.gpr[RT(op)])
                        regs.gpr[RD(op)] = regs.gpr[RS(op)];
                    else
                        regs.gpr[RD(op)] = regs.gpr[RT(op)];
                    break;
                case 0x2D: /* MIN */
                    if(RD(op) == 0)
                        break;
                    if((int32_t)regs.gpr[RS(op)] < (int32_t)regs.gpr[RT(op)])
                        regs.gpr[RD(op)] = regs.gpr[RS(op)];
                    else
                        regs.gpr[RD(op)] = regs.gpr[RT(op)];
                    break;
                case 0x2E: { /* MSUB */
                    int64_t acc = _acc_read(this);
                    int64_t val = (int32_t)regs.gpr[RS(op)] * (int32_t)regs.gpr[RT(op)];
                    _acc_write(this, acc - val);
                    break; }
                case 0x2F: { /* MSUBU */
                    uint64_t acc = _acc_read(this);
                    uint64_t val = regs.gpr[RS(op)] * regs.gpr[RT(op)];
                    _acc_write(this, acc - val);
                    break; }
                case 0x30: /* TGE */
                    if((int32_t)regs.gpr[RS(op)] >= (int32_t)regs.gpr[RT(op)]) {
                        exception(EXC_TRAP);
                    }
                    break;
                case 0x31: /* TGEU */
                    if(regs.gpr[RS(op)] >= regs.gpr[RT(op)]) {
                        exception(EXC_TRAP);
                    }
                    break;
                case 0x32: /* TLT */
                    if((int32_t)regs.gpr[RS(op)] < (int32_t)regs.gpr[RT(op)]) {
                        exception(EXC_TRAP);
                    }
                    break;
                case 0x33: /* TLTU */
                    if(regs.gpr[RS(op)] < regs.gpr[RT(op)]) {
                        exception(EXC_TRAP);
                    }
                    break;
                case 0x34: /* TEQ */
                    if(regs.gpr[RS(op)] == regs.gpr[RT(op)]) {
                        exception(EXC_TRAP);
                    }
                    break;
                case 0x35:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x36: /* TNE */
                    if(regs.gpr[RS(op)] != regs.gpr[RT(op)]) {
                        exception(EXC_TRAP);
                    }
                    break;
                case 0x37:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x38:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x39:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x3A:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x3B:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x3C:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x3D:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x3E:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x3F:
                    exception(EXC_RESERVED_INSTR);
                    break;
            }
            break;
        case 0x01: /* REGIMM */
            switch((op >> 16) & 0x1F) {
                case 0x00: /* BLTZ */
                    if((int32_t)regs.gpr[RS(op)] < 0) {
                        npc = pc + (IMM_S(op) << 2);
                    }
                    break;
                case 0x01: /* BGEZ */
                    if((int32_t)regs.gpr[RS(op)] >= 0) {
                        npc = pc + (IMM_S(op) << 2);
                    }
                    break;
                case 0x02: /* BLTZL */
                    if((int32_t)regs.gpr[RS(op)] < 0) {
                        npc = pc + (IMM_S(op) << 2);
                    }else{
                        pc = npc;
                        npc += 4;
                    }
                    break;
                case 0x03: /* BGEZL */
                    if((int32_t)regs.gpr[RS(op)] >= 0) {
                        npc = pc + (IMM_S(op) << 2);
                    }else{
                        pc = npc;
                        npc += 4;
                    }
                    break;
                case 0x04:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x05:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x06:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x07:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x08: /* TGEI */
                    if((int32_t)regs.gpr[RS(op)] >= IMM_S(op)) {
                        exception(EXC_TRAP);
                    }
                    break;
                case 0x09: /* TGEIU */
                    if(regs.gpr[RS(op)] >= IMM(op)) {
                        exception(EXC_TRAP);
                    }
                    break;
                case 0x0A: /* TLTI */
                    if((int32_t)regs.gpr[RS(op)] < IMM_S(op)) {
                        exception(EXC_TRAP);
                    }
                    break;
                case 0x0B: /* TLTIU */
                    if(regs.gpr[RS(op)] < IMM(op)) {
                        exception(EXC_TRAP);
                    }
                    break;
                case 0x0C: /* TEQI */
                    if(regs.gpr[RS(op)] == IMM(op)) {
                        exception(EXC_TRAP);
                    }
                    break;
                case 0x0E: /* TNEI */
                    if(regs.gpr[RS(op)] != IMM(op)) {
                        exception(EXC_TRAP);
                    }
                    break;
                case 0x0F: /* Not invalid? */
                    /* TODO */
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x10: /* BLTZAL */
                    regs.gpr[31] = npc;
                    if((int32_t)regs.gpr[RS(op)] < 0) {
                        npc = pc + (IMM_S(op) << 2);
                    }
                    break;
                case 0x11: /* BGEZAL */
                    regs.gpr[31] = npc;
                    if((int32_t)regs.gpr[RS(op)] >= 0) {
                        npc = pc + (IMM_S(op) << 2);
                    }
                    break;
                case 0x12: /* BLTZALL */
                    regs.gpr[31] = npc;
                    if((int32_t)regs.gpr[RS(op)] < 0) {
                        npc = pc + (IMM_S(op) << 2);
                    }else{
                        pc = npc;
                        npc += 4;
                    }
                    break;
                case 0x13: /* BGEZALL */
                    regs.gpr[31] = npc;
                    if((int32_t)regs.gpr[RS(op)] >= 0) {
                        npc = pc + (IMM_S(op) << 2);
                    }else{
                        pc = npc;
                        npc += 4;
                    }
                    break;
                case 0x14:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x15:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x16:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x17:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x18:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x19:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x1A:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x1B:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x1C:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x1D:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x1E:
                    exception(EXC_RESERVED_INSTR);
                    break;
                case 0x1F:
                    exception(EXC_RESERVED_INSTR);
                    break;
            }
            break;
        case 0x02: /* J */
            npc = (pc & 0xF0000000) | (TARGET(op) << 2);
            if(npc & 3)
                exception(EXC_ADDRESS_LOAD, 1);
            break;
        case 0x03: /* JAL */
            regs.gpr[31] = npc;
            npc = (pc & 0xF0000000) | (TARGET(op) << 2);
            if(npc & 3)
                exception(EXC_ADDRESS_LOAD, 1);
            break;
        case 0x04: /* BEQ */
            if(regs.gpr[RT(op)] == regs.gpr[RS(op)]) {
                npc = pc + (IMM_S(op) << 2);
            }
            break;
        case 0x05: /* BNE */
            if(regs.gpr[RT(op)] != regs.gpr[RS(op)]) {
                npc = pc + (IMM_S(op) << 2);
            }
            break;
        case 0x06: /* BLEZ */
            if((int32_t)regs.gpr[RS(op)] <= 0) {
                npc = pc + (IMM_S(op) << 2);
            }
            break;
        case 0x07: /* BGTZ */
            if((int32_t)regs.gpr[RS(op)] > 0) {
                npc = pc + (IMM_S(op) << 2);
            }
            break;
        case 0x08: /* ADDI */
            /* TODO: Integer Overflow Exception */
            if(RT(op) == 0)
                break;
            regs.gpr[RT(op)] = regs.gpr[RS(op)] + IMM_S(op);
            break;
        case 0x09: /* ADDIU */
            if(RT(op) == 0)
                break;
            regs.gpr[RT(op)] = regs.gpr[RS(op)] + IMM_S(op);
            break;
        case 0x0A: /* SLTI */
            if(RD(op) == 0)
                break;
            if((int32_t)regs.gpr[RS(op)] < IMM_S(op))
                regs.gpr[RD(op)] = 1;
            else
                regs.gpr[RD(op)] = 0;
            break;
        case 0x0B: /* SLTIU */
            if(RD(op) == 0)
                break;
            if(regs.gpr[RS(op)] < IMM(op))
                regs.gpr[RD(op)] = 1;
            else
                regs.gpr[RD(op)] = 0;
            break;
        case 0x0C: /* ANDI */
            if(RT(op) == 0)
                break;
            regs.gpr[RT(op)] = regs.gpr[RS(op)] & IMM(op);
            break;
        case 0x0D: /* ORI */
            if(RT(op) == 0)
                break;
            regs.gpr[RT(op)] = regs.gpr[RS(op)] | IMM(op);
            break;
        case 0x0E: /* XORI */
            if(RT(op) == 0)
                break;
            regs.gpr[RT(op)] = regs.gpr[RS(op)] ^ IMM(op);
            break;
        case 0x0F: /* LUI */
            if(RT(op) == 0)
                break;
            regs.gpr[RT(op)] = IMM(op) << 16;
            break;
        case 0x10: /* COP0 */
            switch((op >> 21) & 0x1F) {
                case 0x00: /* MFPC/MFPS/MFC0 */
                    if(cop0->alive == false) {
                        exception(EXC_UNUSABLE_COP, 0, 0);
                        break;
                    }
                    if(RT(op) == 0)
                        break;
                    regs.gpr[RT(op)] = cop0->mfc(this, op, RD(op));
                    break;
                case 0x01:
                    /* Nothing */
                    break;
                case 0x02: /* CFC0 */
                    if(cop0->alive == false) {
                        exception(EXC_UNUSABLE_COP, 0, 0);
                        break;
                    }
                    if(RT(op) == 0)
                        break;
                    regs.gpr[RT(op)] = cop0->cfc(this, op, RD(op));
                    break;
                case 0x03:
                    /* Nothing */
                    break;
                case 0x04: /* MTPC/MTPS/MTC0 */
                    if(cop0->alive == false) {
                        exception(EXC_UNUSABLE_COP, 0, 0);
                        break;
                    }
                    cop0->mtc(this, op, RD(op), regs.gpr[RT(op)]);
                    break;
                case 0x05:
                    /* Nothing */
                    break;
                case 0x06: /* CTC0 */
                    if(cop0->alive == false) {
                        exception(EXC_UNUSABLE_COP, 0, 0);
                        break;
                    }
                    cop0->ctc(this, op, RD(op), regs.gpr[RT(op)]);
                    break;
                case 0x07:
                    /* Nothing */
                    break;
                case 0x08:
                    /* Nothing */
                    break;
                case 0x09:
                    /* Nothing */
                    break;
                case 0x0A:
                    /* Nothing */
                    break;
                case 0x0B:
                    /* Nothing */
                    break;
                case 0x0C:
                    /* Nothing */
                    break;
                case 0x0D:
                    /* Nothing */
                    break;
                case 0x0E:
                    /* Nothing */
                    break;
                case 0x0F:
                    /* Nothing */
                    break;
                case 0x10: case 0x11: case 0x12: case 0x13:
                case 0x14: case 0x15: case 0x16: case 0x17:
                case 0x18: case 0x19: case 0x1A: case 0x1B:
                case 0x1C: case 0x1D: case 0x1E: case 0x1F:
                    if(cop0->alive == false) {
                        exception(EXC_UNUSABLE_COP, 0, 0);
                        break;
                    }
                    cop0->cofun(this, op, COFUN(op));
                    break;
            }
            break;
        case 0x11: /* COP1 */
            if(cop1->alive == false) {
                exception(EXC_UNUSABLE_COP, 0, 1);
                break;
            }
            cop1->copFunc(this, op);
            break;
        case 0x12: /* COP2 */
            if(cop2->alive == false) {
                exception(EXC_UNUSABLE_COP, 0, 2);
                break;
            }
            cop2->copFunc(this, op);
            break;
        case 0x14: /* BEQL */
            if(regs.gpr[RT(op)] == regs.gpr[RS(op)]) {
                npc = pc + (IMM_S(op) << 2);
            }else{
                pc = npc;
                npc += 4;
            }
            break;
        case 0x15: /* BNEL */
            if(regs.gpr[RT(op)] != regs.gpr[RS(op)]) {
                npc = pc + (IMM_S(op) << 2);
            }else{
                pc = npc;
                npc += 4;
            }
            break;
        case 0x16: /* BLEZL */
            if((int32_t)regs.gpr[RS(op)] <= 0) {
                npc = pc + (IMM_S(op) << 2);
            }else{
                pc = npc;
                npc += 4;
            }
            break;
        case 0x17: /* BGTZL */
            if((int32_t)regs.gpr[RS(op)] > 0) {
                npc = pc + (IMM_S(op) << 2);
            }else{
                pc = npc;
                npc += 4;
            }
            break;
        case 0x18: /* VFPU0 */
            switch((op >> 23) & 0x7) {
                case 0x0: /* VADD */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        cop2->vec1[i] += cop2->vec2[i];
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec1);
                    break;
                case 0x1: /* VSUB */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        cop2->vec1[i] -= cop2->vec2[i];
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec1);
                    break;
                case 0x2: /* VSBN */
                    /* Only vsize == 1 works? */
                    if(VSIZE(op) != 1) {
                        Doovde::log(Doovde::LogLevel::WARNING, "VSBN not single.");
                    }
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        cop2->vec1[i] = cop2->vec1[i] * powf(2.0f, cop2->vec2i[i]);
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec1);
                    break;
                case 0x03:
                    /* Nothing */
                    break;
                case 0x04:
                    /* Nothing */
                    break;
                case 0x05:
                    /* Nothing */
                    break;
                case 0x06:
                    /* Nothing */
                    break;
                case 0x7: /* VDIV */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        cop2->vec1[i] /= cop2->vec2[i];
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec1);
                    break;
            }
            break;
        case 0x19: /* VFPU1 */
            switch((op >> 23) & 0x7) {
                case 0x0: /* VMUL */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        cop2->vec1[i] *= cop2->vec2[i];
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec1);
                    break;
                case 0x1: { /* VDOT */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    float dprod = 0.0f;
                    for(int i = 0; i < VSIZE(op); i++) {
                        dprod += cop2->vec1[i] * cop2->vec2[i];
                    }
                    cop2->vec3[0] = dprod;
                    cop2->saveDstVector(1, VD(op), cop2->vec3);
                    break; }
                case 0x2: /* VSCL */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        cop2->vec1[i] *= cop2->vec2[0];
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec1);
                    break;
                case 0x4: { /* VHDP */ /* Half Dot Product? */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    float hdprod = 0;
                    int i;
                    for(i = 0; i < VSIZE(op) - 1; i++) {
                        hdprod += cop2->vec1[i] * cop2->vec2[i];
                    }
                    cop2->vec2[0] = hdprod + cop2->vec2[i];
                    cop2->saveDstVector(1, VD(op), cop2->vec2);
                    break; }
                case 0x5: /* VCRS */
                    if(VSIZE(op) != 3) {
                        Doovde::log(Doovde::LogLevel::WARNING, "VCRS not triple.");
                    }
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    cop2->vec3[0] = cop2->vec1[1] * cop2->vec2[2];
                    cop2->vec3[1] = cop2->vec1[2] * cop2->vec2[0];
                    cop2->vec3[2] = cop2->vec1[0] * cop2->vec2[1];
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                    break;
                case 0x6: /* VDET */
                    if(VSIZE(op) != 2) {
                        Doovde::log(Doovde::LogLevel::WARNING, "VDET not pair.");
                    }
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    cop2->vec1[0] = (cop2->vec1[0] * cop2->vec2[1]) - (cop2->vec1[1] * cop2->vec2[0]);
                    cop2->saveDstVector(1, VD(op), cop2->vec1);
                    break;
                case 0x07:
                    /* Nothing */
                    break;
            }
            break;
        case 0x1B: /* VFPU3 */
            switch((op >> 23) & 0x7) {
                case 0x0: { /* VCMP */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    bool cc_or = false;
                    bool cc_and = true;
                    for(int i = 0; i < VSIZE(op); i++) {
                        bool state;
                        switch(op & 0xF) { /* conditions */
                            case 0x0: /* FL */
                                state = false;
                                break;
                            case 0x1: /* EQ */
                                state = cop2->vec1[i] == cop2->vec2[i];
                                break;
                            case 0x2: /* LT */
                                state = cop2->vec1[i] < cop2->vec2[i];
                                break;
                            case 0x3: /* LE */
                                state = cop2->vec1[i] <= cop2->vec2[i];
                                break;
                            case 0x4: /* TR */
                                state = true;
                                break;
                            case 0x5: /* NE */
                                state = cop2->vec1[i] != cop2->vec2[i];
                                break;
                            case 0x6: /* GE */
                                state = cop2->vec1[i] >= cop2->vec2[i];
                                break;
                            case 0x7: /* GT */
                                state = cop2->vec1[i] > cop2->vec2[i];
                                break;
                            case 0x8: /* EZ */
                                state = (cop2->vec1[i] == 0.0f) || (cop2->vec1[i] == -0.0f);
                                break;
                            case 0x9: /* EN */
                                state = isnan(cop2->vec1[i]);
                                break;
                            case 0xA: /* EI */
                                state = isinf(cop2->vec1[i]);
                                break;
                            case 0xB: /* ES */
                                state = isnan(cop2->vec1[i]) || isinf(cop2->vec1[i]);
                                break;
                            case 0xC: /* NZ */
                                state = (cop2->vec1[i] != 0.0f) && (cop2->vec1[i] != -0.0f);
                                break;
                            case 0xD: /* NN */
                                state = !isnan(cop2->vec1[i]);
                                break;
                            case 0xE: /* NI */
                                state = !isinf(cop2->vec1[i]);
                                break;
                            case 0xF: /* NS */
                                state = (!isnan(cop2->vec1[i])) && (!isinf(cop2->vec1[i]));
                                break;
                        }
                        if(state)
                            cc_or = true;
                        else
                            cc_and = false;
                        cop2->cc[i] = state;
                    }
                    cop2->cc[4] = cc_or;
                    cop2->cc[5] = cc_and;
                    break; }
                case 0x1:
                    /* Nothing */
                    break;
                case 0x2: /* VMIN */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        if(cop2->vec1[i] < cop2->vec2[i])
                            cop2->vec3[i] = cop2->vec1[i];
                        else
                            cop2->vec3[i] = cop2->vec2[i];
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                    break;
                case 0x3: /* VMAX */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        if(cop2->vec1[i] > cop2->vec2[i])
                            cop2->vec3[i] = cop2->vec1[i];
                        else
                            cop2->vec3[i] = cop2->vec2[i];
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                    break;
                case 0x4:
                    /* Nothing */
                    break;
                case 0x5: /* VSCMP */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        if((cop2->vec1[i] - cop2->vec2[i]) < 0)
                            cop2->vec3[i] = -1.0f;
                        else if((cop2->vec1[i] - cop2->vec2[i]) > 0)
                            cop2->vec3[i] =  1.0f;
                        else
                            cop2->vec3[i] =  0.0f;
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                    break;
                case 0x6: /* VSGE */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        if(cop2->vec1[i] >= cop2->vec2[i])
                            cop2->vec3[i] = 1.0f;
                        else
                            cop2->vec3[i] = 0.0f;
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                    break;
                case 0x7: /* VSLT */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    cop2->loadTgtVector(VSIZE(op), VT(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        if(cop2->vec1[i] < cop2->vec2[i])
                            cop2->vec3[i] = 1.0f;
                        else
                            cop2->vec3[i] = 0.0f;
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                    break;
            }
            break;
        case 0x1C: /* SPECIAL2 */
            switch(op & 0x3F) {
                case 0x00: /* HALT */
                    /* TODO */
                    break;
                case 0x01:
                    /* Nothing */
                    break;
                case 0x02:
                    /* Nothing */
                    break;
                case 0x03:
                    /* Nothing */
                    break;
                case 0x04:
                    /* Nothing */
                    break;
                case 0x05:
                    /* Nothing */
                    break;
                case 0x06:
                    /* Nothing */
                    break;
                case 0x07:
                    /* Nothing */
                    break;
                case 0x08:
                    /* Nothing */
                    break;
                case 0x09:
                    /* Nothing */
                    break;
                case 0x0A:
                    /* Nothing */
                    break;
                case 0x0B:
                    /* Nothing */
                    break;
                case 0x0C:
                    /* Nothing */
                    break;
                case 0x0D:
                    /* Nothing */
                    break;
                case 0x0E:
                    /* Nothing */
                    break;
                case 0x0F:
                    /* Nothing */
                    break;
                case 0x10:
                    /* Nothing */
                    break;
                case 0x11:
                    /* Nothing */
                    break;
                case 0x12:
                    /* Nothing */
                    break;
                case 0x13:
                    /* Nothing */
                    break;
                case 0x14:
                    /* Nothing */
                    break;
                case 0x15:
                    /* Nothing */
                    break;
                case 0x16:
                    /* Nothing */
                    break;
                case 0x17:
                    /* Nothing */
                    break;
                case 0x18:
                    /* Nothing */
                    break;
                case 0x19:
                    /* Nothing */
                    break;
                case 0x1A:
                    /* Nothing */
                    break;
                case 0x1B:
                    /* Nothing */
                    break;
                case 0x1C:
                    /* Nothing */
                    break;
                case 0x1D:
                    /* Nothing */
                    break;
                case 0x1E:
                    /* Nothing */
                    break;
                case 0x1F:
                    /* Nothing */
                    break;
                case 0x20:
                    /* Nothing */
                    break;
                case 0x21:
                    /* Nothing */
                    break;
                case 0x22:
                    /* Nothing */
                    break;
                case 0x23:
                    /* Nothing */
                    break;
                case 0x24: /* MFIC */
                    /* RD is unused? */
                    if(RT(op) == 0)
                        break;
                    regs.gpr[RT(op)] = int_state;
                    break;
                case 0x25:
                    /* Nothing */
                    break;
                case 0x26: /* MTIC */
                    /* RD is unused? */
                    int_state = regs.gpr[RT(op)];
                    break;
                case 0x27:
                    /* Nothing */
                    break;
                case 0x28:
                    /* Nothing */
                    break;
                case 0x29:
                    /* Nothing */
                    break;
                case 0x2A:
                    /* Nothing */
                    break;
                case 0x2B:
                    /* Nothing */
                    break;
                case 0x2C:
                    /* Nothing */
                    break;
                case 0x2D:
                    /* Nothing */
                    break;
                case 0x2E:
                    /* Nothing */
                    break;
                case 0x2F:
                    /* Nothing */
                    break;
                case 0x30:
                    /* Nothing */
                    break;
                case 0x31:
                    /* Nothing */
                    break;
                case 0x32:
                    /* Nothing */
                    break;
                case 0x33:
                    /* Nothing */
                    break;
                case 0x34:
                    /* Nothing */
                    break;
                case 0x35:
                    /* Nothing */
                    break;
                case 0x36:
                    /* Nothing */
                    break;
                case 0x37:
                    /* Nothing */
                    break;
                case 0x38:
                    /* Nothing */
                    break;
                case 0x39:
                    /* Nothing */
                    break;
                case 0x3A:
                    /* Nothing */
                    break;
                case 0x3B:
                    /* Nothing */
                    break;
                case 0x3C:
                    /* Nothing */
                    break;
                case 0x3D: /* Debug Move */
                    switch((op >> 21) & 0x1F) {
                        case 0x00: /* MFDR */
                            if(RT(op) == 0)
                                break;
                            regs.gpr[RT(op)] = regs.dbgr[RD(op)];
                            break;
                        case 0x01:
                            /* Nothing */
                            break;
                        case 0x02:
                            /* Nothing */
                            break;
                        case 0x03:
                            /* Nothing */
                            break;
                        case 0x04: /* MTDR */
                            regs.dbgr[RD(op)] = regs.gpr[RT(op)];
                            break;
                        case 0x05:
                            /* Nothing */
                            break;
                        case 0x06:
                            /* Nothing */
                            break;
                        case 0x07:
                            /* Nothing */
                            break;
                        case 0x08:
                            /* Nothing */
                            break;
                        case 0x09:
                            /* Nothing */
                            break;
                        case 0x0A:
                            /* Nothing */
                            break;
                        case 0x0B:
                            /* Nothing */
                            break;
                        case 0x0C:
                            /* Nothing */
                            break;
                        case 0x0D:
                            /* Nothing */
                            break;
                        case 0x0E:
                            /* Nothing */
                            break;
                        case 0x0F:
                            /* Nothing */
                            break;
                        case 0x10:
                            /* Nothing */
                            break;
                        case 0x11:
                            /* Nothing */
                            break;
                        case 0x12:
                            /* Nothing */
                            break;
                        case 0x13:
                            /* Nothing */
                            break;
                        case 0x14:
                            /* Nothing */
                            break;
                        case 0x15:
                            /* Nothing */
                            break;
                        case 0x16:
                            /* Nothing */
                            break;
                        case 0x17:
                            /* Nothing */
                            break;
                        case 0x18:
                            /* Nothing */
                            break;
                        case 0x19:
                            /* Nothing */
                            break;
                        case 0x1A:
                            /* Nothing */
                            break;
                        case 0x1B:
                            /* Nothing */
                            break;
                        case 0x1C:
                            /* Nothing */
                            break;
                        case 0x1D:
                            /* Nothing */
                            break;
                        case 0x1E:
                            /* Nothing */
                            break;
                        case 0x1F:
                            /* Nothing */
                            break;
                    }
                    break;
                case 0x3E: /* DRET */
                    pc = npc = regs.dbgr[DEPC];
                    break;
                case 0x3F:
                    /* Nothing */
                    break;
            }
            break;
        case 0x1F: /* SPECIAL3 */
            switch(op & 0x3F) {
                case 0x00: /* EXT */
                    if(RT(op) == 0)
                        break;
                    regs.gpr[RT(op)] = (regs.gpr[RS(op)] >> SA(op)) & ((1 << (RD(op)+1)) - 1);
                    break;
                case 0x01:
                    /* Nothing */
                    break;
                case 0x02:
                    /* Nothing */
                    break;
                case 0x03:
                    /* Nothing */
                    break;
                case 0x04: { /* INS */
                    if(RT(op) == 0)
                        break;
                    uint32_t mask = ((1 << RD(op)) - 1) << SA(op);
                    uint32_t shift = (RD(op) + 1) - SA(op);
                    regs.gpr[RT(op)] &= ~mask;
                    regs.gpr[RT(op)] |= (regs.gpr[RS(op)] << shift) & mask;
                    break; }
                case 0x05:
                    /* Nothing */
                    break;
                case 0x06:
                    /* Nothing */
                    break;
                case 0x07:
                    /* Nothing */
                    break;
                case 0x08:
                    /* Nothing */
                    break;
                case 0x09:
                    /* Nothing */
                    break;
                case 0x0A:
                    /* Nothing */
                    break;
                case 0x0B:
                    /* Nothing */
                    break;
                case 0x0C:
                    /* Nothing */
                    break;
                case 0x0D:
                    /* Nothing */
                    break;
                case 0x0E:
                    /* Nothing */
                    break;
                case 0x0F:
                    /* Nothing */
                    break;
                case 0x10:
                    /* Nothing */
                    break;
                case 0x11:
                    /* Nothing */
                    break;
                case 0x12:
                    /* Nothing */
                    break;
                case 0x13:
                    /* Nothing */
                    break;
                case 0x14:
                    /* Nothing */
                    break;
                case 0x15:
                    /* Nothing */
                    break;
                case 0x16:
                    /* Nothing */
                    break;
                case 0x17:
                    /* Nothing */
                    break;
                case 0x18:
                    /* Nothing */
                    break;
                case 0x19:
                    /* Nothing */
                    break;
                case 0x1A:
                    /* Nothing */
                    break;
                case 0x1B:
                    /* Nothing */
                    break;
                case 0x1C:
                    /* Nothing */
                    break;
                case 0x1D:
                    /* Nothing */
                    break;
                case 0x1E:
                    /* Nothing */
                    break;
                case 0x1F:
                    /* Nothing */
                    break;
                case 0x20: /* BSHFL */
                    switch((op >> 6) & 0x1F) {
                        case 0x00:
                            /* Nothing */
                            break;
                        case 0x01:
                            /* Nothing */
                            break;
                        case 0x02: { /* WSBH */
                            // 01 02 03 04
                            // 02 01 04 03
                            if(RD(op) == 0)
                                break;
                            uint8_t b[4];
                            uint32_t o;
                            b[0] = (regs.gpr[RT(op)] >> 24) & 0xFF;
                            b[1] = (regs.gpr[RT(op)] >> 16) & 0xFF;
                            b[2] = (regs.gpr[RT(op)] >>  8) & 0xFF;
                            b[3] = (regs.gpr[RT(op)] >>  0) & 0xFF;
                            o = (b[0] << 16) |
                                (b[1] << 24) |
                                (b[2] <<  0) |
                                (b[3] <<  8);
                            regs.gpr[RD(op)] = o;
                            break; }
                        case 0x03: { /* WSBW */
                            // 01 02 03 04
                            // 04 03 02 01
                            if(RD(op) == 0)
                                break;
                            uint8_t b[4];
                            uint32_t o;
                            b[0] = (regs.gpr[RT(op)] >> 24) & 0xFF;
                            b[1] = (regs.gpr[RT(op)] >> 16) & 0xFF;
                            b[2] = (regs.gpr[RT(op)] >>  8) & 0xFF;
                            b[3] = (regs.gpr[RT(op)] >>  0) & 0xFF;
                            o = (b[0] <<  0) |
                                (b[1] <<  8) |
                                (b[2] << 16) |
                                (b[3] << 24);
                            regs.gpr[RD(op)] = o;
                            break; }
                        case 0x04:
                            /* Nothing */
                            break;
                        case 0x05:
                            /* Nothing */
                            break;
                        case 0x06:
                            /* Nothing */
                            break;
                        case 0x07:
                            /* Nothing */
                            break;
                        case 0x08:
                            /* Nothing */
                            break;
                        case 0x09:
                            /* Nothing */
                            break;
                        case 0x0A:
                            /* Nothing */
                            break;
                        case 0x0B:
                            /* Nothing */
                            break;
                        case 0x0C:
                            /* Nothing */
                            break;
                        case 0x0D:
                            /* Nothing */
                            break;
                        case 0x0E:
                            /* Nothing */
                            break;
                        case 0x0F:
                            /* Nothing */
                            break;
                        case 0x10: /* SEB */
                            if(RD(op) == 0)
                                break;
                            regs.gpr[RD(op)] = (int8_t)regs.gpr[RT(op)];
                            break;
                        case 0x11:
                            /* Nothing */
                            break;
                        case 0x12:
                            /* Nothing */
                            break;
                        case 0x13:
                            /* Nothing */
                            break;
                        case 0x14: { /* BITREV */
                            if(RD(op) == 0)
                                break;
                            uint32_t x = regs.gpr[RT(op)];
                            uint32_t o;
                            o = ((x & 0x80000000) >> 31) |
                                ((x & 0x40000000) >> 29) |
                                ((x & 0x20000000) >> 27) |
                                ((x & 0x10000000) >> 25) |
                                ((x & 0x08000000) >> 23) |
                                ((x & 0x04000000) >> 21) |
                                ((x & 0x02000000) >> 19) |
                                ((x & 0x01000000) >> 17) |
                                ((x & 0x00800000) >> 15) |
                                ((x & 0x00400000) >> 13) |
                                ((x & 0x00200000) >> 11) |
                                ((x & 0x00100000) >>  9) |
                                ((x & 0x00080000) >>  7) |
                                ((x & 0x00040000) >>  5) |
                                ((x & 0x00020000) >>  3) |
                                ((x & 0x00010000) >>  1) |
                                ((x & 0x00008000) <<  1) |
                                ((x & 0x00004000) <<  3) |
                                ((x & 0x00002000) <<  5) |
                                ((x & 0x00001000) <<  7) |
                                ((x & 0x00000800) <<  9) |
                                ((x & 0x00000400) << 11) |
                                ((x & 0x00000200) << 13) |
                                ((x & 0x00000100) << 15) |
                                ((x & 0x00000080) << 17) |
                                ((x & 0x00000040) << 19) |
                                ((x & 0x00000020) << 21) |
                                ((x & 0x00000010) << 23) |
                                ((x & 0x00000008) << 25) |
                                ((x & 0x00000004) << 27) |
                                ((x & 0x00000002) << 29) |
                                ((x & 0x00000001) << 31);
                            regs.gpr[RD(op)] = o;
                            break; }
                        case 0x15:
                            /* Nothing */
                            break;
                        case 0x16:
                            /* Nothing */
                            break;
                        case 0x17:
                            /* Nothing */
                            break;
                        case 0x18: /* SEH */
                            if(RD(op) == 0)
                                break;
                            regs.gpr[RD(op)] = (int16_t)regs.gpr[RT(op)];
                            break;
                        case 0x19:
                            /* Nothing */
                            break;
                        case 0x1A:
                            /* Nothing */
                            break;
                        case 0x1B:
                            /* Nothing */
                            break;
                        case 0x1C:
                            /* Nothing */
                            break;
                        case 0x1D:
                            /* Nothing */
                            break;
                        case 0x1E:
                            /* Nothing */
                            break;
                        case 0x1F:
                            /* Nothing */
                            break;
                    }
                    break;
            }
            break;
        case 0x20: { /* LB */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op);
            int8_t val = read8(this, addr);
            if(RT(op) == 0)
                break;
            regs.gpr[RT(op)] = val;
            break;}
        case 0x21: { /* LH */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op);
            if(addr & 1) {
                exception(EXC_ADDRESS_LOAD);
                break;
            }
            int16_t val = read16(this, addr);
            if(RT(op) == 0)
                break;
            regs.gpr[RT(op)] = val;
            break;}
        case 0x22: { /* LWL */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op);
            uint32_t val = read32(this, addr);
            if(RT(op) == 0)
                break;
            uint32_t off = (addr & 3) << 3;
            uint32_t omask = (1 << off) - 1;
            regs.gpr[RT(op)] &= omask;
            regs.gpr[RT(op)] |= val & ~omask;
            break;}
        case 0x23: { /* LW */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op);
            if(addr & 3) {
                exception(EXC_ADDRESS_LOAD);
                break;
            }
            uint32_t val = read32(this, addr);
            if(RT(op) == 0)
                break;
            regs.gpr[RT(op)] = val;
            break;}
        case 0x24: { /* LBU */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op);
            uint8_t val = read8(this, addr);
            if(RT(op) == 0)
                break;
            regs.gpr[RT(op)] = val;
            break;}
        case 0x25: { /* LHU */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op);
            if(addr & 1) {
                exception(EXC_ADDRESS_LOAD);
                break;
            }
            uint16_t val = read16(this, addr);
            if(RT(op) == 0)
                break;
            regs.gpr[RT(op)] = val;
            break;}
        case 0x26: { /* LWR */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op) - 3;
            uint32_t val = read32(this, addr - 3);
            if(RT(op) == 0)
                break;
            uint32_t off = (addr & 3) << 3;
            uint32_t omask = (1 << off) - 1;
            regs.gpr[RT(op)] &= ~omask;
            regs.gpr[RT(op)] |= val & omask;
            break;}
        case 0x27:
            exception(EXC_RESERVED_INSTR);
            break;
        case 0x28: { /* SB */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op);
            write8(this, addr, regs.gpr[RT(op)]);
            break;}
        case 0x29: { /* SH */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op);
            if(addr & 1) {
                exception(EXC_ADDRESS_STORE);
                break;
            }
            write16(this, addr, regs.gpr[RT(op)]);
            break;}
        case 0x2A: { /* SWL */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op);
            uint32_t off = ((4 - (addr & 3)) & 3) << 3;
            uint32_t omask = (1 << off) - 1;
            uint32_t val = regs.gpr[RT(op)] & omask;
            val |= read32(this, addr) & ~omask;
            write32(this, addr, val);
            break;}
        case 0x2B: { /* SW */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op);
            if(addr & 3) {
                exception(EXC_ADDRESS_STORE);
                break;
            }
            write32(this, addr, regs.gpr[RT(op)]);
            break;}
        case 0x2C:
            exception(EXC_RESERVED_INSTR);
            break;
        case 0x2D:
            exception(EXC_RESERVED_INSTR);
            break;
        case 0x2E: { /* SWR */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op) - 3;
            uint32_t off = (addr & 3) << 3;
            uint32_t omask = (1 << off) - 1;
            uint32_t val = regs.gpr[RT(op)] & omask;
            val |= read32(this, addr - 3) & ~omask;
            write32(this, addr - 3, val);
            break;}
        case 0x2F: { /* CACHE */
            if(cop0->alive == false) {
                exception(EXC_UNUSABLE_COP, 0, 0);
                break;
            }
            // Not implemented for speed.
            uint32_t vaddr = regs.gpr[RS(op)] + IMM_S(op);
            //uint32_t paddr = virt_to_phys(vaddr);
            int oper = RT(op); // operation
            Doovde::log(Doovde::LogLevel::WARNING, "Cache instruction: Vaddr: %08X Op: %02X", vaddr, oper);
            break;}
        case 0x30: { /* LL */
            uint32_t vaddr = regs.gpr[RS(op)] + IMM_S(op);
            if(vaddr & 3) {
                exception(EXC_ADDRESS_LOAD);
                break;
            }
            uint32_t val = read32(this, vaddr);
            cop0->mtc(this, 0, Doovde::Cop0::REG_LLADDR, virt_to_phys(this, vaddr));
            ll = true;
            if(RT(op) == 0)
                break;
            regs.gpr[RT(op)] = val;
            break;}
        case 0x31: { /* LWC1 */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op);
            if(addr & 3) {
                exception(EXC_ADDRESS_LOAD);
                break;
            }
            if(cop1->alive == false) {
                exception(EXC_UNUSABLE_COP, 0, 1);
                break;
            }
            uint32_t val = read32(this, addr);
            cop1->lwc(this, op, val);
            break;}
        case 0x32: { /* LWC2 */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op);
            if(addr & 3) {
                exception(EXC_ADDRESS_LOAD);
                break;
            }
            if(cop2->alive == false) {
                exception(EXC_UNUSABLE_COP, 0, 2);
                break;
            }
            uint32_t val = read32(this, addr);
            cop2->lwc(this, op, val);
            break;}
        case 0x33: { /* PREF */
            /* TODO: Reserved Instruction exception */
            // Not implemented for speed.
            uint32_t vaddr = regs.gpr[RS(op)] + IMM_S(op);
            //uint32_t paddr = virt_to_phys(vaddr);
            int hint = RT(op); // hint
            Doovde::log(Doovde::LogLevel::WARNING, "PREF instruction: Vaddr: %08X Hint: %02X", vaddr, hint);
            break;}
        case 0x34: /* VFPU4 */
            switch((op >> 21) & 0x1F) {
                case 0x00: /* VFPU4_0 */
                    switch((op >> 16) & 0x1F) {
                        case 0x00: /* VMOV */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec1);
                            break;
                        case 0x01: /* VABS */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = fabs(cop2->vec1[i]);
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x02: /* VNEG */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = -cop2->vec1[i];
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x03: /* VIDT */
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = ((int)(VD(op) & 3) == i) ? 1.0f: 0.0f;
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x04: /* VSAT0 */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 0; i < VSIZE(op); i++) {
                                if(cop2->vec1[i] > 1.0f)
                                    cop2->vec3[i] = 1.0f;
                                else if(cop2->vec1[i] < 0.0f)
                                    cop2->vec3[i] = 0.0f;
                                else
                                    cop2->vec3[i] = cop2->vec1[i];
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x05: /* VSAT1 */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 0; i < VSIZE(op); i++) {
                                if(cop2->vec1[i] > 1.0f)
                                    cop2->vec3[i] = 1.0f;
                                else if(cop2->vec1[i] < -1.0f)
                                    cop2->vec3[i] = -1.0f;
                                else
                                    cop2->vec3[i] = cop2->vec1[i];
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x06: /* VZERO */
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = 0.0f;
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x07: /* VONE */
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = 1.0f;
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x08:
                            /* Nothing */
                            break;
                        case 0x09:
                            /* Nothing */
                            break;
                        case 0x0A:
                            /* Nothing */
                            break;
                        case 0x0B:
                            /* Nothing */
                            break;
                        case 0x0C:
                            /* Nothing */
                            break;
                        case 0x0D:
                            /* Nothing */
                            break;
                        case 0x0E:
                            /* Nothing */
                            break;
                        case 0x0F:
                            /* Nothing */
                            break;
                        case 0x10: /* VRCP */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = 1.0f / cop2->vec1[i];
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x11: /* VRSQ */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = 1.0f / sqrtf(cop2->vec1[i]);
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x12: /* VSIN */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = sinf(cop2->constants[8] * cop2->vec1[i]);
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x13: /* VCOS */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = cosf(cop2->constants[8] * cop2->vec1[i]);
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x14: /* VEXP2 */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = powf(2.0f, cop2->vec1[i]);
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x15: /* VLOG2 */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = log2f(cop2->vec1[i]);
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x16: /* VSQRT */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = sqrtf(cop2->vec1[i]);
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x17: /* VASIN */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = asinf(cop2->vec1[i]) * cop2->constants[5];
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x18: /* VNRCP */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = -(1.0f / cop2->vec1[i]);
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x19:
                            /* Nothing */
                            break;
                        case 0x1A: /* VNSIN */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = -sinf(cop2->constants[8] * cop2->vec1[i]);
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x1B:
                            /* Nothing */
                            break;
                        case 0x1C: /* VREXP2 */
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 0; i < VSIZE(op); i++) {
                                cop2->vec3[i] = 1.0f / powf(2.0f, cop2->vec1[i]);
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x1D:
                            /* Nothing */
                            break;
                        case 0x1E:
                            /* Nothing */
                            break;
                        case 0x1F:
                            /* Nothing */
                            break;
                    }
                    break;
                case 0x01: /* VFPU4_1 */
                    switch((op >> 16) & 0x1F) {
                        case 0x00:
                            /* Nothing */
                            break;
                        case 0x01:
                            /* Nothing */
                            break;
                        case 0x02:
                            /* Nothing */
                            break;
                        case 0x03:
                            /* Nothing */
                            break;
                        case 0x04:
                            /* Nothing */
                            break;
                        case 0x05:
                            /* Nothing */
                            break;
                        case 0x06:
                            /* Nothing */
                            break;
                        case 0x07:
                            /* Nothing */
                            break;
                        case 0x08:
                            /* Nothing */
                            break;
                        case 0x09:
                            /* Nothing */
                            break;
                        case 0x0A:
                            /* Nothing */
                            break;
                        case 0x0B:
                            /* Nothing */
                            break;
                        case 0x0C:
                            /* Nothing */
                            break;
                        case 0x0D:
                            /* Nothing */
                            break;
                        case 0x0E:
                            /* Nothing */
                            break;
                        case 0x0F:
                            /* Nothing */
                            break;
                        case 0x10:
                            /* Nothing */
                            break;
                        case 0x11:
                            /* Nothing */
                            break;
                        case 0x12:
                            /* Nothing */
                            break;
                        case 0x13:
                            /* Nothing */
                            break;
                        case 0x14:
                            /* Nothing */
                            break;
                        case 0x15:
                            /* Nothing */
                            break;
                        case 0x16:
                            /* Nothing */
                            break;
                        case 0x17:
                            /* Nothing */
                            break;
                        case 0x18:
                            /* Nothing */
                            break;
                        case 0x19:
                            /* Nothing */
                            break;
                        case 0x1A:
                            /* Nothing */
                            break;
                        case 0x1B:
                            /* Nothing */
                            break;
                        case 0x1C: /* VI2UC */
                            if(VSIZE(op) != 4) {
                                Doovde::log(Doovde::LogLevel::WARNING, "VI2UC not quad.");
                            }
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            cop2->vec3i[0] =    (((cop2->vec1i[0] < 0) ? 0 : (cop2->vec1i[0] >> 23)) <<  0) |
                                    (((cop2->vec1i[1] < 0) ? 0 : (cop2->vec1i[1] >> 23)) <<  8) |
                                    (((cop2->vec1i[2] < 0) ? 0 : (cop2->vec1i[2] >> 23)) << 16) |
                                    (((cop2->vec1i[3] < 0) ? 0 : (cop2->vec1i[3] >> 23)) << 24);
                            cop2->saveDstVector(1, VD(op), cop2->vec3);
                            break;
                        case 0x1D: /* VI2C */
                            if(VSIZE(op) != 4) {
                                Doovde::log(Doovde::LogLevel::WARNING, "VI2C not quad.");
                            }
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            cop2->vec3i[0] =    ((cop2->vec1i[0] >> 24) <<  0) |
                                    ((cop2->vec1i[1] >> 24) <<  8) |
                                    ((cop2->vec1i[2] >> 24) << 16) |
                                    ((cop2->vec1i[3] >> 24) << 24);
                            cop2->saveDstVector(1, VD(op), cop2->vec3);
                            break;
                        case 0x1E: /* VI2US */
                            if((VSIZE(op) != 2) && (VSIZE(op) != 4)) {
                                Doovde::log(Doovde::LogLevel::WARNING, "VI2US not quad or pair.");
                            }
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            cop2->vec3i[0] =    (((cop2->vec1i[0] < 0) ? 0 : (cop2->vec1i[0] >> 15)) <<  0) |
                                    (((cop2->vec1i[1] < 0) ? 0 : (cop2->vec1i[1] >> 15)) << 16);
                            if(VSIZE(op) == 4) {
                                cop2->vec3i[1] =    (((cop2->vec1i[2] < 0) ? 0 : (cop2->vec1i[2] >> 15)) <<  0) |
                                        (((cop2->vec1i[3] < 0) ? 0 : (cop2->vec1i[3] >> 15)) << 16);
                            }
                            cop2->saveDstVector(VSIZE(op) >> 1, VD(op), cop2->vec3);
                            break;
                        case 0x1F: /* VI2S */
                            if((VSIZE(op) != 2) && (VSIZE(op) != 4)) {
                                Doovde::log(Doovde::LogLevel::WARNING, "VI2S not quad or pair.");
                            }
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            cop2->vec3i[0] =    ((cop2->vec1i[0] >> 16) <<  0) |
                                    ((cop2->vec1i[1] >> 16) << 16);
                            if(VSIZE(op) == 4) {
                                cop2->vec3i[1] =    ((cop2->vec1i[2] >> 16) <<  0) |
                                        ((cop2->vec1i[3] >> 16) << 16);
                            }
                            cop2->saveDstVector(VSIZE(op) >> 1, VD(op), cop2->vec3);
                            break;
                    }
                    break;
                case 0x02: /* VFPU4_2 */
                    switch((op >> 16) & 0x1F) {
                        case 0x00:
                            /* Nothing */
                            break;
                        case 0x01:
                            /* Nothing */
                            break;
                        case 0x02: /* VBFY1 */
                            if((VSIZE(op) != 2) && (VSIZE(op) != 4)) {
                                Doovde::log(Doovde::LogLevel::WARNING, "VBFY1 not quad or pair.");
                            }
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            cop2->vec3[0] = cop2->vec1[0] + cop2->vec1[1];
                            cop2->vec3[1] = cop2->vec1[0] - cop2->vec1[1];
                            if(VSIZE(op) == 4) {
                                cop2->vec3[2] = cop2->vec1[2] + cop2->vec1[3];
                                cop2->vec3[3] = cop2->vec1[2] - cop2->vec1[3];
                            }
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x03: /* VBFY2 */
                            if(VSIZE(op) != 4) {
                                Doovde::log(Doovde::LogLevel::WARNING, "VBFY1 not quad.");
                            }
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            cop2->vec3[0] = cop2->vec1[0] + cop2->vec1[2];
                            cop2->vec3[1] = cop2->vec1[1] + cop2->vec1[3];
                            cop2->vec3[2] = cop2->vec1[0] - cop2->vec1[2];
                            cop2->vec3[3] = cop2->vec1[1] - cop2->vec1[3];
                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break;
                        case 0x04:
                            /* Nothing */
                            break;
                        case 0x05:
                            /* Nothing */
                            break;
                        case 0x06: { /* VFAD */
                            if(VSIZE(op) == 1) {
                                Doovde::log(Doovde::LogLevel::WARNING, "VFAD is single.");
                            }
                            cop2->loadSrcVector(VSIZE(op), VS(op));
                            for(int i = 1; i < VSIZE(op); i++) {
                                cop2->vec1[0] += cop2->vec1[i];
                            }
                            cop2->saveDstVector(1, VD(op), cop2->vec1);
                            break; }
                        case 0x07:
                            /* Nothing */
                            break;
                        case 0x08:
                            /* Nothing */
                            break;
                        case 0x09:
                            /* Nothing */
                            break;
                        case 0x0A:
                            /* Nothing */
                            break;
                        case 0x0B:
                            /* Nothing */
                            break;
                        case 0x0C:
                            /* Nothing */
                            break;
                        case 0x0D:
                            /* Nothing */
                            break;
                        case 0x0E:
                            /* Nothing */
                            break;
                        case 0x0F:
                            /* Nothing */
                            break;
                        case 0x10:
                            /* Nothing */
                            break;
                        case 0x11:
                            /* Nothing */
                            break;
                        case 0x12:
                            /* Nothing */
                            break;
                        case 0x13:
                            /* Nothing */
                            break;
                        case 0x14:
                            /* Nothing */
                            break;
                        case 0x15:
                            /* Nothing */
                            break;
                        case 0x16:
                            /* Nothing */
                            break;
                        case 0x17:
                            /* Nothing */
                            break;
                        case 0x18:
                            /* Nothing */
                            break;
                        case 0x19:
                            /* Nothing */
                            break;
                        case 0x1A:
                            /* Nothing */
                            break;
                        case 0x1B:
                            /* Nothing */
                            break;
                        case 0x1C:
                            /* Nothing */
                            break;
                        case 0x1D:
                            /* Nothing */
                            break;
                        case 0x1E:
                            /* Nothing */
                            break;
                        case 0x1F:
                            /* Nothing */
                            break;
                    }
                    break;
                case 0x03: /* VCST */
                    for(int i = 0; i < VSIZE(op); i++) {
                        cop2->vec3[i] = cop2->constants[IMM5(op)];
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                    break;
                case 0x04:
                    /* Nothing */
                    break;
                case 0x05:
                    /* Nothing */
                    break;
                case 0x06:
                    /* Nothing */
                    break;
                case 0x07:
                    /* Nothing */
                    break;
                case 0x08:
                    /* Nothing */
                    break;
                case 0x09:
                    /* Nothing */
                    break;
                case 0x0A:
                    /* Nothing */
                    break;
                case 0x0B:
                    /* Nothing */
                    break;
                case 0x0C:
                    /* Nothing */
                    break;
                case 0x0D:
                    /* Nothing */
                    break;
                case 0x0E:
                    /* Nothing */
                    break;
                case 0x0F:
                    /* Nothing */
                    break;
                case 0x10: /* VF2IN */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        float val = cop2->vec1[i] * pow(2, IMM5(op));
                        cop2->vec3i[i] = lroundf(val);
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                    break;
                case 0x11: /* VF2IZ */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        float val = cop2->vec1[i] * pow(2, IMM5(op));
                        if(cop2->vec1[i] >= 0)
                            cop2->vec3i[i] = floor(val);
                        else
                            cop2->vec3i[i] = ceil(val);
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                    break;
                case 0x12: /* VF2IU */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        float val = cop2->vec1[i] * pow(2, IMM5(op));
                        cop2->vec3i[i] = ceil(val);
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                    break;
                case 0x13: /* VF2ID */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        float val = cop2->vec1[i] * pow(2, IMM5(op));
                        cop2->vec3i[i] = floor(val);
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                    break;
                case 0x14: /* VI2F */
                    cop2->loadSrcVector(VSIZE(op), VS(op));
                    for(int i = 0; i < VSIZE(op); i++) {
                        cop2->vec3[i] = (float)cop2->vec1i[i] * pow(2, -IMM5(op));
                    }
                    cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                    break;
                case 0x15: /* VCMOV */
                    /* TODO */
                    if((IMM5(op) & 0x18) == 0) {
                        //VCMOVT
                    }else if((IMM5(op) & 0x18) == 8) {
                        //VCMOVF
                    }
                    break;
                case 0x16:
                    /* Nothing */
                    break;
                case 0x17:
                    /* Nothing */
                    break;
                case 0x18:
                    /* Nothing */
                    break;
                case 0x19:
                    /* Nothing */
                    break;
                case 0x1A:
                    /* Nothing */
                    break;
                case 0x1B:
                    /* Nothing */
                    break;
                case 0x1C:
                    /* Nothing */
                    break;
                case 0x1D:
                    /* Nothing */
                    break;
                case 0x1E:
                    /* Nothing */
                    break;
                case 0x1F:
                    /* Nothing */
                    break;
            }
            break;
        case 0x35: { /* ULV.Q [buggy on PSP100x?] */
            int16_t addroff = ((op >> 2) & 0x3FFF) << 2;
            uint32_t addr = addroff + regs.gpr[RS(op)];
            int vt = (op >> 16) & 0x1F;
            int mtx = (vt >> 2) & 7;
            int j = vt & 3;
            if(op & 1) {
                for(int i = 0; i < 4; i++)
                    cop2->regs[cop2->getVfprIndex(mtx, i, j)] = read32(this, addr + (i * 4));
            }else{
                for(int i = 0; i < 4; i++)
                    cop2->regs[cop2->getVfprIndex(mtx, j, i)] = read32(this, addr + (i * 4));
            }
            break; }
        case 0x36: { /* LV.Q */
            int16_t addroff = ((op >> 2) & 0x3FFF) << 2;
            uint32_t addr = (addroff + regs.gpr[RS(op)]) & ~0xF;
            int vt = (op >> 16) & 0x1F;
            int mtx = (vt >> 2) & 7;
            int j = vt & 3;
            if(op & 1) {
                for(int i = 0; i < 4; i++)
                    cop2->regs[cop2->getVfprIndex(mtx, i, j)] = read32(this, addr + (i * 4));
            }else{
                for(int i = 0; i < 4; i++)
                    cop2->regs[cop2->getVfprIndex(mtx, j, i)] = read32(this, addr + (i * 4));
            }
            break; }
        case 0x37: /* VFPU5 */
            switch((op >> 24) & 0x3) {
                case 0x0: /* VPFXS */
                    cop2->pfxs.setValue(op & 0xFFFFFF);
                    cop2->pfxs.enabled = true;
                    break;
                case 0x1: /* VPFXT */
                    cop2->pfxt.setValue(op & 0xFFFFFF);
                    cop2->pfxt.enabled = true;
                    break;
                case 0x2: /* VPFXD */
                    cop2->pfxd.setValue(op & 0xFFFFFF);
                    cop2->pfxd.enabled = true;
                    break;
                case 0x3: /* VIIM/VFIM */
                    if(op & (1 << 23)) { /* VFIM */
                        cop2->vec3[0] = cop2->halfFloatToFloat(IMM_S(op));
                    }else{ /* VIIM */
                        cop2->vec3[0] = IMM_S(op);
                    }
                    cop2->saveDstVector(1, VD(op), cop2->vec3);
                    break;
            }
            break;
        case 0x38: { /* SC */
            uint32_t vaddr = regs.gpr[RS(op)] + IMM_S(op);
            if(vaddr & 3) {
                exception(EXC_ADDRESS_STORE);
                break;
            }
            if(ll) {
                write32(this, vaddr, regs.gpr[RT(op)]);
            }
            if(RT(op) == 0)
                break;
            regs.gpr[RT(op)] = ll ? 1 : 0;
            break;}
        case 0x39: { /* SWC1 */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op);
            if(addr & 3) {
                exception(EXC_ADDRESS_STORE);
                break;
            }
            if(cop1->alive == false) {
                exception(EXC_UNUSABLE_COP, 0, 1);
                break;
            }
            uint32_t val = cop1->swc(this, op);
            write32(this, addr, val);
            break;}
        case 0x3A: { /* SWC2 */
            uint32_t addr = regs.gpr[RS(op)] + IMM_S(op);
            if(addr & 3) {
                exception(EXC_ADDRESS_STORE);
                break;
            }
            if(cop2->alive == false) {
                exception(EXC_UNUSABLE_COP, 0, 2);
                break;
            }
            uint32_t val = cop2->swc(this, op);
            write32(this, addr, val);
            break;}
        case 0x3B:
            exception(EXC_RESERVED_INSTR);
            break;
        case 0x3C: /* VFPU6 */
            switch((op >> 23) & 0x7) {
                case 0: /* VMMUL */
                    if(VSIZE(op) == 1) {
                        Doovde::log(Doovde::LogLevel::WARNING, "VMMUL single.");
                    }
                    for(int i = 0; i < VSIZE(op); i++) {
                        cop2->loadTgtVector(VSIZE(op), VT(op) + i);
                        for(int l = 0; l < VSIZE(op); l++) {
                            cop2->loadSrcVector(VSIZE(op), VS(op) + l);
                            float dot = 0;
                            for(int k = 0; k < VSIZE(op); k++) {
                                dot += cop2->vec1[k] * cop2->vec2[k];
                            }
                            cop2->vec3[l] = dot;
                        }
                        cop2->saveDstVector(VSIZE(op), VD(op) + i, cop2->vec3);
                    }
                    break;
                case 1: /* VHTFM2/VTFM2 */
                    if(op & 0x80) { /* VTFM2 */
                        cop2->loadTgtVector(2, VT(op));
                        cop2->loadSrcVector(2, VS(op));
                        cop2->vec3[0] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]);
                        cop2->loadSrcVector(2, VS(op) + 1);
                        cop2->vec3[1] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]);
                        cop2->saveDstVector(2, VD(op), cop2->vec3);
                    }else{ /* VHTFM2 */
                        cop2->loadTgtVector(1, VT(op));
                        cop2->loadSrcVector(2, VS(op));
                        cop2->vec3[0] = (cop2->vec1[0] * cop2->vec2[0]) + cop2->vec1[1];
                        cop2->loadSrcVector(2, VS(op) + 1);
                        cop2->vec3[1] = (cop2->vec1[0] * cop2->vec2[0]) + cop2->vec1[1];
                        cop2->saveDstVector(2, VD(op), cop2->vec3);
                    }
                    break;
                case 2: /* VHTFM3/VTFM3 */
                    if(op & 0x80) { /* VTFM3 */
                        cop2->loadTgtVector(3, VT(op));
                        cop2->loadSrcVector(3, VS(op));
                        cop2->vec3[0] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]) + (cop2->vec1[2] * cop2->vec2[2]);
                        cop2->loadSrcVector(3, VS(op) + 1);
                        cop2->vec3[1] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]) + (cop2->vec1[2] * cop2->vec2[2]);
                        cop2->loadSrcVector(3, VS(op) + 2);
                        cop2->vec3[2] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]) + (cop2->vec1[2] * cop2->vec2[2]);
                        cop2->saveDstVector(3, VD(op), cop2->vec3);
                    }else{ /* VHTFM3 */
                        cop2->loadTgtVector(2, VT(op));
                        cop2->loadSrcVector(3, VS(op));
                        cop2->vec3[0] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]) + cop2->vec1[2];
                        cop2->loadSrcVector(3, VS(op) + 1);
                        cop2->vec3[1] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]) + cop2->vec1[2];
                        cop2->loadSrcVector(3, VS(op) + 2);
                        cop2->vec3[2] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]) + cop2->vec1[2];
                        cop2->saveDstVector(3, VD(op), cop2->vec3);
                    }
                    break;
                case 3: /* VHTFM4/VTFM4 */
                    if(op & 0x80) { /* VTFM4 */
                        cop2->loadTgtVector(4, VT(op));
                        cop2->loadSrcVector(4, VS(op));
                        cop2->vec3[0] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]) + (cop2->vec1[2] * cop2->vec2[2]) + (cop2->vec1[3] * cop2->vec2[3]);
                        cop2->loadSrcVector(4, VS(op) + 1);
                        cop2->vec3[1] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]) + (cop2->vec1[2] * cop2->vec2[2]) + (cop2->vec1[3] * cop2->vec2[3]);
                        cop2->loadSrcVector(4, VS(op) + 2);
                        cop2->vec3[2] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]) + (cop2->vec1[2] * cop2->vec2[2]) + (cop2->vec1[3] * cop2->vec2[3]);
                        cop2->loadSrcVector(4, VS(op) + 3);
                        cop2->vec3[3] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]) + (cop2->vec1[2] * cop2->vec2[2]) + (cop2->vec1[3] * cop2->vec2[3]);
                        cop2->saveDstVector(4, VD(op), cop2->vec3);
                    }else{ /* VHTFM4 */
                        cop2->loadTgtVector(3, VT(op));
                        cop2->loadSrcVector(4, VS(op));
                        cop2->vec3[0] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]) + (cop2->vec1[2] * cop2->vec2[2]) + cop2->vec1[3];
                        cop2->loadSrcVector(4, VS(op) + 1);
                        cop2->vec3[1] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]) + (cop2->vec1[2] * cop2->vec2[2]) + cop2->vec1[3];
                        cop2->loadSrcVector(4, VS(op) + 2);
                        cop2->vec3[2] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]) + (cop2->vec1[2] * cop2->vec2[2]) + cop2->vec1[3];
                        cop2->loadSrcVector(4, VS(op) + 3);
                        cop2->vec3[3] = (cop2->vec1[0] * cop2->vec2[0]) + (cop2->vec1[1] * cop2->vec2[1]) + (cop2->vec1[2] * cop2->vec2[2]) + cop2->vec1[3];
                        cop2->saveDstVector(4, VD(op), cop2->vec3);
                    }
                    break;
                case 4: /* VMSCL */
                    for(int l = 0; l < VSIZE(op); l++) {
                        cop2->loadSrcVector(VSIZE(op), VS(op) + l);
                        cop2->loadTgtVector(VSIZE(op), VT(op));
                        for(int i = 0; i < VSIZE(op); i++) {
                            cop2->vec1[i] *= cop2->vec2[0];
                        }
                        cop2->saveDstVector(VSIZE(op), VD(op) + l, cop2->vec1);
                    }
                    break;
                case 5: /* VCRSP/VQMUL */
                    if(op & 0x80) { /* VQMUL */
                        cop2->loadSrcVector(4, VS(op));
                        cop2->loadTgtVector(4, VT(op));
                        cop2->vec3[0] = ( cop2->vec1[0] * cop2->vec2[3]) + (cop2->vec1[1] * cop2->vec2[2]) - (cop2->vec1[2] * cop2->vec2[1]) + (cop2->vec1[3] * cop2->vec2[0]);
                        cop2->vec3[1] = (-cop2->vec1[0] * cop2->vec2[2]) + (cop2->vec1[1] * cop2->vec2[3]) + (cop2->vec1[2] * cop2->vec2[0]) + (cop2->vec1[3] * cop2->vec2[1]);
                        cop2->vec3[2] = ( cop2->vec1[0] * cop2->vec2[1]) - (cop2->vec1[1] * cop2->vec2[0]) + (cop2->vec1[2] * cop2->vec2[3]) + (cop2->vec1[3] * cop2->vec2[2]);
                        cop2->vec3[3] = (-cop2->vec1[0] * cop2->vec2[0]) - (cop2->vec1[1] * cop2->vec2[1]) - (cop2->vec1[2] * cop2->vec2[2]) + (cop2->vec1[3] * cop2->vec2[3]);
                        cop2->saveDstVector(4, VD(op), cop2->vec3);
                    }else{ /* VCRSP */
                        cop2->loadSrcVector(3, VS(op));
                        cop2->loadTgtVector(3, VT(op));
                        cop2->vec3[0] = (cop2->vec1[1] * cop2->vec2[2]) - (cop2->vec1[2] * cop2->vec2[1]);
                        cop2->vec3[1] = (cop2->vec1[2] * cop2->vec2[0]) - (cop2->vec1[0] * cop2->vec2[2]);
                        cop2->vec3[2] = (cop2->vec1[0] * cop2->vec2[1]) - (cop2->vec1[1] * cop2->vec2[0]);
                        cop2->saveDstVector(3, VD(op), cop2->vec3);
                    }
                    break;
                case 6:
                    /* Nothing */
                    break;
                case 7: /* VMMOV/VMZERO/VMIDT/VMONE/VROT */
                    switch((op >> 21) & 0x3) {
                        case 0:
                            switch((op >> 16) & 0x7) {
                                case 0x0: /* VMMOV */
                                    for(int i = 0; i < VSIZE(op); i++) {
                                        cop2->loadSrcVector(VSIZE(op), VS(op) + i);
                                        cop2->saveDstVector(VSIZE(op), VD(op) + i, cop2->vec1);
                                    }
                                    break;
                                case 0x1:
                                    /* Nothing */
                                    break;
                                case 0x2:
                                    /* Nothing */
                                    break;
                                case 0x3: /* VMIDT */
                                    for(int l = 0; l < VSIZE(op); l++) {
                                        for(int i = 0; i < VSIZE(op); i++) {
                                            cop2->vec3[i] = ((int)((VD(op) + l) & 3) == i) ? 1.0f: 0.0f;
                                        }
                                        cop2->saveDstVector(VSIZE(op), VD(op) + l, cop2->vec3);
                                    }
                                    break;
                                case 0x4:
                                    /* Nothing */
                                    break;
                                case 0x5:
                                    /* Nothing */
                                    break;
                                case 0x6: /* VMZERO */
                                    for(int i = 0; i < VSIZE(op); i++) {
                                        cop2->vec3[i] = 0.0f;
                                    }
                                    for(int i = 0; i < VSIZE(op); i++) {
                                        cop2->saveDstVector(VSIZE(op), VD(op) + i, cop2->vec3);
                                    }
                                    break;
                                case 0x7: /* VMONE */
                                    for(int i = 0; i < VSIZE(op); i++) {
                                        cop2->vec3[i] = 1.0f;
                                    }
                                    for(int i = 0; i < VSIZE(op); i++) {
                                        cop2->saveDstVector(VSIZE(op), VD(op) + i, cop2->vec3);
                                    }
                                    break;
                            }
                            break;
                        case 1: { /* VROT */
                            cop2->loadSrcVector(1, VS(op));
                            double angle = cop2->constants[8] * cop2->vec1[0];
                            double cosa = cos(angle);
                            double sina = sin(angle);

                            int sinidx = (IMM5(op) >> 2) & 3;
                            int cosidx = (IMM5(op) >> 0) & 3;
                            if(IMM5(op) >> 4) {
                                sina = -sina;
                            }
                            if(sinidx == cosidx) {
                                for(int i = 0; i < VSIZE(op); i++)
                                    cop2->vec3[i] = (float)sina;
                            }else{
                                for(int i = 0; i < VSIZE(op); i++)
                                    cop2->vec3[i] = 0.0f;
                                cop2->vec3[sinidx] = (float)sina;
                            }
                            cop2->vec3[cosidx] = (float)cosa;

                            cop2->saveDstVector(VSIZE(op), VD(op), cop2->vec3);
                            break; }
                        case 2:
                            /* Nothing */
                            break;
                        case 3:
                            /* Nothing */
                            break;
                    }
                    break;
            }
            break;
        case 0x3D: { /* USV.Q */
            int16_t addroff = ((op >> 2) & 0x3FFF) << 2;
            uint32_t addr = addroff + regs.gpr[RS(op)];
            int vt = (op >> 16) & 0x1F;
            int mtx = (vt >> 2) & 7;
            int j = vt & 3;
            if(op & 1) {
                for(int i = 0; i < 4; i++)
                    write32(this, addr + (i * 4), cop2->regs[cop2->getVfprIndex(mtx, i, j)]);
            }else{
                for(int i = 0; i < 4; i++)
                    write32(this, addr + (i * 4), cop2->regs[cop2->getVfprIndex(mtx, j, i)]);
            }
            break; }
        case 0x3E: { /* SV.Q */
            int16_t addroff = ((op >> 2) & 0x3FFF) << 2;
            uint32_t addr = (addroff + regs.gpr[RS(op)]) & ~0xF;
            uint32_t vt = (op >> 16) & 0x1F;
            int mtx = (vt >> 2) & 7;
            int j = vt & 3;
            if(op & 1) {
                for(int i = 0; i < 4; i++)
                    write32(this, addr + (i * 4), cop2->regs[cop2->getVfprIndex(mtx, i, j)]);
            }else{
                for(int i = 0; i < 4; i++)
                    write32(this, addr + (i * 4), cop2->regs[cop2->getVfprIndex(mtx, j, i)]);
            }
            break; }
        case 0x3F: /* VFPU7 */
            /* Figure this out... */
            if(op & 1) { /* VFLUSH */
                /* Not implemented for speed */
                Doovde::log(Doovde::LogLevel::WARNING, "VFLUSH instruction: Opcode: %08X", op);
            }else if(op & 0x20) { /* VNOP */
            }else{ /* VSYNC */
                /* Not implemented for speed */
                Doovde::log(Doovde::LogLevel::WARNING, "VSYNC instruction: Opcode: %08X", op);
            }
            break;
    }

    /* Count/Compare handling */
    countflip ^= 1;
    if(countflip) {
        cop0->regs[Doovde::Cop0::REG_COUNT]++;
        if(cop0->regs[Doovde::Cop0::REG_COUNT] == cop0->regs[Doovde::Cop0::REG_COMPARE])
            cop0->regs[Doovde::Cop0::REG_CAUSE] |= (1 << 15);
    }

    /* Random handling */
    if(cop0->regs[Doovde::Cop0::REG_RANDOM] == cop0->regs[Doovde::Cop0::REG_WIRED])
        cop0->regs[Doovde::Cop0::REG_RANDOM] = 47;
    else
        cop0->regs[Doovde::Cop0::REG_RANDOM]--;

    /* IRQ handling */
    if((cop0->regs[Doovde::Cop0::REG_CAUSE] & 0xFF00) && cop0->getInterruptEnable() &&
       (cop0->getExceptionLevel() == 0) && (cop0->getErrorLevel() == 0))
        exception(0);

    return cycles;
}

void Doovde::Allegrex::disassemble(uint32_t addr, char *outbuf, int len)
{
    char opbuf[16] = "";
    char argbuf[4][16] = { "", "", "", "" };
    char tmpbuf[96] = "";
    char regnames[32][8] = {
            "$zero", "$at",   "$v0",  "$v1",  "$a0",  "$a1",  "$a2",  "$a3",
            "$t0",   "$t1",   "$t2",  "$t3",  "$t4",  "$t5",  "$t6",  "$t7",
            "$s0",   "$s1",   "$s2",  "$s3",  "$s4",  "$s5",  "$s6",  "$s7",
            "$t8",   "$t9",   "$k0",  "$k1",  "$gp",  "$sp",  "$fp",  "$ra"};
    char vregnames[128][8];
    for(int i=0; i < 128; i++)
        sprintf(vregnames[i], "$vf%d", i);
    char vregsuffix[4][6] = { ".S", ".P", ".T", ".Q" };
    char vfpucc[16][6] = { "FL", "EQ", "LT", "LE",
                           "TR", "NE", "GE", "GT",
                           "EZ", "EN", "EI", "ES",
                           "NZ", "NN", "NI", "NS" };
    char vregconsts[32][16];
    for(int i=0; i < 32; i++)
        sprintf(vregconsts[i], "VCONST%d", i);
    char dregnames[32][8];
    for(int i=0; i < 32; i++)
        sprintf(dregnames[i], "$d%d", i);

    fetching = true;
    op = read32(this, addr);
    fetching = false;
    switch((op >> 26) & 0x3F) {
        case 0x00: /* SPECIAL */
            switch(op & 0x3F) {
                case 0x00: /* SLL */
                    sprintf(opbuf, "SLL");
                    if(RD(op) == 0) {
                        sprintf(opbuf, "NOP");
                        break;
                    }
DISAS_RD_RT_SA:

                    sprintf(argbuf[0], "%s", regnames[RD(op)]);
                    sprintf(argbuf[1], "%s", regnames[RT(op)]);
                    sprintf(argbuf[2], "%d", SA(op));
                    break;
                case 0x01:
DISAS_RESV:

                    sprintf(opbuf, "RESERVED");
                    sprintf(argbuf[0], "0x%08X", op);
                    break;
                case 0x02: /* SRL/ROR */
                    if(RS(op) == 1) /* ROR */
                        sprintf(opbuf, "ROR");
                    else /* SRL */
                        sprintf(opbuf, "SRL");
                    goto DISAS_RD_RT_SA;
                case 0x03: /* SRA */
                    sprintf(opbuf, "SRA");
                    goto DISAS_RD_RT_SA;
                case 0x04: /* SLLV */
                    sprintf(opbuf, "SLLV");
DISAS_RD_RS_RT:

                    sprintf(argbuf[0], "%s", regnames[RD(op)]);
                    sprintf(argbuf[1], "%s", regnames[RS(op)]);
                    sprintf(argbuf[2], "%s", regnames[RT(op)]);
                    break;
                case 0x05:
                    goto DISAS_RESV;
                case 0x06: /* SRLV/RORV */
                    if(SA(op) == 1) /* RORV */
                        sprintf(opbuf, "RORV");
                    else
                        sprintf(opbuf, "SRLV");
                    goto DISAS_RD_RS_RT;
                case 0x07: /* SRAV */
                    sprintf(opbuf, "SRAV");
                    goto DISAS_RD_RS_RT;
                case 0x08: /* JR */
                    sprintf(opbuf, "JR");
DISAS_RS:

                    sprintf(argbuf[0], "%s", regnames[RS(op)]);
                    break;
                case 0x09: /* JALR */
                    sprintf(opbuf, "JALR");
                    goto DISAS_RS;
                case 0x0A:
                    goto DISAS_RESV;
                case 0x0B:
                    goto DISAS_RESV;
                case 0x0C: /* SYSCALL */
                    sprintf(opbuf, "SYSCALL");

                    sprintf(argbuf[0], "0x%05X", (op >> 6) & 0xFFFFF);
                    break;
                case 0x0D: /* BREAK */
                    sprintf(opbuf, "BREAK");

                    sprintf(argbuf[0], "0x%05X", (op >> 6) & 0xFFFFF);
                    break;
                case 0x0F: /* SYNC */
                    sprintf(opbuf, "SYNC");
                    break;
                case 0x10: /* MFHI */
                    sprintf(opbuf, "MFHI");
DISAS_RD:

                    sprintf(argbuf[0], "%s", regnames[RD(op)]);
                    break;
                case 0x11: /* MTHI */
                    sprintf(opbuf, "MTHI");
                    goto DISAS_RS;
                case 0x12: /* MFLO */
                    sprintf(opbuf, "MFLO");
                    goto DISAS_RD;
                case 0x13: /* MTLO */
                    sprintf(opbuf, "MTLO");
                    goto DISAS_RS;
                case 0x14:
                    goto DISAS_RESV;
                case 0x15:
                    goto DISAS_RESV;
                case 0x16: /* CLZ */
                    sprintf(opbuf, "CLZ");
DISAS_RD_RS:

                    sprintf(argbuf[0], "%s", regnames[RD(op)]);
                    sprintf(argbuf[1], "%s", regnames[RS(op)]);
                    break;
                case 0x17: /* CLO */
                    sprintf(opbuf, "CLZ");
                    goto DISAS_RD_RS;
                case 0x18: /* MULT */
                    sprintf(opbuf, "MULT");
DISAS_RS_RT:

                    sprintf(argbuf[0], "%s", regnames[RS(op)]);
                    sprintf(argbuf[1], "%s", regnames[RT(op)]);
                    break;
                case 0x19: /* MULTU */
                    sprintf(opbuf, "MULTU");
                    goto DISAS_RS_RT;
                case 0x1A: /* DIV */
                    sprintf(opbuf, "DIV");
                    goto DISAS_RS_RT;
                case 0x1B: /* DIVU */
                    sprintf(opbuf, "DIVU");
                    goto DISAS_RS_RT;
                case 0x1C: /* MADD */
                    sprintf(opbuf, "MADD");
                    goto DISAS_RS_RT;
                case 0x1D: /* MADDU */
                    sprintf(opbuf, "MADDU");
                    goto DISAS_RS_RT;
                case 0x1E:
                    goto DISAS_RESV;
                case 0x1F:
                    goto DISAS_RESV;
                case 0x20: /* ADD */
                    sprintf(opbuf, "ADD");
                    goto DISAS_RD_RS_RT;
                case 0x21: /* ADDU */
                    sprintf(opbuf, "ADDU");
                    goto DISAS_RD_RS_RT;
                case 0x22: /* SUB */
                    sprintf(opbuf, "SUB");
                    goto DISAS_RD_RS_RT;
                case 0x23: /* SUBU */
                    sprintf(opbuf, "SUBU");
                    goto DISAS_RD_RS_RT;
                case 0x24: /* AND */
                    sprintf(opbuf, "AND");
                    goto DISAS_RD_RS_RT;
                case 0x25: /* OR */
                    sprintf(opbuf, "OR");
                    goto DISAS_RD_RS_RT;
                case 0x26: /* XOR */
                    sprintf(opbuf, "XOR");
                    goto DISAS_RD_RS_RT;
                case 0x27: /* NOR */
                    sprintf(opbuf, "NOR");
                    goto DISAS_RD_RS_RT;
                case 0x28:
                    goto DISAS_RESV;
                case 0x29:
                    goto DISAS_RESV;
                case 0x2A: /* SLT */
                    sprintf(opbuf, "SLT");
                    goto DISAS_RD_RS_RT;
                case 0x2B: /* SLTU */
                    sprintf(opbuf, "SLTU");
                    goto DISAS_RD_RS_RT;
                case 0x2C: /* MAX */
                    sprintf(opbuf, "MAX");
                    goto DISAS_RD_RS_RT;
                case 0x2D: /* MIN */
                    sprintf(opbuf, "MIN");
                    goto DISAS_RD_RS_RT;
                case 0x2E: /* MSUB */
                    sprintf(opbuf, "MSUB");
                    goto DISAS_RS_RT;
                case 0x2F: /* MSUBU */
                    sprintf(opbuf, "MSUBU");
                    goto DISAS_RS_RT;
                case 0x30: /* TGE */
                    sprintf(opbuf, "TGE");
                    goto DISAS_RS_RT;
                case 0x31: /* TGEU */
                    sprintf(opbuf, "TGEU");
                    goto DISAS_RS_RT;
                case 0x32: /* TLT */
                    sprintf(opbuf, "TLT");
                    goto DISAS_RS_RT;
                case 0x33: /* TLTU */
                    sprintf(opbuf, "TLTU");
                    goto DISAS_RS_RT;
                case 0x34: /* TEQ */
                    sprintf(opbuf, "TEQ");
                    goto DISAS_RS_RT;
                case 0x35:
                    goto DISAS_RESV;
                case 0x36: /* TNE */
                    sprintf(opbuf, "TNE");
                    goto DISAS_RS_RT;
                case 0x37:
                    goto DISAS_RESV;
                case 0x38:
                    goto DISAS_RESV;
                case 0x39:
                    goto DISAS_RESV;
                case 0x3A:
                    goto DISAS_RESV;
                case 0x3B:
                    goto DISAS_RESV;
                case 0x3C:
                    goto DISAS_RESV;
                case 0x3D:
                    goto DISAS_RESV;
                case 0x3E:
                    goto DISAS_RESV;
                case 0x3F:
                    goto DISAS_RESV;
            }
            break;
        case 0x01: /* REGIMM */
            switch((op >> 16) & 0x1F) {
                case 0x00: /* BLTZ */
                    sprintf(opbuf, "BLTZ");
DISAS_RS_OFFS16:

                    sprintf(argbuf[0], "%s", regnames[RS(op)]);
                    sprintf(argbuf[1], "0x%08X", addr + (IMM_S(op) << 2) + 4);
                    break;
                case 0x01: /* BGEZ */
                    sprintf(opbuf, "BGEZ");
                    goto DISAS_RS_OFFS16;
                case 0x02: /* BLTZL */
                    sprintf(opbuf, "BLTZL");
                    goto DISAS_RS_OFFS16;
                case 0x03: /* BGEZL */
                    sprintf(opbuf, "BGEZL");
                    goto DISAS_RS_OFFS16;
                case 0x04:
                    goto DISAS_RESV;
                case 0x05:
                    goto DISAS_RESV;
                case 0x06:
                    goto DISAS_RESV;
                case 0x07:
                    goto DISAS_RESV;
                case 0x08: /* TGEI */
                    sprintf(opbuf, "TGEI");
DISAS_RS_SIMM16:

                    sprintf(argbuf[0], "%s", regnames[RS(op)]);
                    sprintf(argbuf[1], "%s0x%04X", IMM_S(op) < 0 ? "-" : "", abs(IMM_S(op)));
                    break;
                case 0x09: /* TGEIU */
                    sprintf(opbuf, "TGEIU");
DISAS_RS_UIMM16:

                    sprintf(argbuf[0], "%s", regnames[RS(op)]);
                    sprintf(argbuf[1], "0x%04X", IMM(op));
                    break;
                case 0x0A: /* TLTI */
                    sprintf(opbuf, "TLTI");
                    goto DISAS_RS_SIMM16;
                case 0x0B: /* TLTIU */
                    sprintf(opbuf, "TLTIU");
                    goto DISAS_RS_UIMM16;
                case 0x0C: /* TEQI */
                    sprintf(opbuf, "TEQI");
                    goto DISAS_RS_SIMM16;
                case 0x0E: /* TNEI */
                    sprintf(opbuf, "TNEI");
                    goto DISAS_RS_SIMM16;
                case 0x0F: /* Not invalid? */
                    /* TODO */
                    goto DISAS_RESV;
                case 0x10: /* BLTZAL */
                    sprintf(opbuf, "BLTZAL");
                    goto DISAS_RS_OFFS16;
                case 0x11: /* BGEZAL */
                    sprintf(opbuf, "BGEZAL");
                    goto DISAS_RS_OFFS16;
                case 0x12: /* BLTZALL */
                    sprintf(opbuf, "BLTZALL");
                    goto DISAS_RS_OFFS16;
                case 0x13: /* BGEZALL */
                    sprintf(opbuf, "BGEZALL");
                    goto DISAS_RS_OFFS16;
                case 0x14:
                    goto DISAS_RESV;
                case 0x15:
                    goto DISAS_RESV;
                case 0x16:
                    goto DISAS_RESV;
                case 0x17:
                    goto DISAS_RESV;
                case 0x18:
                    goto DISAS_RESV;
                case 0x19:
                    goto DISAS_RESV;
                case 0x1A:
                    goto DISAS_RESV;
                case 0x1B:
                    goto DISAS_RESV;
                case 0x1C:
                    goto DISAS_RESV;
                case 0x1D:
                    goto DISAS_RESV;
                case 0x1E:
                    goto DISAS_RESV;
                case 0x1F:
                    goto DISAS_RESV;
            }
            break;
        case 0x02: /* J */
            sprintf(opbuf, "J");
DISAS_TARGET:

            sprintf(argbuf[0], "0x%08X", (addr+4 & 0xF0000000) | (TARGET(op) << 2));
            break;
        case 0x03: /* JAL */
            sprintf(opbuf, "JAL");
            goto DISAS_TARGET;
        case 0x04: /* BEQ */
            sprintf(opbuf, "BEQ");
DISAS_RS_RT_OFFS16:

            sprintf(argbuf[0], "%s", regnames[RS(op)]);
            sprintf(argbuf[1], "%s", regnames[RT(op)]);
            sprintf(argbuf[2], "0x%08X", addr+4 + (IMM_S(op) << 2));
            break;
        case 0x05: /* BNE */
            sprintf(opbuf, "BNE");
            goto DISAS_RS_RT_OFFS16;
        case 0x06: /* BLEZ */
            sprintf(opbuf, "BLEZ");
            goto DISAS_RS_OFFS16;
        case 0x07: /* BGTZ */
            sprintf(opbuf, "BGTZ");
            goto DISAS_RS_OFFS16;
        case 0x08: /* ADDI */
            sprintf(opbuf, "ADDI");
DISAS_RT_RS_SIMM16:

            sprintf(argbuf[0], "%s", regnames[RT(op)]);
            sprintf(argbuf[1], "%s", regnames[RS(op)]);
            sprintf(argbuf[2], "%s0x%04X", IMM_S(op) < 0 ? "-" : "", abs(IMM_S(op)));
            break;
        case 0x09: /* ADDIU */
            sprintf(opbuf, "ADDIU");
            goto DISAS_RT_RS_SIMM16;
DISAS_RT_RS_UIMM16:

            sprintf(argbuf[0], "%s", regnames[RT(op)]);
            sprintf(argbuf[1], "%s", regnames[RS(op)]);
            sprintf(argbuf[2], "0x%04X", IMM(op));
            break;
        case 0x0A: /* SLTI */
            sprintf(opbuf, "SLTI");
            goto DISAS_RT_RS_SIMM16;
        case 0x0B: /* SLTIU */
            sprintf(opbuf, "SLTIU");
            goto DISAS_RT_RS_UIMM16;
            break;
        case 0x0C: /* ANDI */
            sprintf(opbuf, "ANDI");
            goto DISAS_RT_RS_UIMM16;
        case 0x0D: /* ORI */
            sprintf(opbuf, "ORI");
            goto DISAS_RT_RS_UIMM16;
        case 0x0E: /* XORI */
            sprintf(opbuf, "XORI");
            goto DISAS_RT_RS_UIMM16;
        case 0x0F: /* LUI */
            sprintf(opbuf, "LUI");
//DISAS_RT_UIMM16:

            sprintf(argbuf[0], "%s", regnames[RT(op)]);
            sprintf(argbuf[1], "0x%04X", IMM(op));
            break;
        case 0x10: /* COP0 */
            switch((op >> 21) & 0x1F) {
                case 0x00: /* MFPC/MFPS/MFC0 */
                    if(RD(op) == 0x19) {
                        if(op & 1)
                            sprintf(opbuf, "MFPC");
                        else
                            sprintf(opbuf, "MFPS");
                        sprintf(argbuf[1], "$%d", (op >> 1) & 0x1F);
                    }else{
                        sprintf(opbuf, "MFC0");
                        sprintf(argbuf[1], "$%d", RD(op));
                    }

                    sprintf(argbuf[0], "%s", regnames[RT(op)]);
                    break;
                case 0x01:
DISAS_UNK:
                    sprintf(opbuf, ".dc.w");

                    sprintf(argbuf[0], "0x%08X", op);
                    break;
                case 0x02: /* CFC0 */
                    sprintf(opbuf, "CFC0");

                    sprintf(argbuf[0], "%s", regnames[RT(op)]);
                    sprintf(argbuf[1], "$%d", RD(op));
                    break;
                case 0x03:
                    goto DISAS_UNK;
                case 0x04: /* MTPC/MTPS/MTC0 */
                    if(RD(op) == 0x19) {
                        if(op & 1)
                            sprintf(opbuf, "MTPC");
                        else
                            sprintf(opbuf, "MTPS");
                        sprintf(argbuf[1], "$%d", (op >> 1) & 0x1F);
                    }else{
                        sprintf(opbuf, "MTC0");
                        sprintf(argbuf[1], "$%d", RD(op));
                    }

                    sprintf(argbuf[0], "%s", regnames[RT(op)]);
                    break;
                case 0x05:
                    goto DISAS_UNK;
                case 0x06: /* CTC0 */
                    sprintf(opbuf, "CTC0");

                    sprintf(argbuf[0], "%s", regnames[RT(op)]);
                    sprintf(argbuf[1], "$%d", RD(op));
                    break;
                case 0x07:
                    goto DISAS_UNK;
                case 0x08:
                    goto DISAS_UNK;
                case 0x09:
                    goto DISAS_UNK;
                case 0x0A:
                    goto DISAS_UNK;
                case 0x0B:
                    goto DISAS_UNK;
                case 0x0C:
                    goto DISAS_UNK;
                case 0x0D:
                    goto DISAS_UNK;
                case 0x0E:
                    goto DISAS_UNK;
                case 0x0F:
                    goto DISAS_UNK;
                case 0x10: case 0x11: case 0x12: case 0x13:
                case 0x14: case 0x15: case 0x16: case 0x17:
                case 0x18: case 0x19: case 0x1A: case 0x1B:
                case 0x1C: case 0x1D: case 0x1E: case 0x1F:
                    /* COFUN */
                    goto DISAS_UNK;
            }
            break;
        case 0x11: /* COP1 */
            if(cop1->alive == false) {
                goto DISAS_RESV;
            }
            cop1->copFuncDisas(this, op, opbuf, argbuf);
            break;
        case 0x12: /* COP2 */
            if(cop2->alive == false) {
                goto DISAS_RESV;
            }
            cop2->copFuncDisas(this, op, opbuf, argbuf);
            break;
        case 0x14: /* BEQL */
            sprintf(opbuf, "BEQL");
            goto DISAS_RS_RT_OFFS16;
        case 0x15: /* BNEL */
            sprintf(opbuf, "BNEL");
            goto DISAS_RS_RT_OFFS16;
        case 0x16: /* BLEZL */
            sprintf(opbuf, "BLEZL");
            goto DISAS_RS_OFFS16;
        case 0x17: /* BGTZL */
            sprintf(opbuf, "BGTZL");
            goto DISAS_RS_OFFS16;
        case 0x18: /* VFPU0 */
            switch((op >> 23) & 0x7) {
                case 0x0: /* VADD */
                    sprintf(opbuf, "VADD");
DISAS_VD_VT_VS:

                    sprintf(opbuf, "%s%s", opbuf, vregsuffix[VSIZE(op)]);
                    sprintf(argbuf[0], "%s", vregnames[VD(op)]);
                    sprintf(argbuf[1], "%s", vregnames[VT(op)]);
                    sprintf(argbuf[2], "%s", vregnames[VS(op)]);
                    break;
                case 0x1: /* VSUB */
                    sprintf(opbuf, "VSUB");
                    goto DISAS_VD_VT_VS;
                case 0x2: /* VSBN */
                    sprintf(opbuf, "VSBN");
                    goto DISAS_VD_VT_VS;
                case 0x03:
                    goto DISAS_UNK;
                case 0x04:
                    goto DISAS_UNK;
                case 0x05:
                    goto DISAS_UNK;
                case 0x06:
                    goto DISAS_UNK;
                case 0x7: /* VDIV */
                    sprintf(opbuf, "VDIV");
                    goto DISAS_VD_VT_VS;
            }
            break;
        case 0x19: /* VFPU1 */
            switch((op >> 23) & 0x7) {
                case 0x0: /* VMUL */
                    sprintf(opbuf, "VMUL");
                    goto DISAS_VD_VT_VS;
                case 0x1: /* VDOT */
                    sprintf(opbuf, "VDOT");
                    goto DISAS_VD_VT_VS;
                case 0x2: /* VSCL */
                    sprintf(opbuf, "VSCL");
                    goto DISAS_VD_VT_VS;
                case 0x4: /* VHDP */ /* Half Dot Product? */
                    sprintf(opbuf, "VHDP");
                    goto DISAS_VD_VT_VS;
                case 0x5: /* VCRS */
                    sprintf(opbuf, "VCRS");
                    goto DISAS_VD_VT_VS;
                case 0x6: /* VDET */
                    sprintf(opbuf, "VDET");
                    goto DISAS_VD_VT_VS;
                case 0x07:
                    goto DISAS_UNK;
            }
            break;
        case 0x1B: /* VFPU3 */
            switch((op >> 23) & 0x7) {
                case 0x0: /* VCMP */
                    sprintf(opbuf, "VCMP");

                    sprintf(opbuf, "%s%s", opbuf, vregsuffix[VSIZE(op)]);
                    sprintf(argbuf[0], "%s", vfpucc[op & 0xF]);
                    sprintf(argbuf[1], "%s", vregnames[VT(op)]);
                    sprintf(argbuf[2], "%s", vregnames[VS(op)]);
                case 0x1:
                    goto DISAS_UNK;
                case 0x2: /* VMIN */
                    sprintf(opbuf, "VMIN");
                    goto DISAS_VD_VT_VS;
                case 0x3: /* VMAX */
                    sprintf(opbuf, "VMAX");
                    goto DISAS_VD_VT_VS;
                case 0x4:
                    goto DISAS_UNK;
                case 0x5: /* VSCMP */
                    sprintf(opbuf, "VSCMP");
                    goto DISAS_VD_VT_VS;
                case 0x6: /* VSGE */
                    sprintf(opbuf, "VSGE");
                    goto DISAS_VD_VT_VS;
                case 0x7: /* VSLT */
                    sprintf(opbuf, "VSLT");
                    goto DISAS_VD_VT_VS;
            }
            break;
        case 0x1C: /* SPECIAL2 */
            switch(op & 0x3F) {
                case 0x00: /* HALT */
                    sprintf(opbuf, "HALT");
                    break;
                case 0x01:
                    goto DISAS_UNK;
                case 0x02:
                    goto DISAS_UNK;
                case 0x03:
                    goto DISAS_UNK;
                case 0x04:
                    goto DISAS_UNK;
                case 0x05:
                    goto DISAS_UNK;
                case 0x06:
                    goto DISAS_UNK;
                case 0x07:
                    goto DISAS_UNK;
                case 0x08:
                    goto DISAS_UNK;
                case 0x09:
                    goto DISAS_UNK;
                case 0x0A:
                    goto DISAS_UNK;
                case 0x0B:
                    goto DISAS_UNK;
                case 0x0C:
                    goto DISAS_UNK;
                case 0x0D:
                    goto DISAS_UNK;
                case 0x0E:
                    goto DISAS_UNK;
                case 0x0F:
                    goto DISAS_UNK;
                case 0x10:
                    goto DISAS_UNK;
                case 0x11:
                    goto DISAS_UNK;
                case 0x12:
                    goto DISAS_UNK;
                case 0x13:
                    goto DISAS_UNK;
                case 0x14:
                    goto DISAS_UNK;
                case 0x15:
                    goto DISAS_UNK;
                case 0x16:
                    goto DISAS_UNK;
                case 0x17:
                    goto DISAS_UNK;
                case 0x18:
                    goto DISAS_UNK;
                case 0x19:
                    goto DISAS_UNK;
                case 0x1A:
                    goto DISAS_UNK;
                case 0x1B:
                    goto DISAS_UNK;
                case 0x1C:
                    goto DISAS_UNK;
                case 0x1D:
                    goto DISAS_UNK;
                case 0x1E:
                    goto DISAS_UNK;
                case 0x1F:
                    goto DISAS_UNK;
                case 0x20:
                    goto DISAS_UNK;
                case 0x21:
                    goto DISAS_UNK;
                case 0x22:
                    goto DISAS_UNK;
                case 0x23:
                    goto DISAS_UNK;
                case 0x24: /* MFIC */
                    sprintf(opbuf, "MFIC");
DISAS_RT_RD:

                    sprintf(argbuf[0], "%s", regnames[RT(op)]);
                    sprintf(argbuf[1], "%s", regnames[RD(op)]);
                    break;
                case 0x25:
                    goto DISAS_UNK;
                case 0x26: /* MTIC */
                    sprintf(opbuf, "MTIC");
                    goto DISAS_RT_RD;
                case 0x27:
                    goto DISAS_UNK;
                case 0x28:
                    goto DISAS_UNK;
                case 0x29:
                    goto DISAS_UNK;
                case 0x2A:
                    goto DISAS_UNK;
                case 0x2B:
                    goto DISAS_UNK;
                case 0x2C:
                    goto DISAS_UNK;
                case 0x2D:
                    goto DISAS_UNK;
                case 0x2E:
                    goto DISAS_UNK;
                case 0x2F:
                    goto DISAS_UNK;
                case 0x30:
                    goto DISAS_UNK;
                case 0x31:
                    goto DISAS_UNK;
                case 0x32:
                    goto DISAS_UNK;
                case 0x33:
                    goto DISAS_UNK;
                case 0x34:
                    goto DISAS_UNK;
                case 0x35:
                    goto DISAS_UNK;
                case 0x36:
                    goto DISAS_UNK;
                case 0x37:
                    goto DISAS_UNK;
                case 0x38:
                    goto DISAS_UNK;
                case 0x39:
                    goto DISAS_UNK;
                case 0x3A:
                    goto DISAS_UNK;
                case 0x3B:
                    goto DISAS_UNK;
                case 0x3C:
                    goto DISAS_UNK;
                case 0x3D: /* Debug Move */
                    switch((op >> 21) & 0x1F) {
                        case 0x00: /* MFDR */

                            sprintf(opbuf, "MFDR");
                            sprintf(argbuf[0], "%s", regnames[RT(op)]);
                            sprintf(argbuf[1], "%s", dregnames[RD(op)]);
                            break;
                        case 0x01:
                            goto DISAS_UNK;
                        case 0x02:
                            goto DISAS_UNK;
                        case 0x03:
                            goto DISAS_UNK;
                        case 0x04: /* MTDR */

                            sprintf(opbuf, "MTDR");
                            sprintf(argbuf[0], "%s", regnames[RT(op)]);
                            sprintf(argbuf[1], "%s", dregnames[RD(op)]);
                            break;
                        case 0x05:
                            goto DISAS_UNK;
                        case 0x06:
                            goto DISAS_UNK;
                        case 0x07:
                            goto DISAS_UNK;
                        case 0x08:
                            goto DISAS_UNK;
                        case 0x09:
                            goto DISAS_UNK;
                        case 0x0A:
                            goto DISAS_UNK;
                        case 0x0B:
                            goto DISAS_UNK;
                        case 0x0C:
                            goto DISAS_UNK;
                        case 0x0D:
                            goto DISAS_UNK;
                        case 0x0E:
                            goto DISAS_UNK;
                        case 0x0F:
                            goto DISAS_UNK;
                        case 0x10:
                            goto DISAS_UNK;
                        case 0x11:
                            goto DISAS_UNK;
                        case 0x12:
                            goto DISAS_UNK;
                        case 0x13:
                            goto DISAS_UNK;
                        case 0x14:
                            goto DISAS_UNK;
                        case 0x15:
                            goto DISAS_UNK;
                        case 0x16:
                            goto DISAS_UNK;
                        case 0x17:
                            goto DISAS_UNK;
                        case 0x18:
                            goto DISAS_UNK;
                        case 0x19:
                            goto DISAS_UNK;
                        case 0x1A:
                            goto DISAS_UNK;
                        case 0x1B:
                            goto DISAS_UNK;
                        case 0x1C:
                            goto DISAS_UNK;
                        case 0x1D:
                            goto DISAS_UNK;
                        case 0x1E:
                            goto DISAS_UNK;
                        case 0x1F:
                            goto DISAS_UNK;
                    }
                    break;
                case 0x3E: /* DRET */
                    sprintf(opbuf, "DRET");
                    break;
                case 0x3F:
                    goto DISAS_UNK;
            }
            break;
        case 0x1F: /* SPECIAL3 */
            switch(op & 0x3F) {
                case 0x00: /* EXT */

                    sprintf(opbuf, "EXT");
                    sprintf(argbuf[0], "%s", regnames[RT(op)]);
                    sprintf(argbuf[1], "%s", regnames[RS(op)]);
                    sprintf(argbuf[2], "%d", SA(op));
                    sprintf(argbuf[3], "%d", RD(op) + 1);
                    break;
                case 0x01:
                    goto DISAS_UNK;
                case 0x02:
                    goto DISAS_UNK;
                case 0x03:
                    goto DISAS_UNK;
                case 0x04: /* INS */

                    sprintf(opbuf, "INS");
                    sprintf(argbuf[0], "%s", regnames[RT(op)]);
                    sprintf(argbuf[1], "%s", regnames[RS(op)]);
                    sprintf(argbuf[2], "%d", SA(op));
                    sprintf(argbuf[3], "%d", (RD(op) + 1) - SA(op));
                    break;
                case 0x05:
                    goto DISAS_UNK;
                case 0x06:
                    goto DISAS_UNK;
                case 0x07:
                    goto DISAS_UNK;
                case 0x08:
                    goto DISAS_UNK;
                case 0x09:
                    goto DISAS_UNK;
                case 0x0A:
                    goto DISAS_UNK;
                case 0x0B:
                    goto DISAS_UNK;
                case 0x0C:
                    goto DISAS_UNK;
                case 0x0D:
                    goto DISAS_UNK;
                case 0x0E:
                    goto DISAS_UNK;
                case 0x0F:
                    goto DISAS_UNK;
                case 0x10:
                    goto DISAS_UNK;
                case 0x11:
                    goto DISAS_UNK;
                case 0x12:
                    goto DISAS_UNK;
                case 0x13:
                    goto DISAS_UNK;
                case 0x14:
                    goto DISAS_UNK;
                case 0x15:
                    goto DISAS_UNK;
                case 0x16:
                    goto DISAS_UNK;
                case 0x17:
                    goto DISAS_UNK;
                case 0x18:
                    goto DISAS_UNK;
                case 0x19:
                    goto DISAS_UNK;
                case 0x1A:
                    goto DISAS_UNK;
                case 0x1B:
                    goto DISAS_UNK;
                case 0x1C:
                    goto DISAS_UNK;
                case 0x1D:
                    goto DISAS_UNK;
                case 0x1E:
                    goto DISAS_UNK;
                case 0x1F:
                    goto DISAS_UNK;
                case 0x20: /* BSHFL */
                    switch((op >> 6) & 0x1F) {
                        case 0x00:
                            goto DISAS_UNK;
                        case 0x01:
                            goto DISAS_UNK;
                        case 0x02: /* WSBH */
                            sprintf(opbuf, "WSBH");
DISAS_RD_RT:

                            sprintf(argbuf[0], "%s", regnames[RD(op)]);
                            sprintf(argbuf[1], "%s", regnames[RT(op)]);
                            break;
                        case 0x03: /* WSBW */
                            sprintf(opbuf, "WSBW");
                            goto DISAS_RD_RT;
                        case 0x04:
                            goto DISAS_UNK;
                        case 0x05:
                            goto DISAS_UNK;
                        case 0x06:
                            goto DISAS_UNK;
                        case 0x07:
                            goto DISAS_UNK;
                        case 0x08:
                            goto DISAS_UNK;
                        case 0x09:
                            goto DISAS_UNK;
                        case 0x0A:
                            goto DISAS_UNK;
                        case 0x0B:
                            goto DISAS_UNK;
                        case 0x0C:
                            goto DISAS_UNK;
                        case 0x0D:
                            goto DISAS_UNK;
                        case 0x0E:
                            goto DISAS_UNK;
                        case 0x0F:
                            goto DISAS_UNK;
                        case 0x10: /* SEB */
                            sprintf(opbuf, "SEB");
                            goto DISAS_RD_RT;
                        case 0x11:
                            goto DISAS_UNK;
                        case 0x12:
                            goto DISAS_UNK;
                        case 0x13:
                            goto DISAS_UNK;
                        case 0x14: /* BITREV */
                            sprintf(opbuf, "BITREV");
                            goto DISAS_RD_RT;
                            break;
                        case 0x15:
                            goto DISAS_UNK;
                        case 0x16:
                            goto DISAS_UNK;
                        case 0x17:
                            goto DISAS_UNK;
                        case 0x18: /* SEH */
                            sprintf(opbuf, "SEH");
                            goto DISAS_RD_RT;
                        case 0x19:
                            goto DISAS_UNK;
                        case 0x1A:
                            goto DISAS_UNK;
                        case 0x1B:
                            goto DISAS_UNK;
                        case 0x1C:
                            goto DISAS_UNK;
                        case 0x1D:
                            goto DISAS_UNK;
                        case 0x1E:
                            goto DISAS_UNK;
                        case 0x1F:
                            goto DISAS_UNK;
                    }
                    break;
            }
            break;
        case 0x20: /* LB */
            sprintf(opbuf, "LB");
DISAS_RT_OFFS16_BASE:

            sprintf(argbuf[0], "%s", regnames[RT(op)]);
            sprintf(argbuf[1], "%s0x%04X(%s)", IMM_S(op) < 0 ? "-" : "", abs(IMM_S(op)), regnames[RS(op)]);
            break;
        case 0x21: /* LH */
            sprintf(opbuf, "LH");
            goto DISAS_RT_OFFS16_BASE;
        case 0x22: /* LWL */
            sprintf(opbuf, "LWL");
            goto DISAS_RT_OFFS16_BASE;
        case 0x23: /* LW */
            sprintf(opbuf, "LW");
            goto DISAS_RT_OFFS16_BASE;
        case 0x24: /* LBU */
            sprintf(opbuf, "LBU");
            goto DISAS_RT_OFFS16_BASE;
        case 0x25: /* LHU */
            sprintf(opbuf, "LHU");
            goto DISAS_RT_OFFS16_BASE;
        case 0x26: /* LWR */
            sprintf(opbuf, "LWR");
            goto DISAS_RT_OFFS16_BASE;
        case 0x27:
            goto DISAS_RESV;
        case 0x28: /* SB */
            sprintf(opbuf, "SB");
            goto DISAS_RT_OFFS16_BASE;
        case 0x29: /* SH */
            sprintf(opbuf, "SH");
            goto DISAS_RT_OFFS16_BASE;
        case 0x2A: /* SWL */
            sprintf(opbuf, "SWL");
            goto DISAS_RT_OFFS16_BASE;
        case 0x2B: /* SW */
            sprintf(opbuf, "SW");
            goto DISAS_RT_OFFS16_BASE;
        case 0x2C:
            goto DISAS_RESV;
        case 0x2D:
            goto DISAS_RESV;
        case 0x2E: /* SWR */
            sprintf(opbuf, "SWR");
            goto DISAS_RT_OFFS16_BASE;
        case 0x2F: /* CACHE */
            sprintf(opbuf, "CACHE");

            sprintf(argbuf[0], "%d", RT(op));
            sprintf(argbuf[1], "%s0x%04X(%s)", IMM_S(op) < 0 ? "-" : "", abs(IMM_S(op)), regnames[RS(op)]);
            break;
        case 0x30: /* LL */
            sprintf(opbuf, "LL");
            goto DISAS_RT_OFFS16_BASE;
        case 0x31: /* LWC1 */
            sprintf(opbuf, "LWC1");
            goto DISAS_RT_OFFS16_BASE;
        case 0x32: /* LWC2 */
            sprintf(opbuf, "LWC2");
            goto DISAS_RT_OFFS16_BASE;
        case 0x33: /* PREF */
            sprintf(opbuf, "PREF");

            sprintf(argbuf[0], "%d", RT(op));
            sprintf(argbuf[1], "%s0x%04X(%s)", IMM_S(op) < 0 ? "-" : "", abs(IMM_S(op)), regnames[RS(op)]);
            break;
        case 0x34: /* VFPU4 */
            switch((op >> 21) & 0x1F) {
                case 0x00: /* VFPU4_0 */
                    switch((op >> 16) & 0x1F) {
                        case 0x00: /* VMOV */
                            sprintf(opbuf, "VMOV");
DISAS_VD_VS:

                            sprintf(opbuf, "%s%s", opbuf, vregsuffix[VSIZE(op)]);
                            sprintf(argbuf[0], "%s", vregnames[VD(op)]);
                            sprintf(argbuf[1], "%s", vregnames[VS(op)]);
                            break;
                        case 0x01: /* VABS */
                            sprintf(opbuf, "VMOV");
                            goto DISAS_VD_VS;
                        case 0x02: /* VNEG */
                            sprintf(opbuf, "VNEG");
                            goto DISAS_VD_VS;
                        case 0x03: /* VIDT */
                            sprintf(opbuf, "VIDT");
DISAS_VD:

                            sprintf(opbuf, "%s%s", opbuf, vregsuffix[VSIZE(op)]);
                            sprintf(argbuf[0], "%s", vregnames[VD(op)]);
                            break;
                        case 0x04: /* VSAT0 */
                            sprintf(opbuf, "VSAT0");
                            goto DISAS_VD_VS;
                        case 0x05: /* VSAT1 */
                            sprintf(opbuf, "VSAT1");
                            goto DISAS_VD_VS;
                        case 0x06: /* VZERO */
                            sprintf(opbuf, "VZERO");
                            goto DISAS_VD;
                        case 0x07: /* VONE */
                            sprintf(opbuf, "VONE");
                            goto DISAS_VD;
                        case 0x08:
                            goto DISAS_UNK;
                        case 0x09:
                            goto DISAS_UNK;
                        case 0x0A:
                            goto DISAS_UNK;
                        case 0x0B:
                            goto DISAS_UNK;
                        case 0x0C:
                            goto DISAS_UNK;
                        case 0x0D:
                            goto DISAS_UNK;
                        case 0x0E:
                            goto DISAS_UNK;
                        case 0x0F:
                            goto DISAS_UNK;
                        case 0x10: /* VRCP */
                            sprintf(opbuf, "VRCP");
                            goto DISAS_VD_VS;
                        case 0x11: /* VRSQ */
                            sprintf(opbuf, "VRSQ");
                            goto DISAS_VD_VS;
                        case 0x12: /* VSIN */
                            sprintf(opbuf, "VSIN");
                            goto DISAS_VD_VS;
                        case 0x13: /* VCOS */
                            sprintf(opbuf, "VCOS");
                            goto DISAS_VD_VS;
                        case 0x14: /* VEXP2 */
                            sprintf(opbuf, "VEXP2");
                            goto DISAS_VD_VS;
                        case 0x15: /* VLOG2 */
                            sprintf(opbuf, "VLOG2");
                            goto DISAS_VD_VS;
                        case 0x16: /* VSQRT */
                            sprintf(opbuf, "VSQRT");
                            goto DISAS_VD_VS;
                        case 0x17: /* VASIN */
                            sprintf(opbuf, "VASIN");
                            goto DISAS_VD_VS;
                        case 0x18: /* VNRCP */
                            sprintf(opbuf, "VNRCP");
                            goto DISAS_VD_VS;
                        case 0x19:
                            goto DISAS_UNK;
                        case 0x1A: /* VNSIN */
                            sprintf(opbuf, "VNSIN");
                            goto DISAS_VD_VS;
                        case 0x1B:
                            goto DISAS_UNK;
                        case 0x1C: /* VREXP2 */
                            sprintf(opbuf, "VREXP2");
                            goto DISAS_VD_VS;
                        case 0x1D:
                            goto DISAS_UNK;
                        case 0x1E:
                            goto DISAS_UNK;
                        case 0x1F:
                            goto DISAS_UNK;
                    }
                    break;
                case 0x01: /* VFPU4_1 */
                    switch((op >> 16) & 0x1F) {
                        case 0x00:
                            goto DISAS_UNK;
                        case 0x01:
                            goto DISAS_UNK;
                        case 0x02:
                            goto DISAS_UNK;
                        case 0x03:
                            goto DISAS_UNK;
                        case 0x04:
                            goto DISAS_UNK;
                        case 0x05:
                            goto DISAS_UNK;
                        case 0x06:
                            goto DISAS_UNK;
                        case 0x07:
                            goto DISAS_UNK;
                        case 0x08:
                            goto DISAS_UNK;
                        case 0x09:
                            goto DISAS_UNK;
                        case 0x0A:
                            goto DISAS_UNK;
                        case 0x0B:
                            goto DISAS_UNK;
                        case 0x0C:
                            goto DISAS_UNK;
                        case 0x0D:
                            goto DISAS_UNK;
                        case 0x0E:
                            goto DISAS_UNK;
                        case 0x0F:
                            goto DISAS_UNK;
                        case 0x10:
                            goto DISAS_UNK;
                        case 0x11:
                            goto DISAS_UNK;
                        case 0x12:
                            goto DISAS_UNK;
                        case 0x13:
                            goto DISAS_UNK;
                        case 0x14:
                            goto DISAS_UNK;
                        case 0x15:
                            goto DISAS_UNK;
                        case 0x16:
                            goto DISAS_UNK;
                        case 0x17:
                            goto DISAS_UNK;
                        case 0x18:
                            goto DISAS_UNK;
                        case 0x19:
                            goto DISAS_UNK;
                        case 0x1A:
                            goto DISAS_UNK;
                        case 0x1B:
                            goto DISAS_UNK;
                        case 0x1C: /* VI2UC */
                            sprintf(opbuf, "VI2UC");
                            goto DISAS_VD_VS;
                        case 0x1D: /* VI2C */
                            sprintf(opbuf, "VI2C");
                            goto DISAS_VD_VS;
                        case 0x1E: /* VI2US */
                            sprintf(opbuf, "VI2US");
                            goto DISAS_VD_VS;
                        case 0x1F: /* VI2S */
                            sprintf(opbuf, "VI2S");
                            goto DISAS_VD_VS;
                    }
                    break;
                case 0x02: /* VFPU4_2 */
                    switch((op >> 16) & 0x1F) {
                        case 0x00:
                            goto DISAS_UNK;
                        case 0x01:
                            goto DISAS_UNK;
                        case 0x02: /* VBFY1 */
                            sprintf(opbuf, "VBFY1");
                            goto DISAS_VD_VS;
                        case 0x03: /* VBFY2 */
                            sprintf(opbuf, "VBFY2");
                            goto DISAS_VD_VS;
                        case 0x04:
                            goto DISAS_UNK;
                        case 0x05:
                            goto DISAS_UNK;
                        case 0x06: /* VFAD */
                            sprintf(opbuf, "VFAD");
                            goto DISAS_VD_VS;
                        case 0x07:
                            goto DISAS_UNK;
                        case 0x08:
                            goto DISAS_UNK;
                        case 0x09:
                            goto DISAS_UNK;
                        case 0x0A:
                            goto DISAS_UNK;
                        case 0x0B:
                            goto DISAS_UNK;
                        case 0x0C:
                            goto DISAS_UNK;
                        case 0x0D:
                            goto DISAS_UNK;
                        case 0x0E:
                            goto DISAS_UNK;
                        case 0x0F:
                            goto DISAS_UNK;
                        case 0x10:
                            goto DISAS_UNK;
                        case 0x11:
                            goto DISAS_UNK;
                        case 0x12:
                            goto DISAS_UNK;
                        case 0x13:
                            goto DISAS_UNK;
                        case 0x14:
                            goto DISAS_UNK;
                        case 0x15:
                            goto DISAS_UNK;
                        case 0x16:
                            goto DISAS_UNK;
                        case 0x17:
                            goto DISAS_UNK;
                        case 0x18:
                            goto DISAS_UNK;
                        case 0x19:
                            goto DISAS_UNK;
                        case 0x1A:
                            goto DISAS_UNK;
                        case 0x1B:
                            goto DISAS_UNK;
                        case 0x1C:
                            goto DISAS_UNK;
                        case 0x1D:
                            goto DISAS_UNK;
                        case 0x1E:
                            goto DISAS_UNK;
                        case 0x1F:
                            goto DISAS_UNK;
                    }
                    break;
                case 0x03: /* VCST */
                    sprintf(opbuf, "VCST");

                    sprintf(opbuf, "%s%s", opbuf, vregsuffix[VSIZE(op)]);
                    sprintf(argbuf[0], "%s", vregnames[VD(op)]);
                    sprintf(argbuf[0], "%s", vregconsts[IMM5(op)]);
                    break;
                case 0x04:
                    goto DISAS_UNK;
                case 0x05:
                    goto DISAS_UNK;
                case 0x06:
                    goto DISAS_UNK;
                case 0x07:
                    goto DISAS_UNK;
                case 0x08:
                    goto DISAS_UNK;
                case 0x09:
                    goto DISAS_UNK;
                case 0x0A:
                    goto DISAS_UNK;
                case 0x0B:
                    goto DISAS_UNK;
                case 0x0C:
                    goto DISAS_UNK;
                case 0x0D:
                    goto DISAS_UNK;
                case 0x0E:
                    goto DISAS_UNK;
                case 0x0F:
                    goto DISAS_UNK;
                case 0x10: /* VF2IN */
                    sprintf(opbuf, "VF2IN");
DISAS_VD_VS_IMM5:

                    sprintf(opbuf, "%s%s", opbuf, vregsuffix[VSIZE(op)]);
                    sprintf(argbuf[0], "%s", vregnames[VD(op)]);
                    sprintf(argbuf[1], "%s", vregnames[VS(op)]);
                    sprintf(argbuf[2], "%d", IMM5(op));
                    break;
                case 0x11: /* VF2IZ */
                    sprintf(opbuf, "VF2IZ");
                    goto DISAS_VD_VS_IMM5;
                case 0x12: /* VF2IU */
                    sprintf(opbuf, "VF2IU");
                    goto DISAS_VD_VS_IMM5;
                case 0x13: /* VF2ID */
                    sprintf(opbuf, "VF2ID");
                    goto DISAS_VD_VS_IMM5;
                case 0x14: /* VI2F */
                    sprintf(opbuf, "VI2F");
                    goto DISAS_VD_VS_IMM5;
                case 0x15: /* VCMOV */
                    if((IMM5(op) & 0x18) == 0)
                        sprintf(opbuf, "VCMOVT");
                    else if((IMM5(op) & 0x18) == 8)
                        sprintf(opbuf, "VCMOVF");

                    sprintf(opbuf, "%s%s", opbuf, vregsuffix[VSIZE(op)]);
                    sprintf(argbuf[0], "%s", vregnames[VD(op)]);
                    sprintf(argbuf[1], "%s", vregnames[VS(op)]);
                    sprintf(argbuf[2], "%d", IMM5(op) & 7);
                    break;
                case 0x16:
                    goto DISAS_UNK;
                case 0x17:
                    goto DISAS_UNK;
                case 0x18:
                    goto DISAS_UNK;
                case 0x19:
                    goto DISAS_UNK;
                case 0x1A:
                    goto DISAS_UNK;
                case 0x1B:
                    goto DISAS_UNK;
                case 0x1C:
                    goto DISAS_UNK;
                case 0x1D:
                    goto DISAS_UNK;
                case 0x1E:
                    goto DISAS_UNK;
                case 0x1F:
                    goto DISAS_UNK;
            }
            break;
        case 0x35: { /* ULV.Q [buggy on PSP100x?] */
            int16_t addroff = ((op >> 2) & 0x3FFF) << 2;
            if(op & 2)
                sprintf(opbuf, "LVR.Q");
            else
                sprintf(opbuf, "LVL.Q");

            sprintf(argbuf[0], "%s", vregnames[RS(op)]);
            sprintf(argbuf[1], "%s0x%04X(%s)", addroff < 0 ? "-" : "", abs(addroff), vregnames[RT(op)]);
            break; }
        case 0x36: { /* LV.Q */
            int16_t addroff = ((op >> 2) & 0x3FFF) << 2;
            sprintf(opbuf, "LV.Q");

            sprintf(argbuf[0], "%s", vregnames[RS(op)]);
            sprintf(argbuf[1], "%s0x%04X(%s)", addroff < 0 ? "-" : "", abs(addroff), vregnames[RT(op)]);
            break; }
        case 0x37: /* VFPU5 */
            switch((op >> 24) & 0x3) {
                case 0x0: /* VPFXS */
                    sprintf(opbuf, "VPFXS");

                    for(int i=0; i < 4; i++) {
                        sprintf(argbuf[i], "%d", ((op & (1 << (16+i))) << 4) |
                                                 ((op & (1 << (12+i))) << 3) |
                                                 ((op & (1 << ( 8+i))) << 2) |
                                                 ((op & (3 << ( 0+i))) << 0));
                    }
                    break;
                case 0x1: /* VPFXT */
                    sprintf(opbuf, "VPFXT");

                    for(int i=0; i < 4; i++) {
                        sprintf(argbuf[i], "%d", ((op & (1 << (16+i))) << 4) |
                                                 ((op & (1 << (12+i))) << 3) |
                                                 ((op & (1 << ( 8+i))) << 2) |
                                                 ((op & (3 << ( 0+i))) << 0));
                    }
                    break;
                case 0x2: /* VPFXD */
                    sprintf(opbuf, "VPFXD");

                    for(int i=0; i < 4; i++) {
                        sprintf(argbuf[i], "%d", ((op & (1 << ( 8+i))) << 2) |
                                                 ((op & (3 << ( 0+i))) << 0));
                    }
                    break;
                case 0x3: /* VIIM/VFIM */
                    if(op & (1 << 23)) { /* VFIM */
                        sprintf(opbuf, "VFIM.S");
                        sprintf(argbuf[0], "%f", cop2->halfFloatToFloat(IMM_S(op)));
                    }else{ /* VIIM */
                        sprintf(opbuf, "VIIM.S");
                        sprintf(argbuf[0], "%s0x%04X", IMM_S(op) < 0 ? "-" : "", IMM_S(op));
                    }

                    break;
            }
            break;
        case 0x38: /* SC */
            sprintf(opbuf, "SC");
            goto DISAS_RT_OFFS16_BASE;
        case 0x39: /* SWC1 */
            sprintf(opbuf, "SWC1");
            goto DISAS_RT_OFFS16_BASE;
        case 0x3A: /* SWC2 */
            sprintf(opbuf, "SWC1");
            goto DISAS_RT_OFFS16_BASE;
        case 0x3B:
            goto DISAS_RESV;
        case 0x3C: /* VFPU6 */
            switch((op >> 23) & 0x7) {
                case 0: /* VMMUL */
                    sprintf(opbuf, "VMMUL");
                    goto DISAS_VD_VT_VS;
                case 1: /* VHTFM2/VTFM2 */
                    if(op & 0x80) { /* VTFM2 */
                        sprintf(opbuf, "VTFM2");
                    }else{ /* VHTFM2 */
                        sprintf(opbuf, "VHTFM2");
                    }
                    goto DISAS_VD_VT_VS;
                case 2: /* VHTFM3/VTFM3 */
                    if(op & 0x80) { /* VTFM3 */
                        sprintf(opbuf, "VTFM3");
                    }else{ /* VHTFM3 */
                        sprintf(opbuf, "VHTFM3");
                    }
                    goto DISAS_VD_VT_VS;
                case 3: /* VHTFM4/VTFM4 */
                    if(op & 0x80) { /* VTFM4 */
                        sprintf(opbuf, "VTFM4");
                    }else{ /* VHTFM4 */
                        sprintf(opbuf, "VHTFM4");
                    }
                    goto DISAS_VD_VT_VS;
                case 4: /* VMSCL */
                    sprintf(opbuf, "VMSCL");
                    goto DISAS_VD_VT_VS;
                case 5: /* VCRSP/VQMUL */
                    if(op & 0x80) { /* VQMUL */
                        sprintf(opbuf, "VQMUL");
                    }else{ /* VCRSP */
                        sprintf(opbuf, "VCRSP");
                    }
                    goto DISAS_VD_VT_VS;
                case 6:
                    goto DISAS_UNK;
                case 7: /* VMMOV/VMZERO/VMIDT/VMONE/VROT */
                    switch((op >> 21) & 0x3) {
                        case 0:
                            switch((op >> 16) & 0x7) {
                                case 0x0: /* VMMOV */
                                    sprintf(opbuf, "VMMOV");
                                    goto DISAS_VD_VS;
                                case 0x1:
                                    goto DISAS_UNK;
                                case 0x2:
                                    goto DISAS_UNK;
                                case 0x3: /* VMIDT */
                                    sprintf(opbuf, "VMIDT");
                                    goto DISAS_VD;
                                case 0x4:
                                    goto DISAS_UNK;
                                case 0x5:
                                    goto DISAS_UNK;
                                case 0x6: /* VMZERO */
                                    sprintf(opbuf, "VMZERO");
                                    goto DISAS_VD;
                                case 0x7: /* VMONE */
                                    sprintf(opbuf, "VMONE");
                                    goto DISAS_VD;
                            }
                            break;
                        case 1: /* VROT */
                            sprintf(opbuf, "VROT");

                            sprintf(argbuf[0], "%s", vregnames[VD(op)]);
                            sprintf(argbuf[1], "%s", vregnames[VS(op)]);
                            sprintf(argbuf[2], "%d", IMM5(op));
                            break;
                        case 2:
                            goto DISAS_UNK;
                        case 3:
                            goto DISAS_UNK;
                    }
                    break;
            }
            break;
        case 0x3D: { /* USV.Q */
            int16_t addroff = ((op >> 2) & 0x3FFF) << 2;
            if(op & 2)
                sprintf(opbuf, "SVR.Q");
            else
                sprintf(opbuf, "SVL.Q");

            sprintf(argbuf[0], "%s", vregnames[RS(op)]);
            sprintf(argbuf[1], "%s0x%04X(%s)", addroff < 0 ? "-" : "", abs(addroff), vregnames[RT(op)]);
            break; }
        case 0x3E: { /* SV.Q */
            int16_t addroff = ((op >> 2) & 0x3FFF) << 2;
            sprintf(opbuf, "SV.Q");

            sprintf(argbuf[0], "%s", vregnames[RS(op)]);
            sprintf(argbuf[1], "%s0x%04X(%s)", addroff < 0 ? "-" : "", abs(addroff), vregnames[RT(op)]);
            break; }
        case 0x3F: /* VFPU7 */
            /* Figure this out... */
            if(op & 1) { /* VFLUSH */
                sprintf(opbuf, "VFLUSH");
            }else if(op & 0x20) { /* VNOP */
                sprintf(opbuf, "VNOP");
            }else{ /* VSYNC */
                sprintf(opbuf, "VSYNC");
            }
            break;
    }
    if(opbuf[0] == '\0')
        sprintf(opbuf, ".dc.w 0x%04X", op);
    snprintf(tmpbuf, len, "%08X:   %-8s", addr, opbuf);
    if(argbuf[0][0] != '\0') {
        snprintf(outbuf, len, "%s %s", tmpbuf, argbuf[0]);
        sprintf(tmpbuf, "%s", outbuf);
    }
    if(argbuf[1][0] != '\0') {
        snprintf(outbuf, len, "%s, %s", tmpbuf, argbuf[1]);
        sprintf(tmpbuf, "%s", outbuf);
    }
    if(argbuf[2][0] != '\0') {
        snprintf(outbuf, len, "%s, %s", tmpbuf, argbuf[2]);
        sprintf(tmpbuf, "%s", outbuf);
    }
    if(argbuf[3][0] != '\0') {
        snprintf(outbuf, len, "%s, %s", tmpbuf, argbuf[3]);
        sprintf(tmpbuf, "%s", outbuf);
    }
    snprintf(outbuf, len, "%-86s [%08X]", tmpbuf, op);
}

void Doovde::Allegrex::exception(uint32_t exc, int delay, int copro)
{
    uint32_t vec;
    uint32_t cause = exc << 2;
    cause |= cop0->regs[Doovde::Cop0::REG_CAUSE] & 0x3FFFFF00;
    uint32_t epc = pc;
    if(delay) {
        epc -= 4;
        cause |= (1 << 31);
    }
    cause |= copro << 28;
    if((cop0->regs[Doovde::Cop0::REG_STATUS] >> 22) & 1) { /* BEV */
        vec = 0xBFC00200;
    }else{
        vec = 0x80000000;
        if((exc == EXC_VIRT_COHERE_DATA) || (exc == EXC_VIRT_COHERE_INSTR))
            vec = 0xA0000000;
    }
    if(exc == EXC_TLB_LOAD) {
        vec += 0x000;
    }else if(0) { /* XTLB refill */
        vec += 0x080;
    }else if((exc == EXC_VIRT_COHERE_DATA) || (exc == EXC_VIRT_COHERE_INSTR)) {
        vec += 0x100;
    }else{
        vec += 0x180;
    }
    if((exc == EXC_RESET) || (exc == EXC_SOFTRESET) || (exc == EXC_NMIRESET))
        vec = 0xBFC00000;
    if((exc == EXC_RESET) || (exc == EXC_SOFTRESET) || (exc == EXC_NMIRESET) ||
       (exc == EXC_VIRT_COHERE_DATA) || (exc == EXC_VIRT_COHERE_INSTR)) {
        cop0->regs[Doovde::Cop0::REG_ERROR_EPC] = epc;
        cop0->setErrorLevel(1);
    }else{
        cop0->regs[Doovde::Cop0::REG_EPC] = epc;
        cop0->regs[Doovde::Cop0::REG_CAUSE] = cause;
        cop0->setExceptionLevel(1);
    }
    pc = vec;
    npc = pc + 4;
}
