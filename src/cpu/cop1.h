
#ifndef DOOVDE_ALLEGREX_COP1_H_
#define DOOVDE_ALLEGREX_COP1_H_

namespace Doovde {
    struct Allegrex;
    struct Cop1 { // FPU
        Cop1();
        ~Cop1();
        //---  Variables
        bool alive;
        union {
            uint32_t regs[32];
            float fprs[32];
        };
        uint32_t ctrlregs[32];

        static const int rev_maj = 0;
        static const int rev_min = 0;
        static const int rev_impl = 0xE;

        enum Cop1Ctrl {
            REG_REVISION = 0,
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            REG_CONDITION = 25,
            REG_EXCEPTIONS,
            /* Reserved */
            REG_ENABLES = 28,
            /* Reserved */
            /* Reserved */
            REG_CONTROL = 31
        };

        //---  Functions
        void lwc(Doovde::Allegrex* cpu, uint32_t op, uint32_t val);
        uint32_t swc(Doovde::Allegrex* cpu, uint32_t op);
        void copFunc(Doovde::Allegrex* cpu, uint32_t op);
        void copFuncDisas(Doovde::Allegrex* cpu, uint32_t op, char opbuf[4], char argbuf[4][16]);
        void reset(Doovde::Allegrex* cpu);
    };
}

#endif
