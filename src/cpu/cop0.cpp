//---  System includes
#include <stdint.h>
#include <math.h>

//---  Doovde includes
#include "log.h"
#include "cpu/allegrex.h"
#include "cpu/cop0.h"

Doovde::Cop0::Cop0()
{
    alive = true;
}

Doovde::Cop0::~Cop0()
{
    alive = false;
}

uint32_t Doovde::Cop0::mfc(Doovde::Allegrex* cpu, uint32_t op, int addr)
{
    (void)op;
    /* COP0 is unusable in User mode */
    if(getFullMode() == MODE_USER) {
        cpu->exception(Doovde::Allegrex::EXC_UNUSABLE_COP, 0, 0);
        return 0;
    }
    int reg = addr & 0x1F;
    switch(reg) {
        case REG_CPU_ID:
            if(cpu->media_engine)
                return 1;
            return 0;
        default:
            return regs[reg];
    }
}

void Doovde::Cop0::mtc(Doovde::Allegrex* cpu, uint32_t op, int addr, uint32_t val)
{
    (void)op;
    /* COP0 is unusable in User mode */
    if(getFullMode() == MODE_USER) {
        cpu->exception(Doovde::Allegrex::EXC_UNUSABLE_COP, 0, 0);
    }
    int reg = addr & 0x1F;
    switch(reg) {
        case REG_RANDOM:
        case REG_PR_ID:
        case REG_SYSCALL_CODE:
        case REG_BAD_VADDR:
            break;
        case REG_CONFIG:
            regs[reg] &= 0xFFFFFF80;
            regs[reg] |= val & 0x7F;
            break;
        default:
            regs[reg] = val;
            break;
    }
}

uint32_t Doovde::Cop0::cfc(Doovde::Allegrex* cpu, uint32_t op, int addr)
{
    (void)op;
    /* COP0 is unusable in User mode */
    if(getFullMode() == MODE_USER) {
        cpu->exception(Doovde::Allegrex::EXC_UNUSABLE_COP, 0, 0);
        return 0;
    }
    return ctrlregs[addr & 0x1F];
}

void Doovde::Cop0::ctc(Doovde::Allegrex* cpu, uint32_t op, int addr, uint32_t val)
{
    (void)op;
    /* COP0 is unusable in User mode */
    if(getFullMode() == MODE_USER) {
        cpu->exception(Doovde::Allegrex::EXC_UNUSABLE_COP, 0, 0);
    }
    ctrlregs[addr & 0x1F] = val;
}

void Doovde::Cop0::lwc(Doovde::Allegrex* cpu, uint32_t op, uint32_t val)
{
    Doovde::log(Doovde::LogLevel::ERROR, "LWC0: %08X %08X", op, val);
    cpu->exception(Doovde::Allegrex::EXC_UNUSABLE_COP, 0, 0);
}

uint32_t Doovde::Cop0::swc(Doovde::Allegrex* cpu, uint32_t op)
{
    Doovde::log(Doovde::LogLevel::ERROR, "SWC0: %08X", op);
    cpu->exception(Doovde::Allegrex::EXC_UNUSABLE_COP, 0, 0);
    return 0;
}

void Doovde::Cop0::cofun(Doovde::Allegrex* cpu, uint32_t op, int func)
{
    (void)op;
    (void)func;
    /* COP0 is unusable in User mode */
    if(getFullMode() == MODE_USER) {
        cpu->exception(Doovde::Allegrex::EXC_UNUSABLE_COP, 0, 0);
    }
    /* Nothing? */
}

void Doovde::Cop0::reset(Doovde::Allegrex* cpu)
{
    for(int i = 0; i < 32; i++)
        regs[i] = 0;
    regs[REG_RANDOM] = 47;
    regs[REG_CPU_ID] = cpu->media_engine ? 1 : 0;
}

int Doovde::Cop0::getFullMode()
{
    if(getExceptionLevel() || getErrorLevel() || (getMode() == MODE_KERNEL))
        return MODE_KERNEL;
    else
        return getMode();
}

int Doovde::Cop0::getMode()
{
    return (regs[REG_STATUS] >> 3) & 3;
}

int Doovde::Cop0::getErrorLevel()
{
    return (regs[REG_STATUS] >> 2) & 1;
}

int Doovde::Cop0::getExceptionLevel()
{
    return (regs[REG_STATUS] >> 1) & 1;
}

int Doovde::Cop0::getInterruptMask()
{
    return (regs[REG_STATUS] >> 8) & 0xFF;
}

int Doovde::Cop0::getInterruptEnable()
{
    return regs[REG_STATUS] & 1;
}

void Doovde::Cop0::setMode(int mode)
{
    mode &= 3;
    regs[REG_STATUS] &= ~(3 << 3);
    regs[REG_STATUS] |= (mode << 3);
}

void Doovde::Cop0::setErrorLevel(int level)
{
    level &= 1;
    regs[REG_STATUS] &= ~(1 << 2);
    regs[REG_STATUS] |= (level << 2);
}

void Doovde::Cop0::setExceptionLevel(int level)
{
    level &= 1;
    regs[REG_STATUS] &= ~(1 << 1);
    regs[REG_STATUS] |= (level << 1);
}

void Doovde::Cop0::setInterruptMask(int mask)
{
    mask &= 0xFF;
    regs[REG_STATUS] &= ~(0xFF << 8);
    regs[REG_STATUS] |= (mask << 8);
}

void Doovde::Cop0::setInterruptEnable(int enable)
{
    enable &= 1;
    regs[REG_STATUS] &= ~1;
    regs[REG_STATUS] |= enable;
}
