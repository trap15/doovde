
#ifndef DOOVDE_ALLEGREX_DECODE_H_
#define DOOVDE_ALLEGREX_DECODE_H_

#define RS(op)     ((op >> 21) & 0x1F)
#define RT(op)     ((op >> 16) & 0x1F)
#define RD(op)     ((op >> 11) & 0x1F)
#define SA(op)     ((op >>  6) & 0x1F)
#define IMM(op)    (op & 0xFFFF)
#define IMM_S(op)  ((int16_t)(op & 0xFFFF))
#define COFUN(op)  ((op >> 0) & 0x01FFFFFF)
#define TARGET(op) ((op >> 0) & 0x03FFFFFF)
#define VT(op)     ((op >> 16) & 0x7F)
#define VTWO(op)   ((op >> 15) & 1)
#define VS(op)     ((op >>  8) & 0x7F)
#define VONE(op)   ((op >>  7) & 1)
#define VD(op)     ((op >>  0) & 0x7F)
#define VSIZE(op)  ((int)(VONE(op) + (VTWO(op) << 1) + 1))
#define IMM5(op)   (RT(op))
#define IMM3(op)   ((op >> 18) & 7)
#define FMT(op)    ((op >> 21) & 0x1F)
#define FT(op)     ((op >> 16) & 0x1F)
#define FS(op)     ((op >> 11) & 0x1F)
#define FD(op)     ((op >>  6) & 0x1F)

#endif
