
#ifndef DOOVDE_ALLEGREX_COP0_H_
#define DOOVDE_ALLEGREX_COP0_H_

namespace Doovde {
    struct Allegrex;
    struct Cop0 {
        Cop0();
        ~Cop0();
        //---  Variables
        bool alive;
        uint32_t regs[32];
        uint32_t ctrlregs[32];

        enum Mode {
            MODE_KERNEL = 0,
            MODE_SUPERVISOR = 1,
            MODE_USER = 2
        };

        enum Cop0Reg {
            REG_INDEX = 0,
            REG_RANDOM,
            REG_ENTRY_LO_0,
            REG_ENTRY_LO_1,
            REG_CONTEXT,
            REG_PAGE_MASK,
            REG_WIRED,
            /* Reserved */
            REG_BAD_VADDR = 8,
            REG_COUNT,
            REG_ENTRY_HI,
            REG_COMPARE,
            REG_STATUS,
            REG_CAUSE,
            REG_EPC,
            REG_PR_ID,
            REG_CONFIG,
            REG_LLADDR,
            REG_WATCH_LO,
            REG_WATCH_HI,
            REG_XCONTEXT,
            REG_SYSCALL_CODE,
            REG_CPU_ID,
            /* Reserved */
            /* Reserved */
            REG_EBASE = 25,
            REG_ECC,
            REG_CACHE_ERR,
            REG_TAG_LO,
            REG_TAG_HI,
            REG_ERROR_EPC
            /* Reserved */
        };

        //---  Functions
        uint32_t mfc(Doovde::Allegrex* cpu, uint32_t op, int addr);
        void     mtc(Doovde::Allegrex* cpu, uint32_t op, int addr, uint32_t val);
        uint32_t cfc(Doovde::Allegrex* cpu, uint32_t op, int addr);
        void     ctc(Doovde::Allegrex* cpu, uint32_t op, int addr, uint32_t val);
        void     lwc(Doovde::Allegrex* cpu, uint32_t op, uint32_t val);
        uint32_t swc(Doovde::Allegrex* cpu, uint32_t op);
        void     cofun(Doovde::Allegrex* cpu, uint32_t op, int func);
        void     reset(Doovde::Allegrex* cpu);

        int getFullMode();

        int getMode();
        int getExceptionLevel();
        int getErrorLevel();
        int getInterruptMask();
        int getInterruptEnable();
        void setMode(int mode);
        void setExceptionLevel(int level);
        void setErrorLevel(int level);
        void setInterruptMask(int mask);
        void setInterruptEnable(int enable);
    };
}

#endif
