
#ifndef DOOVDE_ALLEGREX_COP2_H_
#define DOOVDE_ALLEGREX_COP2_H_

namespace Doovde {
    struct Allegrex;
    struct Cop2 { // VFPU
        Cop2();
        ~Cop2();
        //---  Variables
        bool alive;

        enum Cop2Reg {
            REG_PFXS = 128,
            REG_PFXT = 129,
            REG_PFXD = 130,
            REG_CC = 131,
            REG_INF4 = 132,
            REG_RSV5 = 133,
            REG_RSV6 = 134,
            REG_REV = 135,
            REG_RCX0 = 136,
            REG_RCX1 = 137,
            REG_RCX2 = 138,
            REG_RCX3 = 139,
            REG_RCX4 = 140,
            REG_RCX5 = 141,
            REG_RCX6 = 142,
            REG_RCX7 = 143
        };

        uint32_t ctrlregs[32];
        union {
            uint32_t regs[256];
            struct {
                float vfr[128];
                uint32_t reg_pfxs;
                uint32_t reg_pfxt;
                uint32_t reg_pfxd;
                uint32_t reg_cc;
                uint32_t inf4;
                uint32_t rsv5;
                uint32_t rsv6;
                uint32_t rev;
                uint32_t prng0;
                uint32_t prng1;
                uint32_t prng2;
                uint32_t prng3;
                uint32_t prng4;
                uint32_t prng5;
                uint32_t prng6;
                uint32_t prng7;
            } n;
        };

        union {
            float vec1[4];
            int32_t vec1i[4];
        };
        union {
            float vec2[4];
            int32_t vec2i[4];
        };
        union {
            float vec3[4];
            int32_t vec3i[4];
        };
        struct Pfxs {
            bool enabled;
            int swz[4];
            bool abs[4];
            bool cst[4];
            bool neg[4];
            void reset();
            void setValue(uint32_t val);
            uint32_t value();
        } pfxs, pfxt;
        struct Pfxd {
            bool enabled;
            int satMode[4];
            bool msk[4];
            void reset();
            void setValue(uint32_t val);
            uint32_t value();
        } pfxd;
        float constants[32];
        bool cc[8];
        //---  Functions
        void lwc(Doovde::Allegrex* cpu, uint32_t op, uint32_t val);
        uint32_t swc(Doovde::Allegrex* cpu, uint32_t op);
        void copFunc(Doovde::Allegrex* cpu, uint32_t op);
        void copFuncDisas(Doovde::Allegrex* cpu, uint32_t op, char opbuf[16], char argbuf[4][16]);
        void reset(Doovde::Allegrex* cpu);

        int getVfprIndex(int m, int c, int r);
        float transformVfpr(int swz, bool abs, bool cst, bool neg, float* x);
        float applyPfxsTransform(int i, float* x);
        float applyPfxtTransform(int i, float* x);
        float applyPfxdTransform(int i, float x);
        void loadSrcVector(int vsize, int vs);
        void loadTgtVector(int vsize, int vt);
        void loadDstVector(int vsize, int vd);
        void saveDstVector(int vsize, int vd, float* vr);
        float halfFloatToFloat(int val);
        void updatePfxs();
        void updatePfxt();
        void updatePfxd();
        void updateCc();
        void setCc(uint32_t val);
    };
}

#endif
