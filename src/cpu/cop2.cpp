//---  System includes
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <float.h>

//---  Doovde includes
#include "log.h"
#include "cpu/allegrex.h"
#include "cpu/decode.h"
#include "cpu/cop2.h"

Doovde::Cop2::Cop2()
{
    constants[ 0] = 0.0f;
    constants[ 1] = FLT_MAX;
    constants[ 2] = sqrtf(2.0f);
    constants[ 3] = sqrtf(0.5f);
    constants[ 4] = 2.0f / sqrtf(M_PI);
    constants[ 5] = 2.0f / M_PI;
    constants[ 6] = 1.0f / M_PI;
    constants[ 7] = M_PI / 4.0f;
    constants[ 8] = M_PI / 2.0f;
    constants[ 9] = M_PI;
    constants[10] = M_E;
    constants[11] = M_LOG2E;
    constants[12] = M_LOG10E;
    constants[13] = M_LN2;
    constants[14] = M_LN10;
    constants[15] = 2.0f * M_PI;
    constants[16] = M_PI / 6.0f;
    constants[17] = log10f(2.0f);
    constants[18] = log2f(10.0f);
    constants[19] = sqrt(3.0f) / 2.0f;
    constants[20] = 0.0f;
    constants[21] = 0.0f;
    constants[22] = 0.0f;
    constants[23] = 0.0f;
    constants[24] = 0.0f;
    constants[25] = 0.0f;
    constants[26] = 0.0f;
    constants[27] = 0.0f;
    constants[28] = 0.0f;
    constants[29] = 0.0f;
    constants[30] = 0.0f;
    constants[31] = 0.0f;
    alive = true;
}

Doovde::Cop2::~Cop2()
{
    alive = false;
}

void Doovde::Cop2::copFunc(Doovde::Allegrex* cpu, uint32_t op)
{
    switch((op >> 21) & 0x1F) {
        case 0x00:
            /* Nothing */
            break;
        case 0x01:
            /* Nothing */
            break;
        case 0x02:
            /* Nothing */
            break;
        case 0x03: { /* MFV */
            if(RT(op) == 0)
                break;
            uint32_t val = regs[op & 0xFF];
            if(op & 0x80) { /* Some of these are special case */
                switch(op & 0x7F) {
                    case 0x00: updatePfxs(); val = n.reg_pfxs; break;
                    case 0x01: updatePfxt(); val = n.reg_pfxt; break;
                    case 0x02: updatePfxd(); val = n.reg_pfxd; break;
                    case 0x03: updateCc();   val = n.reg_cc;   break;
                    case 0x08: /* PRNG0 */
                    case 0x09: /* PRNG1 */
                    case 0x0A: /* PRNG2 */
                    case 0x0B: /* PRNG3 */
                    case 0x0C: /* PRNG4 */
                    case 0x0D: /* PRNG5 */
                    case 0x0E: /* PRNG6 */
                    case 0x0F: /* PRNG7 */
                        /* Need to figure out how this works. */
                        /* Give it 1.0 for now... */
                        val = 0x3F800000;
                        break;
                }
            }
            cpu->regs.gpr[RT(op)] = val;
            break; }
        case 0x04:
            /* Nothing */
            break;
        case 0x05:
            /* Nothing */
            break;
        case 0x06:
            /* Nothing */
            break;
        case 0x07: { /* MTV */
            uint32_t val = cpu->regs.gpr[RT(op)];
            if(op & 0x80) { /* Some of these are special case */
                switch(op & 0x7F) {
                    case 0x00: pfxs.setValue(val); pfxs.enabled = true; break;
                    case 0x01: pfxt.setValue(val); pfxt.enabled = true; break;
                    case 0x02: pfxd.setValue(val); pfxd.enabled = true; break;
                    case 0x03: setCc(val); break;
                    case 0x08: /* PRNG0 */
                    case 0x09: /* PRNG1 */
                    case 0x0A: /* PRNG2 */
                    case 0x0B: /* PRNG3 */
                    case 0x0C: /* PRNG4 */
                    case 0x0D: /* PRNG5 */
                    case 0x0E: /* PRNG6 */
                    case 0x0F: /* PRNG7 */
                        /* Need to figure out how this works. */
                        /* This should seed it */
                        break;
                }
            }
            regs[op & 0xFF] = val;
            break; }
        case 0x08: /* BVF/BVT/BVFL/BVTL */
            switch((op >> 16) & 3) {
                case 0: /* BVF */
                    if(!cc[IMM3(op)]) {
                        cpu->npc = cpu->pc + IMM_S(op) << 2;
                    }
                    break;
                case 1: /* BVT */
                    if(cc[IMM3(op)]) {
                        cpu->npc = cpu->pc + IMM_S(op) << 2;
                    }
                    break;
                case 2: /* BVFL */
                    if(!cc[IMM3(op)]) {
                        cpu->npc = cpu->pc + IMM_S(op) << 2;
                    }else{
                        cpu->pc = cpu->npc;
                        cpu->npc += 4;
                    }
                    break;
                case 3: /* BVTL */
                    if(cc[IMM3(op)]) {
                        cpu->npc = cpu->pc + IMM_S(op) << 2;
                    }else{
                        cpu->pc = cpu->npc;
                        cpu->npc += 4;
                    }
                    break;
            }
            break;
        case 0x09:
            /* Nothing */
            break;
        case 0x0A:
            /* Nothing */
            break;
        case 0x0B:
            /* Nothing */
            break;
        case 0x0C:
            /* Nothing */
            break;
        case 0x0D:
            /* Nothing */
            break;
        case 0x0E:
            /* Nothing */
            break;
        case 0x0F:
            /* Nothing */
            break;
        case 0x10:
            /* Nothing */
            break;
        case 0x11:
            /* Nothing */
            break;
        case 0x12:
            /* Nothing */
            break;
        case 0x13:
            /* Nothing */
            break;
        case 0x14:
            /* Nothing */
            break;
        case 0x15:
            /* Nothing */
            break;
        case 0x16:
            /* Nothing */
            break;
        case 0x17:
            /* Nothing */
            break;
        case 0x18:
            /* Nothing */
            break;
        case 0x19:
            /* Nothing */
            break;
        case 0x1A:
            /* Nothing */
            break;
        case 0x1B:
            /* Nothing */
            break;
        case 0x1C:
            /* Nothing */
            break;
        case 0x1D:
            /* Nothing */
            break;
        case 0x1E:
            /* Nothing */
            break;
        case 0x1F:
            /* Nothing */
            break;
    }
}

void Doovde::Cop2::copFuncDisas(Doovde::Allegrex* cpu, uint32_t op, char opbuf[16], char argbuf[4][16])
{
    char regnames[32][6] = {
            "$zero", "$at",   "$v0",  "$v1",  "$a0",  "$a1",  "$a2",  "$a3",
            "$t0",   "$t1",   "$t2",  "$t3",  "$t4",  "$t5",  "$t6",  "$t7",
            "$s0",   "$s1",   "$s2",  "$s3",  "$s4",  "$s5",  "$s6",  "$s7",
            "$t8",   "$t9",   "$k0",  "$k1",  "$gp",  "$sp",  "$fp",  "$ra"};
    switch((op >> 21) & 0x1F) {
        case 0x00:
DISAS_UNK:
            sprintf(opbuf, ".dc.w");
            sprintf(argbuf[0], "0x%08X", op);
            break;
        case 0x01:
            goto DISAS_UNK;
        case 0x02:
            goto DISAS_UNK;
        case 0x03: /* MFV */
            sprintf(opbuf, "MFV");
            sprintf(argbuf[0], "%s", regnames[RT(op)]);
            sprintf(argbuf[1], "0x%02X", op & 0xFF);
            break;
        case 0x04:
            goto DISAS_UNK;
        case 0x05:
            goto DISAS_UNK;
        case 0x06:
            goto DISAS_UNK;
        case 0x07: /* MTV */
            sprintf(opbuf, "MTV");
            sprintf(argbuf[0], "%s", regnames[RT(op)]);
            sprintf(argbuf[1], "0x%02X", op & 0xFF);
        case 0x08: /* BVF/BVT/BVFL/BVTL */
            switch((op >> 16) & 3) {
                case 0: /* BVF */
                    sprintf(opbuf, "BVF");
                    sprintf(argbuf[0], "%d", IMM3(op));
                    sprintf(argbuf[1], "0x%08X", cpu->pc + IMM_S(op) << 2);
                    break;
                case 1: /* BVT */
                    sprintf(opbuf, "BVT");
                    sprintf(argbuf[0], "%d", IMM3(op));
                    sprintf(argbuf[1], "0x%08X", cpu->pc + IMM_S(op) << 2);
                    break;
                case 2: /* BVFL */
                    sprintf(opbuf, "BVFL");
                    sprintf(argbuf[0], "%d", IMM3(op));
                    sprintf(argbuf[1], "0x%08X", cpu->pc + IMM_S(op) << 2);
                    break;
                case 3: /* BVTL */
                    sprintf(opbuf, "BVTL");
                    sprintf(argbuf[0], "%d", IMM3(op));
                    sprintf(argbuf[1], "0x%08X", cpu->pc + IMM_S(op) << 2);
                    break;
            }
            break;
        case 0x09:
            goto DISAS_UNK;
        case 0x0A:
            goto DISAS_UNK;
        case 0x0B:
            goto DISAS_UNK;
        case 0x0C:
            goto DISAS_UNK;
        case 0x0D:
            goto DISAS_UNK;
        case 0x0E:
            goto DISAS_UNK;
        case 0x0F:
            goto DISAS_UNK;
        case 0x10:
            goto DISAS_UNK;
        case 0x11:
            goto DISAS_UNK;
        case 0x12:
            goto DISAS_UNK;
        case 0x13:
            goto DISAS_UNK;
        case 0x14:
            goto DISAS_UNK;
        case 0x15:
            goto DISAS_UNK;
        case 0x16:
            goto DISAS_UNK;
        case 0x17:
            goto DISAS_UNK;
        case 0x18:
            goto DISAS_UNK;
        case 0x19:
            goto DISAS_UNK;
        case 0x1A:
            goto DISAS_UNK;
        case 0x1B:
            goto DISAS_UNK;
        case 0x1C:
            goto DISAS_UNK;
        case 0x1D:
            goto DISAS_UNK;
        case 0x1E:
            goto DISAS_UNK;
        case 0x1F:
            goto DISAS_UNK;
    }
}

void Doovde::Cop2::lwc(Doovde::Allegrex* cpu, uint32_t op, uint32_t val)
{
    (void)cpu;
    // LV.S
    int rt = (op >> 16) & 0x1F;
    int m = (rt >> 2) & 7;
    int c = (rt >> 0) & 3;
    int r = 0;  // the bottom 2 bits should be 0 otherwise we would get an address error
            // other psp emulators ignore that address error though (!?)
    regs[getVfprIndex(m, c, r)] = val;
}

uint32_t Doovde::Cop2::swc(Doovde::Allegrex* cpu, uint32_t op)
{
    (void)cpu;
    // SV.S
    int rt = (op >> 16) & 0x1F;
    int m = (rt >> 2) & 7;
    int c = (rt >> 0) & 3;
    int r = 0;  // the bottom 2 bits should be 0 otherwise we would get an address error
            // other psp emulators ignore that address error though (!?)
    return regs[getVfprIndex(m, c, r)];
}

void Doovde::Cop2::reset(Doovde::Allegrex* cpu)
{
    (void)cpu;
    for(int i = 0; i < 256; i++) {
        regs[i] = 0;
    }
    pfxs.reset();
    pfxt.reset();
    pfxd.reset();
    cc[0] = cc[1] = cc[2] = cc[3] = cc[4] = cc[5] = cc[6] = cc[7] = false;
    vec1i[0] = vec1i[1] = vec1i[2] = vec1i[3] = 0;
    vec2i[0] = vec2i[1] = vec2i[2] = vec2i[3] = 0;
    vec3i[0] = vec3i[1] = vec3i[2] = vec3i[3] = 0;
    updatePfxs();
    updatePfxt();
    updatePfxd();
    updateCc();
}

// Source/Target Prefix Stack
void Doovde::Cop2::Pfxs::reset()
{
    swz[0] = swz[1] = swz[2] = swz[3] = 0;
    abs[0] = abs[1] = abs[2] = abs[3] = false;
    cst[0] = cst[1] = cst[2] = cst[3] = false;
    neg[0] = neg[1] = neg[2] = neg[3] = false;
    enabled = false;
}

void Doovde::Cop2::Pfxs::setValue(uint32_t val)
{
    swz[0] = (val >>  0) & 3;
    swz[1] = (val >>  2) & 3;
    swz[2] = (val >>  4) & 3;
    swz[3] = (val >>  6) & 3;
    abs[0] = (val >>  8) & 1;
    abs[1] = (val >>  9) & 1;
    abs[2] = (val >> 10) & 1;
    abs[3] = (val >> 11) & 1;
    cst[0] = (val >> 12) & 1;
    cst[1] = (val >> 13) & 1;
    cst[2] = (val >> 14) & 1;
    cst[3] = (val >> 15) & 1;
    neg[0] = (val >> 16) & 1;
    neg[1] = (val >> 17) & 1;
    neg[2] = (val >> 18) & 1;
    neg[3] = (val >> 19) & 1;
    // Enabled???
}

uint32_t Doovde::Cop2::Pfxs::value()
{
    return  (swz[0] <<  0)|(swz[1] <<  2)|(swz[2] <<  4)|(swz[3] <<  6) |
        (abs[0] <<  8)|(abs[1] <<  9)|(abs[2] << 10)|(abs[3] << 11) |
        (cst[0] << 12)|(cst[1] << 13)|(cst[2] << 14)|(cst[3] << 15) |
        (neg[0] << 16)|(neg[1] << 17)|(neg[2] << 18)|(neg[3] << 19) |
        (0); // Enabled???
}

// Destination Prefix Stack
void Doovde::Cop2::Pfxd::reset()
{
    satMode[0] = satMode[1] = satMode[2] = satMode[3] = 0;
    msk[0] = msk[1] = msk[2] = msk[3] = false;
    enabled = false;
}

void Doovde::Cop2::Pfxd::setValue(uint32_t val)
{
    satMode[0] = (val >>  0) & 3;
    satMode[1] = (val >>  2) & 3;
    satMode[2] = (val >>  4) & 3;
    satMode[3] = (val >>  6) & 3;
    msk[0] = (val >>  8) & 1;
    msk[1] = (val >>  9) & 1;
    msk[2] = (val >> 10) & 1;
    msk[3] = (val >> 11) & 1;
    // Enabled???
}

uint32_t Doovde::Cop2::Pfxd::value()
{
    return  (satMode[0] <<  0)|(satMode[1] <<  2)|(satMode[2] <<  4)|(satMode[3] <<  6) |
        (msk[0]     <<  8)|(msk[1]     <<  9)|(msk[2]     << 10)|(msk[3]     << 11) |
        (0); // Enabled???
}

int Doovde::Cop2::getVfprIndex(int m, int c, int r)
{
    m &= 7; c &= 3; r &= 3;
    return (m << 4) | (c << 2) | (r << 0);
}

float Doovde::Cop2::transformVfpr(int swz, bool abs, bool cst, bool neg, float* x)
{
    float value = 0.0f;
    if(cst) {
        switch(swz) {
            case 0:
                value = abs ? 3.0f : 0.0f;
                break;
            case 1:
                value = abs ? (1.0f / 3.0f) : 1.0f;
                break;
            case 2:
                value = abs ? (1.0f / 4.0f) : 2.0f;
                break;
            case 3:
                value = abs ? (1.0f / 6.0f) : 0.5f;
                break;
        }
    }else{
        value = x[swz];
        if(abs) {
            value = fabs(value);
        }
    }
    
    return neg ? -value : value;
}

float Doovde::Cop2::applyPfxsTransform(int i, float* x)
{
    return transformVfpr(pfxs.swz[i], pfxs.abs[i], pfxs.cst[i], pfxs.neg[i], x);
}

float Doovde::Cop2::applyPfxtTransform(int i, float* x)
{
    return transformVfpr(pfxt.swz[i], pfxt.abs[i], pfxt.cst[i], pfxt.neg[i], x);
}

float Doovde::Cop2::applyPfxdTransform(int i, float x)
{
    switch(pfxd.satMode[i]) {
        case 1:
            if(1.0f < x)
                return 1.0f;
            else if(0.0f > x)
                return 0.0f;
        case 3:
            if(1.0f < x)
                return 1.0f;
            else if(-1.0f > x)
                return -1.0f;
    }
    return x;
}

void Doovde::Cop2::loadSrcVector(int vsize, int vs)
{
    int m, s, i;
    m = (vs >> 2) & 7;
    i = (vs >> 0) & 3;
    switch(vsize) {
        case 1:
            s = (vs >> 5) & 3;
            vec1[0] = n.vfr[getVfprIndex(m, i, s)];
            if(pfxs.enabled) {
                vec1[0] = applyPfxsTransform(0, vec1);
                pfxs.enabled = false;
            }
            break;
        case 2:
            s = (vs & 64) >> 5;
            if(vs & 32) {
                vec1[0] = n.vfr[getVfprIndex(m, s + 0, i)];
                vec1[1] = n.vfr[getVfprIndex(m, s + 1, i)];
            }else{
                vec1[0] = n.vfr[getVfprIndex(m, i, s + 0)];
                vec1[1] = n.vfr[getVfprIndex(m, i, s + 1)];
            }
            if(pfxs.enabled) {
                vec3[0] = applyPfxsTransform(0, vec1);
                vec3[1] = applyPfxsTransform(1, vec1);
                vec1[0] = vec3[0];
                vec1[1] = vec3[1];
                pfxs.enabled = false;
            }
            break;
        case 3:
            s = (vs & 64) >> 6;
            if(vs & 32) {
                vec1[0] = n.vfr[getVfprIndex(m, s + 0, i)];
                vec1[1] = n.vfr[getVfprIndex(m, s + 1, i)];
                vec1[2] = n.vfr[getVfprIndex(m, s + 2, i)];
            }else{
                vec1[0] = n.vfr[getVfprIndex(m, i, s + 0)];
                vec1[1] = n.vfr[getVfprIndex(m, i, s + 1)];
                vec1[2] = n.vfr[getVfprIndex(m, i, s + 2)];
            }
            if(pfxs.enabled) {
                vec3[0] = applyPfxsTransform(0, vec1);
                vec3[1] = applyPfxsTransform(1, vec1);
                vec3[2] = applyPfxsTransform(2, vec1);
                vec1[0] = vec3[0];
                vec1[1] = vec3[1];
                vec1[2] = vec3[2];
                pfxs.enabled = false;
            }
            break;
        case 4:
            if(vs & 32) {
                vec1[0] = n.vfr[getVfprIndex(m, 0, i)];
                vec1[1] = n.vfr[getVfprIndex(m, 1, i)];
                vec1[2] = n.vfr[getVfprIndex(m, 2, i)];
                vec1[3] = n.vfr[getVfprIndex(m, 3, i)];
            }else{
                vec1[0] = n.vfr[getVfprIndex(m, i, 0)];
                vec1[1] = n.vfr[getVfprIndex(m, i, 1)];
                vec1[2] = n.vfr[getVfprIndex(m, i, 2)];
                vec1[3] = n.vfr[getVfprIndex(m, i, 3)];
            }
            if(pfxs.enabled) {
                vec3[0] = applyPfxsTransform(0, vec1);
                vec3[1] = applyPfxsTransform(1, vec1);
                vec3[2] = applyPfxsTransform(2, vec1);
                vec3[3] = applyPfxsTransform(3, vec1);
                vec1[0] = vec3[0];
                vec1[1] = vec3[1];
                vec1[2] = vec3[2];
                vec1[3] = vec3[3];
                pfxs.enabled = false;
            }
            break;
    }
}

void Doovde::Cop2::loadTgtVector(int vsize, int vt)
{
    int m, s, i;
    m = (vt >> 2) & 7;
    i = (vt >> 0) & 3;
    switch(vsize) {
        case 1:
            s = (vt >> 5) & 3;
            vec2[0] = n.vfr[getVfprIndex(m, i, s)];
            if(pfxt.enabled) {
                vec2[0] = applyPfxtTransform(0, vec2);
                pfxt.enabled = false;
            }
            break;
        case 2:
            s = (vt & 64) >> 5;
            if(vt & 32) {
                vec2[0] = n.vfr[getVfprIndex(m, s + 0, i)];
                vec2[1] = n.vfr[getVfprIndex(m, s + 1, i)];
            }else{
                vec2[0] = n.vfr[getVfprIndex(m, i, s + 0)];
                vec2[1] = n.vfr[getVfprIndex(m, i, s + 1)];
            }
            if(pfxt.enabled) {
                vec3[0] = applyPfxtTransform(0, vec2);
                vec3[1] = applyPfxtTransform(1, vec2);
                vec2[0] = vec3[0];
                vec2[1] = vec3[1];
                pfxt.enabled = false;
            }
            break;
        case 3:
            s = (vt & 64) >> 6;
            if(vt & 32) {
                vec2[0] = n.vfr[getVfprIndex(m, s + 0, i)];
                vec2[1] = n.vfr[getVfprIndex(m, s + 1, i)];
                vec2[2] = n.vfr[getVfprIndex(m, s + 2, i)];
            }else{
                vec2[0] = n.vfr[getVfprIndex(m, i, s + 0)];
                vec2[1] = n.vfr[getVfprIndex(m, i, s + 1)];
                vec2[2] = n.vfr[getVfprIndex(m, i, s + 2)];
            }
            if(pfxt.enabled) {
                vec3[0] = applyPfxtTransform(0, vec2);
                vec3[1] = applyPfxtTransform(1, vec2);
                vec3[2] = applyPfxtTransform(2, vec2);
                vec2[0] = vec3[0];
                vec2[1] = vec3[1];
                vec2[2] = vec3[2];
                pfxt.enabled = false;
            }
            break;
        case 4:
            if(vt & 32) {
                vec2[0] = n.vfr[getVfprIndex(m, 0, i)];
                vec2[1] = n.vfr[getVfprIndex(m, 1, i)];
                vec2[2] = n.vfr[getVfprIndex(m, 2, i)];
                vec2[3] = n.vfr[getVfprIndex(m, 3, i)];
            }else{
                vec2[0] = n.vfr[getVfprIndex(m, i, 0)];
                vec2[1] = n.vfr[getVfprIndex(m, i, 1)];
                vec2[2] = n.vfr[getVfprIndex(m, i, 2)];
                vec2[3] = n.vfr[getVfprIndex(m, i, 3)];
            }
            if(pfxt.enabled) {
                vec3[0] = applyPfxtTransform(0, vec2);
                vec3[1] = applyPfxtTransform(1, vec2);
                vec3[2] = applyPfxtTransform(2, vec2);
                vec3[3] = applyPfxtTransform(3, vec2);
                vec2[0] = vec3[0];
                vec2[1] = vec3[1];
                vec2[2] = vec3[2];
                vec2[3] = vec3[3];
                pfxt.enabled = false;
            }
            break;
    }
}

void Doovde::Cop2::loadDstVector(int vsize, int vd)
{
    int m, s, i;
    m = (vd >> 2) & 7;
    i = (vd >> 0) & 3;
    switch(vsize) {
        case 1:
            s = (vd >> 5) & 3;
            vec3[0] = n.vfr[getVfprIndex(m, i, s)];
            break;
        case 2:
            s = (vd & 64) >> 5;
            if(vd & 32) {
                vec3[0] = n.vfr[getVfprIndex(m, s + 0, i)];
                vec3[1] = n.vfr[getVfprIndex(m, s + 1, i)];
            }else{
                vec3[0] = n.vfr[getVfprIndex(m, i, s + 0)];
                vec3[1] = n.vfr[getVfprIndex(m, i, s + 1)];
            }
            break;
        case 3:
            s = (vd & 64) >> 6;
            if(vd & 32) {
                vec3[0] = n.vfr[getVfprIndex(m, s + 0, i)];
                vec3[1] = n.vfr[getVfprIndex(m, s + 1, i)];
                vec3[2] = n.vfr[getVfprIndex(m, s + 2, i)];
            }else{
                vec3[0] = n.vfr[getVfprIndex(m, i, s + 0)];
                vec3[1] = n.vfr[getVfprIndex(m, i, s + 1)];
                vec3[2] = n.vfr[getVfprIndex(m, i, s + 2)];
            }
            break;
        case 4:
            if(vd & 32) {
                vec3[0] = n.vfr[getVfprIndex(m, 0, i)];
                vec3[1] = n.vfr[getVfprIndex(m, 1, i)];
                vec3[2] = n.vfr[getVfprIndex(m, 2, i)];
                vec3[3] = n.vfr[getVfprIndex(m, 3, i)];
            }else{
                vec3[0] = n.vfr[getVfprIndex(m, i, 0)];
                vec3[1] = n.vfr[getVfprIndex(m, i, 1)];
                vec3[2] = n.vfr[getVfprIndex(m, i, 2)];
                vec3[3] = n.vfr[getVfprIndex(m, i, 3)];
            }
            break;
    }
}

void Doovde::Cop2::saveDstVector(int vsize, int vd, float* vr)
{
    int m, s, i;
    m = (vd >> 2) & 7;
    i = (vd >> 0) & 3;
    switch(vsize) {
        case 1:
            s = (vd >> 5) & 3;
            if(pfxd.enabled) {
                if(!pfxd.msk[0])
                    n.vfr[getVfprIndex(m, i, s)] = applyPfxdTransform(0, vr[0]);
                pfxd.enabled = false;
            }else{
                n.vfr[getVfprIndex(m, i, s)] = vr[0];
            }
            break;
        case 2:
            s = (vd & 64) >> 5;
            if(pfxd.enabled) {
                if(vd & 32) {
                    if(!pfxd.msk[0])
                        n.vfr[getVfprIndex(m, s + 0, i)] = applyPfxdTransform(0, vr[0]);
                    if(!pfxd.msk[1])
                        n.vfr[getVfprIndex(m, s + 1, i)] = applyPfxdTransform(1, vr[1]);
                }else{
                    if(!pfxd.msk[0])
                        n.vfr[getVfprIndex(m, i, s + 0)] = applyPfxdTransform(0, vr[0]);
                    if(!pfxd.msk[1])
                        n.vfr[getVfprIndex(m, i, s + 1)] = applyPfxdTransform(1, vr[1]);
                }
                pfxd.enabled = false;
            }else{
                if(vd & 32) {
                    n.vfr[getVfprIndex(m, s + 0, i)] = vr[0];
                    n.vfr[getVfprIndex(m, s + 1, i)] = vr[1];
                }else{
                    n.vfr[getVfprIndex(m, i, s + 0)] = vr[0];
                    n.vfr[getVfprIndex(m, i, s + 1)] = vr[1];
                }
            }
            break;
        case 3:
            s = (vd & 64) >> 6;
            if(pfxd.enabled) {
                if(vd & 32) {
                    if(!pfxd.msk[0])
                        n.vfr[getVfprIndex(m, s + 0, i)] = applyPfxdTransform(0, vr[0]);
                    if(!pfxd.msk[1])
                        n.vfr[getVfprIndex(m, s + 1, i)] = applyPfxdTransform(1, vr[1]);
                    if(!pfxd.msk[2])
                        n.vfr[getVfprIndex(m, s + 2, i)] = applyPfxdTransform(2, vr[2]);
                }else{
                    if(!pfxd.msk[0])
                        n.vfr[getVfprIndex(m, i, s + 0)] = applyPfxdTransform(0, vr[0]);
                    if(!pfxd.msk[1])
                        n.vfr[getVfprIndex(m, i, s + 1)] = applyPfxdTransform(1, vr[1]);
                    if(!pfxd.msk[2])
                        n.vfr[getVfprIndex(m, i, s + 2)] = applyPfxdTransform(2, vr[2]);
                }
                pfxd.enabled = false;
            }else{
                if(vd & 32) {
                    n.vfr[getVfprIndex(m, s + 0, i)] = vr[0];
                    n.vfr[getVfprIndex(m, s + 1, i)] = vr[1];
                    n.vfr[getVfprIndex(m, s + 2, i)] = vr[2];
                }else{
                    n.vfr[getVfprIndex(m, i, s + 0)] = vr[0];
                    n.vfr[getVfprIndex(m, i, s + 1)] = vr[1];
                    n.vfr[getVfprIndex(m, i, s + 2)] = vr[2];
                }
            }
            break;
        case 4:
            if(pfxd.enabled) {
                if(vd & 32) {
                    if(!pfxd.msk[0])
                        n.vfr[getVfprIndex(m, 0, i)] = applyPfxdTransform(0, vr[0]);
                    if(!pfxd.msk[1])
                        n.vfr[getVfprIndex(m, 1, i)] = applyPfxdTransform(1, vr[1]);
                    if(!pfxd.msk[2])
                        n.vfr[getVfprIndex(m, 2, i)] = applyPfxdTransform(2, vr[2]);
                    if(!pfxd.msk[3])
                        n.vfr[getVfprIndex(m, 3, i)] = applyPfxdTransform(3, vr[3]);
                }else{
                    if(!pfxd.msk[0])
                        n.vfr[getVfprIndex(m, i, 0)] = applyPfxdTransform(0, vr[0]);
                    if(!pfxd.msk[1])
                        n.vfr[getVfprIndex(m, i, 1)] = applyPfxdTransform(1, vr[1]);
                    if(!pfxd.msk[2])
                        n.vfr[getVfprIndex(m, i, 2)] = applyPfxdTransform(2, vr[2]);
                    if(!pfxd.msk[3])
                        n.vfr[getVfprIndex(m, i, 3)] = applyPfxdTransform(3, vr[3]);
                }
                pfxd.enabled = false;
            }else{
                if(vd & 32) {
                    n.vfr[getVfprIndex(m, 0, i)] = vr[0];
                    n.vfr[getVfprIndex(m, 1, i)] = vr[1];
                    n.vfr[getVfprIndex(m, 2, i)] = vr[2];
                    n.vfr[getVfprIndex(m, 3, i)] = vr[3];
                }else{
                    n.vfr[getVfprIndex(m, i, 0)] = vr[0];
                    n.vfr[getVfprIndex(m, i, 1)] = vr[1];
                    n.vfr[getVfprIndex(m, i, 2)] = vr[2];
                    n.vfr[getVfprIndex(m, i, 3)] = vr[3];
                }
            }
            break;
    }
}

float Doovde::Cop2::halfFloatToFloat(int val)
{
    bool retset = false;
    uint32_t ret = 0;
    int s = (val >> 15) & 0x001; /* Signedness */
    int e = (val >> 10) & 0x01F; /* Exponent */
    int m = (val >>  0) & 0x3FF; /* Mantissa */
    if(e == 0x00) { /* Denormal/zero/-0 */
        if(m == 0) { /* Zero */
            ret = s << 31;
            retset = true;
        }else{
            /* Normalize */
            while((m & 0x400) == 0) {
                m <<= 1;
                e--;
            }
            e++;
            m &= 0x3FF;
        }
    }else if(e == 0x1F) { /* +-INF/NaN */
        ret = (s << 31) | 0x7F800000 | (m << 13);
        retset = true;
    }
    if(!retset) {
        e += (127 - 15);
        m <<= 13;
        ret = (s << 31) | (e << 23) | m;
    }
    union {
        float f;
        uint32_t i;
    } crap;
    crap.i = ret;
    return crap.f;
}

void Doovde::Cop2::updatePfxs()
{
    n.reg_pfxs = pfxs.value();
}

void Doovde::Cop2::updatePfxt()
{
    n.reg_pfxt = pfxt.value();
}

void Doovde::Cop2::updatePfxd()
{
    n.reg_pfxd = pfxd.value();
}

void Doovde::Cop2::updateCc()
{
    n.reg_cc=   (cc[0] ? 0x01 : 0x00) |
        (cc[1] ? 0x02 : 0x00) |
        (cc[2] ? 0x04 : 0x00) |
        (cc[3] ? 0x08 : 0x00) |
        (cc[4] ? 0x10 : 0x00) |
        (cc[5] ? 0x20 : 0x00) |
        (cc[6] ? 0x40 : 0x00) |
        (cc[7] ? 0x80 : 0x00);
}

void Doovde::Cop2::setCc(uint32_t val)
{
    cc[0] = val & 0x01;
    cc[1] = val & 0x02;
    cc[2] = val & 0x04;
    cc[3] = val & 0x08;
    cc[4] = val & 0x10;
    cc[5] = val & 0x20;
    cc[6] = val & 0x40;
    cc[7] = val & 0x80;
}
