CPUPATH    = src/cpu
HEADERS   += $${CPUPATH}/allegrex.h $${CPUPATH}/decode.h \
               $${CPUPATH}/cop0.h $${CPUPATH}/cop1.h $${CPUPATH}/cop2.h 
SOURCES   += $${CPUPATH}/allegrex.cpp \
               $${CPUPATH}/cop0.cpp $${CPUPATH}/cop1.cpp $${CPUPATH}/cop2.cpp
RESOURCES += 
