
#ifndef DOOVDE_ALLEGREX_H_
#define DOOVDE_ALLEGREX_H_

namespace Doovde {
    struct Cop0;
    struct Cop1;
    struct Cop2;
    struct Allegrex {
        Allegrex(bool isMe);
        ~Allegrex();
        //---  Variables
        bool alive;
        bool media_engine;
        int cycles_left;

        // Regs
        struct {
            uint32_t gpr[32];
            uint32_t dbgr[32];
            uint32_t lo;
            uint32_t hi;
        } regs;

        uint32_t op;
        uint32_t pc, npc;
        bool fetching;

        enum DbgReg {
            DRCNTL = 0,
            DEPC,
            DDATA0,
            DDATA1,
            IBC,
            DBC,
            IBA = 8,
            IBAM,
            DBA = 12,
            DBAM,
            DBD,
            DBDM
        };

        enum Exception {
            EXC_INTERRUPT = 0,
            EXC_TLB_MOD,
            EXC_TLB_LOAD,
            EXC_TLB_STORE,
            EXC_ADDRESS_LOAD,
            EXC_ADDRESS_STORE,
            EXC_BUS_FETCH,
            EXC_BUS_DATA,
            EXC_SYSCALL,
            EXC_BREAKPOINT,
            EXC_RESERVED_INSTR,
            EXC_UNUSABLE_COP,
            EXC_OVERFLOW,
            EXC_TRAP,
            EXC_VIRT_COHERE_INSTR,
            EXC_FPU,
            /* Reserved */
            EXC_WATCH = 23,
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            /* Reserved */
            EXC_VIRT_COHERE_DATA = 31,
            /* Fake exceptions here */
            EXC_RESET = 0x100,
            EXC_SOFTRESET,
            EXC_NMIRESET
        };

        uint32_t int_state;

        // LL stuff
        bool ll; // possibly also inside cop0?

        // Read/write handlers; all 3 versions available for
        // extra speed.
        void (*write8) (Allegrex* cpu, uint32_t addr, uint8_t val);
        void (*write16)(Allegrex* cpu, uint32_t addr, uint16_t val);
        void (*write32)(Allegrex* cpu, uint32_t addr, uint32_t val);
        uint8_t  (*read8) (Allegrex* cpu, uint32_t addr);
        uint16_t (*read16)(Allegrex* cpu, uint32_t addr);
        uint32_t (*read32)(Allegrex* cpu, uint32_t addr);
        // TLB accessor
        uint32_t (*virt_to_phys)(Allegrex* cpu, uint32_t addr);
        uint32_t (*phys_to_virt)(Allegrex* cpu, uint32_t addr);
        // Coprocessor handlers
        struct Cop0* cop0;
        struct Cop1* cop1;
        struct Cop2* cop2;
        //---  Functions
        int run(int cycles);
        int runSingle();
        void disassemble(uint32_t addr, char *outbuf, int len);
        void reset();
        void exception(uint32_t exc, int delay = 0, int copro = 0);
    };
}

#endif
