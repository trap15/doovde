//---  System includes
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string>

//---  Qt includes
#include <QtGui>

//---  Doovde includes
#include "log.h"
#include "gui/main_win.h"
#include "gui/log_win.h"

static char *loglevel_str[] = {
    "ERROR",
    "WARNING",
    "MISC",
    "DEBUG",
};

static char *loglevel_color[] = {
    "#DD2222",
    "#DDDD22",
    "#22DD22",
    "#4444DD",
};

extern char binary_name[16];

int cur_log_level = Doovde::LogLevel::DEBUG;
static QString logger_toadd;
static QMutex logger_mutex;

void Doovde::log(int level, char* fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    _plugin_log(level, binary_name, fmt, ap);
    va_end(ap);
}

void Doovde::logUpdate()
{
    logger_mutex.lock();
    if(logger_toadd != "")
        mainWin->logwin->appendToHtml(logger_toadd);
    logger_toadd = "";
    logger_mutex.unlock();
}

void _plugin_log(int level, char* bin, char* fmt, va_list ap)
{
    if(level <= cur_log_level) {
        QTime time = QTime::currentTime();
        int hour = time.hour(), minute = time.minute(), second = time.second();
        char* str1;
        char* str2;
        vasprintf(&str1, fmt, ap);
        std::string str(str1);
        std::string searchString("\n"); 
        std::string replaceString("<br/>");

        std::string::size_type pos = 0;
        while((pos = str.find(searchString, pos)) != std::string::npos) {
            str.replace(pos, searchString.size(), replaceString);
            pos++;
        }
        asprintf(&str2, "[%02d:%02d:%02d] <font color=\"%s\">LOG[%s] (%s): %s</font>",
             hour, minute, second,
             loglevel_color[level], bin, loglevel_str[level], str.c_str());
        logger_mutex.lock();
        if(logger_toadd != "")
            logger_toadd += "<br/>";
        logger_toadd += mainWin->tr(str2);
        logger_mutex.unlock();
        free(str2);
        free(str1);
    }
}

