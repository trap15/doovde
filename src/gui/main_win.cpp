//---  System includes
#include <stdlib.h>
#include <stdint.h>

//---  Qt includes
#include <QtGui>
#include <QFileDialog>

//---  Doovde includes
#include "log.h"
#include "core/core.h"
#include "gui/main_win.h"
#include "gui/log_win.h"
#include "gui/debug_win.h"
#include "main_thread.h"

Doovde::Gui::MainWindow* mainWin;
QMenuBar* theMenuBar;

Doovde::Gui::MainWindow::MainWindow()
{
    thread = new MainThread();

    logwin = new Doovde::Gui::LogWindow();
    dbgwin = new Doovde::Gui::DebugWindow();
    theMenuBar = new QMenuBar();
    loadNewCoreWidget(new QWidget());

    createActions();
    createMenus();
    createToolBars();
    createStatusBar();

    readSettings();

    setWindowTitle(tr("Doovdé"));
    setUnifiedTitleAndToolBarOnMac(true);
    setMenuBar(theMenuBar);
    adjustSize();
}

Doovde::Gui::MainWindow::~MainWindow()
{
    thread->stopProcess();
    thread->exit();
    delete thread;
    delete logwin;
}

void Doovde::Gui::MainWindow::closeEvent(QCloseEvent *ev)
{
    thread->stopProcess();
    writeSettings();
    ev->accept();
    qApp->quit();
}

void Doovde::Gui::MainWindow::loadNewCoreWidget(QWidget* widget, bool del_old)
{
    if(del_old)
        delete core_widget;
    core_widget = widget;
    core_widget->setParent(this);
    core_widget->resize(480, 272);
    core_widget->setMinimumWidth(480);
    core_widget->setMinimumHeight(272);
    setCentralWidget(core_widget);
    adjustSize();
}

void Doovde::Gui::MainWindow::open()
{
    QString file = QFileDialog::getOpenFileName(this, tr("Open Image"), QString(), tr("Image Files (*.bin)"));
    if(core->load(file.toUtf8().constData())) {
        Doovde::log(Doovde::LogLevel::MISC, "Image loaded.");
        dbgwin->updateContents();
    }else{
        Doovde::log(Doovde::LogLevel::ERROR, "Image wasn't loaded!");
    }
}

void Doovde::Gui::MainWindow::emuReset()
{
    core->reset();
    dbgwin->updateContents();
}

void Doovde::Gui::MainWindow::emuPause()
{
    static bool called_before = false;
    if(!called_before) {
        thread->start();
        called_before = true;
    }
    if(thread->pauseProcess()) {
        emuPauseAct->setText("Pause");
    }else{
        emuPauseAct->setText("Run");
    }
}

void Doovde::Gui::MainWindow::openLog()
{
    logwin->show();
    Doovde::log(Doovde::LogLevel::MISC, "Log window opened.");
}

void Doovde::Gui::MainWindow::openDebug()
{
    dbgwin->show();
    Doovde::log(Doovde::LogLevel::MISC, "Debugger windows opened.");
}

void Doovde::Gui::MainWindow::about()
{
    QMessageBox::about(this, tr("About Doovdé"),
                             tr("Poospé emulator."));
}

void Doovde::Gui::MainWindow::createActions()
{
    openAct = new QAction(tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit Doovdé"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

    logOpenAct = new QAction(tr("Open Log"), this);
    logOpenAct->setShortcut(Qt::CTRL | Qt::Key_L);
    connect(logOpenAct, SIGNAL(triggered()), this, SLOT(openLog()));

    logClearAct = new QAction(tr("Clear Log"), this);
    connect(logClearAct, SIGNAL(triggered()), logwin, SLOT(clear()));

    dbgOpenAct = new QAction(tr("Open Debugger"), this);
    dbgOpenAct->setShortcut(Qt::CTRL | Qt::Key_D);
    connect(dbgOpenAct, SIGNAL(triggered()), this, SLOT(openDebug()));

    emuPauseAct = new QAction(tr("&Run"), this);
    emuPauseAct->setShortcut(Qt::CTRL | Qt::Key_R);
    connect(emuPauseAct, SIGNAL(triggered()), this, SLOT(emuPause()));
    
    emuResetAct = new QAction(tr("&Reset"), this);
    emuResetAct->setShortcut(Qt::CTRL | Qt::SHIFT | Qt::Key_R);
    connect(emuResetAct, SIGNAL(triggered()), this, SLOT(emuReset()));
    
    aboutAct = new QAction(tr("&About"), this);
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));
}

void Doovde::Gui::MainWindow::createMenus()
{
    fileMenu = theMenuBar->addMenu(tr("&File"));
    fileMenu->addAction(openAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    emuMenu = theMenuBar->addMenu(tr("Emu"));
    emuMenu->addAction(emuPauseAct);
    emuMenu->addAction(emuResetAct);
    
    logMenu = theMenuBar->addMenu(tr("Log"));
    logMenu->addAction(logOpenAct);
    logMenu->addAction(logClearAct);
    
    dbgMenu = theMenuBar->addMenu(tr("Debugger"));
    dbgMenu->addAction(dbgOpenAct);

    theMenuBar->addSeparator();
    
    helpMenu = theMenuBar->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
}

void Doovde::Gui::MainWindow::createToolBars()
{
}

void Doovde::Gui::MainWindow::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));
}

void Doovde::Gui::MainWindow::readSettings()
{
    QSettings settings("uTech Enterprises", "Doovdé");
    QPoint winpos = settings.value("main_pos", QPoint(0, 0)).toPoint();
    QSize winsz = settings.value("main_sz", QSize(480, 272)).toSize();
    move(winpos);
    resize(winsz);
}

void Doovde::Gui::MainWindow::writeSettings()
{
    QSettings settings("uTech Enterprises", "Doovdé");
    settings.setValue("main_pos", pos());
    settings.setValue("main_sz", size());
}
