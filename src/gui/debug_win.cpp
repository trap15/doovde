//---  System includes
#include <stdlib.h>
#include <stdint.h>

//---  Qt includes
#include <QtGui>

//---  Doovde includes
#include "log.h"
#include "core/core.h"
#include "cpu/allegrex.h"
#include "mem/mem.h"
#include "gui/debug_win.h"

Doovde::Gui::Debugger::DisasWindow::DisasWindow()
{
    QFont debug_font;
    debug_font.setFamily("Courier");
    debug_font.setFixedPitch(true);
    debug_font.setPointSize(10);

    disas_text = new QTextEdit();
    disas_text->setFont(debug_font);
    disas_text->setReadOnly(true);
    disas_text->setLineWrapMode(QTextEdit::NoWrap);

    lyt = new QVBoxLayout();
    lyt->addWidget(disas_text);
    setLayout(lyt);

    setWindowTitle(tr("Disassembly"));
    clear();

    readSettings();
}

void Doovde::Gui::Debugger::DisasWindow::closeEvent(QCloseEvent *ev)
{
    // Possibly send close events to the other debug windows
    writeSettings();
    ev->accept();
}

void Doovde::Gui::Debugger::DisasWindow::clear()
{
    disas_text->clear();
}

void Doovde::Gui::Debugger::DisasWindow::updateContents()
{
#define DISAS_PROLOGUE 16
#define DISAS_EPILOGUE 16
    QString contents;
    char disasbuf[256];
    // We need to remove the bus error callbacks for disassembly.
    void (*buserrcbs[2])(int, uint32_t, int);
    buserrcbs[0] = core->sc_mem->busErrCb;
    buserrcbs[1] = core->me_mem->busErrCb;

    core->sc_mem->busErrCb = NULL;
    core->me_mem->busErrCb = NULL;

    contents += "<pre>\n";
    for(int i=0; i < DISAS_PROLOGUE; i++) {
        core->sc_cpu->disassemble(core->sc_cpu->pc - (DISAS_PROLOGUE - i)*4, disasbuf, 255);
        contents += disasbuf;
        contents += "\n";
    }
    core->sc_cpu->disassemble(core->sc_cpu->pc, disasbuf, 255);
    contents += "<span style=\"background-color:#EEAAAA\">";
    contents += disasbuf;
    contents += "</span>\n";
    for(int i=0; i < DISAS_EPILOGUE; i++) {
        core->sc_cpu->disassemble(core->sc_cpu->pc + (i+1)*4, disasbuf, 255);
        contents += disasbuf;
        contents += "\n";
    }
    contents += "</pre>\n";
    disas_text->setHtml(contents);

    core->sc_mem->busErrCb = buserrcbs[0];
    core->me_mem->busErrCb = buserrcbs[1];
}

void Doovde::Gui::Debugger::DisasWindow::readSettings()
{
    QSettings settings("uTech Enterprises", "Doovdé");
    QPoint winpos = settings.value("disas_pos", QPoint(500, 0)).toPoint();
    QSize winsz = settings.value("disas_sz", QSize(400, 450)).toSize();
    move(winpos);
    resize(winsz);
}

void Doovde::Gui::Debugger::DisasWindow::writeSettings()
{
    QSettings settings("uTech Enterprises", "Doovdé");
    settings.setValue("disas_pos", pos());
    settings.setValue("disas_sz", size());
}


Doovde::Gui::Debugger::RegWindow::RegWindow()
{
    QFont reg_font;
    reg_font.setFamily("Courier");
    reg_font.setFixedPitch(true);
    reg_font.setPointSize(10);

    reg_labels[ 0] = new QLabel("$zero:");
    reg_labels[ 1] = new QLabel("$at:");
    reg_labels[ 2] = new QLabel("$v0:");
    reg_labels[ 3] = new QLabel("$v1:");
    reg_labels[ 4] = new QLabel("$a0:");
    reg_labels[ 5] = new QLabel("$a1:");
    reg_labels[ 6] = new QLabel("$a2:");
    reg_labels[ 7] = new QLabel("$a3:");
    reg_labels[ 8] = new QLabel("$t0:");
    reg_labels[ 9] = new QLabel("$t1:");
    reg_labels[10] = new QLabel("$t2:");
    reg_labels[11] = new QLabel("$t3:");
    reg_labels[12] = new QLabel("$t4:");
    reg_labels[13] = new QLabel("$t5:");
    reg_labels[14] = new QLabel("$t6:");
    reg_labels[15] = new QLabel("$t7:");
    reg_labels[16] = new QLabel("$s0:");
    reg_labels[17] = new QLabel("$s1:");
    reg_labels[18] = new QLabel("$s2:");
    reg_labels[19] = new QLabel("$s3:");
    reg_labels[20] = new QLabel("$s4:");
    reg_labels[21] = new QLabel("$s5:");
    reg_labels[22] = new QLabel("$s6:");
    reg_labels[23] = new QLabel("$s7:");
    reg_labels[24] = new QLabel("$t8:");
    reg_labels[25] = new QLabel("$t9:");
    reg_labels[26] = new QLabel("$k0:");
    reg_labels[27] = new QLabel("$k1:");
    reg_labels[28] = new QLabel("$gp:");
    reg_labels[29] = new QLabel("$sp:");
    reg_labels[30] = new QLabel("$fp:");
    reg_labels[31] = new QLabel("$ra:");
    reg_labels[32] = new QLabel("DRCNTL:");
    reg_labels[33] = new QLabel("DEPC:");
    reg_labels[34] = new QLabel("DDATA0:");
    reg_labels[35] = new QLabel("DDATA1:");
    reg_labels[36] = new QLabel("IBC:");
    reg_labels[37] = new QLabel("DBC:");
    reg_labels[38] = new QLabel("DBR6:");
    reg_labels[39] = new QLabel("DBR7:");
    reg_labels[40] = new QLabel("IBA:");
    reg_labels[41] = new QLabel("IBAM:");
    reg_labels[42] = new QLabel("DBR10:");
    reg_labels[43] = new QLabel("DBR11:");
    reg_labels[44] = new QLabel("DBA:");
    reg_labels[45] = new QLabel("DBAM:");
    reg_labels[46] = new QLabel("DBD:");
    reg_labels[47] = new QLabel("DBDM:");
    reg_labels[48] = new QLabel("DBR16:");
    reg_labels[49] = new QLabel("DBR17:");
    reg_labels[50] = new QLabel("DBR18:");
    reg_labels[51] = new QLabel("DBR19:");
    reg_labels[52] = new QLabel("DBR20:");
    reg_labels[53] = new QLabel("DBR21:");
    reg_labels[54] = new QLabel("DBR22:");
    reg_labels[55] = new QLabel("DBR23:");
    reg_labels[56] = new QLabel("DBR24:");
    reg_labels[57] = new QLabel("DBR25:");
    reg_labels[58] = new QLabel("DBR26:");
    reg_labels[59] = new QLabel("DBR27:");
    reg_labels[60] = new QLabel("DBR28:");
    reg_labels[61] = new QLabel("DBR29:");
    reg_labels[62] = new QLabel("DBR30:");
    reg_labels[63] = new QLabel("DBR31:");
    reg_labels[64] = new QLabel("HI:");
    reg_labels[65] = new QLabel("LO:");
    reg_labels[66] = new QLabel("PC:");
    reg_labels[67] = new QLabel("NPC:");
    for(int i=0; i < 32+32+2+2; i++) {
        reg_values[i] = new QLabel("0x00000000");
        reg_values[i]->setFont(reg_font);
        reg_values[i]->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
        reg_labels[i]->setFont(reg_font);
        reg_labels[i]->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    }

    lyt = new QGridLayout();
    for(int i=0; i < 32; i++) {
        lyt->addWidget(reg_labels[i], i, 0);
        lyt->addWidget(reg_values[i], i, 1);
    }
    for(int i=0; i < 32; i++) {
        lyt->addWidget(reg_labels[i+32], i, 3);
        lyt->addWidget(reg_values[i+32], i, 4);
    }
    for(int i=0; i < 4; i++) {
        lyt->addWidget(reg_labels[i+64], i, 6);
        lyt->addWidget(reg_values[i+64], i, 7);
    }
    lyt->setVerticalSpacing(2);
    setLayout(lyt);

    setWindowTitle(tr("Registers"));
    clear();

    readSettings();
}

void Doovde::Gui::Debugger::RegWindow::closeEvent(QCloseEvent *ev)
{
    // Possibly send close events to the other debug windows
    writeSettings();
    ev->accept();
}

void Doovde::Gui::Debugger::RegWindow::clear()
{
}

void Doovde::Gui::Debugger::RegWindow::updateContents()
{
    for(int i=0; i < 32; i++) {
        reg_values[i]->setText(QString("0x%1").arg((uint)core->sc_cpu->regs.gpr[i], 8, 16, QChar('0')));
    }
    for(int i=0; i < 32; i++) {
        reg_values[i+32]->setText(QString("0x%1").arg((uint)core->sc_cpu->regs.dbgr[i], 8, 16, QChar('0')));
    }
    reg_values[64]->setText(QString("0x%1").arg((uint)core->sc_cpu->regs.hi, 8, 16, QChar('0')));
    reg_values[65]->setText(QString("0x%1").arg((uint)core->sc_cpu->regs.lo, 8, 16, QChar('0')));
    reg_values[66]->setText(QString("0x%1").arg((uint)core->sc_cpu->pc, 8, 16, QChar('0')));
    reg_values[67]->setText(QString("0x%1").arg((uint)core->sc_cpu->npc, 8, 16, QChar('0')));
}

void Doovde::Gui::Debugger::RegWindow::readSettings()
{
    QSettings settings("uTech Enterprises", "Doovdé");
    QPoint winpos = settings.value("reg_pos", QPoint(920, 0)).toPoint();
    QSize winsz = settings.value("reg_sz", QSize(350, 450)).toSize();
    move(winpos);
    resize(winsz);
}

void Doovde::Gui::Debugger::RegWindow::writeSettings()
{
    QSettings settings("uTech Enterprises", "Doovdé");
    settings.setValue("reg_pos", pos());
    settings.setValue("reg_sz", size());
}


Doovde::Gui::Debugger::CmdWindow::CmdWindow(Doovde::Gui::DebugWindow *_wins)
{
    QFont cmd_font;
    cmd_font.setFamily("Courier");
    cmd_font.setFixedPitch(true);
    cmd_font.setPointSize(10);

    cmd_text = new QTextEdit();
    cmd_text->setFont(cmd_font);
    cmd_text->setReadOnly(true);
    cmd_text->setLineWrapMode(QTextEdit::NoWrap);
    cmd_text->setText("");

    cmd_line = new QLineEdit();
    cmd_line->setFont(cmd_font);
    cmd_line->setText("> ");
    connect(cmd_line, SIGNAL(returnPressed()), this, SLOT(cmdEntered()) );

    lyt = new QVBoxLayout();
    lyt->addWidget(cmd_text);
    lyt->addWidget(cmd_line);
    setLayout(lyt);

    wins = _wins;

    setWindowTitle(tr("Debugger"));
    clear();

    readSettings();
}

void Doovde::Gui::Debugger::CmdWindow::closeEvent(QCloseEvent *ev)
{
    // Possibly send close events to the other debug windows
    writeSettings();
    ev->accept();
}

void Doovde::Gui::Debugger::CmdWindow::clear()
{
    cmd_text->clear();
}

void Doovde::Gui::Debugger::CmdWindow::cmdEntered()
{
    QString line = cmd_line->text();
    line.remove(0, 2);
    appendText("> " + line + "\n");
    cmd_line->setText("> ");
    line = line.toLower();

    runCommand(line);

    wins->reg_win->updateContents();
    wins->disas_win->updateContents();
}

static uint32_t _dbgcmd_get_uint(QString param)
{
    bool ok;
    uint32_t val;
    if(param.startsWith("0x"))
        val = param.toUInt(&ok, 16);
    else if(param.startsWith("$"))
        val = param.toUInt(&ok, 16);
    else if(param.startsWith("H'"))
        val = param.toUInt(&ok, 16);
    else if(param.startsWith("0"))
        val = param.toUInt(&ok, 8);
    else if(param.startsWith("Q'"))
        val = param.toUInt(&ok, 8);
    else if(param.startsWith("0b"))
        val = param.toUInt(&ok, 2);
    else if(param.startsWith("b"))
        val = param.toUInt(&ok, 2);
    else if(param.startsWith("B'"))
        val = param.toUInt(&ok, 2);
    else
        val = param.toUInt(&ok, 10);
    return val;
}

void Doovde::Gui::Debugger::CmdWindow::runCommand(QString line)
{
    if(line == "") {
        runCommand(lastcmd);
        return;
    }
    QStringList params = line.split(" ", QString::SkipEmptyParts);
    QString cmd = params[0];
    if(cmd == "s") {
        uint32_t len = 1;
        if(params.count() > 1)
            len = _dbgcmd_get_uint(params[1]);
        for(uint32_t i=0; i < len; i++)
            core->runSingle();
        appendText(QString("Stepped %1 cycles\n").arg(len));
    }else if(cmd == "reset") {
        core->reset();
        appendText("Resetting...\n");
    }else if(cmd == "runto") {
        if(params.count() > 1) {
            uint32_t addr = _dbgcmd_get_uint(params[1]);
            while(core->sc_cpu->pc != addr) {
                core->runSingle();
            }
        }
    }
    lastcmd = line;
}

void Doovde::Gui::Debugger::CmdWindow::appendText(QString str)
{
    cmd_text->setText(cmd_text->toPlainText() + str);
    QTextCursor tcursor = cmd_text->textCursor();
    tcursor.movePosition(QTextCursor::End);
    cmd_text->setTextCursor(tcursor);
}

void Doovde::Gui::Debugger::CmdWindow::readSettings()
{
    QSettings settings("uTech Enterprises", "Doovdé");
    QPoint winpos = settings.value("cmd_pos", QPoint(500, 510)).toPoint();
    QSize winsz = settings.value("cmd_sz", QSize(770, 200)).toSize();
    move(winpos);
    resize(winsz);
}

void Doovde::Gui::Debugger::CmdWindow::writeSettings()
{
    QSettings settings("uTech Enterprises", "Doovdé");
    settings.setValue("cmd_pos", pos());
    settings.setValue("cmd_sz", size());
}


Doovde::Gui::DebugWindow::DebugWindow()
{
    disas_win = new Doovde::Gui::Debugger::DisasWindow();
    reg_win = new Doovde::Gui::Debugger::RegWindow();
    cmd_win = new Doovde::Gui::Debugger::CmdWindow(this);
}

void Doovde::Gui::DebugWindow::show()
{
    reg_win->show();
    disas_win->show();
    cmd_win->show();
    updateContents();
}

void Doovde::Gui::DebugWindow::updateContents()
{
    reg_win->updateContents();
    disas_win->updateContents();
}
