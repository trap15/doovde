
#ifndef DOOVDE_DEBUGWIN_H_
#define DOOVDE_DEBUGWIN_H_

#include <QMainWindow>

class QString;
class QTextEdit;
class QLineEdit;
class QLabel;
class QGridLayout;
class QVBoxLayout;

namespace Doovde {
    namespace Gui {
        class DebugWindow;
        namespace Debugger {
            class DisasWindow : public QWidget {
                Q_OBJECT
            public:
                DisasWindow();

                void updateContents();

            protected:
                void closeEvent(QCloseEvent *event);

            private slots:
                void clear();

            private:
                void readSettings();
                void writeSettings();
                QTextEdit* disas_text;
                QVBoxLayout* lyt;
            };

            class RegWindow : public QWidget {
                Q_OBJECT
            public:
                RegWindow();

                void updateContents();

            protected:
                void closeEvent(QCloseEvent *event);

            private slots:
                void clear();

            private:
                void readSettings();
                void writeSettings();
                QLabel* reg_labels[32+32+2+2];
                QLabel* reg_values[32+32+2+2];
                QGridLayout* lyt;
            };


            class CmdWindow : public QWidget {
                Q_OBJECT
            public:
                CmdWindow(DebugWindow* _wins);

            protected:
                void closeEvent(QCloseEvent *event);

            private slots:
                void clear();
                void cmdEntered();

            private:
                void appendText(QString str);
                void runCommand(QString cmd);
                void readSettings();
                void writeSettings();
                QString lastcmd;
                DebugWindow* wins;
                QTextEdit* cmd_text;
                QLineEdit* cmd_line;
                QVBoxLayout* lyt;
            };
        }
        class DebugWindow { // A misnomer; it's actually a collection of windows
        public:
            DebugWindow();

            void show();
            void updateContents();

            Debugger::DisasWindow* disas_win;
            Debugger::RegWindow* reg_win;
            Debugger::CmdWindow* cmd_win;
        };
    }
}

#endif
