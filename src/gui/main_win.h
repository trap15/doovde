
#ifndef DOOVDE_MAINWIN_H_
#define DOOVDE_MAINWIN_H_

#include <QMainWindow>

class QAction;
class QMenu;
class QMenuBar;
class QPlainTextEdit;

namespace Doovde {
    class MainThread;
    namespace Gui {
        class LogWindow;
        class DebugWindow;

        class MainWindow : public QMainWindow {
            Q_OBJECT
        public:
            MainWindow();
            ~MainWindow();
            void loadNewCoreWidget(QWidget* widget, bool del_old = false);
            LogWindow* logwin;
            DebugWindow* dbgwin;
            QWidget* core_widget;

        protected:
            void closeEvent(QCloseEvent *event);

        private slots:
            void open();
            void openLog();
            void openDebug();
            void about();
            void emuReset();
            void emuPause();

        private:
            void createActions();
            void createMenus();
            void createToolBars();
            void createStatusBar();

            void readSettings();
            void writeSettings();

            QMenu* fileMenu;
            QMenu* logMenu;
            QMenu* dbgMenu;
            QMenu* emuMenu;
            QMenu* helpMenu;
            QAction* openAct;
            QAction* exitAct;
            QAction* emuResetAct;
            QAction* emuPauseAct;
            QAction* logOpenAct;
            QAction* logClearAct;
            QAction* dbgOpenAct;
            QAction* aboutAct;

            MainThread* thread;
        };
    }
}

extern Doovde::Gui::MainWindow* mainWin;
extern QMenuBar* theMenuBar;

#endif
