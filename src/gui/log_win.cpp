//---  System includes
#include <stdlib.h>
#include <stdint.h>

//---  Qt includes
#include <QtGui>

//---  Doovde includes
#include "log.h"
#include "gui/log_win.h"

Doovde::Gui::LogWindow::LogWindow()
{
    QFont log_font;
    log_font.setFamily("Courier");
    log_font.setFixedPitch(true);
    log_font.setPointSize(10);

    log_text = new QTextEdit();
    log_text->setFont(log_font);
    log_text->setReadOnly(true);
    log_text->setLineWrapMode(QTextEdit::NoWrap);
    setCentralWidget(log_text);

    update_timer = new QTimer(this);
    connect(update_timer, SIGNAL(timeout()), this, SLOT(updateLog()));
    update_timer->start(100);
    setWindowTitle(tr("Log"));
    clear();

    readSettings();
}

void Doovde::Gui::LogWindow::closeEvent(QCloseEvent *ev)
{
    writeSettings();
    ev->accept();
}

void Doovde::Gui::LogWindow::updateLog()
{
    Doovde::logUpdate();
}

void Doovde::Gui::LogWindow::clear()
{
    log_text->clear();
}

void Doovde::Gui::LogWindow::appendToHtml(QString str)
{
    log_text->setHtml(str.append(log_text->toHtml()));
}

void Doovde::Gui::LogWindow::readSettings()
{
    QSettings settings("uTech Enterprises", "Doovdé");
    QPoint winpos = settings.value("log_pos", QPoint(0, 350)).toPoint();
    QSize winsz = settings.value("log_sz", QSize(480, 360)).toSize();
    move(winpos);
    resize(winsz);
}

void Doovde::Gui::LogWindow::writeSettings()
{
    QSettings settings("uTech Enterprises", "Doovdé");
    settings.setValue("log_pos", pos());
    settings.setValue("log_sz", size());
}
