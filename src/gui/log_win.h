
#ifndef DOOVDE_LOGWIN_H_
#define DOOVDE_LOGWIN_H_

#include <QMainWindow>

class QString;
class QTextEdit;

namespace Doovde {
    namespace Gui {
        class LogWindow : public QMainWindow {
            Q_OBJECT
        public:
            LogWindow();
            void appendToHtml(QString str);

        protected:
            void closeEvent(QCloseEvent *event);

        private slots:
            void clear();
            void updateLog();

        private:
            void readSettings();
            void writeSettings();

            QTextEdit* log_text;
            QTimer* update_timer;
        };
    }
}

#endif
