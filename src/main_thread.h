
#ifndef DOOVDE_MAIN_THREAD_H_
#define DOOVDE_MAIN_THREAD_H_

#include <QThread>
#include <QMutex>

namespace Doovde {
    class MainThread : public QThread {
        Q_OBJECT
    public:
        MainThread(QObject *parent = 0);
        ~MainThread();
        bool stopProcess();
        bool pauseProcess();

    protected:
        void run();

    private slots:

    private:
        bool running;
        bool stopped;
        QMutex mutex;
    };
}

#endif
