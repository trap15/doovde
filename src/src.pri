SRCPATH    = src
HEADERS   += $${SRCPATH}/log.h $${SRCPATH}/main_thread.h
SOURCES   += $${SRCPATH}/log.cpp $${SRCPATH}/main_thread.cpp \
             $${SRCPATH}/main.cpp
RESOURCES += 

include(gui/gui.pri)
include(core/core.pri)
include(plugin/plugin.pri)
include(cpu/cpu.pri)
include(mem/mem.pri)
include(gfx/gfx.pri)
include(io/io.pri)
include(crypto/crypto.pri)
