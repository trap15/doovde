
#ifndef DOOVDE_LOG_H_
#define DOOVDE_LOG_H_

#include <stdarg.h>

namespace Doovde {
    namespace LogLevel {
        enum {
            ERROR = 0,
            WARNING,
            MISC,
            DEBUG
        };
    }
    void log(int level, char* fmt, ...);
    void logUpdate();
}

extern int cur_log_level;

extern "C" {
    void _plugin_log(int level, char* bin, char* fmt, va_list ap);
}

#endif
