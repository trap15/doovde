//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//---  Doovde includes
#include "log.h"
#include "cpu/allegrex.h"
#include "mem/mem.h"
#include "io/io.h"
#include "io/ge.h"

Doovde::IoDevice::Ge::Ge()
{
    alive = true;
}

Doovde::IoDevice::Ge::~Ge()
{
    alive = false;
}

void Doovde::IoDevice::Ge::reset()
{
    cycles_left = 0;
    ge_reset = false;

    // EDRAM stuff
    edram.reset = false;
    edram.size = 8; // 8KiB I guess

    // Display list stuff
    dlist.addr = 0;
    dlist.running = false;
}

int Doovde::IoDevice::Ge::run(int cycles)
{
    cycles_left += cycles;
    int startcycles = cycles_left;
    while(cycles_left > 0) {
        cycles_left -= runSingle();
    }
    return startcycles - cycles_left;
}

int Doovde::IoDevice::Ge::runSingle()
{
    ge_reset = false;
    edram.reset = false;
    dlist.running = false;
    return 1;
}

uint32_t Doovde::IoDevice::Ge::read(Doovde::Allegrex* cpu, uint32_t addr)
{
    switch(addr) {
        case 0x1D400000: // Reset?
            if(ge_reset)
                return 0xFFFFFFFF;
            return 0;
        case 0x1D400008: // EDRAM size
            return edram.size;
        case 0x1D400100: // Execute display list
            if(dlist.running)
                return 0xFFFFFFFF;
            return 0;
        case 0x1D400108: // Display list address
            return dlist.addr;
        case 0x1D500010: // EDRAM reset?
            if(edram.reset)
                return 0xFFFFFFFF;
            return 0;

        default:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad GE read %s %08X", SOURCENAME(cpu), addr);
            return 0;
    }
}

void Doovde::IoDevice::Ge::write(Doovde::Allegrex* cpu, uint32_t addr, uint32_t val)
{
    switch(addr) {
        case 0x1D400000: // Reset?
            ge_reset = true;
            break;
        case 0x1D400100: // Execute display list
            // Doesn't require writing 1... Is everything like this?
            dlist.running = true;
            break;
        case 0x1D400108: // Display list address
            dlist.addr = val;
            break;
        case 0x1D500010: // EDRAM reset?
            edram.reset = true;
            break;

        default:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad GE write %s %08X %08X", SOURCENAME(cpu), addr, val);
    }
}
