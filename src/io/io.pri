IOPATH    = src/io
HEADERS   += $${IOPATH}/io.h \
             $${IOPATH}/intmng.h \
             $${IOPATH}/kirk.h \
             $${IOPATH}/ge.h \
             $${IOPATH}/gpio.h \
             $${IOPATH}/lcdc.h \
             $${IOPATH}/mectrl.h \
             $${IOPATH}/memmng.h \
             $${IOPATH}/nand.h \
             $${IOPATH}/prof.h \
             $${IOPATH}/syscon.h \
             $${IOPATH}/uart.h \

SOURCES   += $${IOPATH}/io.cpp \
             $${IOPATH}/intmng.cpp \
             $${IOPATH}/kirk.cpp \
             $${IOPATH}/ge.cpp \
             $${IOPATH}/gpio.cpp \
             $${IOPATH}/lcdc.cpp \
             $${IOPATH}/mectrl.cpp \
             $${IOPATH}/memmng.cpp \
             $${IOPATH}/nand.cpp \
             $${IOPATH}/prof.cpp \
             $${IOPATH}/syscon.cpp \
             $${IOPATH}/uart.cpp \

RESOURCES +=
