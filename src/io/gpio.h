
#ifndef DOOVDE_IO_GPIO_H_
#define DOOVDE_IO_GPIO_H_

namespace Doovde {
    struct Allegrex;
    namespace IoDevice {
        struct Gpio {
            Gpio();
            ~Gpio();
            //---  Variables
            bool alive;
            int cycles_left;

            //---  Functions
            void reset();
            int run(int cycles);
            int runSingle();
            uint32_t read(Allegrex* cpu, uint32_t addr);
            void write(Allegrex* cpu, uint32_t addr, uint32_t val);
        };
    };
}

#endif
