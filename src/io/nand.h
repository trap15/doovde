
#ifndef DOOVDE_IO_NAND_H_
#define DOOVDE_IO_NAND_H_

namespace Doovde {
    struct Allegrex;
    namespace IoDevice {
        struct Nand {
            Nand();
            ~Nand();
            //---  Enums
            enum Commands {
                CMD_READ1     = 0x00,
                CMD_COPY_PRG0 = 0x00, // ???
                CMD_READ1X    = 0x01,
                CMD_PAGE_PRG1 = 0x10,
                CMD_READ2     = 0x50,
                CMD_ERASE0    = 0x60,
                CMD_STATUS    = 0x70,
                CMD_PAGE_PRG0 = 0x80,
                CMD_COPY_PRG1 = 0x8A,
                CMD_READ_ID   = 0x90,
                CMD_ERASE1    = 0xD0,
                CMD_RESET     = 0xFF
            };

            enum Sizes {
                SIZE_PAGE   = 0x200, // In bytes
                SIZE_SPARE  = 0x10,  // In bytes
                SIZE_BLOCK  = 0x20,  // In pages
                SIZE_DEVICE = 0x800  // In blocks
            };

            //---  Variables
            bool alive;
            int cycles_left;

            int size;
            void* mem;

            bool write_prot;
            bool ready;

            bool cmd_written;
            uint8_t cmd;
            uint8_t data[4];
            uint16_t nandaddr;
            uint16_t dmaaddr;
            uint32_t dmactrl;
            uint32_t dmastatus;

            uint8_t buffer[0x1000];

            //---  Functions
            void reset();
            bool load(const char *file);
            bool save(const char *file);
            int run(int cycles);
            int runSingle();
            uint32_t read(Allegrex* cpu, uint32_t addr);
            void write(Allegrex* cpu, uint32_t addr, uint32_t val);

        private:
            uint32_t calcAddress(int block, int page, int byte);
            void handleCommand();
            void handleDma();
        };
    }
}

#endif
