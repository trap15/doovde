
#ifndef DOOVDE_IO_GE_H_
#define DOOVDE_IO_GE_H_

namespace Doovde {
    struct Allegrex;
    namespace IoDevice {
        struct Ge {
            Ge();
            ~Ge();
            //---  Variables
            bool alive;
            int cycles_left;

            bool ge_reset;
            struct Edram {
                bool reset;
                int size;
            } edram;
            struct Dlist {
                bool running;
                uint32_t addr;
            } dlist;

            //---  Functions
            void reset();
            int run(int cycles);
            int runSingle();
            uint32_t read(Allegrex* cpu, uint32_t addr);
            void write(Allegrex* cpu, uint32_t addr, uint32_t val);
        };
    };
}

#endif
