//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//---  Doovde includes
#include "log.h"
#include "cpu/allegrex.h"
#include "mem/mem.h"
#include "io/io.h"
#include "io/intmng.h"

// TODO: Everything

Doovde::IoDevice::IntMng::IntMng()
{
    alive = true;
}

Doovde::IoDevice::IntMng::~IntMng()
{
    alive = false;
}

void Doovde::IoDevice::IntMng::reset()
{
    cycles_left = 0;
}

int Doovde::IoDevice::IntMng::run(int cycles)
{
    cycles_left += cycles;
    int startcycles = cycles_left;
    while(cycles_left > 0) {
        cycles_left -= runSingle();
    }
    return startcycles - cycles_left;
}

int Doovde::IoDevice::IntMng::runSingle()
{
    return 1;
}

uint32_t Doovde::IoDevice::IntMng::read(Doovde::Allegrex* cpu, uint32_t addr)
{
    switch(addr) {
        case 0x1C300000: // Some enable?
            goto intman_unk_read;
        case 0x1C300008: // Some ack?
            goto intman_unk_read;
        case 0x1C300010: // Some mask?
            goto intman_unk_read;
        case 0x1C300018: // Some mask?
            goto intman_unk_read;

        default:
intman_unk_read:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad Interrupt Manager read %s %08X", SOURCENAME(cpu), addr);
            return 0;
    }
}

void Doovde::IoDevice::IntMng::write(Doovde::Allegrex* cpu, uint32_t addr, uint32_t val)
{
    switch(addr) {
        case 0x1C300000: // Some enable?
            goto intman_unk_write;
        case 0x1C300008: // Some ack?
            goto intman_unk_write;
        case 0x1C300010: // Some mask?
            goto intman_unk_write;
        case 0x1C300018: // Some mask?
            goto intman_unk_write;

        default:
intman_unk_write:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad Interrupt Manager write %s %08X %08X", SOURCENAME(cpu), addr, val);
    }
}
