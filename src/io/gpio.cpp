//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//---  Doovde includes
#include "log.h"
#include "cpu/allegrex.h"
#include "mem/mem.h"
#include "io/io.h"
#include "io/gpio.h"

Doovde::IoDevice::Gpio::Gpio()
{
    alive = true;
}

Doovde::IoDevice::Gpio::~Gpio()
{
    alive = false;
}

void Doovde::IoDevice::Gpio::reset()
{
    cycles_left = 0;
}

int Doovde::IoDevice::Gpio::run(int cycles)
{
    cycles_left += cycles;
    int startcycles = cycles_left;
    while(cycles_left > 0) {
        cycles_left -= runSingle();
    }
    return startcycles - cycles_left;
}

int Doovde::IoDevice::Gpio::runSingle()
{
    return 1;
}

uint32_t Doovde::IoDevice::Gpio::read(Doovde::Allegrex* cpu, uint32_t addr)
{
    switch(addr) {
        case 0x1E240004: // Port Read
            // bit4:  Service mode???
            return 0 | (1<<4);
        case 0x1E240008: // Port Write
            goto gpio_unk_read;
        case 0x1E24000C: // Port Clear
            // TODO: Clear GPIO target?
            goto gpio_unk_read;

        default:
gpio_unk_read:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad GPIO read %s %08X", SOURCENAME(cpu), addr);
            return 0;
    }
}

void Doovde::IoDevice::Gpio::write(Doovde::Allegrex* cpu, uint32_t addr, uint32_t val)
{
    switch(addr) {
        case 0x1E240004: // Port Read
            goto gpio_unk_write;
        case 0x1E240008: // Port Write
            // TODO: Write to GPIO target?
            break;
        case 0x1E24000C: // Port Clear
            // TODO: Clear GPIO target?
            break;

        default:
gpio_unk_write:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad GPIO write %s %08X %08X", SOURCENAME(cpu), addr, val);
    }
}
