
#ifndef DOOVDE_IO_KIRK_H_
#define DOOVDE_IO_KIRK_H_

namespace Doovde {
    struct Allegrex;
    namespace Crypto {
        struct AesCbc;
    }
    namespace IoDevice {
        struct Kirk {
            Kirk();
            ~Kirk();
            //---  Enums
            enum Commands {
                CMD_DECRYPT    = 0x01, // Super Decryption
                CMD_ENCx       = 0x02, // Encrypt
                CMD_DECx       = 0x03, // Decrypt
                CMD_ENC_IV0    = 0x04, // Encrypt, IV=0
                CMD_ENC_IVF    = 0x05, // Encrypt, IV=FuseID
                CMD_ENC_IVU    = 0x06, // Encrypt, IV=User Defined
                CMD_DEC_IV0    = 0x07, // Decrypt, IV=0
                CMD_DEC_IVF    = 0x08, // Decrypt, IV=FuseID
                CMD_DEC_IVU    = 0x09, // Decrypt, IV=User Defined
                CMD_PSIG_CHECK = 0x0A, // Private Signature Check
                CMD_SHA1       = 0x0B, // SHA-1 Hash
                CMD_ECDSA_GEN  = 0x0C, // Signature Key Gen
                CMD_ECDSA_MUL  = 0x0D, // Signature Mult Point
                CMD_PRNG       = 0x0E, // PRNG
                CMD_PRNG_SEED  = 0x0F, // Maybe seed?
                CMD_SIG_GEN    = 0x10, // Signature Gen
                CMD_SIG_CHECK  = 0x11, // Signature Check
                CMD_CERT_CHECK = 0x12  // Certificate Check
            };

            enum HeaderMode {
                MODE_CMD1 = 1,
                MODE_CMD2 = 2,
                MODE_CMD3 = 3,
                MODE_ENC_CBC = 4,
                MODE_DEC_CBC = 5
            };

            //---  Variables
            bool alive;
            int cycles_left;

            bool error;
            int process_phase;
            int cmd;
            uint32_t result;
            uint32_t source_addr;
            uint32_t dest_addr;
            uint32_t status;

            Doovde::Crypto::AesCbc* key1;

            //---  Functions
            void reset();
            int run(int cycles);
            int runSingle();
            uint32_t read(Allegrex* cpu, uint32_t addr);
            void write(Allegrex* cpu, uint32_t addr, uint32_t val);
            bool getKey(int c, int key, uint8_t *dst);

        private:
            void processCmd();
            void processCmd01();
            void processCmd02();
            void processCmd03();
            void processCmd04();
            void processCmd05();
            void processCmd06();
            void processCmd07();
            void processCmd08();
            void processCmd09();
            void processCmd0A();
            void processCmd0B();
            void processCmd0C();
            void processCmd0D();
            void processCmd0E();
            void processCmd0F();
            void processCmd10();
            void processCmd11();
            void processCmd12();
        };
    }
}

#endif
