//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//---  Doovde includes
#include "log.h"
#include "cpu/allegrex.h"
#include "mem/mem.h"
#include "io/io.h"
#include "io/prof.h"

// TODO: Everything

Doovde::IoDevice::Profiler::Profiler()
{
    alive = true;
}

Doovde::IoDevice::Profiler::~Profiler()
{
    alive = false;
}

void Doovde::IoDevice::Profiler::reset()
{
    cycles_left = 0;
}

int Doovde::IoDevice::Profiler::run(int cycles)
{
    cycles_left += cycles;
    int startcycles = cycles_left;
    while(cycles_left > 0) {
        cycles_left -= runSingle();
    }
    return startcycles - cycles_left;
}

int Doovde::IoDevice::Profiler::runSingle()
{
    return 1;
}

uint32_t Doovde::IoDevice::Profiler::read(Doovde::Allegrex* cpu, uint32_t addr)
{
    switch(addr) {
        case 0x1C400000: // Enable
            goto prof_unk_read;

        default:
prof_unk_read:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad Profiler read %s %08X", SOURCENAME(cpu), addr);
            return 0;
    }
}

void Doovde::IoDevice::Profiler::write(Doovde::Allegrex* cpu, uint32_t addr, uint32_t val)
{
    switch(addr) {
        case 0x1C400000: // Enable
            goto prof_unk_write;

        default:
prof_unk_write:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad Profiler write %s %08X %08X", SOURCENAME(cpu), addr, val);
    }
}
