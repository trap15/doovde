
#ifndef DOOVDE_IO_MECTRL_H_
#define DOOVDE_IO_MECTRL_H_

namespace Doovde {
    struct Allegrex;
    namespace IoDevice {
        struct MeCtrl {
            MeCtrl();
            ~MeCtrl();
            //---  Variables
            bool alive;
            int cycles_left;

            bool me_reset;

            //---  Functions
            void reset();
            int run(int cycles);
            int runSingle();
            uint32_t read(Allegrex* cpu, uint32_t addr);
            void write(Allegrex* cpu, uint32_t addr, uint32_t val);
        };
    };
}

#endif
