//---  System includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//---  Doovde includes
#include "log.h"
#include "core/core.h"
#include "mem/mem.h"
#include "cpu/allegrex.h"
#include "crypto/crypto.h"
#include "io/io.h"
#include "io/kirk.h"

Doovde::IoDevice::Kirk::Kirk()
{
    uint8_t k[16] = {0x98, 0xC9, 0x40, 0x97,
                     0x5C, 0x1D, 0x10, 0xE8,
                     0x7F, 0xE6, 0x0E, 0xA3,
                     0xFD, 0x03, 0xA8, 0xBA};
    key1 = new Doovde::Crypto::AesCbc(k);
    alive = true;
}

Doovde::IoDevice::Kirk::~Kirk()
{
    alive = false;
    delete key1;
}

void Doovde::IoDevice::Kirk::reset()
{
    cycles_left = 0;

    error = false;
    process_phase = 0;
    result = 0;
    status = 0;
    cmd = 0;
}

int Doovde::IoDevice::Kirk::run(int cycles)
{
    cycles_left += cycles;
    int startcycles = cycles_left;
    while(cycles_left > 0) {
        cycles_left -= runSingle();
    }
    return startcycles - cycles_left;
}

int Doovde::IoDevice::Kirk::runSingle()
{
    if(process_phase != 0)
        processCmd();
    return 1;
}

uint32_t Doovde::IoDevice::Kirk::read(Doovde::Allegrex* cpu, uint32_t addr)
{
    switch(addr) {
        case 0x1DE00000: // Signature
            return ('K' << 24) | ('I' << 16) | ('R' << 8) | 'K';
        case 0x1DE00004: // Version
            return ('0' << 24) | ('0' << 16) | ('1' << 8) | '0';
        case 0x1DE00008: // Error
            if(error)
                return 1;
            return 0;
        case 0x1DE0000C: // Start Processing
            goto kirk_unk_read;
        case 0x1DE00010: // Command
            return cmd;
        case 0x1DE00014: // Result
            return result;
        case 0x1DE0001C: // Status
            return status;
        case 0x1DE0002C: // Source Address
            return source_addr;
        case 0x1DE00030: // Destination Address
            return dest_addr;

        default:
kirk_unk_read:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad KIRK read %s %08X", SOURCENAME(cpu), addr);
            return 0;
    }
}

void Doovde::IoDevice::Kirk::write(Doovde::Allegrex* cpu, uint32_t addr, uint32_t val)
{
    switch(addr) {
        case 0x1DE00000: // Signature
            goto kirk_unk_write;
        case 0x1DE00004: // Version
            goto kirk_unk_write;
        case 0x1DE00008: // Error
            goto kirk_unk_write;
        case 0x1DE0000C: // Start Processing
            process_phase = val & 3;
            break;
        case 0x1DE00010: // Command
            cmd = val & 0x1F;
            break;
        case 0x1DE00014: // Result
            break;
        case 0x1DE0001C: // Status
            goto kirk_unk_write;
        case 0x1DE0002C: // Source Address
            source_addr = val;
            break;
        case 0x1DE00030: // Destination Address
            dest_addr = val;
            break;

        default:
kirk_unk_write:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad KIRK write %s %08X %08X", SOURCENAME(cpu), addr, val);
    }
}

bool Doovde::IoDevice::Kirk::getKey(int c, int key, uint8_t *dst)
{
    switch(c) {
        case CMD_DECRYPT: // Super-secret
            switch(key) {
                case 0x00: { // AES
                    uint8_t k[16] = {0x98, 0xC9, 0x40, 0x97,
                                     0x5C, 0x1D, 0x10, 0xE8,
                                     0x7F, 0xE6, 0x0E, 0xA3,
                                     0xFD, 0x03, 0xA8, 0xBA};
                    memcpy(dst, k, 16);
                    break; }
                case 0x01: { // ECC b
                    uint8_t k[20] = {0x65, 0xD1, 0x48, 0x8C,
                                     0x03, 0x59, 0xE2, 0x34,
                                     0xAD, 0xC9, 0x5B, 0xD3,
                                     0x90, 0x80, 0x14, 0xBD,
                                     0x91, 0xA5, 0x25, 0xF9};
                    memcpy(dst, k, 20);
                    break; }
                case 0x02: { // ECC N
                    uint8_t k[21] = {0x00,
                                     0xFF, 0xFF, 0xFF, 0xFF,
                                     0xFF, 0xFF, 0xFF, 0xFF,
                                     0x00, 0x01, 0xB5, 0xC6,
                                     0x17, 0xF2, 0x90, 0xEA,
                                     0xE1, 0xDB, 0xAD, 0x8F};
                    memcpy(dst, k, 21);
                    break; }
                case 0x03: { // ECC Gx
                    uint8_t k[20] = {0x22, 0x59, 0xAC, 0xEE,
                                     0x15, 0x48, 0x9C, 0xB0,
                                     0x96, 0xA8, 0x82, 0xF0,
                                     0xAE, 0x1C, 0xF9, 0xFD,
                                     0x8E, 0xE5, 0xF8, 0xFA};
                    memcpy(dst, k, 20);
                    break; }
                case 0x04: { // ECC Gy
                    uint8_t k[20] = {0x60, 0x43, 0x58, 0x45,
                                     0x6D, 0x0A, 0x1C, 0xB2,
                                     0x90, 0x8D, 0xE9, 0x0F,
                                     0x27, 0xD7, 0x5C, 0x82,
                                     0xBE, 0xC1, 0x08, 0xC0};
                    memcpy(dst, k, 20);
                    break; }
                case 0x05: { // ECC Px
                    uint8_t k[20] = {0xED, 0x9C, 0xE5, 0x82,
                                     0x34, 0xE6, 0x1A, 0x53,
                                     0xC6, 0x85, 0xD6, 0x4D,
                                     0x51, 0xD0, 0x23, 0x6B,
                                     0xC3, 0xB5, 0xD4, 0xB9};
                    memcpy(dst, k, 20);
                    break; }
                case 0x06: { // ECC Py
                    uint8_t k[20] = {0x04, 0x9D, 0xF1, 0xA0,
                                     0x75, 0xC0, 0xE0, 0x4F,
                                     0xB3, 0x44, 0x85, 0x8B,
                                     0x61, 0xB7, 0x9B, 0x69,
                                     0xA6, 0x3D, 0x2C, 0x39};
                    memcpy(dst, k, 20);
                    break; }
                case 0x80: case 0x81: { // ECC
                    getKey(CMD_SIG_CHECK, key, dst);
                    break; }
            }
            break;
        case CMD_ENCx:
            break;
        case CMD_DECx:
            switch(key) {
                case 0x00: { // ? A decent guess
                    getKey(CMD_DECRYPT, key, dst);
                    break; }
            }
            break;
        case CMD_ENC_IV0: case CMD_ENC_IVF: case CMD_ENC_IVU:
        case CMD_DEC_IV0: case CMD_DEC_IVF: case CMD_DEC_IVU:
            switch(key) {
                case 0x03: {
                    uint8_t k[16] = {0x98, 0x02, 0xC4, 0xE6,
                                     0xEC, 0x9E, 0x9E, 0x2F,
                                     0xFC, 0x63, 0x4C, 0xE4,
                                     0x2F, 0xBB, 0x46, 0x68};
                    memcpy(dst, k, 16);
                    break; }
                case 0x04: {
                    uint8_t k[16] = {0x99, 0x24, 0x4C, 0xD2,
                                     0x58, 0xF5, 0x1B, 0xCB,
                                     0xB0, 0x61, 0x9C, 0xA7,
                                     0x38, 0x30, 0x07, 0x5F};
                    memcpy(dst, k, 16);
                    break; }
                case 0x05: {
                    uint8_t k[16] = {0x02, 0x25, 0xD7, 0xBA,
                                     0x63, 0xEC, 0xB9, 0x4A,
                                     0x9D, 0x23, 0x76, 0x01,
                                     0xB3, 0xF6, 0xAC, 0x17};
                    memcpy(dst, k, 16);
                    break; }
                case 0x0C: {
                    uint8_t k[16] = {0x84, 0x85, 0xC8, 0x48,
                                     0x75, 0x08, 0x43, 0xBC,
                                     0x9B, 0x9A, 0xEC, 0xA7,
                                     0x9C, 0x7F, 0x60, 0x18};
                    memcpy(dst, k, 16);
                    break; }
                case 0x0D: {
                    uint8_t k[16] = {0xB5, 0xB1, 0x6E, 0xDE,
                                     0x23, 0xA9, 0x7B, 0x0E,
                                     0xA1, 0x7C, 0xDB, 0xA2,
                                     0xDC, 0xDE, 0xC4, 0x6E};
                    memcpy(dst, k, 16);
                    break; }
                case 0x0E: {
                    uint8_t k[16] = {0xC8, 0x71, 0xFD, 0xB3,
                                     0xBC, 0xC5, 0xD2, 0xF2,
                                     0xE2, 0xD7, 0x72, 0x9D,
                                     0xDF, 0x82, 0x68, 0x82};
                    memcpy(dst, k, 16);
                    break; }
                case 0x0F: {
                    uint8_t k[16] = {0x0A, 0xBB, 0x33, 0x6C,
                                     0x96, 0xD4, 0xCD, 0xD8,
                                     0xCB, 0x5F, 0x4B, 0xE0,
                                     0xBA, 0xDB, 0x9E, 0x03};
                    memcpy(dst, k, 16);
                    break; }
                case 0x10: {
                    uint8_t k[16] = {0x32, 0x29, 0x5B, 0xD5,
                                     0xEA, 0xF7, 0xA3, 0x42,
                                     0x16, 0xC8, 0x8E, 0x48,
                                     0xFF, 0x50, 0xD3, 0x71};
                    memcpy(dst, k, 16);
                    break; }
                case 0x11: {
                    uint8_t k[16] = {0x46, 0xF2, 0x5E, 0x8E,
                                     0x4D, 0x2A, 0xA5, 0x40,
                                     0x73, 0x0B, 0xC4, 0x6E,
                                     0x47, 0xEE, 0x6F, 0x0A};
                    memcpy(dst, k, 16);
                    break; }
                case 0x12: {
                    uint8_t k[16] = {0x5D, 0xC7, 0x11, 0x39,
                                     0xD0, 0x19, 0x38, 0xBC,
                                     0x02, 0x7F, 0xDD, 0xDC,
                                     0xB0, 0x83, 0x7D, 0x9D};
                    memcpy(dst, k, 16);
                    break; }
                case 0x38: {
                    uint8_t k[16] = {0x12, 0x46, 0x8D, 0x7E,
                                     0x1C, 0x42, 0x20, 0x9B,
                                     0xBA, 0x54, 0x26, 0x83,
                                     0x5E, 0xB0, 0x33, 0x03};
                    memcpy(dst, k, 16);
                    break; }
                case 0x39: {
                    uint8_t k[16] = {0xC4, 0x3B, 0xB6, 0xD6,
                                     0x53, 0xEE, 0x67, 0x49,
                                     0x3E, 0xA9, 0x5F, 0xBC,
                                     0x0C, 0xED, 0x6F, 0x8A};
                    memcpy(dst, k, 16);
                    break; }
                case 0x3A: {
                    uint8_t k[16] = {0x2C, 0xC3, 0xCF, 0x8C,
                                     0x28, 0x78, 0xA5, 0xA6,
                                     0x63, 0xE2, 0xAF, 0x2D,
                                     0x71, 0x5E, 0x86, 0xBA};
                    memcpy(dst, k, 16);
                    break; }
                case 0x4B: {
                    uint8_t k[16] = {0x0C, 0xFD, 0x67, 0x9A,
                                     0xF9, 0xB4, 0x72, 0x4F,
                                     0xD7, 0x8D, 0xD6, 0xE9,
                                     0x96, 0x42, 0x28, 0x8B};
                    memcpy(dst, k, 16);
                    break; }
                case 0x53: {
                    uint8_t k[16] = {0xAF, 0xFE, 0x8E, 0xB1,
                                     0x3D, 0xD1, 0x7E, 0xD8,
                                     0x0A, 0x61, 0x24, 0x1C,
                                     0x95, 0x92, 0x56, 0xB6};
                    memcpy(dst, k, 16);
                    break; }
                case 0x57: {
                    uint8_t k[16] = {0x1C, 0x9B, 0xC4, 0x90,
                                     0xE3, 0x06, 0x64, 0x81,
                                     0xFA, 0x59, 0xFD, 0xB6,
                                     0x00, 0xBB, 0x28, 0x70};
                    memcpy(dst, k, 16);
                    break; }
                case 0x5D: {
                    uint8_t k[16] = {0x11, 0x5A, 0x5D, 0x20,
                                     0xD5, 0x3A, 0x8D, 0xD3,
                                     0x9C, 0xC5, 0xAF, 0x41,
                                     0x0F, 0x0F, 0x18, 0x6F};
                    memcpy(dst, k, 16);
                    break; }
                case 0x63: {
                    uint8_t k[16] = {0x9C, 0x9B, 0x13, 0x72,
                                     0xF8, 0xC6, 0x40, 0xCF,
                                     0x1C, 0x62, 0xF5, 0xD5,
                                     0x92, 0xDD, 0xB5, 0x82};
                    memcpy(dst, k, 16);
                    break; }
                case 0x64: {
                    uint8_t k[16] = {0x03, 0xB3, 0x02, 0xE8,
                                     0x5F, 0xF3, 0x81, 0xB1,
                                     0x3B, 0x8D, 0xAA, 0x2A,
                                     0x90, 0xFF, 0x5E, 0x61};
                    memcpy(dst, k, 16);
                    break; }
            }
            break;
        case CMD_SIG_GEN:
            switch(key) {
                case 0x00: { // AES
                    uint8_t k[16] = {0x47, 0x5E, 0x09, 0xF4,
                                     0xA2, 0x37, 0xDA, 0x9B,
                                     0xEF, 0xFF, 0x3B, 0xC0,
                                     0x77, 0x14, 0x3D, 0x8A};
                    memcpy(dst, k, 16);
                    break; }
                default:
                    getKey(CMD_ECDSA_MUL, key, dst);
                    break;
            }
            break;
        case CMD_SIG_CHECK:
            switch(key) {
                case 0x80: { // ECC p
                    uint8_t k[20] = {0xFF, 0xFF, 0xFF, 0xFF,
                                     0xFF, 0xFF, 0xFF, 0xFF,
                                     0x00, 0x00, 0x00, 0x01,
                                     0xFF, 0xFF, 0xFF, 0xFF,
                                     0xFF, 0xFF, 0xFF, 0xFF};
                    memcpy(dst, k, 20);
                    break; }
                case 0x81: { // ECC a
                    uint8_t k[20] = {0xFF, 0xFF, 0xFF, 0xFF,
                                     0xFF, 0xFF, 0xFF, 0xFF,
                                     0x00, 0x00, 0x00, 0x01,
                                     0xFF, 0xFF, 0xFF, 0xFF,
                                     0xFF, 0xFF, 0xFF, 0xFC};
                    memcpy(dst, k, 20);
                    break; }
                default:
                    getKey(CMD_ECDSA_MUL, key, dst);
                    break;
            }
            break;
        case CMD_ECDSA_MUL: case CMD_ECDSA_GEN:
        case CMD_CERT_CHECK: // ECC Curves
            switch(key) {
                case 0x00: { // ECC b
                    uint8_t k[20] = {0xA6, 0x8B, 0xED, 0xC3,
                                     0x34, 0x18, 0x02, 0x9C,
                                     0x1D, 0x3C, 0xE3, 0x3B,
                                     0x9A, 0x32, 0x1F, 0xCC,
                                     0xBB, 0x9E, 0x0F, 0x0B};
                    memcpy(dst, k, 20);
                    break; }
                case 0x01: { // ECC N
                    uint8_t k[21] = {0x00,
                                     0xFF, 0xFF, 0xFF, 0xFF,
                                     0xFF, 0xFF, 0xFF, 0xFE,
                                     0xFF, 0xFF, 0xB5, 0xAE,
                                     0x3C, 0x52, 0x3E, 0x63,
                                     0x94, 0x4F, 0x21, 0x27};
                    memcpy(dst, k, 21);
                    break; }
                case 0x02: { // ECC Gx
                    uint8_t k[20] = {0x12, 0x8E, 0xC4, 0x25,
                                     0x64, 0x87, 0xFD, 0x8F,
                                     0xDF, 0x64, 0xE2, 0x43,
                                     0x7B, 0xC0, 0xA1, 0xF6,
                                     0xD5, 0xAF, 0xDE, 0x2C};
                    memcpy(dst, k, 20);
                    break; }
                case 0x03: { // ECC Gy
                    uint8_t k[20] = {0x59, 0x58, 0x55, 0x7E,
                                     0xB1, 0xDB, 0x00, 0x12,
                                     0x60, 0x42, 0x55, 0x24,
                                     0xDB, 0xC3, 0x79, 0xD5,
                                     0xAC, 0x5F, 0x4A, 0xDF};
                    memcpy(dst, k, 20);
                    break; }
            }
            break;
    }
    return true;
}

void Doovde::IoDevice::Kirk::processCmd()
{
    Doovde::log(Doovde::LogLevel::MISC, "Processing KIRK command %d phase %d", cmd, process_phase);
    switch(cmd) {
        case CMD_DECRYPT:
            processCmd01();
            break;
        case CMD_ENCx:
            processCmd02();
            break;
        case CMD_DECx:
            processCmd03();
            break;
        case CMD_ENC_IV0:
            processCmd04();
            break;
        case CMD_ENC_IVF:
            processCmd05();
            break;
        case CMD_ENC_IVU:
            processCmd06();
            break;
        case CMD_DEC_IV0:
            processCmd07();
            break;
        case CMD_DEC_IVF:
            processCmd08();
            break;
        case CMD_DEC_IVU:
            processCmd09();
            break;
        case CMD_PSIG_CHECK:
            processCmd0A();
            break;
        case CMD_SHA1:
            processCmd0B();
            break;
        case CMD_ECDSA_GEN:
            processCmd0C();
            break;
        case CMD_ECDSA_MUL:
            processCmd0D();
            break;
        case CMD_PRNG:
            processCmd0E();
            break;
        case CMD_PRNG_SEED:
            processCmd0F();
            break;
        case CMD_SIG_GEN:
            processCmd10();
            break;
        case CMD_SIG_CHECK:
            processCmd11();
            break;
        case CMD_CERT_CHECK:
            processCmd12();
            break;
        default:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad KIRK command %d phase %d", cmd, process_phase);
            break;
    }
    process_phase = 0;
}

void Doovde::IoDevice::Kirk::processCmd01()
{
    uint8_t key[16];

    uint8_t header[0x30];
    uint8_t aes_key[16];
    uint8_t aes_key2[16];
    uint8_t hdr_sig_r[20];
    uint8_t hdr_sig_s[20];
    uint8_t data_sig_r[20];
    uint8_t data_sig_s[20];
    uint8_t ec_p[20], ec_a[20], ec_b[20], ec_N[21], Gx[20], Gy[20], Px[20], Py[20];

    uint8_t cmac_key[16];
    uint8_t cmac_key2[16];
    uint8_t cmac_hdr_hash[16];
    uint8_t cmac_data_hash[16];

    uint32_t mode;

    uint8_t ecdsa_hash;

    uint32_t data_size, data_off;

    uint8_t* src;
    uint8_t* dst;

    uint8_t dec_keys[16+16];

    uint8_t fullheader[0x100];

    uint32_t srcaddr = source_addr;

    for(int i=0; i < 0x100; i++) {
        if((i % 0x10) == 0)
            printf("%02X: ", i);
        fullheader[i] = core->sc_mem->read8(NULL, Doovde::Cop0::MODE_KERNEL, source_addr + i);
        printf("%02X ", fullheader[i]);
        if((i % 0x10) == 0xF)
            printf("\n");
    }
    for(int i=0; i < 16; i++) {
        aes_key[i] = core->sc_mem->read8(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr++;
    }
    for(int i=0; i < 16; i++) {
        cmac_key[i] = core->sc_mem->read8(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr++;
        hdr_sig_r[i] = cmac_key[i];
    }
    for(int i=0; i < 16; i++) {
        cmac_hdr_hash[i] = core->sc_mem->read8(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr++;
        if(i < 4)
            hdr_sig_r[i+16] = cmac_hdr_hash[i];
        else
            hdr_sig_s[i-4] = cmac_hdr_hash[i];
    }
    for(int i=0; i < 16; i++) {
        cmac_data_hash[i] = core->sc_mem->read8(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr++;
        if(i < 8)
            hdr_sig_s[i+12] = cmac_data_hash[i];
        else
            data_sig_r[i-8] = cmac_data_hash[i];
    }
    for(int i=0; i < 12; i++) {
        data_sig_r[i+8] = core->sc_mem->read8(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr++;
    }
    for(int i=0; i < 20; i++) {
        data_sig_s[i] = core->sc_mem->read8(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr++;
    }
    for(int i=0; i < 0x30; i++) {
        header[i] = core->sc_mem->read8(NULL, Doovde::Cop0::MODE_KERNEL, source_addr + i);
    }
    mode = core->sc_mem->read32(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr+=4;
    ecdsa_hash = core->sc_mem->read8(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr++;
    source_addr += 11;
    data_size = core->sc_mem->read32(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr+=4;
    data_off = core->sc_mem->read32(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr+=4;
    source_addr += 8;
    source_addr += 16;

    if(mode != MODE_CMD1) { // Bad mode
        Doovde::log(Doovde::LogLevel::WARNING, "Bad mode in KIRK command %d: %d (expected %d)", cmd, mode, MODE_CMD1);
        status |= 0x10;
        return;
    }

    // Get the necessary keys
    getKey(CMD_DECRYPT, 0x80, ec_p);
    getKey(CMD_DECRYPT, 0x81, ec_a);
    getKey(CMD_DECRYPT, 0, key);
    getKey(CMD_DECRYPT, 1, ec_b);
    getKey(CMD_DECRYPT, 2, ec_N);
    getKey(CMD_DECRYPT, 3, Gx);
    getKey(CMD_DECRYPT, 4, Gy);
    getKey(CMD_DECRYPT, 5, Px);
    getKey(CMD_DECRYPT, 6, Py);

    Doovde::log(Doovde::LogLevel::MISC, "Key1: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X",
                key[ 0], key[ 1], key[ 2], key[ 3],
                key[ 4], key[ 5], key[ 6], key[ 7],
                key[ 8], key[ 9], key[10], key[11],
                key[12], key[13], key[14], key[15]);
#define PK ec_p
    Doovde::log(Doovde::LogLevel::MISC, "P: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X",
                PK[ 0], PK[ 1], PK[ 2], PK[ 3],
                PK[ 4], PK[ 5], PK[ 6], PK[ 7],
                PK[ 8], PK[ 9], PK[10], PK[11],
                PK[12], PK[13], PK[14], PK[15],
                PK[16], PK[17], PK[18], PK[19]);
#undef PK
#define PK ec_a
    Doovde::log(Doovde::LogLevel::MISC, "A: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X",
                PK[ 0], PK[ 1], PK[ 2], PK[ 3],
                PK[ 4], PK[ 5], PK[ 6], PK[ 7],
                PK[ 8], PK[ 9], PK[10], PK[11],
                PK[12], PK[13], PK[14], PK[15],
                PK[16], PK[17], PK[18], PK[19]);
#undef PK
#define PK ec_b
    Doovde::log(Doovde::LogLevel::MISC, "B: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X",
                PK[ 0], PK[ 1], PK[ 2], PK[ 3],
                PK[ 4], PK[ 5], PK[ 6], PK[ 7],
                PK[ 8], PK[ 9], PK[10], PK[11],
                PK[12], PK[13], PK[14], PK[15],
                PK[16], PK[17], PK[18], PK[19]);
#undef PK
#define PK ec_N
    Doovde::log(Doovde::LogLevel::MISC, "N: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X",
                PK[ 0], PK[ 1], PK[ 2], PK[ 3],
                PK[ 4], PK[ 5], PK[ 6], PK[ 7],
                PK[ 8], PK[ 9], PK[10], PK[11],
                PK[12], PK[13], PK[14], PK[15],
                PK[16], PK[17], PK[18], PK[19], PK[20]);
#undef PK
#define PK Gx
    Doovde::log(Doovde::LogLevel::MISC, "Gx: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X",
                PK[ 0], PK[ 1], PK[ 2], PK[ 3],
                PK[ 4], PK[ 5], PK[ 6], PK[ 7],
                PK[ 8], PK[ 9], PK[10], PK[11],
                PK[12], PK[13], PK[14], PK[15],
                PK[16], PK[17], PK[18], PK[19]);
#undef PK
#define PK Gy
    Doovde::log(Doovde::LogLevel::MISC, "Gy: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X",
                PK[ 0], PK[ 1], PK[ 2], PK[ 3],
                PK[ 4], PK[ 5], PK[ 6], PK[ 7],
                PK[ 8], PK[ 9], PK[10], PK[11],
                PK[12], PK[13], PK[14], PK[15],
                PK[16], PK[17], PK[18], PK[19]);
#undef PK
#define PK Px
    Doovde::log(Doovde::LogLevel::MISC, "Px: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X",
                PK[ 0], PK[ 1], PK[ 2], PK[ 3],
                PK[ 4], PK[ 5], PK[ 6], PK[ 7],
                PK[ 8], PK[ 9], PK[10], PK[11],
                PK[12], PK[13], PK[14], PK[15],
                PK[16], PK[17], PK[18], PK[19]);
#undef PK
#define PK Py
    Doovde::log(Doovde::LogLevel::MISC, "Py: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X",
                PK[ 0], PK[ 1], PK[ 2], PK[ 3],
                PK[ 4], PK[ 5], PK[ 6], PK[ 7],
                PK[ 8], PK[ 9], PK[10], PK[11],
                PK[12], PK[13], PK[14], PK[15],
                PK[16], PK[17], PK[18], PK[19]);

    /* Get the real AES and CMAC keys (decrypt with KIRK key 1) */
    uint8_t tmpkeys[16+16];
    for(int i=0; i < 16; i++) {
        tmpkeys[i] = aes_key[i];
        tmpkeys[i+16] = cmac_key[i];
    }
    key1->decrypt(dec_keys, tmpkeys, 16+16);
    for(int i=0; i < 16; i++) {
        aes_key2[i] = dec_keys[i];
        cmac_key2[i] = dec_keys[i+16];
    }

    if(ecdsa_hash == 1) { // Do shit
        Doovde::log(Doovde::LogLevel::MISC, "Doing hash checks");
        uint8_t header_hash[20];
        uint8_t data_hash[20];

        // Setup ECDSA
        Doovde::Crypto::ecdsa_curve(ec_p, ec_a, ec_b, ec_N, Gx, Gy);
        Doovde::Crypto::ecdsa_pubkey(Px, Py);

        // Hash and verify the header
        Doovde::Crypto::sha1(header_hash, header+0x60, 0x30);
        if(!Doovde::Crypto::ecdsa_verify(header_hash, hdr_sig_r, hdr_sig_s)) {
            Doovde::log(Doovde::LogLevel::WARNING, "Header hash invalid in KIRK command %d!", cmd);
            status |= 0x10;
            return;
        }

        // Hash and verify the data
        Doovde::Crypto::sha1(data_hash, header+0x60, data_size+data_off+0x30);
        if(!Doovde::Crypto::ecdsa_verify(data_hash, data_sig_r, data_sig_s)) {
            Doovde::log(Doovde::LogLevel::WARNING, "Data hash invalid in KIRK command %d!", cmd);
            status |= 0x10;
            return;
        }
    }else{ // CMD10
        Doovde::log(Doovde::LogLevel::MISC, "Doing alt. checks");
        source_addr = srcaddr;
        for(int i=0; i < 16; i++) {
            tmpkeys[i] = aes_key[i];
            tmpkeys[i+16] = cmac_key[i];
        }
        key1->decrypt(dec_keys, tmpkeys, 16+16);
        for(int i=0; i < 16; i++) {
            //aes_key2[i] = dec_keys[i];
            cmac_key2[i] = dec_keys[i+16];
        }
        if(status & 0x10)
            return;
    }

    Doovde::log(Doovde::LogLevel::MISC, "AES Key: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X",
                aes_key[ 0], aes_key[ 1], aes_key[ 2], aes_key[ 3],
                aes_key[ 4], aes_key[ 5], aes_key[ 6], aes_key[ 7],
                aes_key[ 8], aes_key[ 9], aes_key[10], aes_key[11],
                aes_key[12], aes_key[13], aes_key[14], aes_key[15]);
    src = (uint8_t*)malloc(data_size*4);
    dst = (uint8_t*)malloc(data_size*4);
    source_addr = srcaddr + 0x90 + data_off;
    for(uint32_t i=0; i < data_size*4; i++) {
        src[i] = core->sc_mem->read8(NULL, Doovde::Cop0::MODE_KERNEL, source_addr++);
    }
    printf("src:");
    for(uint32_t i=0; i < data_size*4; i++)
        printf(" %02X", src[i]);
    printf("\n");

    // Setup IV
    Doovde::Crypto::AesCbc aes(aes_key);
    aes.decrypt(dst, src, data_size*4);

    printf("dst:");
    for(uint32_t i=0; i < data_size*4; i++)
        printf(" %02X", dst[i]);
    printf("\n");
    for(uint32_t i=0; i < data_size*4; i++) {
        core->sc_mem->write8(NULL, Doovde::Cop0::MODE_KERNEL, dest_addr++, dst[i]);
    }
    free(src);
    free(dst);

    status |= 0x01;
}

void Doovde::IoDevice::Kirk::processCmd02()
{
    Doovde::log(Doovde::LogLevel::WARNING, "Unimplemented KIRK command %d phase %d", cmd, process_phase);
}

void Doovde::IoDevice::Kirk::processCmd03()
{
    Doovde::log(Doovde::LogLevel::WARNING, "Unimplemented KIRK command %d phase %d", cmd, process_phase);
}

// CMD_ENC_IV0
void Doovde::IoDevice::Kirk::processCmd04()
{
    uint32_t header[5];
    uint8_t iv[16];
    uint8_t key[16];
    uint8_t *src;
    uint8_t *dst;
    header[0] = core->sc_mem->read32(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr += 4;
    header[1] = core->sc_mem->read32(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr += 4;
    header[2] = core->sc_mem->read32(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr += 4;
    header[3] = core->sc_mem->read32(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr += 4;
    header[4] = core->sc_mem->read32(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr += 4;

    if(header[0] != MODE_ENC_CBC) { // Bad mode
        Doovde::log(Doovde::LogLevel::WARNING, "Bad mode in KIRK command %d: %d (expected %d)", cmd, header[0], MODE_ENC_CBC);
        status |= 0x10;
        return;
    }

    if(header[4] == 0) { // Size 0
        Doovde::log(Doovde::LogLevel::WARNING, "Size 0 to KIRK command %d", cmd);
        status |= 0x10;
        return;
    }

    // Get key
    getKey(CMD_ENC_IV0, header[3], key);

    src = (uint8_t*)malloc(header[4]);
    dst = (uint8_t*)malloc(header[4]);
    for(uint32_t i=0; i < header[4]; i++) {
        src[i] = core->sc_mem->read8(NULL, Doovde::Cop0::MODE_KERNEL, source_addr++);
    }

    Doovde::Crypto::AesCbc aes(key);
    aes.encrypt(dst, src, header[4]);

    for(uint32_t i=0; i < header[4]; i++) {
        core->sc_mem->write8(NULL, Doovde::Cop0::MODE_KERNEL, dest_addr++, dst[i]);
    }
    free(src);
    free(dst);

    status |= 1;
}

// CMD_ENC_IVF
void Doovde::IoDevice::Kirk::processCmd05()
{
    Doovde::log(Doovde::LogLevel::WARNING, "Unimplemented KIRK command %d phase %d", cmd, process_phase);
}

// CMD_ENC_IVU
void Doovde::IoDevice::Kirk::processCmd06()
{
    Doovde::log(Doovde::LogLevel::WARNING, "Unimplemented KIRK command %d phase %d", cmd, process_phase);
}

// CMD_DEC_IV0
void Doovde::IoDevice::Kirk::processCmd07()
{
    uint32_t header[5];
    uint8_t iv[16];
    uint8_t key[16];
    uint8_t *src;
    uint8_t *dst;
    header[0] = core->sc_mem->read32(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr += 4;
    header[1] = core->sc_mem->read32(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr += 4;
    header[2] = core->sc_mem->read32(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr += 4;
    header[3] = core->sc_mem->read32(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr += 4;
    header[4] = core->sc_mem->read32(NULL, Doovde::Cop0::MODE_KERNEL, source_addr); source_addr += 4;

    if(header[0] != MODE_DEC_CBC) { // Bad mode
        Doovde::log(Doovde::LogLevel::WARNING, "Bad mode in KIRK command %d: %d (expected %d)", cmd, header[0], MODE_DEC_CBC);
        status |= 0x10;
        return;
    }

    if(header[4] == 0) { // Size 0
        Doovde::log(Doovde::LogLevel::WARNING, "Size 0 to KIRK command %d", cmd);
        status |= 0x10;
        return;
    }

    // Get key
    getKey(CMD_DEC_IV0, header[3], key);

    src = (uint8_t*)malloc(header[4]);
    dst = (uint8_t*)malloc(header[4]);
    for(uint32_t i=0; i < header[4]; i++) {
        src[i] = core->sc_mem->read8(NULL, Doovde::Cop0::MODE_KERNEL, source_addr++);
    }

    Doovde::Crypto::AesCbc aes(key);
    aes.decrypt(dst, src, header[4]);

    for(uint32_t i=0; i < header[4]; i++) {
        core->sc_mem->write8(NULL, Doovde::Cop0::MODE_KERNEL, dest_addr++, dst[i]);
    }
    free(src);
    free(dst);

    status |= 0x01;
}

// CMD_DEC_IVF
void Doovde::IoDevice::Kirk::processCmd08()
{
    Doovde::log(Doovde::LogLevel::WARNING, "Unimplemented KIRK command %d phase %d", cmd, process_phase);
}

// CMD_DEC_IVU
void Doovde::IoDevice::Kirk::processCmd09()
{
    Doovde::log(Doovde::LogLevel::WARNING, "Unimplemented KIRK command %d phase %d", cmd, process_phase);
}

// CMD_PSIG_CHECK
void Doovde::IoDevice::Kirk::processCmd0A()
{

}

void Doovde::IoDevice::Kirk::processCmd0B()
{
    Doovde::log(Doovde::LogLevel::WARNING, "Unimplemented KIRK command %d phase %d", cmd, process_phase);
}

void Doovde::IoDevice::Kirk::processCmd0C()
{
    Doovde::log(Doovde::LogLevel::WARNING, "Unimplemented KIRK command %d phase %d", cmd, process_phase);
}

void Doovde::IoDevice::Kirk::processCmd0D()
{
    Doovde::log(Doovde::LogLevel::WARNING, "Unimplemented KIRK command %d phase %d", cmd, process_phase);
}

void Doovde::IoDevice::Kirk::processCmd0E()
{
    Doovde::log(Doovde::LogLevel::WARNING, "Unimplemented KIRK command %d phase %d", cmd, process_phase);
}

void Doovde::IoDevice::Kirk::processCmd0F()
{
    Doovde::log(Doovde::LogLevel::WARNING, "Unimplemented KIRK command %d phase %d", cmd, process_phase);
}

void Doovde::IoDevice::Kirk::processCmd10()
{
    Doovde::log(Doovde::LogLevel::WARNING, "Unimplemented KIRK command %d phase %d", cmd, process_phase);
}

void Doovde::IoDevice::Kirk::processCmd11()
{
    Doovde::log(Doovde::LogLevel::WARNING, "Unimplemented KIRK command %d phase %d", cmd, process_phase);
}

void Doovde::IoDevice::Kirk::processCmd12()
{
    Doovde::log(Doovde::LogLevel::WARNING, "Unimplemented KIRK command %d phase %d", cmd, process_phase);
}



