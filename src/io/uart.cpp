//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//---  Doovde includes
#include "log.h"
#include "cpu/allegrex.h"
#include "mem/mem.h"
#include "io/io.h"
#include "io/uart.h"

Doovde::IoDevice::Uart::Uart()
{
    alive = true;
}

Doovde::IoDevice::Uart::~Uart()
{
    alive = false;
}

void Doovde::IoDevice::Uart::reset()
{
    cycles_left = 0;

    txbuf_ptr = rxbuf_ptr = 0;
    txbuf_used = txbuf_used = 0;
    baudrate = 96000000;
    stored_baudrate = 1;
}

int Doovde::IoDevice::Uart::run(int cycles)
{
    cycles_left += cycles;
    int startcycles = cycles_left;
    while(cycles_left > 0) {
        cycles_left -= runSingle();
    }
    return startcycles - cycles_left;
}

int Doovde::IoDevice::Uart::runSingle()
{
    // TODO: Transfer/receive
    return 1;
}

uint32_t Doovde::IoDevice::Uart::read(Doovde::Allegrex* cpu, uint32_t addr)
{
    addr &= 0xFFFF;
    switch(addr) {
        case 0x0000: { // FIFO
            uint8_t ret = rxbuf[rxbuf_ptr++];
            if(rxbuf_used)
                rxbuf_used--;
            rxbuf_ptr %= RXBUF_SIZE;
            return ret;
        }
        case 0x0018: { // Status
            int ret = 0;
            if(txbuf_used == TXBUF_SIZE)
                ret |= 1 << 5;
            if(rxbuf_used == 0)
                ret |= 1 << 4;
            return ret;
        }
        case 0x0024: // Upper bits of Divisor
            return stored_baudrate >> 6;
        case 0x0028: // Low 6 bits of Divisor
            return stored_baudrate & 0x3F;
        case 0x002C: // Control
            goto uart_unk_read;

        default:
uart_unk_read:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad UART read %s %08X", SOURCENAME(cpu), addr);
            return 0;
    }
}

void Doovde::IoDevice::Uart::write(Doovde::Allegrex* cpu, uint32_t addr, uint32_t val)
{
    addr &= 0xFFFF;
    switch(addr) {
        case 0x0000: // FIFO
            txbuf[txbuf_ptr++];
            if(txbuf_used < TXBUF_SIZE)
                txbuf_used++;
            txbuf_ptr %= TXBUF_SIZE;
            break;
        case 0x0018: // Status
            break;
        case 0x0024: // Upper bits of Divisor
            // TODO: Maybe mask the input?
            stored_baudrate &= 0x3F;
            stored_baudrate |= val << 6;
            goto uart_unk_write;
        case 0x0028: // Low 6 bits of Divisor
            stored_baudrate &= ~0x3F;
            stored_baudrate |= val & 0x3F;
            break;
        case 0x002C: // Control
            if(val & (3 << 5)) { // TODO: Validate?
                baudrate = 96000000 / stored_baudrate;
            }
            goto uart_unk_write;

        default:
uart_unk_write:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad UART write %s %08X %08X", SOURCENAME(cpu), addr, val);
    }
}
