//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//---  Doovde includes
#include "log.h"
#include "cpu/allegrex.h"
#include "mem/mem.h"
#include "io/io.h"
#include "io/lcdc.h"

Doovde::IoDevice::Lcdc::Lcdc()
{
    alive = true;
}

Doovde::IoDevice::Lcdc::~Lcdc()
{
    alive = false;
}

void Doovde::IoDevice::Lcdc::reset()
{
    cycles_left = 0;

    enable    = 0;
    sync_diff = 0;
    x_back    = y_back    = 0;
    x_sync    = y_sync    = 0;
    x_front   = y_front   = 0;
    x_res     = y_res     = 0;
    x_pos     = y_pos     = 0;
    x_shift   = y_shift   = 0;
    x_res_scl = y_res_scl = 0;
}

int Doovde::IoDevice::Lcdc::run(int cycles)
{
    cycles_left += cycles;
    int startcycles = cycles_left;
    while(cycles_left > 0) {
        cycles_left -= runSingle();
    }
    return startcycles - cycles_left;
}

int Doovde::IoDevice::Lcdc::runSingle()
{
    return 1;
}

uint32_t Doovde::IoDevice::Lcdc::read(Doovde::Allegrex* cpu, uint32_t addr)
{
    switch(addr) {
        case 0x1E140000: // LCDC enable
            return enable;
        case 0x1E140004: // Sync difference
            return sync_diff;
        case 0x1E140008: // Unk?
            goto lcdc_unk_read;
        case 0x1E140010: // X back porch
            return x_back;
        case 0x1E140014: // X sync width
            return x_sync;
        case 0x1E140018: // X front porch
            return x_front;
        case 0x1E14001C: // X resolution
            return x_res;
        case 0x1E140020: // Y back porch
            return y_back;
        case 0x1E140024: // Y sync width
            return y_sync;
        case 0x1E140028: // Y front porch
            return y_front;
        case 0x1E14002C: // Y resolution
            return y_res;
        case 0x1E140030: // X raster position
            return x_pos;
        case 0x1E140034: // Y raster position
            return y_pos;
        case 0x1E140040: // Y shift
            return y_shift;
        case 0x1E140044: // X shift
            return x_shift;
        case 0x1E140048: // Scaled X resolution
            return x_res_scl;
        case 0x1E14004C: // Scaled Y resolution
            return y_res_scl;

        default:
lcdc_unk_read:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad LCDC read %s %08X", SOURCENAME(cpu), addr);
            return 0;
    }
}

void Doovde::IoDevice::Lcdc::write(Doovde::Allegrex* cpu, uint32_t addr, uint32_t val)
{
    switch(addr) {
        case 0x1E140000: // LCDC enable
            enable = val & 3; // ? How is this used
            break;
        case 0x1E140004: // Sync difference
            sync_diff = val;
            break;
        case 0x1E140008: // Unk?
            goto lcdc_unk_write;
        case 0x1E140010: // X back porch
            x_back = val;
            break;
        case 0x1E140014: // X sync width
            x_sync = val;
            break;
        case 0x1E140018: // X front porch
            x_front = val;
            break;
        case 0x1E14001C: // X resolution
            x_res = val;
            break;
        case 0x1E140020: // Y back porch
            y_back = val;
            break;
        case 0x1E140024: // Y sync width
            y_sync = val;
            break;
        case 0x1E140028: // Y front porch
            y_front = val;
            break;
        case 0x1E14002C: // Y resolution
            y_res = val;
            break;
        case 0x1E140030: // X raster position
            x_pos = val;
            break;
        case 0x1E140034: // Y raster position
            y_pos = val;
            break;
        case 0x1E140040: // Y shift
            y_shift = val;
            break;
        case 0x1E140044: // X shift
            x_shift = val;
            break;
        case 0x1E140048: // Scaled X resolution
            x_res_scl = val;
            break;
        case 0x1E14004C: // Scaled Y resolution
            y_res_scl = val;
            break;

        default:
lcdc_unk_write:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad LCDC write %s %08X %08X", SOURCENAME(cpu), addr, val);
    }
}
