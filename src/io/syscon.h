
#ifndef DOOVDE_IO_SYSCON_H_
#define DOOVDE_IO_SYSCON_H_

namespace Doovde {
    struct Allegrex;
    namespace IoDevice {
        struct SysCon {
            SysCon();
            ~SysCon();
            //---  Variables
            bool alive;
            int cycles_left;

            uint32_t nmi_enable;
            int ram_size;
            int sc_me_sema;
            uint32_t reset_enable, bus_clock_enable, io_enable;
            uint32_t gpio_io_enable;

            //---  Functions
            void reset();
            int run(int cycles);
            int runSingle();
            uint32_t read(Allegrex* cpu, uint32_t addr);
            void write(Allegrex* cpu, uint32_t addr, uint32_t val);
        };
    };
}

#endif
