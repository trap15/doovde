
#ifndef DOOVDE_IO_H_
#define DOOVDE_IO_H_

namespace Doovde {
    namespace IoDevice {
        struct Ge;
        struct Gpio;
        struct IntMng;
        struct Kirk;
        struct Lcdc;
        struct MeCtrl;
        struct MemMng;
        struct Nand;
        struct Profiler;
        struct SysCon;
        struct Uart;
    }
    struct Io {
        Io();
        ~Io();
        //---  Structures
        //---  Variables
        bool alive;
        IoDevice::Ge* ge;
        IoDevice::Gpio* gpio;
        IoDevice::IntMng* intmng;
        IoDevice::Kirk* kirk;
        IoDevice::Lcdc* lcdc;
        IoDevice::MeCtrl* mectrl;
        IoDevice::MemMng* memmng;
        IoDevice::Nand* nand;
        IoDevice::Profiler* prof;
        IoDevice::SysCon* syscon;
        IoDevice::Uart* uart3;
        IoDevice::Uart* uart4;

        //---  Functions
        void reset();
        int run(int cycles);
        int runSingle();
        void addMemoryMap(int mode, uint32_t base);
    };
}

#endif
