//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//---  Doovde includes
#include "log.h"
#include "cpu/allegrex.h"
#include "mem/mem.h"
#include "io/io.h"
#include "io/memmng.h"

Doovde::IoDevice::MemMng::MemMng()
{
    alive = true;
}

Doovde::IoDevice::MemMng::~MemMng()
{
    alive = false;
}

void Doovde::IoDevice::MemMng::reset()
{
    cycles_left = 0;
    for(int i=0; i < 12; i++)
        prot[i] = 0xFFFFFFFF;
}

int Doovde::IoDevice::MemMng::run(int cycles)
{
    cycles_left += cycles;
    int startcycles = cycles_left;
    while(cycles_left > 0) {
        cycles_left -= runSingle();
    }
    return startcycles - cycles_left;
}

int Doovde::IoDevice::MemMng::runSingle()
{
    return 1;
}

uint32_t Doovde::IoDevice::MemMng::read(Doovde::Allegrex* cpu, uint32_t addr)
{
    switch(addr) {
        case 0x1C000000: // Memory protection 0MiB ~ 2MiB
            return prot[0];
        case 0x1C000004: // Memory protection 2MiB ~ 4MiB
            return prot[1];
        case 0x1C000008: // Memory protection 4MiB ~ 6MiB
            return prot[2];
        case 0x1C00000C: // Memory protection 6MiB ~ 8MiB
            return prot[3];
        case 0x1C000010: // Memory protection 8MiB ~ 10MiB
            return prot[4];
        case 0x1C000014: // Memory protection 10MiB ~ 12MiB
            return prot[5];
        case 0x1C000018: // Memory protection 12MiB ~ 14MiB
            return prot[6];
        case 0x1C00001C: // Memory protection 14MiB ~ 16MiB
            return prot[7];
        case 0x1C000020: // Memory protection 16MiB ~ 18MiB
            return prot[8];
        case 0x1C000024: // Memory protection 18MiB ~ 20MiB
            return prot[9];
        case 0x1C000028: // Memory protection 20MiB ~ 22MiB
            return prot[10];
        case 0x1C00002C: // Memory protection 22MiB ~ 24MiB
            return prot[11];
        case 0x1C000030: // Profiler enable?
            goto memmng_unk_read;
        case 0x1C000044: // Unk
            goto memmng_unk_read;

        default:
memmng_unk_read:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad Memory Manager read %s %08X", SOURCENAME(cpu), addr);
            return 0;
    }
}

void Doovde::IoDevice::MemMng::write(Doovde::Allegrex* cpu, uint32_t addr, uint32_t val)
{
    switch(addr) {
        // TODO: Apply memory protection
        case 0x1C000000: // Memory protection 0MiB ~ 2MiB
            prot[0] = val;
            break;
        case 0x1C000004: // Memory protection 2MiB ~ 4MiB
            prot[1] = val;
            break;
        case 0x1C000008: // Memory protection 4MiB ~ 6MiB
            prot[2] = val;
            break;
        case 0x1C00000C: // Memory protection 6MiB ~ 8MiB
            prot[3] = val;
            break;
        case 0x1C000010: // Memory protection 8MiB ~ 10MiB
            prot[4] = val;
            break;
        case 0x1C000014: // Memory protection 10MiB ~ 12MiB
            prot[5] = val;
            break;
        case 0x1C000018: // Memory protection 12MiB ~ 14MiB
            prot[6] = val;
            break;
        case 0x1C00001C: // Memory protection 14MiB ~ 16MiB
            prot[7] = val;
            break;
        case 0x1C000020: // Memory protection 16MiB ~ 18MiB
            prot[8] = val;
            break;
        case 0x1C000024: // Memory protection 18MiB ~ 20MiB
            prot[9] = val;
            break;
        case 0x1C000028: // Memory protection 20MiB ~ 22MiB
            prot[10] = val;
            break;
        case 0x1C00002C: // Memory protection 22MiB ~ 24MiB
            prot[11] = val;
            break;
        case 0x1C000030: // Profiler enable?
            // bit8+9 1: thread profiler mode
            //        3: Make profiler accessible at $1C400000
            goto memmng_unk_write;
        case 0x1C000044: // Unk
            // bit9 ? used by threadman
            goto memmng_unk_write;

        default:
memmng_unk_write:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad Memory Manager write %s %08X %08X", SOURCENAME(cpu), addr, val);
    }
}
