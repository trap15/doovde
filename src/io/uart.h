
#ifndef DOOVDE_IO_UART_H_
#define DOOVDE_IO_UART_H_

namespace Doovde {
    struct Allegrex;
    namespace IoDevice {
        struct Uart {
            Uart();
            ~Uart();
            //---  Enums
            enum BufferSize {
                TXBUF_SIZE = 32,
                RXBUF_SIZE = 32
            };
            //---  Variables
            bool alive;
            int cycles_left;

            uint8_t txbuf[TXBUF_SIZE];
            int txbuf_ptr;
            int txbuf_used;
            uint8_t rxbuf[RXBUF_SIZE];
            int rxbuf_ptr;
            int rxbuf_used;
            int baudrate;
            uint32_t stored_baudrate;

            //---  Functions
            void reset();
            int run(int cycles);
            int runSingle();
            uint32_t read(Allegrex* cpu, uint32_t addr);
            void write(Allegrex* cpu, uint32_t addr, uint32_t val);
        };
    };
}

#endif
