//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//---  Doovde includes
#include "log.h"
#include "core/core.h"
#include "mem/mem.h"
#include "io/io.h"
#include "io/ge.h"
#include "io/gpio.h"
#include "io/intmng.h"
#include "io/kirk.h"
#include "io/lcdc.h"
#include "io/mectrl.h"
#include "io/memmng.h"
#include "io/nand.h"
#include "io/prof.h"
#include "io/syscon.h"
#include "io/uart.h"

Doovde::Io::Io()
{
    ge     = new Doovde::IoDevice::Ge();
    gpio   = new Doovde::IoDevice::Gpio();
    intmng = new Doovde::IoDevice::IntMng();
    kirk   = new Doovde::IoDevice::Kirk();
    lcdc   = new Doovde::IoDevice::Lcdc();
    mectrl = new Doovde::IoDevice::MeCtrl();
    memmng = new Doovde::IoDevice::MemMng();
    nand   = new Doovde::IoDevice::Nand();
    prof   = new Doovde::IoDevice::Profiler();
    syscon = new Doovde::IoDevice::SysCon();
    uart3  = new Doovde::IoDevice::Uart();
    uart4  = new Doovde::IoDevice::Uart();
    // TODO: Attach UARTs
    alive = true;
}

Doovde::Io::~Io()
{
    alive = false;
    delete ge;
    delete gpio;
    delete intmng;
    delete kirk;
    delete lcdc;
    delete mectrl;
    delete memmng;
    delete nand;
    delete prof;
    delete syscon;
    delete uart3;
    delete uart4;
}

void Doovde::Io::reset()
{
    ge->reset();
    gpio->reset();
    intmng->reset();
    kirk->reset();
    lcdc->reset();
    mectrl->reset();
    memmng->reset();
    nand->reset();
    prof->reset();
    syscon->reset();
    uart3->reset();
    uart4->reset();
}

int Doovde::Io::run(int cycles)
{
    ge->run(cycles);
    gpio->run(cycles);
    intmng->run(cycles);
    kirk->run(cycles);
    lcdc->run(cycles);
    mectrl->run(cycles);
    memmng->run(cycles);
    nand->run(cycles);
    prof->run(cycles);
    syscon->run(cycles);
    uart3->run(cycles);
    uart4->run(cycles);
    return cycles;
}

int Doovde::Io::runSingle()
{
    return run(1);
}

// TODO: Expand these macros for cleanliness

#define IO32TEMP(name) \
    static uint8_t name##Read8(Doovde::Memory* mem, Doovde::Allegrex* cpu, uint32_t addr) \
    { \
        uint32_t ret = name##Read32(mem, cpu, addr & ~3); \
        ret >>= (addr & 3) << 3; \
        ret &= 0xFF; \
        return ret; \
    } \
    static uint16_t name##Read16(Doovde::Memory* mem, Doovde::Allegrex* cpu, uint32_t addr) \
    { \
        uint32_t ret = name##Read32(mem, cpu, addr & ~1); \
        ret >>= (addr & 1) << 4; \
        ret &= 0xFFFF; \
        return ret; \
    } \
    /* TODO: Writing invalid size writes nothing */ \
    static void name##Write8(Doovde::Memory* mem, Doovde::Allegrex* cpu, uint32_t addr, uint8_t val) \
    { \
        (void)mem; \
        (void)cpu; \
        (void)addr; \
        (void)val; \
    } \
    static void name##Write16(Doovde::Memory* mem, Doovde::Allegrex* cpu, uint32_t addr, uint16_t val) \
    { \
        (void)mem; \
        (void)cpu; \
        (void)addr; \
        (void)val; \
    }

#define IO32RWGEN(name, member) \
    static uint32_t name##Read32(Doovde::Memory* mem, Doovde::Allegrex* cpu, uint32_t addr) \
    { \
        (void)mem; \
        return core->io->member->read(cpu, addr & ~0xE0000000); \
    } \
    static void name##Write32(Doovde::Memory* mem, Doovde::Allegrex* cpu, uint32_t addr, uint32_t val) \
    { \
        (void)mem; \
        core->io->member->write(cpu, addr & ~0xE0000000, val); \
    } \
    IO32TEMP(name)

IO32RWGEN(ge, ge)
IO32RWGEN(gpio, gpio)
IO32RWGEN(intMng, intmng)
IO32RWGEN(kirk, kirk)
IO32RWGEN(lcdc, lcdc)
IO32RWGEN(memMng, memmng)
IO32RWGEN(meCtrl, mectrl)
IO32RWGEN(nand, nand)
IO32RWGEN(prof, prof)
IO32RWGEN(sysCon, syscon)
IO32RWGEN(uart3, uart3)
IO32RWGEN(uart4, uart4)

void Doovde::Io::addMemoryMap(int mode, uint32_t base)
{
    // Memory Manager
    core->sc_mem->addEntry(mode, base + 0x1C000000, 0x00100000, "MemMng", true, true,
                             memMngRead8, memMngRead16, memMngRead32,
                             memMngWrite8,memMngWrite16,memMngWrite32);
    core->me_mem->addEntry(mode, base + 0x1C000000, 0x00100000, "MemMng", true, true,
                             memMngRead8, memMngRead16, memMngRead32,
                             memMngWrite8,memMngWrite16,memMngWrite32);

    // System Configuration
    core->sc_mem->addEntry(mode, base + 0x1C100000, 0x00100000, "SysCon", true, true,
                             sysConRead8, sysConRead16, sysConRead32,
                             sysConWrite8,sysConWrite16,sysConWrite32);
    core->me_mem->addEntry(mode, base + 0x1C100000, 0x00100000, "SysCon", true, true,
                             sysConRead8, sysConRead16, sysConRead32,
                             sysConWrite8,sysConWrite16,sysConWrite32);

    // Interrupt Manager
    core->sc_mem->addEntry(mode, base + 0x1C300000, 0x00100000, "IntMng", true, true,
                             intMngRead8, intMngRead16, intMngRead32,
                             intMngWrite8,intMngWrite16,intMngWrite32);
    core->me_mem->addEntry(mode, base + 0x1C300000, 0x00100000, "IntMng", true, true,
                             intMngRead8, intMngRead16, intMngRead32,
                             intMngWrite8,intMngWrite16,intMngWrite32);

    // Profiler
    core->sc_mem->addEntry(mode, base + 0x1C400000, 0x00100000, "Profiler", true, true,
                             profRead8, profRead16, profRead32,
                             profWrite8,profWrite16,profWrite32);
    core->me_mem->addEntry(mode, base + 0x1C400000, 0x00100000, "Profiler", true, true,
                             profRead8, profRead16, profRead32,
                             profWrite8,profWrite16,profWrite32);

    // ME Control
    core->sc_mem->addEntry(mode, base + 0x1CC00000, 0x00100000, "ME Ctrl", true, true,
                             meCtrlRead8, meCtrlRead16, meCtrlRead32,
                             meCtrlWrite8,meCtrlWrite16,meCtrlWrite32);
    core->me_mem->addEntry(mode, base + 0x1CC00000, 0x00100000, "ME Ctrl", true, true,
                             meCtrlRead8, meCtrlRead16, meCtrlRead32,
                             meCtrlWrite8,meCtrlWrite16,meCtrlWrite32);

    // NAND
    core->sc_mem->addEntry(mode, base + 0x1D101000, 0x00001000, "NAND", true, true,
                             nandRead8, nandRead16, nandRead32,
                             nandWrite8,nandWrite16,nandWrite32);
    core->me_mem->addEntry(mode, base + 0x1D101000, 0x00001000, "NAND", true, true,
                             nandRead8, nandRead16, nandRead32,
                             nandWrite8,nandWrite16,nandWrite32);

    // GE
    core->sc_mem->addEntry(mode, base + 0x1D400000, 0x00200000, "GE", true, true,
                             geRead8, geRead16, geRead32,
                             geWrite8,geWrite16,geWrite32);
    core->me_mem->addEntry(mode, base + 0x1D400000, 0x00200000, "GE", true, true,
                             geRead8, geRead16, geRead32,
                             geWrite8,geWrite16,geWrite32);

    // KIRK
    core->sc_mem->addEntry(mode, base + 0x1DE00000, 0x00100000, "KIRK", true, true,
                             kirkRead8, kirkRead16, kirkRead32,
                             kirkWrite8,kirkWrite16,kirkWrite32);
    core->me_mem->addEntry(mode, base + 0x1DE00000, 0x00100000, "KIRK", true, true,
                             kirkRead8, kirkRead16, kirkRead32,
                             kirkWrite8,kirkWrite16,kirkWrite32);

    // LCDC
    core->sc_mem->addEntry(mode, base + 0x1E140000, 0x00040000, "LCDC", true, true,
                             lcdcRead8, lcdcRead16, lcdcRead32,
                             lcdcWrite8,lcdcWrite16,lcdcWrite32);
    core->me_mem->addEntry(mode, base + 0x1E140000, 0x00040000, "LCDC", true, true,
                             lcdcRead8, lcdcRead16, lcdcRead32,
                             lcdcWrite8,lcdcWrite16,lcdcWrite32);

    // GPIO
    core->sc_mem->addEntry(mode, base + 0x1E240000, 0x00040000, "GPIO", true, true,
                             gpioRead8, gpioRead16, gpioRead32,
                             gpioWrite8,gpioWrite16,gpioWrite32);
    core->me_mem->addEntry(mode, base + 0x1E240000, 0x00040000, "GPIO", true, true,
                             gpioRead8, gpioRead16, gpioRead32,
                             gpioWrite8,gpioWrite16,gpioWrite32);

    // UART4
    core->sc_mem->addEntry(mode, base + 0x1E4C0000, 0x00040000, "UART4", true, true,
                             uart4Read8, uart4Read16, uart4Read32,
                             uart4Write8,uart4Write16,uart4Write32);
    core->me_mem->addEntry(mode, base + 0x1E4C0000, 0x00040000, "UART4", true, true,
                             uart4Read8, uart4Read16, uart4Read32,
                             uart4Write8,uart4Write16,uart4Write32);

    // UART3
    core->sc_mem->addEntry(mode, base + 0x1E500000, 0x00040000, "UART3", true, true,
                             uart3Read8, uart3Read16, uart3Read32,
                             uart3Write8,uart3Write16,uart3Write32);
    core->me_mem->addEntry(mode, base + 0x1E500000, 0x00040000, "UART3", true, true,
                             uart3Read8, uart3Read16, uart3Read32,
                             uart3Write8,uart3Write16,uart3Write32);

    // NAND
    core->sc_mem->addEntry(mode, base + 0x1FF00000, 0x00001000, "NAND Buffer", true, true,
                             nand->buffer);
    core->me_mem->addEntry(mode, base + 0x1FF00000, 0x00001000, "NAND Buffer", true, true,
                             nand->buffer);

}
