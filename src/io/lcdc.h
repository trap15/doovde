
#ifndef DOOVDE_IO_LCDC_H_
#define DOOVDE_IO_LCDC_H_

namespace Doovde {
    struct Allegrex;
    namespace IoDevice {
        struct Lcdc {
            Lcdc();
            ~Lcdc();
            //---  Variables
            bool alive;
            int cycles_left;

            int enable;
            int sync_diff;
            int x_back, y_back;
            int x_sync, y_sync;
            int x_front, y_front;
            int x_res, y_res;
            int x_pos, y_pos;
            int x_shift, y_shift;
            int x_res_scl, y_res_scl;

            //---  Functions
            void reset();
            int run(int cycles);
            int runSingle();
            uint32_t read(Allegrex* cpu, uint32_t addr);
            void write(Allegrex* cpu, uint32_t addr, uint32_t val);
        };
    };
}

#endif
