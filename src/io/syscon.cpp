//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//---  Doovde includes
#include "log.h"
#include "core/core.h"
#include "cpu/allegrex.h"
#include "mem/mem.h"
#include "io/io.h"
#include "io/syscon.h"

Doovde::IoDevice::SysCon::SysCon()
{
    alive = true;
}

Doovde::IoDevice::SysCon::~SysCon()
{
    alive = false;
}

void Doovde::IoDevice::SysCon::reset()
{
    cycles_left = 0;

    nmi_enable = 0;
    ram_size = 1;
    sc_me_sema = 0;
    reset_enable = 0;
    bus_clock_enable = 0;
    io_enable = 0;
    gpio_io_enable = 0;
}

int Doovde::IoDevice::SysCon::run(int cycles)
{
    cycles_left += cycles;
    int startcycles = cycles_left;
    while(cycles_left > 0) {
        cycles_left -= runSingle();
    }
    return startcycles - cycles_left;
}

int Doovde::IoDevice::SysCon::runSingle()
{
    return 1;
}

uint32_t Doovde::IoDevice::SysCon::read(Doovde::Allegrex* cpu, uint32_t addr)
{
    switch(addr) {
        case 0x1C100000: // NMI enable mask
            return nmi_enable << 16;
        case 0x1C100004: // NMI ACK?
            goto syscon_unk_read;
        case 0x1C100010: // NMI0 flags?
            goto syscon_unk_read;
        case 0x1C100014: // NMI1 flags?
            goto syscon_unk_read;
        case 0x1C100018: // NMI2 flags?
            goto syscon_unk_read;
        case 0x1C10001C: // NMI3 flags?
            goto syscon_unk_read;
        case 0x1C100020: // NMI4 flags?
            goto syscon_unk_read;
        case 0x1C100024: // NMI5 flags?
            goto syscon_unk_read;
        case 0x1C100028: // NMI6 flags?
            goto syscon_unk_read;
        case 0x1C10002C: // NMI7 flags?
            goto syscon_unk_read;
        case 0x1C100030: // NMI8 flags?
            goto syscon_unk_read;
        case 0x1C100034: // NMI9 flags?
            goto syscon_unk_read;
        case 0x1C100040: // RAM size
            // 0-16MiB, 1-32MiB, 2-64MiB, 3-128MiB
            return ram_size;
        case 0x1C100044: // SC/ME RPC Interrupt
            goto syscon_unk_read;
        case 0x1C100048: // SC/ME Semaphore
            return sc_me_sema;
        case 0x1C10004C: // Reset Enable
            return reset_enable;
        case 0x1C100050: // Bus Clock Enable
            return bus_clock_enable;
        case 0x1C100078: // I/O Enable
            return io_enable;
        case 0x1C10007C: // GPIO I/O Enable
            return gpio_io_enable;
        case 0x1C100080: // Sysmem exception disable?
            goto syscon_unk_read;

        default:
syscon_unk_read:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad System Configuration read %s %08X", SOURCENAME(cpu), addr);
            return 0;
    }
}

void Doovde::IoDevice::SysCon::write(Doovde::Allegrex* cpu, uint32_t addr, uint32_t val)
{
    switch(addr) {
        case 0x1C100000: // NMI enable mask
            nmi_enable = (val >> 16) & 0x3FF;
            break;
        case 0x1C100004: // NMI ACK?
            // TODO: ACK
            goto syscon_unk_write;
        case 0x1C100010: // NMI0 flags?
            goto syscon_unk_write;
        case 0x1C100014: // NMI1 flags?
            goto syscon_unk_write;
        case 0x1C100018: // NMI2 flags?
            goto syscon_unk_write;
        case 0x1C10001C: // NMI3 flags?
            goto syscon_unk_write;
        case 0x1C100020: // NMI4 flags?
            goto syscon_unk_write;
        case 0x1C100024: // NMI5 flags?
            goto syscon_unk_write;
        case 0x1C100028: // NMI6 flags?
            goto syscon_unk_write;
        case 0x1C10002C: // NMI7 flags?
            goto syscon_unk_write;
        case 0x1C100030: // NMI8 flags?
            goto syscon_unk_write;
        case 0x1C100034: // NMI9 flags?
            goto syscon_unk_write;
        case 0x1C100040: // RAM size
            // 0-16MiB, 1-32MiB, 2-64MiB, 3-128MiB
            // TODO: Resize memory?
            ram_size = val & 3;
            break;
        case 0x1C100044: // SC/ME RPC Interrupt
            if(cpu->media_engine) {
                goto syscon_unk_write; // TODO: Post interrupt to SC
            }else{
                goto syscon_unk_write; // TODO: Post interrupt to ME
            }
            break;
        case 0x1C100048: // SC/ME Semaphore
            // TODO: Semaphore stuff?
            sc_me_sema = val;
            goto syscon_unk_write;
        case 0x1C10004C: // Reset Enable
            // TODO: Reset stuff
            // bit10:  KIRK
            // bit8+9: MSIF
            // bit7:   ATA
            // bit6:   USB
            // bit5:   AVC
            // bit4:   VME
            // bit3:   AW
            // bit2:   ME
            // bit1:   SC
            // bit0:   Top
            reset_enable = val;
            goto syscon_unk_write;
        case 0x1C100050: // Bus Clock Enable
            // TODO: Enable stuff
            // bit15+16: Audio
            // bit14:    UART4?
            // bit13:    EMCSM (NAND)
            // bit12:    ?
            // bit10+11: MSIF
            // bit9:     USB
            // bit8:     ATA
            // bit7:     KIRK
            // bit5+6:   DMAC
            // bit4:     DMACPlus
            // bit3:     AW?
            // bit2:     AW?
            // bit1:     AW?
            // bit0:     ME
            bus_clock_enable = val;
            goto syscon_unk_write;
        case 0x1C100078: // I/O Enable
            // TODO: Enable I/O stuff
            // bit19~24: SPI
            // bit13~18: UART
            // bit12:    PWM
            // bit11:    KEY
            // bit10:    Audio?
            // bit9:     SIRCS
            // bit8:     IIC
            // bit6+7:   Audio
            // bit5:     LCDC
            // bit3+4:   MSIF
            // bit2:     ATA
            // bit1:     USB
            // bit0:     EMCSM (NAND)
            io_enable = val;
            goto syscon_unk_write;
        case 0x1C10007C: // GPIO I/O Enable
            // TODO ?
            gpio_io_enable = val;
            goto syscon_unk_write;
        case 0x1C100080: // Sysmem exception disable?
            goto syscon_unk_write;

        default:
syscon_unk_write:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad System Configuration write %s %08X %08X", SOURCENAME(cpu), addr, val);
    }
}
