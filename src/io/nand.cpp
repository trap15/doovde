//---  System includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//---  Doovde includes
#include "log.h"
#include "cpu/allegrex.h"
#include "mem/mem.h"
#include "io/io.h"
#include "io/nand.h"

// TODO: Most commands

Doovde::IoDevice::Nand::Nand()
{
    alive = true;
    load("../nand-dump.bin");
}

Doovde::IoDevice::Nand::~Nand()
{
    save("../nand-dump.bin.new");
    alive = false;
}

void Doovde::IoDevice::Nand::reset()
{
    cycles_left = 0;
    //size = 0;
    //mem = NULL;
    //ready = false;
    //write_prot = true;
    cmd = 0;
    nandaddr = 0;
    cmd_written = false;
    dmactrl = 0;
    dmastatus = 0;
}

bool Doovde::IoDevice::Nand::load(const char *file)
{
    FILE* fp = fopen(file, "rb+");
    if(fp == NULL)
        return false;
    fseek(fp, 0, SEEK_END);
    size = ftell(fp);
    mem = malloc(size);
    fseek(fp, 0, SEEK_SET);
    fread(mem, size, 1, fp);
    fclose(fp);

    ready = true;
    write_prot = false;
    return true;
}

bool Doovde::IoDevice::Nand::save(const char *file)
{
    FILE* fp = fopen(file, "wb+");
    if(fp == NULL)
        return false;
    fwrite(mem, size, 1, fp);
    fclose(fp);
    return true;
}

int Doovde::IoDevice::Nand::run(int cycles)
{
    cycles_left += cycles;
    int startcycles = cycles_left;
    while(cycles_left > 0) {
        cycles_left -= runSingle();
    }
    return startcycles - cycles_left;
}

int Doovde::IoDevice::Nand::runSingle()
{
    if(cmd_written) {
        handleCommand();
    }
    if(dmactrl & 1) {
        handleDma();
    }
    return 1;
}

uint32_t Doovde::IoDevice::Nand::read(Doovde::Allegrex* cpu, uint32_t addr)
{
    uint32_t ret = 0;
    switch(addr) {
        case 0x1D101000: // Control
            goto nand_unk_read;
        case 0x1D101004: // Status
            if(ready)
                ret |= 1;
            if(!write_prot)
                ret |= 1<<7;
            break;
        case 0x1D101008: // Command
            goto nand_unk_read;
        case 0x1D10100C: // Address
            goto nand_unk_read;
        case 0x1D101014: // NAND reset?
            goto nand_unk_read;
        case 0x1D101020: // DMA address
            goto nand_unk_read;
        case 0x1D101024: // DMA control
            ret = dmactrl;
            break;
        case 0x1D101028: // DMA status
            ret = dmastatus;
            break;
        case 0x1D101038: // DMA Intr?
            goto nand_unk_read;
        case 0x1D101300: // Data
            ret = data[0];
            ret |= data[1] << 8;
            ret |= data[2] << 16;
            ret |= data[3] << 24;
            break;

        default:
nand_unk_read:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad NAND read %s %08X", SOURCENAME(cpu), addr);
            break;
    }
    return ret;
}

void Doovde::IoDevice::Nand::write(Doovde::Allegrex* cpu, uint32_t addr, uint32_t val)
{
    switch(addr) {
        case 0x1D101000: // Control
            goto nand_unk_write;
        case 0x1D101004: // Status
            write_prot = (val & 0x80) == 0;
            break;
        case 0x1D101008: // Command
            cmd = val & 0xFF;
            cmd_written = true;
            ready = false;
            break;
        case 0x1D10100C: // Address
            nandaddr = (val >> 10) & 0xFFFF;
            break;
        case 0x1D101014: // NAND reset?
            goto nand_unk_write;
        case 0x1D101020: // DMA address
            dmaaddr = (val >> 10) & 0xFFFF;
            break;
        case 0x1D101024: // DMA control
            dmactrl = val;
            break;
        case 0x1D101028: // DMA status
            goto nand_unk_write;
        case 0x1D101038: // DMA Intr?
            goto nand_unk_write;
        case 0x1D101300: // Data
            data[0] = (val >>  0) & 0xFF;
            data[1] = (val >>  8) & 0xFF;
            data[2] = (val >> 16) & 0xFF;
            data[3] = (val >> 24) & 0xFF;
            break;

        default:
nand_unk_write:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad NAND write %s %08X %08X", SOURCENAME(cpu), addr, val);
    }
}

uint32_t Doovde::IoDevice::Nand::calcAddress(int block, int page, int byte)
{
    return (((block * SIZE_BLOCK) + page) * (SIZE_PAGE+SIZE_SPARE)) + byte;
}

void Doovde::IoDevice::Nand::handleCommand()
{
    switch(cmd) {
        case CMD_READ1:
            goto nand_cmd_unk;
        case CMD_READ1X:
            goto nand_cmd_unk;
        case CMD_READ2:
            goto nand_cmd_unk;
        case CMD_ERASE0:
            goto nand_cmd_unk;
        case CMD_STATUS:
            // What should these be?
            data[0] = data[1] = data[2] = data[3] = 0;
            ready = true;
            break;
        case CMD_READ_ID:
            goto nand_cmd_unk;
        case CMD_ERASE1:
            goto nand_cmd_unk;
        case CMD_RESET:
            // Reset stuff
            ready = true;
            break;
        //case CMD_COPY_PRG0:
            goto nand_cmd_unk;
        case CMD_COPY_PRG1:
            goto nand_cmd_unk;
        case CMD_PAGE_PRG0:
            goto nand_cmd_unk;
        case CMD_PAGE_PRG1:
            goto nand_cmd_unk;

default:
nand_cmd_unk:
            Doovde::log(Doovde::LogLevel::WARNING, "Unhandled NAND command %02X", cmd);
            break;
    }
}

void Doovde::IoDevice::Nand::handleDma()
{
    uint32_t addr = calcAddress(0, dmaaddr, 0);
    addr %= size;
    uint8_t* nand = (uint8_t*)mem;
    if(dmactrl & (1<<1)) { // Buffer -> NAND
        if(dmactrl & (1<<8)) { // Transfer user data?
            memcpy(nand+addr, buffer, SIZE_PAGE);
        }
        if(dmactrl & (1<<9)) { // Transfer ECC data?
            memcpy(nand+addr+SIZE_PAGE+4, buffer+0x900, SIZE_SPARE-4);
            // I don't think we do this?
            memcpy(nand+addr+SIZE_PAGE, buffer+0x800, 4);
        }
        dmactrl &= ~1;
        dmastatus &= ~1;
    }else{ // NAND -> Buffer
        if(dmactrl & (1<<8)) { // Transfer user data?
            memcpy(buffer, nand+addr, SIZE_PAGE);
        }
        if(dmactrl & (1<<9)) { // Transfer ECC data?
            memcpy(buffer+0x900, nand+addr+SIZE_PAGE+4, SIZE_SPARE-4);
            memcpy(buffer+0x800, nand+addr+SIZE_PAGE, 4);
        }
        dmactrl &= ~1;
        dmastatus &= ~1;
    }
}

