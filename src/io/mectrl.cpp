//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//---  Doovde includes
#include "log.h"
#include "core/core.h"
#include "cpu/allegrex.h"
#include "mem/mem.h"
#include "io/io.h"
#include "io/mectrl.h"

Doovde::IoDevice::MeCtrl::MeCtrl()
{
    alive = true;
}

Doovde::IoDevice::MeCtrl::~MeCtrl()
{
    alive = false;
}

void Doovde::IoDevice::MeCtrl::reset()
{
    cycles_left = 0;

    me_reset = false;
}

int Doovde::IoDevice::MeCtrl::run(int cycles)
{
    cycles_left += cycles;
    int startcycles = cycles_left;
    while(cycles_left > 0) {
        cycles_left -= runSingle();
    }
    return startcycles - cycles_left;
}

int Doovde::IoDevice::MeCtrl::runSingle()
{
    if(me_reset) {
        core->me_cpu->reset();
        me_reset = false;
    }
    return 1;
}

uint32_t Doovde::IoDevice::MeCtrl::read(Doovde::Allegrex* cpu, uint32_t addr)
{
    switch(addr) {
        case 0x1CC00010: // ME Reset
            if(me_reset)
                return 0xFFFFFFFF;
            return 0;
        case 0x1CC00030: // ?
            goto mectrl_unk_read;
        case 0x1CC00040: // ?
            goto mectrl_unk_read;
        case 0x1CC00070: // ?
            goto mectrl_unk_read;

        default:
mectrl_unk_read:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad ME Control read %s %08X", SOURCENAME(cpu), addr);
            return 0;
    }
}

void Doovde::IoDevice::MeCtrl::write(Doovde::Allegrex* cpu, uint32_t addr, uint32_t val)
{
    switch(addr) {
        case 0x1CC00010: // ME Reset
            me_reset = true;
            break;
        case 0x1CC00030: // ?
            goto mectrl_unk_write;
        case 0x1CC00040: // ?
            goto mectrl_unk_write;
        case 0x1CC00070: // ?
            goto mectrl_unk_write;
        default:
mectrl_unk_write:
            Doovde::log(Doovde::LogLevel::WARNING, "Bad ME Control write %s %08X %08X", SOURCENAME(cpu), addr, val);
    }
}
