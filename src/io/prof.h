
#ifndef DOOVDE_IO_PROF_H_
#define DOOVDE_IO_PROF_H_

namespace Doovde {
    struct Allegrex;
    namespace IoDevice {
        struct Profiler {
            Profiler();
            ~Profiler();
            //---  Variables
            bool alive;
            int cycles_left;

            //---  Functions
            void reset();
            int run(int cycles);
            int runSingle();
            uint32_t read(Allegrex* cpu, uint32_t addr);
            void write(Allegrex* cpu, uint32_t addr, uint32_t val);
        };
    };
}

#endif
