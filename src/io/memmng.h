
#ifndef DOOVDE_IO_MEMMNG_H_
#define DOOVDE_IO_MEMMNG_H_

namespace Doovde {
    struct Allegrex;
    namespace IoDevice {
        struct MemMng {
            MemMng();
            ~MemMng();
            //---  Variables
            bool alive;
            int cycles_left;

            uint32_t prot[12];

            //---  Functions
            void reset();
            int run(int cycles);
            int runSingle();
            uint32_t read(Allegrex* cpu, uint32_t addr);
            void write(Allegrex* cpu, uint32_t addr, uint32_t val);
        };
    };
}

#endif
