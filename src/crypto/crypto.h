
#ifndef DOOVDE_CRYPTO_H_
#define DOOVDE_CRYPTO_H_

struct AES_ctx;

namespace Doovde {
    namespace Crypto {
        void reset();

        // SHA1
        struct Sha1 {
            Sha1();
            uint8_t hash[20];

            void digest(uint8_t* src, int bytes);
        };

        // PRNG
        void prng(uint8_t* buf, int bytes);
        extern uint8_t prng_state[20];

        // AES-CBC
        struct AesCbc {
            AesCbc(uint8_t k[16]);
            AesCbc(uint8_t k[16], uint8_t iv[16]);
            ~AesCbc();
            uint8_t cbc[16];
            uint16_t key[16];
            AES_ctx* aes;

            void encrypt(uint8_t* dst, uint8_t* src, int bytes);
            void decrypt(uint8_t* dst, uint8_t* src, int bytes);
        };

        // ECDSA
        void ecdsa_curve(uint8_t* p, uint8_t* a, uint8_t* b, uint8_t* N, uint8_t* Gx, uint8_t* Gy);
        void ecdsa_pubkey(uint8_t* Px, uint8_t* Py);
        bool ecdsa_verify(uint8_t* hash, uint8_t* r, uint8_t* s);
    }
}

extern "C" {
void doovde_prng(uint8_t* buf, int bytes);
}

#endif
