//---  System includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

//---  Doovde includes
#include "log.h"
#include "crypto/crypto.h"
#include "crypto/aes.h"
#include "crypto/sha1.h"
#include "crypto/ec.h"

uint8_t Doovde::Crypto::prng_state[20];

void Doovde::Crypto::reset()
{
    memset(prng_state, 0, 20);
}

void Doovde::Crypto::Sha1::Sha1()
{
    memset(hash, 0, 20);
}

void Doovde::Crypto::Sha1::digest(uint8_t* src, int bytes)
{
    SHA_CTX sha;

    SHAInit(&sha);
    SHAUpdate(&sha, src, bytes);
    SHAFinal(hash, &sha);
}

void Doovde::Crypto::AesCbc::AesCbc(uint8_t k[16])
{
    aes = new AES_ctx;
    AES_set_key(aes, key, 128);
    memset(cbc, 0, 16);
    for(int i=0; i < 16; i++)
        key[i] = k[i];
}

void Doovde::Crypto::AesCbc::AesCbc(uint8_t k[16], uint8_t iv[16])
{
    aes = new AES_ctx;
    AES_set_key(aes, key, 128);
    for(int i=0; i < 16; i++) {
        cbc[i] = iv[i];
        key[i] = k[i];
    }
}

void Doovde::Crypto::AesCbc::~AesCbc()
{
    delete aes;
}

void Doovde::Crypto::AesCbc::encrypt(uint8_t *dst, uint8_t *src, int bytes)
{
    AES_cbc_encrypt(aes, src, dst, bytes);
/*    aes_set_key(key);
    aes_encrypt(iv, src, dst, bytes);*/
}

void Doovde::Crypto::AesCbc::decrypt(uint8_t *dst, uint8_t *src, int bytes)
{
    AES_cbc_decrypt(aes, src, dst, bytes);
/*    aes_set_key(key);
    aes_decrypt(iv, src, dst, bytes);*/
}

void Doovde::Crypto::prng(uint8_t* buf, int bytes)
{
    // TODO: This is probably totally wrong
    // Also it's taken from kirk-engine: http://kirk-engine.googlecode.com/

    // Some randomly selected data for a "key" to add to each randomization
    uint8_t key[0x10] = { 0xA7, 0x2E, 0x4C, 0xB6, 0xC3, 0x34, 0xDF, 0x85, 0x70, 0x01, 0x49, 0xFC, 0xC0, 0x87, 0xC4, 0x77 };
    if(bytes <= 0)
        return;

    uint8_t temp[0x100];

    uint32_t curtime;

    memcpy(temp, prng_state, 20);
    // This uses the standard C time function for portability.
    curtime = time(0);

    temp[0x14] = curtime & 0xFF;
    temp[0x15] = (curtime>>8) & 0xFF;
    temp[0x16] = (curtime>>16) & 0xFF;
    temp[0x17] = (curtime>>24) & 0xFF;

    memcpy(&temp[0x18], key, 0x10);
    //This leaves the remainder of the 0x100 bytes in temp to whatever remains on the stack
    // in an uninitialized state. This should add unpredicableness to the results as well
    sha1(prng_state, temp, 0x100);

    while(bytes) {
        int blockrem = bytes % 20;
        int block = bytes / 20;

        if(block) {
            memcpy(buf, prng_state, 20);
            buf += 20;
            bytes -= 20;
            prng(buf, bytes);
        }else{
            if(blockrem) {
                memcpy(buf, prng_state, blockrem);
                bytes -= blockrem;
            }
        }
    }
}

void doovde_prng(uint8_t* buf, int bytes)
{
    Doovde::Crypto::prng(buf, bytes);
}

void Doovde::Crypto::ecdsa_curve(uint8_t* p, uint8_t* a, uint8_t* b, uint8_t* N, uint8_t* Gx, uint8_t* Gy)
{
    ecdsa_set_curve(p, a, b, N, Gx, Gy);
}

void Doovde::Crypto::ecdsa_pubkey(uint8_t* Px, uint8_t* Py)
{
    uint8_t Q[40];
    for(int i=0; i < 20; i++) {
        Q[i] = Px[i];
        Q[i+20] = Py[i];
    }
    ecdsa_set_pub(Q);
}

bool Doovde::Crypto::ecdsa_verify(uint8_t* hash, uint8_t* r, uint8_t* s)
{
    if(::ecdsa_verify(hash, r, s))
        return true;
    else
        return false;
}
