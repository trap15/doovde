/* Taken from kirk-engine: http://kirk-engine.googlecode.com/ */

/* sha.h */

#ifndef _SHA_H_
#define _SHA_H_

#ifdef __cplusplus
extern "C" {
#endif

/* #include "global.h" */

/* The structure for storing SHS info */

typedef struct
{
    uint32_t digest[ 5 ];            /* Message digest */
    uint32_t countLo, countHi;       /* 64-bit bit count */
    uint32_t data[ 16 ];             /* SHS data buffer */
    int Endianness;
} SHA_CTX;

/* Message digest functions */

void SHAInit(SHA_CTX *ctx);
void SHAUpdate(SHA_CTX *ctx, uint8_t *buffer, int count);
void SHAFinal(uint8_t *output, SHA_CTX *ctx);

void endianTest(int *endianness);

#ifdef __cplusplus
}
#endif

#endif /* _SHA_H_ */
