/* Taken from kirk-engine: http://kirk-engine.googlecode.com/ */

// Copyright 2007,2008,2010  Segher Boessenkool  <segher@kernel.crashing.org>
// Licensed under the terms of the GNU GPL, version 2
// http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt


// Modified for Kirk engine by setting single curve and internal function
// to support Kirk elliptic curve options.- July 2011

#ifndef _BC_H_
#define _BC_H_

#ifdef __cplusplus
extern "C" {
#endif

void bn_copy(uint8_t *d, uint8_t *a, uint32_t n);
int bn_compare(uint8_t *a, uint8_t *b, uint32_t n);
void bn_reduce(uint8_t *d, uint8_t *N, uint32_t n);
void bn_add(uint8_t *d, uint8_t *a, uint8_t *b, uint8_t *N, uint32_t n);
void bn_sub(uint8_t *d, uint8_t *a, uint8_t *b, uint8_t *N, uint32_t n);
void bn_to_mon(uint8_t *d, uint8_t *N, uint32_t n);
void bn_from_mon(uint8_t *d, uint8_t *N, uint32_t n);
void bn_mon_mul(uint8_t *d, uint8_t *a, uint8_t *b, uint8_t *N, uint32_t n);
void bn_mon_inv(uint8_t *d, uint8_t *a, uint8_t *N, uint32_t n);
void hex_dump(char *str, uint8_t *buf, int size);

#ifdef __cplusplus
}
#endif

#endif
