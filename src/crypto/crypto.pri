CRYPTOPATH   = src/crypto
HEADERS     += $${CRYPTOPATH}/crypto.h \
               $${CRYPTOPATH}/aes.h \
               $${CRYPTOPATH}/sha1.h \
               $${CRYPTOPATH}/bn.h \
               $${CRYPTOPATH}/ec.h \

SOURCES     += $${CRYPTOPATH}/crypto.cpp \
               $${CRYPTOPATH}/aes.c \
               $${CRYPTOPATH}/sha1.c \
               $${CRYPTOPATH}/bn.c \
               $${CRYPTOPATH}/ec.c \

RESOURCES   += 
