//---  System includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

//---  Doovde includes
#include "log.h"
#include "core/core.h"
#include "cpu/allegrex.h"
#include "cpu/cop0.h"
#include "mem/mem.h"
#include "gfx/gfx.h"
#include "gfx/doovde_gl.h"
#include "io/io.h"

Doovde::Core* core;

static void sc_write8(Doovde::Allegrex* cpu, uint32_t addr, uint8_t val)
{
    core->sc_mem->write8(cpu, cpu->cop0->getFullMode(), addr, val);
}

static void sc_write16(Doovde::Allegrex* cpu, uint32_t addr, uint16_t val)
{
    core->sc_mem->write16(cpu, cpu->cop0->getFullMode(), addr, val);
}

static void sc_write32(Doovde::Allegrex* cpu, uint32_t addr, uint32_t val)
{
    core->sc_mem->write32(cpu, cpu->cop0->getFullMode(), addr, val);
}

static uint8_t sc_read8(Doovde::Allegrex* cpu, uint32_t addr)
{
    return core->sc_mem->read8(cpu, cpu->cop0->getFullMode(), addr);
}

static uint16_t sc_read16(Doovde::Allegrex* cpu, uint32_t addr)
{
    return core->sc_mem->read16(cpu, cpu->cop0->getFullMode(), addr);
}

static uint32_t sc_read32(Doovde::Allegrex* cpu, uint32_t addr)
{
    return core->sc_mem->read32(cpu, cpu->cop0->getFullMode(), addr);
}

static void me_write8(Doovde::Allegrex* cpu, uint32_t addr, uint8_t val)
{
    core->me_mem->write8(cpu, cpu->cop0->getFullMode(), addr, val);
}

static void me_write16(Doovde::Allegrex* cpu, uint32_t addr, uint16_t val)
{
    core->me_mem->write16(cpu, cpu->cop0->getFullMode(), addr, val);
}

static void me_write32(Doovde::Allegrex* cpu, uint32_t addr, uint32_t val)
{
    core->me_mem->write32(cpu, cpu->cop0->getFullMode(), addr, val);
}

static uint8_t me_read8(Doovde::Allegrex* cpu, uint32_t addr)
{
    return core->me_mem->read8(cpu, cpu->cop0->getFullMode(), addr);
}

static uint16_t me_read16(Doovde::Allegrex* cpu, uint32_t addr)
{
    return core->me_mem->read16(cpu, cpu->cop0->getFullMode(), addr);
}

static uint32_t me_read32(Doovde::Allegrex* cpu, uint32_t addr)
{
    return core->me_mem->read32(cpu, cpu->cop0->getFullMode(), addr);
}

static void sc_busErr(int mode, uint32_t addr, int type)
{
    Doovde::log(Doovde::LogLevel::ERROR, "SC CPU Bus Error %d 0x%08X %d.", mode, addr, type);
    if(core->sc_cpu->fetching)
        core->sc_cpu->exception(Doovde::Allegrex::EXC_BUS_FETCH);
    else
        core->sc_cpu->exception(Doovde::Allegrex::EXC_BUS_DATA);
}

static void me_busErr(int mode, uint32_t addr, int type)
{
    Doovde::log(Doovde::LogLevel::ERROR, "ME CPU Bus Error %d 0x%08X %d.", mode, addr, type);
    if(core->me_cpu->fetching)
        core->me_cpu->exception(Doovde::Allegrex::EXC_BUS_FETCH);
    else
        core->me_cpu->exception(Doovde::Allegrex::EXC_BUS_DATA);
}

Doovde::Core::Core()
{
    sc_cpu = new Doovde::Allegrex(false);
    me_cpu = new Doovde::Allegrex(true);
    sc_mem = new Doovde::Memory();
    me_mem = new Doovde::Memory();

    gfx    = new Doovde::Gfx();

    io     = new Doovde::Io();

    sc_mem->busErrCb = sc_busErr;
    me_mem->busErrCb = me_busErr;

    sc_cpu->write8  = sc_write8;
    sc_cpu->write16 = sc_write16;
    sc_cpu->write32 = sc_write32;
    sc_cpu->read8   = sc_read8;
    sc_cpu->read16  = sc_read16;
    sc_cpu->read32  = sc_read32;
    me_cpu->write8  = me_write8;
    me_cpu->write16 = me_write16;
    me_cpu->write32 = me_write32;
    me_cpu->read8   = me_read8;
    me_cpu->read16  = me_read16;
    me_cpu->read32  = me_read32;

    pmem.scratch = new uint8_t[0x00004000];
    pmem.mainmem = new uint8_t[0x02000000];
    pmem.vectors = new uint8_t[0x00200000];

    pmem.me_ram = new uint8_t[0x00200000];
    pmem.me_vectors = new uint8_t[0x00200000];

    alive = true;
}

Doovde::Core::~Core()
{
    delete me_cpu;
    delete sc_cpu;

    delete me_mem;
    delete sc_mem;

    delete io;

    delete gfx;

    delete pmem.scratch;
    delete pmem.mainmem;
    delete pmem.vectors;

    delete pmem.me_ram;
    delete pmem.me_vectors;

    alive = false;
}

static uint8_t framebufRead8(Doovde::Memory* mem, Doovde::Allegrex* cpu, uint32_t addr)
{
    (void)mem;
    (void)cpu;
    uint8_t ret;
    core->gfx->intf->readFramebuffer(addr, 1, &ret);
    return ret;
}

static uint16_t framebufRead16(Doovde::Memory* mem, Doovde::Allegrex* cpu,  uint32_t addr)
{
    (void)mem;
    (void)cpu;
    uint8_t rets[2];
    core->gfx->intf->readFramebuffer(addr, 2, rets);
    return (rets[0]) | (rets[1] << 8);
}

static uint32_t framebufRead32(Doovde::Memory* mem, Doovde::Allegrex* cpu,  uint32_t addr)
{
    (void)mem;
    (void)cpu;
    uint8_t rets[4];
    core->gfx->intf->readFramebuffer(addr, 4, rets);
    return (rets[0]) | (rets[1] << 8) | (rets[2] << 16) | (rets[3] << 24);
}

static void framebufWrite8(Doovde::Memory* mem, Doovde::Allegrex* cpu,  uint32_t addr, uint8_t val)
{
    (void)mem;
    (void)cpu;
    if((addr & ~0xF0000000) < 0x00200000) {
        core->gfx->intf->writeFramebuffer(addr, 1, &val);
    }
}

static void framebufWrite16(Doovde::Memory* mem, Doovde::Allegrex* cpu,  uint32_t addr, uint16_t val)
{
    (void)mem;
    (void)cpu;
    if((addr & ~0xF0000000) < 0x00200000-1) {
        uint8_t vals[2];
        vals[0] = val & 0xFF; val >>= 8;
        vals[1] = val & 0xFF;
        core->gfx->intf->writeFramebuffer(addr, 2, vals);
    }
}

static void framebufWrite32(Doovde::Memory* mem, Doovde::Allegrex* cpu,  uint32_t addr, uint32_t val)
{
    (void)mem;
    (void)cpu;
    if((addr & ~0xF0000000) < 0x00200000-3) {
        uint8_t vals[4];
        vals[0] = val & 0xFF; val >>= 8;
        vals[1] = val & 0xFF; val >>= 8;
        vals[2] = val & 0xFF; val >>= 8;
        vals[3] = val & 0xFF;
        core->gfx->intf->writeFramebuffer(addr, 4, vals);
    }
}

void Doovde::Core::reset()
{
    sc_mem->reset();
    me_mem->reset();

    // Main CPU memory mapping (without I/O)
    // KU0
    sc_mem->addEntry(7, 0x00010000, 0x00004000, "Scratch",     true, true, pmem.scratch);
    sc_mem->addEntry(7, 0x04000000, 0x00800000, "Framebuffer", true, true,
                     framebufRead8,  framebufRead16,  framebufRead32,
                     framebufWrite8, framebufWrite16, framebufWrite32);
    sc_mem->addEntry(7, 0x08000000, 0x02000000, "Main RAM",    true, true, pmem.mainmem);
    sc_mem->addEntry(7, 0x1FC00000, 0x00200000, "Vectors",     true, true, pmem.vectors);
    io->addMemoryMap(7, 0x00000000);

    // KU1
    sc_mem->addEntry(7, 0x40010000, 0x00004000, "Scratch",     true, true, pmem.scratch);
    sc_mem->addEntry(7, 0x44000000, 0x00800000, "Framebuffer", true, true,
                     framebufRead8,  framebufRead16,  framebufRead32,
                     framebufWrite8, framebufWrite16, framebufWrite32);
    sc_mem->addEntry(7, 0x48000000, 0x02000000, "Main RAM",    true, true, pmem.mainmem);
    sc_mem->addEntry(7, 0x5FC00000, 0x00200000, "Vectors",     true, true, pmem.vectors);
    io->addMemoryMap(7, 0x40000000);

    // K0
    sc_mem->addEntry(1, 0x80010000, 0x00004000, "Scratch",     true, true, pmem.scratch);
    sc_mem->addEntry(1, 0x84000000, 0x00800000, "Framebuffer", true, true,
                     framebufRead8,  framebufRead16,  framebufRead32,
                     framebufWrite8, framebufWrite16, framebufWrite32);
    sc_mem->addEntry(1, 0x88000000, 0x02000000, "Main RAM",    true, true, pmem.mainmem);
    sc_mem->addEntry(1, 0x9FC00000, 0x00200000, "Vectors",     true, true, pmem.vectors);
    io->addMemoryMap(1, 0x80000000);

    // K1
    sc_mem->addEntry(1, 0xA0010000, 0x00004000, "Scratch",     true, true, pmem.scratch);
    sc_mem->addEntry(1, 0xA4000000, 0x00800000, "Framebuffer", true, true,
                     framebufRead8,  framebufRead16,  framebufRead32,
                     framebufWrite8, framebufWrite16, framebufWrite32);
    sc_mem->addEntry(1, 0xA8000000, 0x02000000, "Main RAM",    true, true, pmem.mainmem);
    sc_mem->addEntry(1, 0xBFC00000, 0x00200000, "Vectors",     true, true, pmem.vectors);
    io->addMemoryMap(1, 0xA0000000);

    // K2/KS
    sc_mem->addEntry(3, 0xC0010000, 0x00004000, "Scratch",     true, true, pmem.scratch);
    sc_mem->addEntry(3, 0xC4000000, 0x00800000, "Framebuffer", true, true,
                     framebufRead8,  framebufRead16,  framebufRead32,
                     framebufWrite8, framebufWrite16, framebufWrite32);
    sc_mem->addEntry(3, 0xC8000000, 0x02000000, "Main RAM",    true, true, pmem.mainmem);
    sc_mem->addEntry(3, 0xDFC00000, 0x00200000, "Vectors",     true, true, pmem.vectors);
    io->addMemoryMap(3, 0xC0000000);

    // K3
    sc_mem->addEntry(1, 0xE0010000, 0x00004000, "Scratch",     true, true, pmem.scratch);
    sc_mem->addEntry(1, 0xE4000000, 0x00800000, "Framebuffer", true, true,
                     framebufRead8,  framebufRead16,  framebufRead32,
                     framebufWrite8, framebufWrite16, framebufWrite32);
    sc_mem->addEntry(1, 0xE8000000, 0x02000000, "Main RAM",    true, true, pmem.mainmem);
    sc_mem->addEntry(1, 0xFFC00000, 0x00200000, "Vectors",     true, true, pmem.vectors);
    io->addMemoryMap(1, 0xE0000000);

    // ME memory mapping (based on Main CPU memory map; needs research)
    me_mem->addEntry(7, 0x00000000, 0x00200000, "ME RAM",   true, true, pmem.me_ram);
    me_mem->addEntry(7, 0x08000000, 0x02000000, "Main RAM", true, true, pmem.mainmem);
    me_mem->addEntry(7, 0x1FC00000, 0x00200000, "Vectors",  true, true, pmem.me_vectors);

    me_mem->addEntry(7, 0x40000000, 0x00200000, "ME RAM",   true, true, pmem.me_ram);
    me_mem->addEntry(7, 0x48000000, 0x02000000, "Main RAM", true, true, pmem.mainmem);
    me_mem->addEntry(7, 0x5FC00000, 0x00200000, "Vectors",  true, true, pmem.me_vectors);

    me_mem->addEntry(1, 0x80000000, 0x00200000, "ME RAM",   true, true, pmem.me_ram);
    me_mem->addEntry(1, 0x88000000, 0x02000000, "Main RAM", true, true, pmem.mainmem);
    me_mem->addEntry(1, 0x9FC00000, 0x00200000, "Vectors",  true, true, pmem.me_vectors);

    me_mem->addEntry(1, 0xA0000000, 0x00200000, "ME RAM",   true, true, pmem.me_ram);
    me_mem->addEntry(1, 0xA8000000, 0x02000000, "Main RAM", true, true, pmem.mainmem);
    me_mem->addEntry(1, 0xBFC00000, 0x00200000, "Vectors",  true, true, pmem.me_vectors);

    me_mem->addEntry(3, 0xC0000000, 0x00200000, "ME RAM",   true, true, pmem.me_ram);
    me_mem->addEntry(3, 0xC8000000, 0x02000000, "Main RAM", true, true, pmem.mainmem);
    me_mem->addEntry(3, 0xDFC00000, 0x00200000, "Vectors",  true, true, pmem.me_vectors);

    me_mem->addEntry(1, 0xE0000000, 0x00200000, "ME RAM",   true, true, pmem.me_ram);
    me_mem->addEntry(1, 0xE8000000, 0x02000000, "Main RAM", true, true, pmem.mainmem);
    me_mem->addEntry(1, 0xFFC00000, 0x00200000, "Vectors",  true, true, pmem.me_vectors);

    io->reset();

    sc_cpu->reset();
    me_cpu->reset();

    gfx->reset();

    Doovde::log(Doovde::LogLevel::MISC, "Doovde reset.");
}

bool Doovde::Core::run()
{
    sc_cpu->run(50);
    //me_cpu->run(5);
    io->run(50);
    gfx->display();
    return true;
}

bool Doovde::Core::runSingle()
{
    sc_cpu->runSingle();
    //me_cpu->runSingle();
    io->runSingle();
    gfx->display();
    return true;
}

bool Doovde::Core::load(const char* file)
{
    FILE *fp = fopen(file, "rb");
    fseek(fp, 0, SEEK_END);
    uint32_t size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    fread(pmem.vectors, size, 1, fp);
    fclose(fp);
    return true;
}
