
#ifndef DOOVDE_CORE_H_
#define DOOVDE_CORE_H_

namespace Doovde {
    struct Allegrex;
    struct Memory;
    struct Gfx;
    struct Io;
    struct Core {
        Core();
        ~Core();
        //---  Variables
        bool alive;

        Allegrex* sc_cpu;
        Allegrex* me_cpu;
        Memory* sc_mem;
        Memory* me_mem;
        Gfx* gfx;
        Io* io;

        struct PhysMem {
            uint8_t* scratch;
            uint8_t* mainmem;
            uint8_t* vectors;

            uint8_t* me_ram;
            uint8_t* me_vectors;
        } pmem;

        //---  Functions
        bool run();
        bool runSingle();
        void reset();
        bool load(const char* file);
    };
}

extern Doovde::Core* core;

#endif
