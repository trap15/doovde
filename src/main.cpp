//---  System includes
#include <stdlib.h>
#include <stdint.h>

//---  Qt includes
#include <QApplication>
#include <QTextCodec>

//---  Doovde includes
#include "log.h"
#include "core/core.h"
#include "gui/main_win.h"

char binary_name[16] = "Doovde";

int main(int argc, char *argv[])
{
    int ret;
    QApplication app(argc, argv);
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    app.setOrganizationName("uTech Enterprises");
    app.setApplicationName("Doovde");

    mainWin = new Doovde::Gui::MainWindow();
    mainWin->show();

    core = new Doovde::Core();
    if((core == NULL) || (core->alive == false)) {
        Doovde::log(Doovde::LogLevel::ERROR, "Doovde core init failed.");
    }

    if(argc >= 2)
        core->load(argv[1]);

    ret = app.exec();

    delete core;
    delete mainWin;

    return ret;
}
