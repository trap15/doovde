
#ifndef DOOVDE_GL_INTF_H_
#define DOOVDE_GL_INTF_H_

namespace Doovde {
    struct Plugin;
}

struct QWidget;

namespace DoovdeGL {
    struct Intf {
        Intf(Doovde::Plugin* plugin) : plug(plugin) {
            indata_temp = malloc(0x10000); /* Should be PLENTY */
        }
        ~Intf() {
            free(indata_temp);
        }
        //---  Variables
        Doovde::Plugin* plug;
        void* indata_temp;

        //---  Functions
        bool endDraw();
        bool getMainWidget(QWidget** widget);
        bool readFramebuffer(uint32_t addr, uint32_t length, void* buf);
        bool writeFramebuffer(uint32_t addr, uint32_t length, void* buf);
        bool createTexture(int count, uint32_t* oidxs);
        bool deleteTexture(int count, uint32_t* idxs);
        bool setTextureData(uint32_t tex, uint32_t w, uint32_t h, uint32_t fmt, void* data);
        bool drawTri(uint32_t tex, float* p0, float* p1, float* p2);
        bool drawQuad(uint32_t tex, float* p0, float* p1, float* p2, float* p3);
    };
}

#endif
