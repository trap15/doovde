
#ifndef DOOVDE_GFX_H_
#define DOOVDE_GFX_H_

namespace DoovdeGL {
    struct Intf;
}

namespace Doovde {
    struct Plugin;
    struct Gfx {
        Gfx();
        ~Gfx();
        //---  Variables
        bool alive;

        Plugin* gfx_plug;
        DoovdeGL::Intf* intf;

        //---  Functions
        void reset();
        void display();
        void updateMirrors();
    };
}

#endif
