//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//---  Qt includes
#include <QWidget>

//---  Doovde includes
#include "log.h"
#include "gfx/gfx.h"
#include "plugin/plugin.h"
#include "gfx/doovde_gl.h"
#include "plugin_sdk/doovde_gl.h"
#include "gui/main_win.h"

Doovde::Gfx::Gfx()
{
    gfx_plug = new Doovde::Plugin();

    gfx_plug->load("ogl_plugin/liblicdatuv.dylib");
    if(!gfx_plug->alive) {
        Doovde::log(Doovde::LogLevel::ERROR, "GFX Plugin couldn't load.");
        delete gfx_plug;
        alive = false;
        return;
    }
    gfx_plug->reset();

    intf = new DoovdeGL::Intf(gfx_plug);
    QWidget* mainWidget;
    if(intf->getMainWidget(&mainWidget)) {
        if(mainWidget != NULL) {
            mainWin->loadNewCoreWidget(mainWidget, true);
        }else{
            Doovde::log(Doovde::LogLevel::ERROR, "Main Widget NULL.");
        }
    }else{
        Doovde::log(Doovde::LogLevel::ERROR, "Main Widget false.");
    }

    alive = true;
}

Doovde::Gfx::~Gfx()
{
    delete intf;
    gfx_plug->unload();
    delete gfx_plug;

    alive = false;
}

void Doovde::Gfx::reset()
{
    uint8_t framebuf[0x200000];
    for(int i = 0; i < 0x200000; i++) {
        // Fun stuff
        framebuf[i] = (i << 5) ^ (i >> 2) ^ ((i << 3) + (i << 1)) ^ ((i << 3) - (i << 2));
    }
    gfx_plug->reset();
    intf->writeFramebuffer(0, 0x200000, (void*)framebuf);
}

void Doovde::Gfx::display()
{
    intf->endDraw();
}
