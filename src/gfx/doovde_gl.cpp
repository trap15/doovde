//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//---  Qt includes
#include <QWidget>

//---  Doovde includes
#include "log.h"
#include "gfx/doovde_gl.h"
#include "plugin/plugin.h"
#include "plugin_sdk/doovde_gl.h"

bool DoovdeGL::Intf::endDraw()
{
    return plug->commandSimple(DoovdeGL::END_DRAW);
}

bool DoovdeGL::Intf::getMainWidget(QWidget** widget)
{
    return plug->command(DoovdeGL::GET_MAIN_WIDGET, 0, NULL, 1, widget);
}

bool DoovdeGL::Intf::readFramebuffer(uint32_t addr, uint32_t length, void* buf)
{
    uint32_t* data32 = (uint32_t*)indata_temp;
    *data32++ = addr;
    *data32++ = length;
    return plug->command(DoovdeGL::READ_FRAMEBUFFER, 2, data32, 1, buf);
}

bool DoovdeGL::Intf::writeFramebuffer(uint32_t addr, uint32_t length, void* buf)
{
    uint32_t* data32 = (uint32_t*)indata_temp;
    *data32++ = addr;
    *data32++ = length;
    *(void**)data32 = buf;
    return plug->command(DoovdeGL::READ_FRAMEBUFFER, 3, data32, 0, NULL);
}

bool DoovdeGL::Intf::createTexture(int count, uint32_t* oidxs)
{
    return plug->command(DoovdeGL::CREATE_TEXTURE, count, NULL, count, oidxs);
}

bool DoovdeGL::Intf::deleteTexture(int count, uint32_t* idxs)
{
    return plug->command(DoovdeGL::DELETE_TEXTURE, count, idxs, 0, NULL);
}

bool DoovdeGL::Intf::setTextureData(uint32_t tex, uint32_t w, uint32_t h, uint32_t fmt, void* data)
{
    bool ret;
    uint32_t* data32 = (uint32_t*)indata_temp;
    *data32++ = tex;
    *data32++ = w;
    *data32++ = h;
    *data32++ = fmt;
    *(void**)data32 = data;
    ret = plug->command(DoovdeGL::SET_TEXTURE_DATA, 5, indata_temp, 0, NULL);
    return ret;
}

bool DoovdeGL::Intf::drawTri(uint32_t tex, float* p0, float* p1, float* p2)
{
    bool ret;
    uint32_t* data32 = (uint32_t*)indata_temp;
    *data32++ = tex;
    float** datafp = (float**)data32;
    *datafp++ = p0;
    *datafp++ = p1;
    *datafp = p2;
    ret = plug->command(DoovdeGL::DRAW_TRI, 4, indata_temp, 0, NULL);
    return ret;
}

bool DoovdeGL::Intf::drawQuad(uint32_t tex, float* p0, float* p1, float* p2, float* p3)
{
    bool ret;
    uint32_t* data32 = (uint32_t*)indata_temp;
    *data32++ = tex;
    float** datafp = (float**)data32;
    *datafp++ = p0;
    *datafp++ = p1;
    *datafp++ = p2;
    *datafp = p3;
    ret = plug->command(DoovdeGL::DRAW_QUAD, 5, indata_temp, 0, NULL);
    return ret;
}
