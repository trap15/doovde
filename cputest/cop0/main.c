#include <pspkernel.h>
#include <pspdebug.h>
#include <pspctrl.h>
#include <psputilsforkernel.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* Define the module info section */
PSP_MODULE_INFO("COP0TEST", 0, 1, 1);

/* Define the main thread's attribute value (optional) */
PSP_MAIN_THREAD_ATTR(THREAD_ATTR_USER);

/* Define printf, just to make typing easier */
#define printf	pspDebugScreenPrintf

void dump_threadstatus(void);

int done = 0;

char test1results[256] = {0,};

/* Exit callback */
int exit_callback(int arg1, int arg2, void *common)
{
	done = 1;
	return 0;
}

/* Callback thread */
int CallbackThread(SceSize args, void *argp)
{
	int cbid;

	cbid = sceKernelCreateCallback("Exit Callback", exit_callback, NULL);
	sceKernelRegisterExitCallback(cbid);
	sceKernelSleepThreadCB();

	return 0;
}

/* Sets up the callback thread and returns its thread id */
int SetupCallbacks(void)
{
	int thid = 0;

	thid = sceKernelCreateThread("update_thread", CallbackThread,
				     0x11, 0xFA0, 0, 0);
	if(thid >= 0)
	{
		sceKernelStartThread(thid, 0, 0);
	}

	return thid;
}

void revisionTest()
{
	uint32_t rev;
	asm volatile(	"	mfc0	%0, $15\n"
			: "=r" (rev) :
			);
	sprintf(test1results, "Revision: 0x%08X", rev);
}

int main(void)
{
	int oldbuttons = 0, buttons = 0;
	int tests_done = 0;
	SceCtrlData pad;

	pspDebugScreenInit();
	SetupCallbacks();

	sceCtrlSetSamplingCycle(0);
	sceCtrlSetSamplingMode(PSP_CTRL_MODE_ANALOG);

	while(!done) {
		pspDebugScreenSetXY(0, 2);

    		sceCtrlReadBufferPositive(&pad, 1); 

		printf("COP0 Tester (c)2011 trap15\n");
		printf("Press  X  to get processor revision\n");
		printf("Press [ ] to crash the PSP\n");

		if(tests_done & 1) {
			printf("Test 1 Results: %s\n", test1results);
		}
		buttons = pad.Buttons & ~oldbuttons;
		if(buttons != 0) {
			if(buttons & PSP_CTRL_SQUARE) {
				asm volatile(	"	j	0\n"
						: : );
			}
			if(buttons & PSP_CTRL_TRIANGLE) {
			}
			if(buttons & PSP_CTRL_CIRCLE) {
			}
			if(buttons & PSP_CTRL_CROSS) {
				if(!(tests_done & 1)) {
					revisionTest();
					tests_done |= 1;
				}
			} 

			if(buttons & PSP_CTRL_UP) {
			} 
			if(buttons & PSP_CTRL_DOWN) {
			} 
			if(buttons & PSP_CTRL_LEFT) {
			} 
			if(buttons & PSP_CTRL_RIGHT) {
			}      

			if(buttons & PSP_CTRL_START) {
			}
			if(buttons & PSP_CTRL_SELECT) {
			}
			if(buttons & PSP_CTRL_LTRIGGER) {
			}
			if(buttons & PSP_CTRL_RTRIGGER) {
			}      
		}
		oldbuttons = pad.Buttons;
	}

	sceKernelExitGame();
	return 0;
}
