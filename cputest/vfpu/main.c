#include <pspkernel.h>
#include <pspdebug.h>
#include <pspctrl.h>
#include <psputilsforkernel.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* Define the module info section */
PSP_MODULE_INFO("VFPUTEST", 0, 1, 1);

/* Define the main thread's attribute value (optional) */
PSP_MAIN_THREAD_ATTR(THREAD_ATTR_USER | THREAD_ATTR_VFPU);

/* Define printf, just to make typing easier */
#define printf	pspDebugScreenPrintf

void dump_threadstatus(void);

int done = 0;

/* Exit callback */
int exit_callback(int arg1, int arg2, void *common)
{
	done = 1;
	return 0;
}

/* Callback thread */
int CallbackThread(SceSize args, void *argp)
{
	int cbid;

	cbid = sceKernelCreateCallback("Exit Callback", exit_callback, NULL);
	sceKernelRegisterExitCallback(cbid);
	sceKernelSleepThreadCB();

	return 0;
}

/* Sets up the callback thread and returns its thread id */
int SetupCallbacks(void)
{
	int thid = 0;

	thid = sceKernelCreateThread("update_thread", CallbackThread,
				     0x11, 0xFA0, 0, 0);
	if(thid >= 0)
	{
		sceKernelStartThread(thid, 0, 0);
	}

	return thid;
}

unsigned int doMe[512] = {0,};

#define VPFXS	((0x37 << 26) | (0x0 << 24))
#define VPFXT	((0x37 << 26) | (0x1 << 24))
#define VPFXD	((0x37 << 26) | (0x2 << 24))
#define VADD	((0x18 << 26) | (0x0 << 23))
#define VSBN	((0x18 << 26) | (0x2 << 23))
#define JR	(8)
#define RET	(JR | (31 << 21))

#define VD(val) ((val & 0x7F) <<  0)
#define VSIZE(val) (((val & 1) << 7) | ((val & 2) << 14))
#define VS(val) ((val & 0x7F) <<  8)
#define VT(val) ((val & 0x7F) << 16)

/* VDOT.S behavior:
 * VDOT.S is just like a VMUL.S
 */

void unknownInstructionTest()
{
	
	asm volatile(	"	.long 0xD0070000\n" /* VONE.S S000 */
			"	.long 0xD0070020\n" /* VONE.S S001 */
			"	.long 0xD0070001\n" /* VONE.S S010 */
			"	.long 0xD0070021\n" /* VONE.S S011 */
			"	.long 0x60010101\n" /* VADD.S S010, S010, S010 */
			"	.long 0x60010101\n" /* VADD.S S010, S010, S010 */
			"	.long 0x60212121\n" /* VADD.S S011, S011, S011 */
			"	.long 0x64A10125\n" /* VDOT.S S111, S010, S011 */
			: : );
}

int main(void)
{
	int oldbuttons = 0, buttons = 0;
	int tests_done = 0;
	SceCtrlData pad;

	pspDebugScreenInit();
	SetupCallbacks();

	sceCtrlSetSamplingCycle(0);
	sceCtrlSetSamplingMode(PSP_CTRL_MODE_ANALOG);

	while(!done) {
		pspDebugScreenSetXY(0, 2);

    		sceCtrlReadBufferPositive(&pad, 1); 

		printf("VFPU Tester (c)2011 trap15\n");
		printf("Press  X  to test Unknown Instructions\n");
		printf("Press [ ] to crash the PSP\n");

		if(tests_done & 1) {
			printf("Test 1 Done\n");
		}
		buttons = pad.Buttons & ~oldbuttons;
		if(buttons != 0) {
			if(buttons & PSP_CTRL_SQUARE) {
				asm volatile(	"	j	0\n"
						: : );
			}
			if(buttons & PSP_CTRL_TRIANGLE) {
			}
			if(buttons & PSP_CTRL_CIRCLE) {
			}
			if(buttons & PSP_CTRL_CROSS) {
				if(!(tests_done & 1)) {
					unknownInstructionTest();
					tests_done |= 1;
				}
			} 

			if(buttons & PSP_CTRL_UP) {
			} 
			if(buttons & PSP_CTRL_DOWN) {
			} 
			if(buttons & PSP_CTRL_LEFT) {
			} 
			if(buttons & PSP_CTRL_RIGHT) {
			}      

			if(buttons & PSP_CTRL_START) {
			}
			if(buttons & PSP_CTRL_SELECT) {
			}
			if(buttons & PSP_CTRL_LTRIGGER) {
			}
			if(buttons & PSP_CTRL_RTRIGGER) {
			}      
		}
		oldbuttons = pad.Buttons;
	}

	sceKernelExitGame();
	return 0;
}
