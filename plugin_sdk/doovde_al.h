// Don't change this file

#ifndef PLUGIN_DOOVDEAL_H_
#define PLUGIN_DOOVDEAL_H_

// These commands must be implemented by all audio plugins.
namespace DoovdeAL {
    enum Commands {
        /* InCnt: 0
         *   In: Nothing
         * OutCnt: 0
         *   Out: Nothing
         ***
         * Do nothing
         */
        NOP = (0x2 << 24)
    };
}

#endif
