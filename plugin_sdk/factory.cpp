// Only change references to "myplugin" to your plugin.

//---  System includes
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>

//---  Doovde includes
#include "log.h"
#include "plugin_base.h"
// change me
#include "myplugin.h"

char binary_name[16] = PLUGIN_NAME;

PluginBase* create_plugin()
{
    return new MyPlugin(); // change me
}
