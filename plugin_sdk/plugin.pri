CONFIG                 -= release
CONFIG                 += debug

CONFIG                 += ordered mmx sse sse2 shared

HEADERS                += ../plugin_sdk/plugin_base.h ../plugin_sdk/doovde_gl.h ../plugin_sdk/doovde_pl.h ../plugin_sdk/doovde_al.h ../plugin_sdk/doovde_il.h ../plugin_sdk/log.h
SOURCES                += factory.cpp ../plugin_sdk/log.cpp
RESOURCES              += 
INCLUDEPATH            += ./ ../plugin_sdk/
LIBS                   += -lm
DEFINES                += IS_PLUGIN=1 PLUGIN_NAME="\"\\\"$${PLUGIN_NAME}\\\"\""

WARNINGS               += -Wall -Wshadow -Werror -pedantic

CFLAGS_RELEASE         += -O3
CFLAGS_DEBUG           += -g -O0

LFLAGS_DEBUG           += -g

CXXFLAGS               += -Wno-write-strings -fno-exceptions -fno-rtti

QMAKE_CFLAGS           += $${CFLAGS}
QMAKE_CFLAGS_RELEASE   += $${CFLAGS_RELEASE}
QMAKE_CFLAGS_DEBUG     += $${CFLAGS_DEBUG}

QMAKE_CXXFLAGS         += $${CXXFLAGS} $${CFLAGS}
QMAKE_CXXFLAGS_RELEASE += $${CXXFLAGS_RELEASE}
QMAKE_CXXFLAGS_DEBUG   += $${CXXFLAGS_DEBUG}

QMAKE_LFLAGS_DEBUG     += $${LFLAGS_DEBUG}

TEMPLATE                = lib

CODECFORTR              = UTF-8

release {
	OBJECTS_DIR     = build/release/
}

debug {
	OBJECTS_DIR     = build/debug/
}
