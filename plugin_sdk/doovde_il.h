// Don't change this file

#ifndef PLUGIN_DOOVDEIL_H_
#define PLUGIN_DOOVDEIL_H_

// These commands must be implemented by all input plugins.
namespace DoovdeIL {
    enum Commands {
        /* InCnt: 0
         *   In: Nothing
         * OutCnt: 0
         *   Out: Nothing
         ***
         * Do nothing
         */
        NOP = (0x3 << 24)
    };
}

#endif
