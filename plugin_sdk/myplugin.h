
#ifndef MYPLUGIN_H_
#define MYPLUGIN_H_

struct MyPlugin : PluginBase {
    virtual ~MyPlugin();
    //---  Variables

    //---  Functions
    virtual void init(void (*logger)(int level, char* bin, char* fmt, va_list ap));
    virtual void reset();
    virtual bool command(uint32_t cmd, uint32_t incnt, void* indata,
                uint32_t outcnt, void* outdata);
};

#endif
