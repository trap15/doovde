// Don't change this file

#ifndef PLUGIN_DOOVDEPL_H_
#define PLUGIN_DOOVDEPL_H_

// These commands must be implemented by all plugins.
namespace DoovdePL {
    enum Commands {
        /* InCnt: 0
         *   In: Nothing
         * OutCnt: 0
         *   Out: Nothing
         ***
         * Do nothing
         */
        NOP = (0x0 << 24),
        /* InCnt: Input buffer in bytes
         *   In: Data
         * OutCnt: Output buffer in bytes
         *   Out: Data
         ***
         * Copy the maximum amount of bytes from input to output
         */
        COPY
    };
}

#endif
