//---  System includes
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>

//---  Doovde includes
#include "log.h"
#include "plugin_base.h"
#include "myplugin.h"

extern void (*_plugin_logX)(int level, char* bin, char* fmt, va_list ap);

void MyPlugin::init(void (*logger)(int level, char* bin, char* fmt, va_list ap))
{
    // Register the Doovde logger
    _plugin_logX = logger;

    Doovde::log(Doovde::LogLevel::MISC, "My Plugin loaded.\n");
    plugin_type = PluginBase::TYPE_MISC;
    alive = true;
}

MyPlugin::~MyPlugin()
{
    Doovde::log(Doovde::LogLevel::MISC, "My Plugin unloaded.\n");
    alive = false;
}

void MyPlugin::reset()
{
    Doovde::log(Doovde::LogLevel::MISC, "My Plugin reset.\n");
}

bool MyPlugin::command(uint32_t cmd, uint32_t incnt, void* indata, uint32_t outcnt, void* outdata)
{
    Doovde::log(Doovde::LogLevel::MISC, "Receive command %08X, %08X:%p, %08X:%p\n", cmd, incnt, indata, outcnt, outdata);
    return true;
}
