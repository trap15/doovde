
#ifndef DOOVDE_LOG_H_
#define DOOVDE_LOG_H_

namespace Doovde {
    namespace LogLevel {
        enum {
            ERROR = 0,
            WARNING,
            MISC,
            DEBUG
        };
    }
    void log(int level, char* fmt, ...);
}

#endif
