// Don't change this file

#ifndef PLUGIN_BASE_H_
#define PLUGIN_BASE_H_

struct PluginBase {
    PluginBase() { alive = false; }
    virtual ~PluginBase() {}
    //---  Variables
    bool alive;
    int plugin_type;

    enum Type {
        TYPE_MISC = 0,
        TYPE_VIDEO,
        TYPE_AUDIO,
        TYPE_INPUT
    };

    //---  Functions
    virtual void init(void (*logger)(int level, char* bin, char* fmt, va_list ap)) = 0;
    virtual void reset() = 0;
    virtual bool command(uint32_t cmd, uint32_t incnt, void* indata,
                    uint32_t outcnt, void* outdata) = 0;
};

extern "C" {
    PluginBase* create_plugin();
}

#endif
