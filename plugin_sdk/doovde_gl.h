// Don't change this file

#ifndef PLUGIN_DOOVDE_GL_H_
#define PLUGIN_DOOVDE_GL_H_

namespace DoovdeGL {
    enum ScreenRes {
        RES_X = 480,
        RES_Y = 272
    };
    enum TextureFmt {
        TEXFMT_ARGB0565 = 0,
        TEXFMT_ARGB1555,
        TEXFMT_ARGB4444,
        TEXFMT_ARGB8888
    };
    // These commands must be implemented by all video plugins.
    enum Commands {
        /* InCnt: 0
         *   In: Nothing
         * OutCnt: 0
         *   Out: Nothing
         ***
         * Nothing
         */
        NOP = (0x1 << 24),
        /* InCnt: 0
         *   In: Nothing
         * OutCnt: 0
         *   Out: Nothing
         ***
         * End the drawing context (end of frame)
         */
        END_DRAW,
        /* InCnt: 0
         *   In: Nothing
         * OutCnt: 1
         *   Out0: New main widget (QWidget*)
         ***
         * Get the new main widget
         */
        GET_MAIN_WIDGET,
        /* InCnt: Texture Count
         *   In: Nothing
         * OutCnt: * (Texture count)
         *   Out: Texture indices (uint32_t)
         ***
         * Allocate textures
         */
        CREATE_TEXTURE,
        /* InCnt: Texture Count
         *   In*: Texture indices (uint32_t)
         * OutCnt: 0
         *   Out: Nothing
         ***
         * Delete allocated textures
         */
        DELETE_TEXTURE,
        /* InCnt: 5
         *   In0: Texture index (uint32_t)
         *   In1: Width (uint32_t)
         *   In2: Height (uint32_t)
         *   In3: Format (uint32_t)
         *   In4: Data ptr (void*)
         * OutCnt: 0
         *   Out: Nothing
         ***
         * Set texture information
         */
        SET_TEXTURE_DATA,
        /* InCnt: 4
         *   In0: Texture index (uint32_t)
         *   In1: Point 0 data ptr (8 floats in X,Y,Z,S,T,NX,NY,NZ order)
         *   In2: Point 1 data ptr (8 floats in X,Y,Z,S,T,NX,NY,NZ order)
         *   In3: Point 2 data ptr (8 floats in X,Y,Z,S,T,NX,NY,NZ order)
         * OutCnt: 0
         *   Out: Nothing
         ***
         * Draw a triangle
         */
        DRAW_TRI,
        /* InCnt: 5
         *   In0: Texture index (uint32_t)
         *   In1: Point 0 data ptr (8 floats in X,Y,Z,S,T,NX,NY,NZ order)
         *   In2: Point 1 data ptr (8 floats in X,Y,Z,S,T,NX,NY,NZ order)
         *   In3: Point 2 data ptr (8 floats in X,Y,Z,S,T,NX,NY,NZ order)
         *   In4: Point 3 data ptr (8 floats in X,Y,Z,S,T,NX,NY,NZ order)
         * OutCnt: 0
         *   Out: Nothing
         ***
         * Draw a quad
         */
        DRAW_QUAD,
        /* InCnt: 2
         *   In0: Address
         *   In1: Length
         * OutCnt: 1
         *   Out0: Buffer (pre-allocated)
         ***
         * Read the framebuffer(s).
         */
        READ_FRAMEBUFFER,
        /* InCnt: 3
         *   In0: Address
         *   In1: Length
         *   In2: Buffer
         * OutCnt: 0
         *   Out: Nothing
         ***
         * Write the framebuffer(s).
         */
        WRITE_FRAMEBUFFER
    };
}

#endif
