//---  System includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>

//---  Doovde includes
#include "log.h"
#include "plugin_base.h"

void (*_plugin_logX)(int level, char* bin, char* fmt, va_list ap);

extern char binary_name[16];

void Doovde::log(int level, char* fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    _plugin_logX(level, binary_name, fmt, ap);
    va_end(ap);
}
