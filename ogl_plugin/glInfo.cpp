///////////////////////////////////////////////////////////////////////////////
// glInfo.cpp
// ==========
// get GL vendor, version, supported extensions and other states using glGet*
// functions and store them glInfo struct variable
//
// To get valid OpenGL infos, OpenGL rendering context (RC) must be opened
// before calling glInfo::getInfo(). Otherwise it returns false.
//
//  AUTHOR: Song Ho Ahn (song.ahn@gmail.com)
// CREATED: 2005-10-04
// UPDATED: 2009-10-06
//
// Copyright (c) 2005 Song Ho Ahn
// Modified 2011 trap15 (Alex Marshall)
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <cstring>
#include "glInfo.h"
#include "ogl.h"

#include "log.h"

bool glInfo::getInfo()
{
    char* str = 0;
    char* tok = 0;
    
    // get vendor string
    str = (char*)glGetString(GL_VENDOR);
    if(str) vendor = str;                  // check NULL return value
    else return false;
    
    // get renderer string
    str = (char*)glGetString(GL_RENDERER);
    if(str) renderer = str;                // check NULL return value
    else return false;
    
    // get version string
    str = (char*)glGetString(GL_VERSION);
    if(str) version = str;                 // check NULL return value
    else return false;
    
    // get all extensions as a string
    str = (char*)glGetString(GL_EXTENSIONS);
    
    // split extensions
    if(str) {
        tok = strtok((char*)str, " ");
        while(tok) {
            extensions.push_back(tok);    // put a extension into struct
            tok = strtok(0, " ");               // next token
        }
    }else{
        return false;
    }
    
    // sort extension by alphabetical order
    std::sort(extensions.begin(), extensions.end());
    
    // get number of color bits
    glGetIntegerv(GL_RED_BITS, &redBits);
    glGetIntegerv(GL_GREEN_BITS, &greenBits);
    glGetIntegerv(GL_BLUE_BITS, &blueBits);
    glGetIntegerv(GL_ALPHA_BITS, &alphaBits);
    
    // get depth bits
    glGetIntegerv(GL_DEPTH_BITS, &depthBits);
    
    // get stecil bits
    glGetIntegerv(GL_STENCIL_BITS, &stencilBits);
    
    // get max number of lights allowed
    glGetIntegerv(GL_MAX_LIGHTS, &maxLights);
    
    // get max texture resolution
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize);
    
    // get max number of clipping planes
    glGetIntegerv(GL_MAX_CLIP_PLANES, &maxClipPlanes);
    
    // get max modelview and projection matrix stacks
    glGetIntegerv(GL_MAX_MODELVIEW_STACK_DEPTH, &maxModelViewStacks);
    glGetIntegerv(GL_MAX_PROJECTION_STACK_DEPTH, &maxProjectionStacks);
    glGetIntegerv(GL_MAX_ATTRIB_STACK_DEPTH, &maxAttribStacks);
    
    // get max texture stacks
    glGetIntegerv(GL_MAX_TEXTURE_STACK_DEPTH, &maxTextureStacks);
    
    return true;
}

bool glInfo::isExtensionSupported(const std::string& ext)
{
    // search corresponding extension
    std::vector<std::string>::const_iterator iter = extensions.begin();
    std::vector<std::string>::const_iterator endIter = extensions.end();

    for(; iter != endIter; iter++) {
        if(ext == *iter)
            return true;
    }
    return false;
}



///////////////////////////////////////////////////////////////////////////////
// print OpenGL info to screen and save to a file
///////////////////////////////////////////////////////////////////////////////
void glInfo::printSelf()
{
    std::stringstream ss;
    
    ss << std::endl; // blank line
    ss << "OpenGL Driver Info" << std::endl;
    ss << "==================" << std::endl;
    ss << "Vendor: " << vendor << std::endl;
    ss << "Version: " << version << std::endl;
    ss << "Renderer: " << renderer << std::endl;
    
    ss << std::endl;
    ss << "Color Bits(R,G,B,A): (" << redBits << ", " << greenBits<< ", " << blueBits << ", " << alphaBits << ")\n";
    ss << "Depth Bits: " << depthBits << std::endl;
    ss << "Stencil Bits: " << stencilBits << std::endl;
    
    ss << std::endl;
    ss << "Max Texture Size: " << maxTextureSize << "x" << maxTextureSize << std::endl;
    ss << "Max Lights: " << maxLights << std::endl;
    ss << "Max Clip Planes: " << maxClipPlanes << std::endl;
    ss << "Max Modelview Matrix Stacks: " << maxModelViewStacks << std::endl;
    ss << "Max Projection Matrix Stacks: " << maxProjectionStacks << std::endl;
    ss << "Max Attribute Stacks: " << maxAttribStacks << std::endl;
    ss << "Max Texture Stacks: " << maxTextureStacks << std::endl;
    
    ss << std::endl;
    ss << "Total Number of Extensions: " << extensions.size() << std::endl;
    ss << "==============================" << std::endl;
    for(unsigned int i = 0; i < extensions.size(); ++i)
        ss << extensions.at(i) << std::endl;
    
    ss << "======================================================================" << std::endl;
    
    Doovde::log(Doovde::LogLevel::DEBUG, "\n%s\n", ss.str().c_str());
}
