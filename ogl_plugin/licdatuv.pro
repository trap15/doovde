PLUGIN_NAME  = LicdaTuv
HEADERS      = licdatuv.h licdatuv_gl.h glInfo.h
SOURCES      = licdatuv.cpp licdatuv_impl.cpp licdatuv_gl.cpp glInfo.cpp
RESOURCES    = 
TARGET       = licdatuv
VERSION      = 0.0.1

QT          += opengl

include(../plugin_sdk/plugin.pri)
