
#ifndef LICDATUV_H_
#define LICDATUV_H_

//---  Qt includes
#include <QWidget>

struct LicdaTuvGL;

struct LicdaTuv : PluginBase {
    virtual ~LicdaTuv();
    //---  Variables
    LicdaTuvGL* glwidge;

    //---  Functions
    virtual void init(void (*logger)(int level, char* bin, char* fmt, va_list ap));
    virtual void reset();
    virtual bool command(uint32_t cmd, uint32_t incnt, void* indata,
                uint32_t outcnt, void* outdata);

    //---  Our functions
    bool end_draw();
    bool get_main_widget(QWidget** ptr);
    bool read_framebuffer(uint32_t addr, uint32_t length, void* buf);
    bool write_framebuffer(uint32_t addr, uint32_t length, void* buf);
    bool create_texture(uint32_t cnt, uint32_t* oidxs);
    bool delete_texture(uint32_t cnt, uint32_t* idxs);
    bool set_texture_data(uint32_t tex, uint32_t w, uint32_t h, uint32_t fmt, void* data);
    bool draw_tri(uint32_t tex, float* p0, float* p1, float* p2);
    bool draw_quad(uint32_t tex, float* p0, float* p1, float* p2, float* p3);
};

#endif
