
#ifndef LICDATUV_GL_H_
#define LICDATUV_GL_H_

//---  Qt includes
#include <QGLWidget>
#include "glInfo.h"

//---  Doovde includes
#include "doovde_gl.h"

class LicdaTuvGL : public QGLWidget {
    Q_OBJECT

public:
    LicdaTuvGL(QWidget *parent = 0) : QGLWidget(parent) {}
    glInfo info;
    int cur_w;
    int cur_h;
    int cur_fmt;
    void setDisplayFormat(int fmt, int w);
    void mapPixBuf();
    void unmapPixBuf();
    void end_draw();

    static void formatInfo(int fmt, int& glfmt, int& gltype, int& pxsize) {
        switch(fmt) {
            case DoovdeGL::TEXFMT_ARGB0565:
                glfmt = GL_RGB;
                gltype = GL_UNSIGNED_SHORT_5_6_5;
                pxsize = 2;
                break;
            case DoovdeGL::TEXFMT_ARGB1555:
                glfmt = GL_RGBA;
                gltype = GL_UNSIGNED_SHORT_5_5_5_1;
                pxsize = 2;
                break;
            case DoovdeGL::TEXFMT_ARGB4444:
                glfmt = GL_RGBA;
                gltype = GL_UNSIGNED_SHORT_4_4_4_4;
                pxsize = 2;
                break;
            case DoovdeGL::TEXFMT_ARGB8888:
                glfmt = GL_RGBA;
                gltype = GL_UNSIGNED_INT_8_8_8_8;
                pxsize = 4;
                break;
        }
    }

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

protected slots:
    void updateFrame();

private:
    void frameAdvance();
    float time_elapsed;

    GLuint frameBufTex;

    GLuint depthRendBufId;
    GLuint frameBufId;
    GLuint pixBufId;

    bool update_allowed;

    QTimer* update_timer;
};

#endif
