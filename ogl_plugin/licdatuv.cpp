//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include "ogl.h"

//---  Qt includes
#include <QWidget>

//---  Doovde includes
#include "log.h"
#include "plugin_base.h"
#include "doovde_pl.h"
#include "doovde_gl.h"
#include "licdatuv.h"
#include "licdatuv_gl.h"

extern void (*_plugin_logX)(int level, char* bin, char* fmt, va_list ap);

void LicdaTuv::init(void (*logger)(int level, char* bin, char* fmt, va_list ap))
{
    // Register the Doovde logger
    _plugin_logX = logger;

    glwidge = new LicdaTuvGL();

    plugin_type = PluginBase::TYPE_VIDEO;
    alive = true;
    Doovde::log(Doovde::LogLevel::MISC, "LicdaTuv loaded.");
}

LicdaTuv::~LicdaTuv()
{
    delete glwidge;
    Doovde::log(Doovde::LogLevel::MISC, "LicdaTuv unloaded.");
    alive = false;
}

void LicdaTuv::reset()
{
    Doovde::log(Doovde::LogLevel::MISC, "LicdaTuv reset.");
}

bool LicdaTuv::command(uint32_t cmd, uint32_t incnt, void* indata, uint32_t outcnt, void* outdata)
{
    switch(cmd) {
        case DoovdePL::NOP:
        case DoovdeGL::NOP:
            if((outcnt != 0) || (incnt != 0))
                return false;
            return true;
        case DoovdePL::COPY:
            memcpy(outdata, indata, (incnt < outcnt) ? incnt : outcnt);
            return true;
        case DoovdeGL::END_DRAW: {
            if((outcnt != 0) || (incnt != 0))
                return false;
            return end_draw();
        }
        case DoovdeGL::GET_MAIN_WIDGET: {
            if((outcnt != 1) || (incnt != 0))
                return false;
            return get_main_widget((QWidget**)outdata);
        }
        case DoovdeGL::READ_FRAMEBUFFER: {
            if((outcnt != 1) || (incnt != 2))
                return false;
            uint32_t* data = (uint32_t*)indata;
            uint32_t addr = *data++;
            uint32_t len = *data++;
            return read_framebuffer(addr, len, outdata);
        }
        case DoovdeGL::WRITE_FRAMEBUFFER: {
            if((outcnt != 0) || (incnt != 2))
                return false;
            uint32_t* data = (uint32_t*)indata;
            uint32_t addr = *data++;
            uint32_t len = *data++;
            void* buf = *(void**)data;
            return write_framebuffer(addr, len, buf);
        }
        case DoovdeGL::CREATE_TEXTURE: {
            uint32_t* data = (uint32_t*)outdata;
            int cnt = (incnt < outcnt) ? incnt : outcnt;
            return create_texture(cnt, data);
        }
        case DoovdeGL::DELETE_TEXTURE: {
            if(outcnt != 0)
                return false;
            uint32_t* data = (uint32_t*)indata;
            return delete_texture(incnt, data);
        }
        case DoovdeGL::SET_TEXTURE_DATA: {
            if((outcnt != 0) || (incnt != 5))
                return false;
            uint32_t* data = (uint32_t*)indata;
            uint32_t texidx = *data++;
            uint32_t w = *data++;
            uint32_t h = *data++;
            uint32_t fmt = *data++;
            void* texdata = *(void**)data;
            return set_texture_data(texidx, w, h, fmt, texdata);
        }
        case DoovdeGL::DRAW_TRI: {
            if((outcnt != 0) || (incnt != 4))
                return false;
            uint32_t* data = (uint32_t*)indata;
            uint32_t texidx = *data++;
            float** fdatas = (float**)data;
            float* p0data = *fdatas++;
            float* p1data = *fdatas++;
            float* p2data = *fdatas;
            return draw_tri(texidx, p0data, p1data, p2data);
        }
        case DoovdeGL::DRAW_QUAD: {
            if((outcnt != 0) || (incnt != 5))
                return false;
            uint32_t* data = (uint32_t*)indata;
            uint32_t texidx = *data++;
            float** fdatas = (float**)data;
            float* p0data = *fdatas++;
            float* p1data = *fdatas++;
            float* p2data = *fdatas++;
            float* p3data = *fdatas;
            return draw_quad(texidx, p0data, p1data, p2data, p3data);
        }
        default:
            Doovde::log(Doovde::LogLevel::ERROR, "Receive unknown command %08X, %08X:%p, %08X:%p", cmd, incnt, indata, outcnt, outdata);
            return false;
    }
}
