
//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>

//---  Doovde includes
#include "log.h"
#include "plugin_base.h"
#include "licdatuv.h"

char binary_name[16] = PLUGIN_NAME;

PluginBase* create_plugin()
{
    return new LicdaTuv();
}
