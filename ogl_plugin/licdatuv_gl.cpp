//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include "ogl.h"

//---  Qt includes
#include <QWidget>
#include <QTimer>

//---  Doovde includes
#include "log.h"
#include "plugin_base.h"
#include "doovde_gl.h"
#include "licdatuv.h"
#include "licdatuv_gl.h"

static uint32_t _ogl_get_power_of_2(uint32_t val)
{
  int i, hit = -1, clean = 1;
  for ( i=30; i > 0; i-- )
  {
    if ( hit != -1 && val & (1 << i) )
      clean = 0;
    if ( val & (1 << i) )
      if ( hit == -1 )
        hit = i;
  }
  if ( hit != -1 && clean )
    return val;
  else if ( hit != -1 )
    return (1U << (hit + 1));
  return 0;
}

void LicdaTuvGL::initializeGL()
{
    if(!info.getInfo()) {
        Doovde::log(Doovde::LogLevel::WARNING, "GlInfo unavailable?!");
    }else{
        info.printSelf();
    }

    if(!info.isExtensionSupported("GL_EXT_framebuffer_object")) {
        Doovde::log(Doovde::LogLevel::ERROR, "Your machine doesn't support EXT_framebuffer_object");
    }

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_DEPTH_TEST);

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClearDepth(1.0f);

    glDisable(GL_LIGHTING);

    glColor4f(1.0f,1.0f,1.0f,1.0f);
    glDisable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glAlphaFunc(GL_GREATER, 0.1f);
    glDisable(GL_COLOR_MATERIAL);

    glEnable(GL_TEXTURE_2D);
    glGenTextures(1, &frameBufTex);
    glBindTexture(GL_TEXTURE_2D, frameBufTex);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri(GL_TEXTURE, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 512,512, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, NULL);
    glGenerateMipmapEXT(GL_TEXTURE_2D);

    resizeGL(DoovdeGL::RES_X, DoovdeGL::RES_Y);

    // Set up FBO
    glGenFramebuffersEXT(1, &frameBufId);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frameBufId);
    // Set up (necessary) RBO for depth
    glGenRenderbuffersEXT(1, &depthRendBufId);
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depthRendBufId);
    glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT24, 512, 512);
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);
    // Bind FBO to texture
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, frameBufTex, 0);
    // Bind RBO to FBO
    glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, depthRendBufId);

    // Clear texture
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    // Unbind FBO
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

    // Set up PBO
    glGenBuffers(1, &pixBufId);

    setDisplayFormat(DoovdeGL::TEXFMT_ARGB0565, 512);

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NEAREST);

    Doovde::log(Doovde::LogLevel::MISC, "GLWidget Initialized");

    setAutoBufferSwap(false);

    update_allowed = true;

    update_timer = new QTimer(this);
    connect(update_timer, SIGNAL(timeout()), this, SLOT(updateFrame()));
    frameAdvance();
}

void LicdaTuvGL::resizeGL(int w, int h)
{
    cur_w = w;
    cur_h = h;

    // Unbind FBO and RBO
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, w, h);
    glOrtho(0.0,w, 0.0,h, -1.0,1.0);

    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, w, h);

    glLoadIdentity();

    // Re-bind FBO and RBO
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frameBufId);
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depthRendBufId);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, w, h);
    glOrtho(0.0,w, 0.0,h, -1.0,1.0);

    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, w, h);

    glLoadIdentity();

    update_allowed = true;
    Doovde::log(Doovde::LogLevel::MISC, "GLWidget Resized %d %d", w, h);
}

void LicdaTuvGL::paintGL()
{
}

void LicdaTuvGL::setDisplayFormat(int fmt, int w)
{
    int tex_fmt;
    int tex_type;
    int buf_size;
    cur_fmt = fmt;

    LicdaTuvGL::formatInfo(fmt, tex_fmt, tex_type, buf_size);
    buf_size *= w * DoovdeGL::RES_Y;

    glBindBuffer(GL_PIXEL_PACK_BUFFER, pixBufId);
    glBufferData(GL_PIXEL_PACK_BUFFER, buf_size, NULL, GL_STREAM_READ);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

    glBindTexture(GL_TEXTURE_2D, frameBufTex);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri(GL_TEXTURE, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexImage2D(GL_TEXTURE_2D, 0, tex_fmt, 512,512, 0, tex_fmt, tex_type, NULL);
    glGenerateMipmapEXT(GL_TEXTURE_2D);
}

void LicdaTuvGL::mapPixBuf()
{
    int tex_fmt;
    int tex_type;
    int buf_size;

    LicdaTuvGL::formatInfo(cur_fmt, tex_fmt, tex_type, buf_size);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, pixBufId);
    glReadPixels(0, 0, 512, DoovdeGL::RES_Y, tex_fmt, tex_type, NULL);
}

void LicdaTuvGL::unmapPixBuf()
{
    makeCurrent();
    glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
}

void LicdaTuvGL::updateFrame()
{
    int tex_fmt;
    int tex_type;
    int buf_size;

    LicdaTuvGL::formatInfo(cur_fmt, tex_fmt, tex_type, buf_size);

    if(!update_allowed) {
        swapBuffers();
        frameAdvance();
        return;
    }

    makeCurrent();

    // Unbind FBO
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    glFinish();

    // Regenerate the framebuffer's mipmaps
    glBindTexture(GL_TEXTURE_2D, frameBufTex);
    glGenerateMipmapEXT(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);
    glFinish();

    glViewport(0, 0, cur_w, cur_h);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glBindTexture(GL_TEXTURE_2D, frameBufTex);
    float tx = (float)DoovdeGL::RES_X / 512.0;
    float ty = (float)DoovdeGL::RES_Y / 512.0;
    glBegin(GL_QUADS);
        glColor3f(1.0f, 1.0f, 1.0f);
        glTexCoord2f(0.0f, 0.0f); glVertex3d(  0.0, cur_h, 0.0);
        glTexCoord2f(  tx, 0.0f); glVertex3d(cur_w, cur_h, 0.0);
        glTexCoord2f(  tx,   ty); glVertex3d(cur_w,   0.0, 0.0);
        glTexCoord2f(0.0f,   ty); glVertex3d(  0.0,   0.0, 0.0);
    glEnd();
    glFinish();
    glBindTexture(GL_TEXTURE_2D, 0);

    swapBuffers();
    frameAdvance();

    glViewport(0, 0, DoovdeGL::RES_X, DoovdeGL::RES_Y);

    // Re-bind FBO
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frameBufId);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    update_allowed = false;
}

void LicdaTuvGL::end_draw()
{
    update_allowed = true;
}

void LicdaTuvGL::frameAdvance()
{
    static int frame = 0;
    int cur_len = 17;
    frame++;
    if(frame == 3) {
        cur_len = 16;
        frame = 0;
    }
    update_timer->start(cur_len);
}
