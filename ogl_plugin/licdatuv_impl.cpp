//---  System includes
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include "ogl.h"

//---  Qt includes
#include <QWidget>

//---  Doovde includes
#include "log.h"
#include "plugin_base.h"
#include "doovde_gl.h"
#include "licdatuv.h"
#include "licdatuv_gl.h"

bool LicdaTuv::end_draw()
{
    glwidge->end_draw();
    return true;
}

bool LicdaTuv::get_main_widget(QWidget** ptr)
{
    if(glwidge->isValid()) {
        Doovde::log(Doovde::LogLevel::MISC, "GL Widget valid");
        *ptr = (QWidget*)glwidge;
        return true;
    }else{
        Doovde::log(Doovde::LogLevel::ERROR, "GL Widget invalid");
        return false;
    }
}

bool LicdaTuv::read_framebuffer(uint32_t addr, uint32_t length, void* buf)
{
    glwidge->mapPixBuf();
    void* fb = glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_ONLY);
    memcpy(buf, ((uint8_t*)fb) + addr, length);
    glwidge->unmapPixBuf();
    return false;
}

bool LicdaTuv::write_framebuffer(uint32_t addr, uint32_t length, void* buf)
{
    glwidge->mapPixBuf();
    void* fb = glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_WRITE_ONLY);
    memcpy(((uint8_t*)fb) + addr, buf, length);
    glwidge->unmapPixBuf();
    return false;
}

bool LicdaTuv::create_texture(uint32_t cnt, uint32_t* oidxs)
{
    GLuint* textures = new GLuint[cnt];
    glGenTextures(cnt, textures);
    for(uint32_t i = 0; i < cnt; i++) {
        oidxs[i] = textures[i];
    }
    delete textures;
    return true;
}

bool LicdaTuv::delete_texture(uint32_t cnt, uint32_t* idxs)
{
    GLuint* textures = new GLuint[cnt];
    for(uint32_t i = 0; i < cnt; i++) {
        textures[i] = idxs[i];
    }
    glDeleteTextures(cnt, textures);
    delete textures;
    return true;
}

bool LicdaTuv::set_texture_data(uint32_t tex, uint32_t w, uint32_t h, uint32_t fmt, void* data)
{
    int tex_fmt;
    int tex_type;
    int buf_size;
    LicdaTuvGL::formatInfo(fmt, tex_fmt, tex_type, buf_size);

    return false;
}

bool LicdaTuv::draw_tri(uint32_t tex, float* p0, float* p1, float* p2)
{
    return false;
}

bool LicdaTuv::draw_quad(uint32_t tex, float* p0, float* p1, float* p2, float* p3)
{
    return false;
}
